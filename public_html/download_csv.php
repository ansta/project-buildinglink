<?php
	
	/*
	* This file Retrieves and outputs a set of data from the database as a download CSV file
	* The table name is specified as a POST or GET parameter by the name of 'file'
	* The output is defined by a set of functions
	* The column headers output on the first row are based on the MySQL table column names in the database
	* 
	* Written by: Loki Wijnen
	*/


	// Connect this process to the rest of the CMS
	session_start();
	if (!isset($_SESSION['initiated'])) {
		session_regenerate_id();
		$_SESSION['initiated'] = true;
	}
	define('PIXXA_EXECUTE', 'OK');
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/core/get_user_ip.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/core/load_modules.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/core/load_classes.php';
	/*
	$input = '1.1';
	echo '<br>'.$input.': '.Data::enforceSkuFormat($input);
	
	$input = '12.12';
	echo '<br>'.$input.': '.Data::enforceSkuFormat($input);
	
	$input = '123';
	echo '<br>'.$input.': '.Data::enforceSkuFormat($input);
	
	$input = '1234.00';
	echo '<br>'.$input.': '.Data::enforceSkuFormat($input);
	
	$input = '0';
	echo '<br>'.$input.': '.Data::enforceSkuFormat($input);
	
	$input = '.2';
	echo '<br>'.$input.': '.Data::enforceSkuFormat($input);
	
	$input = '.';
	echo '<br>'.$input.': '.Data::enforceSkuFormat($input);
	
	die();
	*/
	// Set some parameters
	if ($_REQUEST['file'] == 'products') {
		$table = 'import_products';
		$output_filename = 'products-'.date('Ymd-His').'.csv';
	#	$output_filename = 'products.csv';
		$results = Products::getListForCsvExport();
		$function_output_data_type = 'object';
		
	} elseif ($_REQUEST['file'] == 'products_variants') {
		$table = 'import_products_variants';
		$output_filename = 'products_variants-'.date('Ymd-His').'.csv';
	#	$output_filename = 'products_variants.csv';
		$results = Products_variants::getListForCsvExport();
		$function_output_data_type = 'array';
		
	} else {
		echo '<p>ERROR: You have requested to download '.$_REQUEST['file'].'.
		<br />The file that you are trying to export does not appear to be setup yet.
		<br />Please contact the development team who can set this up for you.</p>';
		die();
	}
	
	// A simple check to make sure that the user is logged in to the Admin area
	if (!User::isAdmin()) {
		echo '<p>ERROR: Permission denied. Please login and try again. If the problem persists, please contact the development team.</p>';
		die();
	}
	
#	echo '<pre>';
#	var_dump($results);
#	die();
	
	// Build up the CSV data to be returned
	if ($function_output_data_type == 'object') {
		$data = array();
		if ($data[] = Data::makeCsvColumnHeaders($table)) {
			if ($results->num_rows > 0) {
				while ($result = $results->fetch_assoc()) {
					$line = array();
					foreach ($result as $column => $value) {
						$line[$column] = $value;
					}
					$data[] = $line;
				}
			}
		}
	} elseif ($function_output_data_type == 'array') {
		if ($headers = Data::makeCsvColumnHeaders($table)) {
			$results[0] = $headers;
			$data = $results;
			ksort ($data);
		}
	} else {
		echo '<p>ERROR: Cannot decode data as data_type not specified in the settings for this export.</p>';
		die();
	}
	
#	echo '<pre>';
#	var_dump($data);
#	die();
	
	// Outpur the necessary header information
	header('Content-type: text/csv');
	header('Content-Disposition: attachment; filename="'.basename($output_filename).'"');
	header('Pragma: no-cache');
	header('Expires: 0');
	header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($output_filename)).' GMT');
#	header('Pragma: public');   // required
#	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
#	header('Cache-Control: private',false);
#	header('Content-Type: '.$mime);
#	header('Content-Transfer-Encoding: binary');
#	header('Content-Length: '.filesize($file_name));  // provide file size
#	header('Connection: close');
#	readfile($file_name);    // push it out
	
	// Output the data as a download file
	$output = fopen("php://output", "w");
	foreach ($data as $row) {
		fputcsv($output, $row, ',', '"');
	}
	fclose($output);

















