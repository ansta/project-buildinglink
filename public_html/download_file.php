<?php
	session_start();
	if (!isset($_SESSION['initiated'])) {
		session_regenerate_id();
		$_SESSION['initiated'] = true;
	}
	define('PIXXA_EXECUTE', 'OK');
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/core/get_user_ip.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/config.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/core/load_modules.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/core/load_classes.php';
	
	
#	if (!Properties::checkAccess($_GET['gb'],$_GET['key'])) {
#		header('HTTP/1.1 403 Forbidden');
#		echo '<p>ACCESS DENIED: you do not appear to have permission to access the requested document.</p>';
#		die();
#	}
	
	
	// A simple check to make sure that the user is logged in to the Admin area
#	if (!User::isAdmin()) {
#		echo '<p>ERROR: Permission denied. Please login and try again. If the problem persists, please contact the development team.</p>';
#		die();
#	}
	
	
	// grab the requested file's name
	$file_name = SYSTEM_ROOT."/modules/".$_GET['r']."/uploads/".$_GET['f']."/".$_GET['gb']."/".$_GET['i'];
	
	
	// make sure it's a file before doing anything!
	if(is_file($file_name)) {
	
		/*
		Do any processing you'd like here:
		1.  Increment a counter
		2.  Do something with the DB
		3.  Check user permissions
		4.  Anything you want!
		*/
		if(filesize($file_name) < 10) {
			die('Too small: <strong>'.$file_name.'</strong> does not appear to be a valid document or is corrupted.');	
		}
		
		// required for IE
		if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');  }
	
		// get the file mime type using the file extension
		switch(strtolower(substr(strrchr($file_name,'.'),1)))
		{
			case 'txt'	:	$mime = 'text/plain'; break;
			case 'php'	:	$mime = 'text/plain'; break;
			case 'html'	:	$mime = 'text/html'; break;
			case 'htm'	:	$mime = 'text/html'; break;
			case 'gif'	:	$mime = 'image/gif'; break;
			case 'png'	:	$mime = 'image/png'; break;
			case 'jpeg'	:	$mime = 'image/jpeg'; break;
			case 'jpg'	:	$mime = 'image/jpeg'; break;
			case 'zip'	:	$mime = 'application/zip'; break;
			case 'pdf'	:	$mime = 'application/pdf'; break;
			case 'ai'	:	$mime = 'application/pdf'; break;
			case 'exe'	:	$mime = 'application/octet-stream'; break;
			case 'eps'	:	$mime = 'application/octet-stream'; break;
			case 'doc'	:	$mime = 'application/msword'; break;
			case 'xls'	:	$mime = 'application/vnd.ms-excel'; break;
			case 'ppt'	:	$mime = 'application/vnd.ms-powerpoint'; break;
		#	default		:	$mime = 'application/force-download';
			default		:	$mime = mime_content_type($file_name);
		}
		
		// Un-comment these lines to see mime information about
		// a file that you are having trouble downloading
	#	echo '<br>file_name: '.$file_name;
	#	echo '<br>file extention: '.strtolower(substr(strrchr($file_name,'.'),1));
	#	echo '<br>detected Mime type: '.mime_content_type($file_name);
	#	echo '<br>applied mime type: '.$mime;
	#	die();
		
		header('Pragma: public');   // required
		header('Expires: 0');    // no cache
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($file_name)).' GMT');
		header('Cache-Control: private',false);
		header('Content-Type: '.$mime);
		header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.filesize($file_name));  // provide file size
		header('Connection: close');
		readfile($file_name);    // push it out
		exit();
	
	} else {
		header("HTTP/1.0 404 Not Found");
		die('File <strong>'.$_GET['i'].'</strong> not found.');
	}

