<?php
/**
 * All requests pass through here except for real paths.
 * But we try not to do anything in here other than include our
 * initialisation file.
 *
 * There will be times when we won't want to go through here so make sure
 * that your htaccess file or you server config file allows requests for
 * real files to be handled by themselves directly without going through
 * this file. (see: http://wiki.pixxacms.com/documentation/htaccess/
 */

#ini_set('max_execution_time', 120); 

// Include initialisation file
require_once($_SERVER['DOCUMENT_ROOT'].'/../cms/system/core/boot.php');

/*
* Load debugging output
*/
require_once($_SERVER['DOCUMENT_ROOT'].'/../cms/system/core/debug.php');

// End everything here
exit();

