// All Custom JavaScript 

jQuery(document).ready(function ($) {
	
	// Ensure all images are loaded
	$(window).load(function(){

		// Header Slider
		$('.flexslider').flexslider({
			animation: "fade",
			controlNav: false,
			directionNav: false,
			slideshowSpeed: 5000
		});
		
	});
	
});