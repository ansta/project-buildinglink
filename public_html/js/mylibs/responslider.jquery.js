// JavaScript Document
	var rsImages = 3;
	var rsTransition = 1000;
	var rsTimout = 3000;
	var rsAuto = 1;
	
	//DO NOT EDIT BELOW THIS LINE
	var rsCount = 0;
	
function responSliderVars(direction) {
	if(direction == 'next') {
		if(rsCount == rsImages) {
			rsLast = rsImages;
			rsCount = 1;
		} else {
			rsLast = rsCount;
			rsCount++;
		}
	} else if(direction == 'prev') {
		if(rsCount == 1) {
			rsLast = 1;
			rsCount = rsImages;
		} else {
			rsLast = rsCount;
			rsCount--;
		}
	}
}

function responSliderStart() {
	responSliderVars('next');

	$('.responsliderFoo').removeClass("responslider" + rsLast).addClass("responslider" + rsCount);

	$('.responsliderBar').fadeOut(rsTransition, function() {
		$(this).removeClass("responslider" + rsLast).addClass("responslider" + rsCount);
		$(this).fadeIn(1);
	});
	
	if(rsAuto == 1) {
		setTimeout(responSliderAuto,rsTimout);	
	}
}

function responSliderAuto() {
	if(rsAuto == 1) {
		responSliderVars('next');

		$('.responsliderFoo').removeClass("responslider" + rsLast).addClass("responslider" + rsCount);
	
		$('.responsliderBar').fadeOut(rsTransition, function() {
			$(this).removeClass("responslider" + rsLast).addClass("responslider" + rsCount);
			$(this).fadeIn(1,function(){setTimeout(responSliderAuto,rsTimout);});
		});
	}
}

function responSliderManual(direction) {
	rsAuto = 0;
	if($(".responslider:animated").length == 0) {
		responSliderVars(direction);
	
		$('.responsliderFoo').removeClass("responslider" + rsLast).addClass("responslider" + rsCount);
	
		$('.responsliderBar').fadeOut(rsTransition, function() {
			$(this).removeClass("responslider" + rsLast).addClass("responslider" + rsCount);
			$(this).fadeIn(1);
		});
	}
}

$(".responsliderPrev").click(function() {
  responSliderManual('prev');
});

$(".responsliderNext").click(function() {
  responSliderManual('next');
});

responSliderStart();