/*

  Admin functionality

*/
$(function(){
	pixxaAdmin.initialise();
});

pixxaAdmin = {
	
	initialise : function() {
		this.handleURLNames();
		this.focusOnFirstFormField();
	},

	// Focus on first form field
	//
	// Put focus in first field on an admin form to save the user having to do
	// itself. Little time saver :)
	focusOnFirstFormField : function() {
	  $(':input:first').focus();
	},

	// Slugify fields that are used in URL paths
	//
	// Certain fields are used to generate URL path parts and as such are
	// subject to restrictions on what charaters are allowed (etc.). Rather than
	// expect the user to manually enter these we attempt to auto-generate them
	// from the regular content the user provides.
	handleURLNames : function() {
		var	 urlparts = window.location.href.split('/');
		var last = urlparts[urlparts.length-2]; // last slug in the url
		var butlast = urlparts[urlparts.length-3]; // last but 1 slug in the url
		if(last == 'new' || butlast == 'new') {
			$('#in_title').bind('keyup blur', function(){
				var str = $(this).val();
				var newstr = str.replace(/(\s){2,}/g, ' '); // Collapse multiple spaces
				newstr = newstr.replace(/\s+/g, '-'); // Replace spaces with hyphens
				newstr = newstr.replace(/(-){2,}/g, '-'); // Collapse repeated hyphens
				newstr = newstr.toLowerCase(); // Make it lowercase
				newstr = newstr.replace(/&/g, 'and'); // Replace ampersands with 'and'
				newstr = newstr.replace(/[^a-zA-Z0-9_\-]+/g, ''); // Remove non alphanumeric characters
				newstr = newstr.replace(/^[\-]+|[\-]+$/g, ''); // Remove begining and ending hyphens
				$('#in_label').val($('#in_title').val());
				$('#in_slug').val(newstr);
			});
		};
	},
};

// get your select element and listen for a change event on it
$('.redirectSelection').change(function() {
  // set the window's location property to the value of the option the user has selected
  window.location = $(this).val();
});

$(document).ready( function() {
	$('.color-picker').each( function() {
		
		var $this = $(this);
		
		if( $().iris ) {

			$this.iris({
				palettes: true,
				hide: false,
				change: function(event, ui) {
					$this.css( 'background-color', ui.color.toString());
				}	
			});
			
			$this.css('background-color', $this.val());
			
		}
	});
});
