/*
	SLIDING MENU FUNCTION
*/
$(document).ready(function() {
	//SET INITIAL STATE
	if($.cookie("cmsmenu") == "hidden") {
		//$('#slideMenu').stop(true).animate({marginLeft:'-250px'},'fast');
		$('#slideMenu').attr({"style": 'margin-left:-250px;'});
	}
	//SLIDE AWAY MENU TOGGLE
	$('#cmsMenuTab').click(function() {
		if($.cookie("cmsmenu") == "hidden") {
			$('#slideMenu').stop(true).animate({marginLeft:'0px'},'slow');
			$.cookie("cmsmenu", null,{path: '/'});
		} else {
			$('#slideMenu').stop(true).animate({marginLeft:'-250px'},'slow');
			$.cookie("cmsmenu", "hidden",{path: '/'});
		}
	});
});



