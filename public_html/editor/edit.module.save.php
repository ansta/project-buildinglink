<?php
include('inc.php');

if(User::isAdmin()) {
	$id		= $_POST["id"];
	$type	= $_POST["type"];
	$module	= $_POST["module"];
	$content= $_POST["content"];
	$column	= $_POST["column"];
	$content= Page::saveModuleContent($id,$module,$content,$column,true);
	echo $content;

} else {
	echo '<p>Authentication required, please login.</p>';
}