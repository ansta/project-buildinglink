<?php
include('inc.php');

if(User::isAdmin()) {
	$arr	= explode("::",$_GET["id"]);
	$id		= $arr[1];
	$module	= $arr[0];
	$column	= $arr[2];
	$content=Page::getModuleContent($module,$id,$column, true);
	?>
	<form name="form"  id="ajaxForm"  method="post">
		<input type="hidden" value="<?php echo $id ?>" name="id" />
		<input type="hidden" value="<?php echo $module ?>" name="module" />
		<input type="hidden" value="<?php echo $column ?>" name="column" />
		<input type="hidden" value="module" name="type" />
		<table cellpadding="0" cellspacing="0" border="0">
			<tr><td><textarea name="details" class="editor"><?php echo $content; ?></textarea></td></tr>
		</table>
	</form>
	<?php
} else {
	echo '<p>Authentication required, please login.</p>';
}


