<?php
include('inc.php');

if(User::isAdmin()) {
	$id		= $_POST["id"];
	$type	= $_POST["type"];
	$content= $_POST["content"];
	$content= Page::savePageContent($id,$content);
	echo $content;
	
} else {
	echo '<p>Authentication required, please login.</p>';
}

