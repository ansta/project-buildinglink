<?php
include('inc.php');

if(User::isAdmin()) {
	$id		= $_GET["id"];
	$content= Content::getContentByContentId($id,true);
	?>
	<form name="form"  id="ajaxForm"  method="post">
		<input type="hidden" value="<?php echo $content['id']; ?>" name="id" />
		<input type="hidden" value="<?php echo $id; ?>" name="content_id" />
		<input type="hidden" value="content" name="type" />
		<table cellpadding="0" cellspacing="0" border="0">
			<tr><td><textarea name="details" class="editor"><?php echo $content['content']; ?></textarea></td></tr>
		</table>
	</form>
	<?php
} else {
	echo '<p>Authentication required, please login.</p>';
}


