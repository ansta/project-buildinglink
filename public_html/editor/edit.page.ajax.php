<?php
include('inc.php');

if(User::isAdmin()) {
	$id=$_GET["id"];
	$content=Page::getContentById($id);
	?>
	<form name="form" id="ajaxForm"  method="post">
		<input type="hidden" value="<?php echo $id ?>" name="id" />
		<input type="hidden" value="page" name="type" />
		<table cellpadding="0" cellspacing="0" border="0">
			<tr><td><textarea name="details" class="editor"><?php echo $content; ?></textarea></td></tr>
		</table>
	</form>
	<?php
} else {
	echo '<p>Authentication required, please login.</p>';
}


