<?php
include('inc.php');

if(User::isAdmin()) {
	$id			= $_POST["id"];
	$content_id	= $_POST["content_id"];
	$type		= $_POST["type"];
	$content	= $_POST["content"];
	$content	= Content::saveContent($id,$content,$content_id, true);
	echo $content;

} else {
	echo '<p>Authentication required, please login.</p>';
}

