<?php
error_reporting(E_ALL ^ E_NOTICE);
define('PIXXA_EXECUTE', 'OK');
require_once $_SERVER['DOCUMENT_ROOT'] . '/../cms/system/core/boot.php';
Core::initialise();
/*
function __autoload($class_name) {
	$class = strtolower($class_name);
	if (file_exists(SYSTEM_ROOT.'modules/'.$class.'/'.$class.'.class.php')) {
		require_once SYSTEM_ROOT.'modules/'.$class.'/'.$class.'.class.php';
	} else {
		require_once SYSTEM_ROOT.'system/'.SYSTEM_VERSION.'/'.$class.'.class.php';
	}
}*/
?>
<html>
<head>
	<title>Image Gallery</title>
    <script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="tiny_mce_popup.js"></script>
	<script language="javascript" type="text/javascript">
	$(function(){
		$('.imgitem').click(function(){
			var rel = $(this).attr('rel');
			var title = $(this).attr('title');
			$('#in_imagepath').val(rel);
			$('#in_imagetitle').val(title);
		});
	});
	</script>
	<script language="javascript" type="text/javascript">
		var FileBrowserDialogue = {
			init : function () {
				// Here goes your code for setting your custom things onLoad.
				
			},
			mySubmit : function () {
				var URL = document.imageselectorform.imagepath.value;
				var win = tinyMCEPopup.getWindowArg("window");
		
				// insert information now
				win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;
		
				// are we an image browser
				if (typeof(win.ImageDialog) != "undefined")
				{
					// we are, so update image dimensions and preview if necessary
					if (win.ImageDialog.getImageData) win.ImageDialog.getImageData();
					if (win.ImageDialog.showPreviewImage) win.ImageDialog.showPreviewImage(URL);
				}
		
				// close popup window
				tinyMCEPopup.close();
			}
		}
		tinyMCEPopup.onInit.add(FileBrowserDialogue.init, FileBrowserDialogue);
	</script>
	<style type="text/css">
		.gallery { list-style:none;margin:0;padding:0;height:268px;overflow-y:scroll;clear:both; }
		.imgitem { display:block;float:left;width:100px;height:100px;margin:3px;text-align:center;border:1px solid #000;background-color:#FFF;background-repeat:no-repeat;background-position:center center; }
		.imgitem:hover { cursor:pointer; }
		form { clear:both; }
		
		div.field {clear:both;padding:3px 0; }
		label { display:block;width:10%;float:left; }
		
		div.input { display:block;float:right;width:88%; }
		.type-text input { width:100%; }
		.type-submit input { float:right; }
		
		.nav {list-style:none;margin:0;padding:0;}
		.nav li { float:left;padding:1px 3px 7px;}
		.nav .active { font-weight:bold; }
		
		.errormsg { color:#C00;display:block;clear:both;padding:7px 10px;background-color:#FDD;border:1px solid #A00;margin-bottom:5px; }
		.error { color:#E00;display:block;width:100%;clear:both; }
		.hint  { display:block;width:100%;clear:both;color:#777; }
	</style>
</head>
<body>
<?php
if (isset($_REQUEST['viewaction'])) {
	$nav_active[$_REQUEST['viewaction']] = 'active';
}
echo '<ul class="nav">';
	echo '<li class="'.$nav_active['list'].'"><a href="?viewaction=list">View gallery</a></li>';
	echo '<li class="'.$nav_active['new'].'"><a href="?viewaction=new">Upload new image</a></li>';
echo '</ul>';


if (isset($_REQUEST['viewaction']) && $_REQUEST['viewaction'] == 'new') {
	echo Page::showErrors();
	?>
	<form action="" method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>Upload a new Image</legend>
			<?php
			foreach (Config::$fields['images'] AS $field) {
				
				# Set the options for the PARENT drop down field
				# TODO: possibly need to find a better place for this
				if ($field['name'] == 'parent' && Module::$name == 'pages') {
					Form::setOption('parent',0,'ROOT');
					Pages::getChildren('pages');
				}
				
				if ($field['name'] == 'id') {
					#nothing
				} else {
					# Loop through all the fields in the config and create the appropriate form field
					if ( ($field['formtype'] == 'hidden') || ($field['protected'] == true && $_SESSION['id'] > 1) ) {
						if ($field['name'] == 'created' || $field['name'] == 'createdby' || $field['name'] == 'updated' || $field['name'] == 'updatedby') {
							# do nothing
						} else {
							$field['formtype'] = 'hidden';
							# Ensure that hidden fields are tagged on to the end of the form
							$hidden_fields .= Form::makeField($field);
						}
					} else {
						 echo Form::makeField($field);
					}
				}
			}
			echo $hidden_fields;
	#		Attachments::getAttachments();
			?>
			<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'new')); ?>
			<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>'images')); ?>
			<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Create')); ?>
		</fieldset>
	</form>
	<?php
} else {
	$images = Images::getList();
	if ($images->num_rows > 0) {
		echo '<ul class="gallery">';
		while ($image = $images->fetch_object()) {
	#		echo '<li class="imgitem" style="background-image:url(/userfiles/images/image/'.$image->id.'/thumb/'.$image->image.');" title="'.$image->title.'" rel="/userfiles/images/image/'.$image->id.'/gallery/'.$image->image.'"></li>';
			echo '<li class="imgitem" style="background-image:url(/userfiles/images/image/'.$image->id.'/thumb/'.$image->image.');" title="'.$image->title.'" rel="/image.php?i=/images/uploads/image/'.$image->id.'/original/'.$image->image.'"></li>';
		}
		echo '</ul>';
		echo '<form action="" method="post" enctype="multipart/form-data" autocomplete="off" name="imageselectorform">';
			echo '<fieldset>';
				echo '<legend>Chosen image</legend>';
				
				echo '<div class="field name-imagetitle type-text error-false">';
					echo '<label for="in_imagetitle">Title</label>';
					echo '<div class="input"><input type="text" name="imagetitle" id="in_imagetitle" value="" /></div>';
				echo '</div>';
				
				echo '<div class="field name-imagepath type-text error-false">';
					echo '<label for="inimagepath">Path</label>';
					echo '<div class="input"><input type="text" name="imagepath" id="in_imagepath" value="" /></div>';
				echo '</div>';
		
				echo '<div class="field name-submit type-submit error-false">';
					echo '<div class="input"><input type="submit" name="submit" id="in_submit" value="Select" onClick="FileBrowserDialogue.mySubmit();" /></div>';
				echo '</div>';
			echo '</fieldset>';
		echo '</form>';
	} else {
		echo 'No images found in your library, why not upload some now?';
	}
}
?>
</body>
</html>