<?php
/*
* Load in the module config files
*/
class Config {
	
	public static $settings		= array();
	public static $fields		= array();
	public static $global_admins= array();
	
	/*
	* Load in the individual config files of each module
	*/
	public static function tableData() {
		$modules = Security::getModuleList();
		foreach($modules AS $key => $value) {
			if (file_exists(SYSTEM_ROOT.'/modules/'.$value.'/'.$value.'.config.php')) {
				include_once(SYSTEM_ROOT.'/modules/'.$value.'/'.$value.'.config.php');
			} else {
				Error::addDebugMessage('Config file not found for module "'.$value.'", this file is needed even if it is going to be empty.');
			}
		}
	}
}