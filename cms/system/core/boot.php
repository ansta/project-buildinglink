<?php
/*
* Start the users session
* Regenerate session ID if $_SESSION['initiated'] is not set to true
*/
session_start();
if (!isset($_SESSION['initiated'])) {
    session_regenerate_id();
    $_SESSION['initiated'] = true;
}

//echo session_id();
/*
* This will allow us to prevent users from running scripts directly
* e.g.: http://www.my-website.com/somefolder/some-php-file-that-should-only-be-used-as-an-include.php
* To put this into practice in your file add this next line at the top of your script:
* 	defined('PIXXA_EXECUTE') or die("No direct script access allowed");
*/
define('PIXXA_EXECUTE', 'OK');


/*
* Get the users IP in any way we can as the standard method doesn't work when running on a server behind a load balancer
*/
require_once 'get_user_ip.php';


/*
* Load in the global site configuration file
* All site-wide settings will be in this file as well as rule exceptions based on 
* whether the website is on the live server or the local testing server
*/
require_once $_SERVER['DOCUMENT_ROOT'].'/../cms/system/config.php';


/*
* Load in the module config files
*/
require_once 'load_modules.php';


/*
* Auto-load any requested classes
*/
require_once 'load_classes.php';


/*
* Start the CMS initialisation
*/
Core::initialise();




















