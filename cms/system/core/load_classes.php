<?php
/*
* Auto-load any requested classes
*/
function __autoload($class_name) {
    $class = strtolower($class_name);
	if (file_exists(SYSTEM_ROOT.'/modules/' . $class . '/' . $class . '.class.php')) {
		require_once SYSTEM_ROOT.'/modules/' . $class . '/' . $class . '.class.php';
	} elseif (file_exists(SYSTEM_ROOT.'/system/'.SYSTEM_VERSION.'/' . $class . '.class.php')) {
		require_once SYSTEM_ROOT.'/system/'.SYSTEM_VERSION.'/' . $class . '.class.php';
	} else {
		#	die('Class: '.$class.' is missing.');
	}
}
