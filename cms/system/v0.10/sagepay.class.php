<?php

if (!function_exists('mcrypt_encrypt') || !function_exists('mcrypt_decrypt')) {
	throw new \Exception('MCRYPT is required');
}

class Sagepay {

	public static	$items			= array()
				,	$vat			= 0
				,	$vat_divider	= 1.2
		#		,	$basket_discount= 0
		#		,	$basket_total	= 0
		#		,	$basket_deliv	= 0
		#		,	$basket_bundle	= 0
		#		,	$basket_insur	= 0
				,	$total_to_pay	= 0
				,	$vendortxcode	= 0
				,	$shopper_details= 0
				,	$domain			= SERVER_NAME
				,	$sage_pay 		= array(
						'mode' 		=> SAGEPAY_MODE
					,	'test'		=> array('username' => 'buildinglink',	'password' => 'VBgy43ha5tZzGuwq',	'url' => SAGEPAY_URL)
					,	'live'		=> array('username' => 'buildinglink',	'password' => 'yj4QV3tfab2DViMX',	'url' => SAGEPAY_URL)
					,	'ansta'		=> array('username' => 'testplace',	'password' => 'a49f0a70ae58a0ee',	'url' => SAGEPAY_URL)
				)
			;
	/*
	*
	*/
	public static function my_dump($var) {
		ob_start();
		var_dump($var);
		return ob_get_clean();
	}

	function addItem($description,$item_price,$qty,$tax) {
	#	$item = strlen($item) > 100 ? substr($item, 0, 97) . "..." : $item;
		$description = str_replace("&pound;","GBP ",$description);
		$description = str_replace(":","",$description);
	#	$amount_inc_vat = $amount;
	#	$vat = $amount - $amount * (1 - self::$vat/100);
	#	$vat = $amount - ($amount/self::$vat_divider);
		$details = array(
			$description
		,	(int)$qty
		,	number_format($item_price,2, '.', '')
		,	number_format($tax,2, '.', '')
		,	number_format(($item_price * $qty),2, '.', '')
		,	number_format((($item_price * $qty) + $tax),2, '.', '')
		);
		self::$items[] = implode(":",$details);

	}

	function addExtraLine($item_description,$amount) {
	#	$item_description = strlen($item_description) > 100 ? substr($item_description, 0, 97) . "..." : $item_description;
		$item_description = str_replace("&pound;","£",$item_description);
		$details = array(
			str_replace(":","-",$item_description)
		,	"-"
		,	"-"
		,	"-"
		,	"-"
		,	number_format($amount,2, '.', '')
		);
		self::$items[] = implode(":",$details);
	}


	function createCrypt() {
		$description = self::$domain;
		$basket = count(self::$items).":".implode(":",self::$items);
		$q = 	"VendorTxCode=".self::$vendortxcode
				."&Amount=".number_format(self::$total_to_pay,2, '.', '')
				."&Currency=GBP"
				."&Description=Thank you for shopping at ".Site::$sitedata['title']."" #.$description.
				."&Basket=".$basket
				."&SuccessURL=http://".$_SERVER["HTTP_HOST"]."/order/process/"
				."&FailureURL=http://".$_SERVER["HTTP_HOST"]."/order/error/"

				."&CustomerEMail=".self::$shopper_details->email

				."&BillingSurname=".self::$shopper_details->ship_lastname
				."&BillingFirstNames=".self::$shopper_details->bill_firstname
				."&BillingAddress1=".self::$shopper_details->bill_add1
				."&BillingAddress2=".self::$shopper_details->bill_add2
				."&BillingCity=".self::$shopper_details->bill_town
				."&BillingPostCode=".self::$shopper_details->bill_zip
				."&BillingCountry=".Countries::getItem(self::$shopper_details->bill_country,'code2')

				."&DeliverySurname=".self::$shopper_details->ship_lastname
				."&DeliveryFirstnames=".self::$shopper_details->ship_firstname
				."&DeliveryAddress1=".self::$shopper_details->ship_add1
				."&DeliveryAddress2=".self::$shopper_details->ship_add2
				."&DeliveryCity=".self::$shopper_details->ship_town
				."&DeliveryPostCode=".self::$shopper_details->ship_zip
				."&DeliveryCountry=".Countries::getItem(self::$shopper_details->ship_country,'code2');

		if (strtolower(Countries::getItem(self::$shopper_details->bill_country,'code2')) == 'us') {
			$q .= "&BillingState=".Us_states::getItem(self::$shopper_details->bill_usstate,'code2');
		}
		if (strtolower(Countries::getItem(self::$shopper_details->ship_country,'code2')) == 'us') {
			$q .= "&DeliveryState=".Us_states::getItem(self::$shopper_details->ship_usstate,'code2');
		}
# WARNING: any output printed here will only display within the hidden Crypt form input field
#	so you MUST make sure any debuging output is removed in order for payments to work.
#
#		echo '<pre>';
#		echo '<br>';
#		echo $q;
#		echo '<br>';
#		echo self::$sage_pay['mode'];
#		echo '<br>';
#		echo self::$sage_pay[self::$sage_pay['mode']]['password'];
#		echo '<br>';

		$debugging .= self::my_dump($q);
		Email::sendEmail('loki.ansta@gmail.com','Building Link - createCrypt()','<pre>'.$debugging.'</pre>');


//		return base64_encode(self::SimpleXor($q,self::$sage_pay[self::$sage_pay['mode']]['password']));
		$pwd = self::$sage_pay[SAGEPAY_CONFIG]['password'];

		return self::encryptAes($q, $pwd);
	}

	private function simpleXor($InString, $Key) {
		$KeyList = array();
		$output = "";
		for($i = 0; $i < strlen($Key); $i++)
			$KeyList[$i] = ord(substr($Key, $i, 1));
		for($i = 0; $i < strlen($InString); $i++)
			$output.= chr(ord(substr($InString, $i, 1)) ^ ($KeyList[$i % strlen($Key)]));
		return $output;
	}


	static public function encryptAes($string, $key)
	{
		// AES encryption, CBC blocking with PKCS5 padding then HEX encoding.
		// Add PKCS5 padding to the text to be encypted.
		$string = self::addPKCS5Padding($string);

		// Perform encryption with PHP's MCRYPT module.
		$crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $string, MCRYPT_MODE_CBC, $key);

		// Perform hex encoding and return.
		return "@" . strtoupper(bin2hex($crypt));
	}

	static public function decryptAes($strIn, $password)
	{
		// HEX decoding then AES decryption, CBC blocking with PKCS5 padding.
		// Use initialization vector (IV) set from $str_encryption_password.
		$strInitVector = $password;

		// Remove the first char which is @ to flag this is AES encrypted and HEX decoding.
		$hex = substr($strIn, 1);

		// Throw exception if string is malformed
		if (!preg_match('/^[0-9a-fA-F]+$/', $hex))
		{
			throw new \Exception('Invalid encryption string');
		}
		$strIn = pack('H*', $hex);

		// Perform decryption with PHP's MCRYPT module.
		$string = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $password, $strIn, MCRYPT_MODE_CBC, $strInitVector);
		return self::removePKCS5Padding($string);
	}

	static protected function addPKCS5Padding($input)
	{
		$blockSize = 16;
		$padd = "";

		// Pad input to an even block size boundary.
		$length = $blockSize - (strlen($input) % $blockSize);
		for ($i = 1; $i <= $length; $i++)
		{
			$padd .= chr($length);
		}

		return $input . $padd;
	}

	static protected function removePKCS5Padding($input)
	{
		$blockSize = 16;
		$padChar = ord($input[strlen($input) - 1]);

		/* Check for PadChar is less then Block size */
		if ($padChar > $blockSize)
		{
			throw new \Exception('Invalid encryption string');
		}
		/* Check by padding by character mask */
		if (strspn($input, chr($padChar), strlen($input) - $padChar) != $padChar)
		{
			throw new \Exception('Invalid encryption string');
		}

		$unpadded = substr($input, 0, (-1) * $padChar);
		/* Chech result for printable characters */
		if (preg_match('/[[:^print:]]/', $unpadded))
		{
			throw new \Exception('Invalid encryption string');
		}
		return $unpadded;
	}

	private function getToken($string) {
		$Tokens = array("Status","StatusDetail", "VendorTxCode", "VPSTxId", "TxAuthNo", "Amount", "AVSCV2", "AddressResult", "PostCodeResult", "CV2Result", "GiftAid", "3DSecureStatus", "CAVV", "AddressStatus", "CardType", "Last4Digits", "PayerStatus", "CardType");
		$output = false;
		$q = explode("&",$string);
		$resp = array();
		foreach ($q as $val) $resp[] = explode("=",$val);
		foreach ($resp as $val) if (in_array($val[0],$Tokens)) $output[$val[0]] = $val[1];
		return $output;
	}

	private function base64Decode($scrambled) {
  		return  base64_decode(str_replace(" ","+",$scrambled));
	}


	static public function queryStringToArray($data, $delimeter = "&")
	{
		// Explode query by delimiter
		$pairs = explode($delimeter, $data);
		$queryArray = array();

		// Explode pairs by "="
		foreach ($pairs as $pair)
		{
			$keyValue = explode('=', $pair);

			// Use first value as key
			$key = array_shift($keyValue);

			// Implode others as value for $key
			$queryArray[$key] = implode('=', $keyValue);
		}

		return $queryArray;
	}

	public static function decodeCrypt($crypt) {
//		return self::getToken(self::simpleXor(self::base64Decode($crypt),self::$sage_pay[self::$sage_pay['mode']]['password']));

		$pwd = self::$sage_pay[SAGEPAY_CONFIG]['password'];
		$output = self::queryStringToArray(self::decryptAes($crypt, $pwd));
		return $output;
	}

}
