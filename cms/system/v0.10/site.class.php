<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Relates to all sitewide data
* In the future, the plan is for the system to support multiple sites and allow them
* to be managed via the same admin.
* But for now, it only supports a single site and for this we have a very lonely table
* holding only a single row of varying amounts of data. The data held here is typically
* information that is identical across every page of the site. For example: Company name,
* copyright info, main contact email address and telephone numbers. This could also hold
* the google analytics key and other such data.
*/
class Site {

	public static 	$sitedata = array();
	
	/*
	* These provide a means of overiding some data
	*/
	public static function setTitle($data) {
		self::$title = $data;
	}
	public static function setStrapline($data) {
		self::$strapline = $data;
	}
	
	/*
	* Return various page elements via get functions
	* 
	* Todo:
	* - Rather than writing loads of functions like I have currently
	*   I think I will convert to using a single get($item_to_get) function instead
	*   which would ultimately be much more flexible
	*/
	public static function getTitle() {
		return self::$sitedata['title'];
	}
	public static function getStrapline() {
		return self::$sitedata['strapline'];
	}
	public static function getCopyright() {
		return self::$sitedata['copyright'];
	}
	
}