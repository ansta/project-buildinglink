<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Setup provides the tools to create tables from config files
* 
* The plan is to eventually provide the means to update tables to in a non-destructive manner
*
*/
class Setup {
	
	/*
	* Create a new table based on the chosen module name
	*/
	public static function createNewTable($module_name) {
		
		// Only allow this functionality to be carried out by super-users
		if ($_SESSION['id'] != 1) {
			return;
		}
		
		$nl = '
		';
		
		// Begin building the query
		$create_table_query = "
			CREATE TABLE IF NOT EXISTS
				`".$module_name."`
			(
				`auto_id` int(11) NOT NULL AUTO_INCREMENT,
		";
		
		// Build the query from each field in the config
		foreach(Config::$fields[$module_name] AS $field => $values) {
			
			// Specify a default value
			$default = 'DEFAULT NULL';
			if ($values['datatype'] == 'int' && !isset($values['default'])) {
				$default = 'DEFAULT 0';
				
			} elseif ($values['datatype'] == 'int' && isset($values['default'])) {
				$default = 'DEFAULT '.$values['default'];
				
			} elseif ($values['datatype'] == 'boolean' && !isset($values['default'])) {
				$default = 'DEFAULT 0';
				
			} elseif ($values['datatype'] == 'boolean' && isset($values['default'])) {
				$default = 'DEFAULT 1';
				
			}
			
			// Create the field details
			if ($values['datatype'] == 'varchar') {
				$create_table_query .= '`'.$values['name'].'` VARCHAR(255) '.$default.','.$nl;
				
			} elseif ($values['datatype'] == 'text') {
				$create_table_query .= '`'.$values['name'].'` TEXT '.$default.','.$nl;
				
			} elseif ($values['datatype'] == 'int') {
				$create_table_query .= '`'.$values['name'].'` INT(11) '.$default.','.$nl;
				
			} elseif ($values['datatype'] == 'boolean') {
				$create_table_query .= '`'.$values['name'].'` TINYINT(1) '.$default.','.$nl;
				
			}
		}
		
		// Continue building the query by adding the remaining persistent details
		$create_table_query .= "
			  `ttv_start` TIMESTAMP NULL,
			  `ttv_end` TIMESTAMP NULL,
			  `create_by` VARCHAR(70) DEFAULT NULL,
			  `create_id` INT(11) NOT NULL DEFAULT 0,
			  `create_action` VARCHAR(32) DEFAULT NULL,
			  `modify_by` VARCHAR(70) DEFAULT NULL,
			  `modify_id` INT(11) NOT NULL DEFAULT 0,
			  `modify_action` VARCHAR(32) DEFAULT NULL,
			  PRIMARY KEY (auto_id)
			)
			ENGINE = INNODB
			AUTO_INCREMENT = 1
			CHARACTER SET utf8
			COLLATE utf8_bin;
		";
		
		db::link()->query($create_table_query) or ( die('<pre>'.$create_table_query.' <br>create failed <br>mysql_error: '.mysql_error().' <br>mysql_errno:'.mysql_errno()) );
		
		// If an install method exists in the module class then run it otherwise leave the table empty and ready to receive new data
		if (method_exists($module_name,'install')) {
			call_user_func_array(array($module_name,'install'),'');
		}
		
	}
	
}




