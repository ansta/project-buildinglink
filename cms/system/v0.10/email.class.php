<?php

class Email {
	
	/*
	* Decide what email system to use depending on whether we are on live or test server
	*/
	public static function send($to,$subject,$html,$cc=array(),$bcc=array()) {
		self::sendWithMail($to,$subject,$html,$cc,$bcc);
	}
	
	/*
	* Decide what email system to use depending on whether we are on live or test server
	*/
	public static function sendEmail($email,$subject,$html) {
		self::sendWithMail($email,$subject,$html);
	}

	/*
	* Send emails with Zend (test server)
	*/
	public static function sendWithZend($email,$subject,$html) {
		ob_start();
		require(SYSTEM_ROOT."/system/core/email.tpl.php");
		$template = ob_get_clean();
		
		$html = sprintf($template, $html);
		
		$mail = new Zend_Mail();
		$mail->setFrom(EMAIL_FROM);
		$mail->setReplyTo($email);
		$mail->addTo($email);
		$mail->addTo(EMAIL_TO); // Re-routing all emails to the developer while on the test server, this is to prevent accidental emails to members
	#	$mail->addBcc('developer@ansta.co.uk');
		$mail->setSubject($subject);
		$mail->setBodyHtml($html);
		
		$mail->send(); // self::$transport
	}
	
	/*
	* Send emails with PHP mail (live server)
	*/
	public static function sendWithMail($email_to,$subject,$html,$email_cc=array(),$email_bcc=array()) {
		
		// If CC array is not empty then build the CC list
		$cc_list = '';
		if (!empty($email_cc)) {
			foreach ($email_cc AS $cc_value) {
				if (filter_var($cc_value, FILTER_VALIDATE_EMAIL)) {
					if ($cc_list == '') {
						$cc_list = $cc_value;
					} else {
						$cc_list .= ','.$cc_value;
					}
				}
			}
		}
		
		// If BCC array is not empty then build the CC list
		$bcc_list = '';
		if (!empty($email_bcc)) {
			foreach ($email_bcc AS $bcc_value) {
				if (filter_var($bcc_value, FILTER_VALIDATE_EMAIL)) {
					if ($bcc_list == '') {
						$bcc_list = $bcc_value;
					} else {
						$bcc_list .= ','.$bcc_value;
					}
				}
			}
		}
		
		if (filter_var($email_to, FILTER_VALIDATE_EMAIL)) {
			ob_start();
			require(SYSTEM_ROOT."/system/core/email.tpl.php");
			$template = ob_get_clean();
			
			$html = sprintf($template, $html);
			
			$from		= EMAIL_FROM;
			$replyto	= $from;
			$to			= $email_to;
			$cc			= $cc_list;
			$bcc		= $bcc_list;
	#		$cc			= 'loki.ansta@gmail.com';
	//		$bcc		= 'loki.ansta@gmail.com';
			$subject	= $subject;
			$message	= $html;
			
			$headers  = "From: " . strip_tags($from) . "\r\n";
			$headers .= "Reply-To: ". strip_tags($replyto) . "\r\n";
			$headers .= "CC: ". strip_tags($cc) . "\r\n";
			$headers .= "BCC: ". strip_tags($bcc) . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			mail($to, $subject, $message, $headers);
		} else {
			$_SESSION['form']['field-error']['email'] = 'Email address <strong>"'.$email.'"</strong> does not appear to be valid, email send process has been aborted!';
		}
	}
}