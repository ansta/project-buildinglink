<?php

class Generic {
	
	public static 	$view_item_object
				, 	$item_id = 0
				,	$paging = array(
									'start_point' => 0
								,	'items_per_page' => 2
								);
	
	/*
	* Retrieve a list of items from a given module
	*/
	public static function getList($module_name,$order_by = '',$start = 0,$end = 0) {
		$version_control = Config::$settings[$module_name]['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`".$module_name."` a
			WHERE
				a.`id` > 0
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND a.`ttv_end` IS null";
		}
		if($order_by != '') {
			$query .= "
				ORDER BY ".$order_by."
			";
		} else {
			$query .= "
				ORDER BY id DESC
			";
		}
		if($end > 0) {
			$query .= "
				LIMIT ".(int)$start.",".(int)$end."
			";
		}
		$results = db::link()->query($query);
		if ($results->num_rows > 0) {
			return $results;
		}
		return NULL;
	}
	
	/*
	* Generate an array of items, for use in module config files for generating select drop down menus
	*/
	public static function getSelectList($module_name) {
		$version_control = Config::$settings[$module_name]['version_control'];
		$query = "
			SELECT
				a.id
			,	a.title
			FROM
				`".$module_name."` a
            WHERE
				a.id > 0
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND
				(	a.`ttv_end` IS null
				OR 	a.modify_action = 'delete'
				)
			";
		}
		$query .= "
			ORDER BY
				a.`title` ASC
		";
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			$output[0] = '-- please select --';
			while ($item = $items->fetch_object()) {
				$output[$item->id] = $item->title;
			}
		} else {
			$output[0] = 'Unable to retrieve '.Config::$settings['properties']['plural'].' list!';
		}
		return $output;
	}
	
	/*
	* Check a specific item exists in a givem module
	*/
	public static function checkItemExists($module_name,$item_slug,$item_id = 0) {
		$version_control = Config::$settings[$module_name]['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`".$module_name."` a
			WHERE
				a.slug = '".db::link()->real_escape_string($item_slug)."'
		";
		if ($item_id > 0) {
			$query .= " AND a.id = ".(int)db::link()->real_escape_string($item_id);
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS NULL";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			while($item = $items->fetch_object()) {
				self::$view_item_object = $item;
				self::$item_id = $item->id;
				Meta::setTag('title',$item->title);
				Meta::setTag('description',$item->metadesc);
				Meta::setTag('keywords',$item->metakeys);
				Meta::setTag('author',$item->create_by);
				return TRUE;
			}
		} else {
			return FALSE;
		}
	}
	
}

















