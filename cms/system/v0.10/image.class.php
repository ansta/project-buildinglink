<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* 
* 
* 
*/
class Image {
	
	/*
	* 
	*/
	public static function get() {
		$path = $_GET['i'];
		$extension = substr($path, -3, 3);
		if ($extension == 'png' || $extension == 'jpg' || $extension == 'gif') {
			// Make sure the image file exists
			$path = $_SERVER['DOCUMENT_ROOT'].'/../cms/modules/'.$path;
			if (!file_exists($path)) {
				self::SendErrorHeader(404);
			}
			header("Content-type: image/" . $extension);
			readfile($path);
		} else {
			die('Invalid image type, please use png, jpg or gif.');
			self::SendErrorHeader(404);
		}
	}
	
}



