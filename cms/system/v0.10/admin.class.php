<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* This is the Admins version of Page class
* 
* The admin area of the website functions in a much stricter format than the front-end.
* URL structure is typically as follows:
* 	http://www.domain.com/admin/MODULE/ACTION/ID/
*
*/
class Admin {

	/*
	* Todo:
	* - These are currently a bit in a mess and need to be tidied up
	* - Make these private or at least protected and create the necessary functions
	*   that should be used to access this data
	*/
	public static 	$depth = 0,
					$page_depth = 0,	#replace one of these 2 with $uri_length
					$slug = array(),
					$module = FALSE,
					$id = 0,
					$parent = 0,
					$title = '',
					$content = '',
					
					$haspos = FALSE,
					$haseditor = FALSE,
					
					$fulluri,
					$scheme,
					$host,
					$path,
					$query,
					
					$template = 'default',
					$allow = array()	// to store the new, edit, delete buttom permissions
					;
	
	/*
	* Resolve the Admin page for the current URL (similar to the Page version)
	*/
	public static function resolve() {
		// At this stage, simply check that we have a backend module to look in otherwise we can give up already.
		if (!self::checkExists()) {
			if (DEBUG_MODE == 2 && USER_IP == OFFICE_IP) {
				die('404: Page not found - reason: page does not appear to exist according to Admin::resolve()');
			} else {
				Error::type(404);
			}
			break;
		}
	}
	
	/*
	* Decide when to 404 and when to render a page
	*/
	public static function checkExists() {
		// Get the module config settings to determine which ones are accessible via the admin
		$permitted_admin_slug_array[0] = 'dashboard';
		foreach (Config::$settings AS $k => $v) {
		#	if ($v['in_admin_menu']) {
				$permitted_admin_slug_array[] = $k;
		#	}
		}
		// Validate the current URL against the data to see if it is valid
		if (Page::$slug[2] != '' && in_array(Page::$slug[2], $permitted_admin_slug_array)) {
			Module::$name = Page::$slug[2];
			return TRUE;
		} elseif (Page::$slug[2] == '') {
			self::$module = TRUE;
			Module::$name = 'dashboard';
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/*
	* Return various page elements via get functions
	*/
	public static function getElement($element_name,$module_name = '') {

		if($module_name != '') {
			$module_name = strtolower($module_name);
		} else {
			$module_name = strtolower(Module::$name);
		}
		
		Security::checkModuleList($module_name,__FILE__.' on line '.__LINE__);
		if (trim($module_task) != '') {
			$module_task_name	= strtolower($module_task);
		} else {
			$module_task_name	= strtolower(Core::$module_task);
		}
		$element_name		= strtolower($element_name);
		
		// Specifically for the SECONDARY element which is used by the admin to display help text and hints.
		// This data is held centrally on this server but can be over-written locally on a site-by-site basis if needed.
		if ($element_name == 'secondary') {
			
			// Check if application.element file exists in the current module
			if (file_exists(SYSTEM_ROOT.'modules/'.$module_name.'/back_end/'.$module_task_name.'.'.$element_name.'.php')) {
				require_once(SYSTEM_ROOT.'modules/'.$module_name.'/back_end/'.$module_task_name.'.'.$element_name.'.php');
				
			// if not, then check if default.element file exists in the default module
			} elseif (file_exists(ADMINDOCS_ROOT.'admindocs/'.$module_name.'/back_end/'.$module_task_name.'.'.$element_name.'.php')) {
				require_once(ADMINDOCS_ROOT.'admindocs/'.$module_name.'/back_end/'.$module_task_name.'.'.$element_name.'.php');
				
			// if not, then check if default.element file exists in the default module
			} elseif (file_exists(SYSTEM_ROOT.'modules/default/back_end/'.$module_task_name.'.'.$element_name.'.php')) {
				require_once(SYSTEM_ROOT.'modules/default/back_end/'.$module_task_name.'.'.$element_name.'.php');
				
			// if not, then check if default.element file exists in the default module
			} elseif (file_exists(ADMINDOCS_ROOT.'admindocs/default/back_end/'.$module_task_name.'.'.$element_name.'.php')) {
				require_once(ADMINDOCS_ROOT.'admindocs/default/back_end/'.$module_task_name.'.'.$element_name.'.php');
				
			// if not, then check if default.element file exists in the default module
			} elseif (file_exists(SYSTEM_ROOT.'modules/default/back_end/default.'.$element_name.'.php')) {
				require_once(SYSTEM_ROOT.'modules/default/back_end/default.'.$element_name.'.php');
				
			// if not, then check if default.element file exists in the default module
			} elseif (file_exists(ADMINDOCS_ROOT.'admindocs/default/back_end/default.'.$element_name.'.php')) {
				require_once(ADMINDOCS_ROOT.'admindocs/default/back_end/default.'.$element_name.'.php');
				
			// if still not then error
			} else {
				echo('<p><strong>Unable to load documentation files, please check the config.</strong></p><p>'.ADMINDOCS_ROOT.'admindocs/default/back_end/default.'.$element_name.'.php</p>');
			}
		
		// For all other elements, load as normal.
		} else {
			
			// Check if application.element file exists in the current module
			if (file_exists(SYSTEM_ROOT.'modules/'.$module_name.'/back_end/'.$module_task_name.'.'.$element_name.'.php')) {
				require_once(SYSTEM_ROOT.'modules/'.$module_name.'/back_end/'.$module_task_name.'.'.$element_name.'.php');
				
			// if not, then check if default.element file exists in the default module
			} elseif (file_exists(SYSTEM_ROOT.'modules/default/back_end/'.$module_task_name.'.'.$element_name.'.php')) {
				require_once(SYSTEM_ROOT.'modules/default/back_end/'.$module_task_name.'.'.$element_name.'.php');
				
			// if not, then check if default.element file exists in the default module
			} elseif (file_exists(SYSTEM_ROOT.'modules/default/back_end/default.'.$element_name.'.php')) {
				require_once(SYSTEM_ROOT.'modules/default/back_end/default.'.$element_name.'.php');
				
			// if still not then error
			} else {
				die(SYSTEM_ROOT.'modules/'.$module_name.'/back_end/'.Core::$module_task.'.'.$element_name.'.php not found');
			}
		}
	}
	
	/*
	* This function allows us to prevent child pages
	* 
	* Todo:
	* - Check if this is actually necessary as the admin already has a very strict
	*   url format. This is probably redundant and left over from the front end version.
	*/
	public static function disallowChildren() {
		if (self::$depth > self::$page_depth) {
			if (DEBUG_MODE == 2 && USER_IP == OFFICE_IP) {
				die('404: Page not found - reason: subpages not permitted in this module, edit the controller.php for this module.');
			} else {
				Error::type(404);
			}
		}
	}
	
	/*
	* This function produces the default data list for the module.
	* Whether it be pages or documents or news or anything, by default
	* this function will produce a table style list allowing admins to
	* Create new and edit, copy or delete existing records.
	* 
	* Simply copy this funtion into your custom module to create your own version.
	* 
	* Todo:
	* - Make it simpler and more flexible
	* - improve the drag and drop reordering to cater for depth and parent
	*/
	public static function getData($search = '') {
		
		// Validate the table (ie: module) name
		$table_name = Security::checkTableList(Page::$slug[2],__FILE__.' on line '.__LINE__);
		
		if ($table_name != '') {
			// Start building the SELECT for this list
			$query = "
				SELECT
					*
				FROM
					`".$table_name."`
				WHERE
					id > 0
			";
			
			// Hide record 1 from anyone but superusers in the users table
			if ($_SESSION['id'] > 1 && $table_name == 'users') {
				$query .= " AND id != 1";
			}
			
			// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
			$filter_by_site_id = false;
			foreach (Config::$fields[$table_name] AS $field) {
				if ($field['name'] == 'site_id') {
					$filter_by_site_id = true;
				}
			}
			if ($filter_by_site_id) {
				$query .= " AND site_id = ".(int)db::link()->real_escape_string($_SESSION['active_site_id']);
			}
			
			// If version control is in place for this table then we must only show the current records
			$version_control = Config::$settings[$table_name]['version_control'];
			if($version_control == 'audit' || $version_control == 'full') {
				$query .= " AND ttv_end IS NULL";
			}
			
			if(trim($search) != '') {
				$query .= " AND ".$search;
			}
			
			// List ordering
			$query_orderby = '';
			if(Config::$settings[$table_name]['orderby'] && !isset($_GET['sort'])) {
				$query_orderby = Config::$settings[$table_name]['orderby'];
			}
			if (isset($_GET['sort']) && isset($_GET['dir'])) {
				if($_GET['dir'] == 'asc' OR $_GET['dir'] == 'desc') {
					$query_orderby = $_GET['sort'].' '.$_GET['dir'];
				}
			}
			if ($query_orderby != '') {
				$query = $query.' ORDER BY '.$query_orderby; 
			}
			
			// Query the database
			$items = db::link()->query($query);
			if ($items->num_rows > 0) {
				return $items;
			}
		}
		return FALSE;
	}
	
	/*
	* This function fetches the data object for a specific ID in a specif MODULE table
	* 
	* Useful for displaying the data of a record in a VIEW page
	*/
	public static function getItem($item_id,$item_module = '') {
		
		if ($item_module == '') {
			$module = Page::$slug[2];
		} else {
			$module = $item_module;
		}
		
		$module = Security::checkModuleList($module);
		
		$version_control = Config::$settings[$module]['version_control'];
		$query = "
			SELECT
				*
			FROM
				".$module."
			WHERE
				`id` = ".(int)db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS null";
		}
		
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items->fetch_object();
		} else {
			return FALSE;
		}
	}
	
	/*
	* This function checks how many max items are allowed for the module and limits adding if already reached
	*/
	public static function applyMaxItems() {
		if(!empty(Config::$settings['pages']['max_items'])) {
			if ($item_module == '') {
				$module = Page::$slug[2];
			} else {
				$module = $item_module;
			}
			
			$module = Security::checkModuleList($module);
			
			$version_control = Config::$settings[$module]['version_control'];
			$query = "
				SELECT
					id
				FROM
					".$module."
				WHERE
					`id` > 0
			";
			if($version_control == 'audit' || $version_control == 'full') {
				$query .= "	AND `ttv_end` IS null";
			}
			
			$items = db::link()->query($query);
			if ($items->num_rows >= Config::$settings['pages']['max_items']) {
				Admin::allowButton('new',0);
			} else {
				//DO NOTHING
			}
		}
	}
	
	/*
	* This function allows us to check if a particular field exists in the config
	* of a database table.
	* Currently it doesn't check if the column actually exists in the table itself.
	* 
	* Todo:
	* - review over time to see if this needs improving
	*/
	public static function fieldCheckExists($table_name,$field_name) {
		foreach(Config::$fields[$table_name] AS $k => $fieldlist){
			if($fieldlist['name'] == $field_name) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/*
	* This function generates the various New, Edit, Copy and Delete buttons
	* Depending on a users permissions or an item or modules permissions these
	* actions will be either enabled or disabled.
	* 
	* Todo:
	* - Not so much for this funtion specifically, but for tha admin in general:
	*   make sure that if for some reason an action is disabled, ensure that the
	*   action can't still be triggered by URL or FORM tampering.
	*/
	public static function makeButton($type,$id='',$module='',$label='',$url='',$force_allow=0,$icon_name='') {
		if ($module == '') {
			$module = Page::$slug[2];
		}
		if ($label == '') {
			$label = ucfirst($type);
		}
		if ($icon_name != '') {
			$icon_css = ' class="custom-btn" style="background-image:url(/images/admin/'.$icon_name.'.png);"';
		} else {
			$icon_css = '';
		}
		if (self::$allow[$type] == 1 || $force_allow == 1) {
			if ($type == 'new' && $id == 0) {
				$item = '';
			} else {
				$item = $id.'/';
			}
			if (trim($url) != '') {
				if ($type == 'view') {
					$output = '<a href="'.$url.'">'.$label.'</a>';
				} else {
					$output = '<a href="/admin/'.$url.'"'.$icon_css.'>'.$label.'</a>';
				}
			} else {
				$output = '<a href="/admin/'.$module.'/'.strtolower($type).'/'.$item.'">'.$label.'</a>';
			}
		} else {
			$output = '<span class="disabled">'.$label.'</span>';
		}
		return $output;
	}
	
	public static function checkButtonColumn($type) {
		if (self::$allow[$type]) {
			return TRUE;
		}
		return FALSE;
	}
	
	/*
	* Relating to the makeButton function, these 3 functions attempt to enforce
	* the rules.
	* The values are typically set within the controller file of a given module
	* 
	* Todo:
	* - Anable the controller settings to be over-written by user permissions
	*   therefore allowing us to lock down a module until a user with specific
	*   permissions logs in and the new rules are applied
	*/
	public static function allowButton($type,$answer=1) {
		self::$allow[$type] = ($answer == 1 ? true : false);
		if ($answer == 0 && Page::$slug[3] == $type) {
			Error::type(404);
		}
	}
	public static function allowNew($answer=1) {
		self::$allow['new'] = ($answer == 1 ? true : false);
		if ($answer == 0 && Page::$slug[3] == 'new') {
			Error::type(404);
		}
	}
	public static function allowEdit($answer=1) {
		self::$allow['edit'] = ($answer == 1 ? true : false);
		if ($answer == 0 && Page::$slug[3] == 'edit') {
			Error::type(404);
		}
	}
	public static function allowDelete($answer=1) {
		self::$allow['delete'] = ($answer == 1 ? true : false);
		if ($answer == 0 && Page::$slug[3] == 'delete') {
			Error::type(404);
		}
	}
	public static function allowView($answer=1) {
		self::$allow['view'] = ($answer == 1 ? true : false);
		if ($answer == 0 && Page::$slug[3] == 'view') {
			Error::type(404);
		}
	}
	
	
	/*
	*
	*/
	public static function checkHasViewUrl($module_name = '') {
		if ($module_name == '') {
			$module_name = Module::$name;
		}
		if (Config::$settings[$module_name]['link_to_item'] && method_exists($module_name,'getAdminViewUrl')) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	
	/*
	* This allows logged in admins to edit page content directly from the
	* front-end of the website
	* 
	* This is NOT yet ready to be used!
	* 
	* Todo:
	* - Build!
	*/
	public static function getEditor() {
		#echo '<div class="editor"><iframe src="/admin/pages/edit/3/" style="width:100%;height:100%;"></iframe></div>';
	}
}