<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* This is currently used to set and get page meta data (not meta as in only keywords and description)
* 
* I think this class is a bit redundant and could be cut out.
*/
class Meta {
	
	public static 	$data = '';
	
	public static function setTag($name,$value) {
		self::$data[$name] = $value.$suffix;
	}
	
	public static function getTag($name,$format = 'tag',$echo = true,$suffix='') {
		if ($format == 'tag') {
			$output = self::buildTag($name,$suffix);
		} else {
			$output = self::$data[$name];
		}
		if ($echo == true) {
			echo $output;
		} else {
			return $output;
		}
	}
	
	private static function buildTag($name,$suffix) {
		if (self::$data[$name] == '') {
			return;
		}
		if ($name == 'title') {
			return '<title>'.self::$data[$name].$suffix.'</title>'.PHP_EOL;
			
		} elseif($name == 'canonical' || $name == 'shortlink') {
			return '<link rel="'.$name.'" href="'.self::$data[$name].'" />'.PHP_EOL;
			
		} else {
			return '<meta name="'.$name.'" content="'.self::$data[$name].'" />'.PHP_EOL;
			
		}
	}
	
}
