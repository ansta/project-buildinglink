<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Everything relating to the Page aspect of the system is here
* 
* Todo:
* - This could do with a tidy up
*/
class Page {

	/*
	* Some defaults
	*/
	public static 	$depth = 0,
					$page_depth = 0,	#replace one of these 2 with $uri_length
					$slug = array(),
					$module = FALSE,
					$id = array(),
					$parent = 0,
					$title = '',
					$content = '',
					$page_type = '',
					
					$fulluri,
					$scheme,
					$host,
					$path,
					$query,
					$crumbs = array(),
					$crumbs_id = array(),
					$crumbs_url = array(),
					$viewstyle = 'default';	# This is a stop-gap value used to apply a specific class to the body tag

	
	/*
	* Resolve the page for the current URL
	* 
	* Todo:
	* - Although it currently work, this needs some serious tidying up
	*/
	public static function resolve() {
		// Lookup each page starting with the first slug until we find a non page-tree-page
		// By the time we have finished looping we should have found a module name (if any, otherwise use default)
		// We will also have retrieved the last page's content
		$i = 1;
		if (self::$slug[$i] == '') {
			// This exception is for the homepage which ideally wants to have an empty slug rather than /home/
			if(self::checkExists($i,'')) {
				Core::$module_folder = 'default';
			} else {
				if (DEBUG_MODE == 2 && USER_IP == OFFICE_IP) {
					die('404: Page not found - reason: this site does not appear to have a homepage.');
				} else {
					Error::type(404);
				}
			}
		} else {
			// For all other pages, do the following checks
			while ($i < (self::$depth + 1)) {
				if(self::checkExists($i,self::$slug[$i])) {
					Core::$module_folder = 'default';
				} else {
					if(self::$module) {
						$real_length = count(self::$slug) - 2 - Module::$offset;
						Module::$depth = $real_length;
						// hand over to module controller
						Core::$module_folder = 'default';
					} else {
						// 404
						if (DEBUG_MODE == 2 && USER_IP == OFFICE_IP) {
							die('404: Page not found - reason: unknown!');
						} else {
							Error::type(404);
						}
					}
					break;
				}
				
				// Increment the module depth which is NOT the page depth
				if (self::$module) {
					Module::$depth = Module::$depth + 1;
				}
				$i++;
			}
		}
		Module::getMeta();
	}
	
	
	/*
	* Check that a page exists for a given URL
	* This funtion doesn't currently take into account the URL field although as
	* this is now a cached field in the Pages table, maybe this could be achieved
	* in a single query.
	* However this function performs another vital task that we cannot work without
	* which is the populating of the Page::$slug array which is essential to any module
	*/
	public static function checkExists($depth,$slug) {
		
		
		$version_control = Config::$settings['pages']['version_control'];
		
		// First prepare the query to get all pages for the chosen site from the pages module
		// Site dependant
		$query = "
			SELECT
				id
			,	site_id
			,	use_module
			,	parent
			,	title
			,	label
			,	depth
			,	slug
			,	metakeys
			,	metadesc
			,   url
			,	'site_specific' AS page_type
			FROM
				pages
			WHERE
				slug = '".db::link()->real_escape_string($slug)."'
			AND	depth = ".db::link()->real_escape_string($depth)."
			AND	active = TRUE
			AND site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id'])."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND `ttv_end` IS NULL";
		}
		// Then also prepare the query to get all generic pages from the pages_common module
		// NOT site dependant
		$query .= "
			UNION ALL
			
			SELECT
				id
			,	'0' AS site_id
			,	use_module
			,	parent
			,	title
			,	label
			,	depth
			,	slug
			,	metakeys
			,	metadesc
			,   url
			,	'common' AS page_type
			FROM
				pages_common
			WHERE
				slug = '".db::link()->real_escape_string($slug)."'
			AND	depth = ".db::link()->real_escape_string($depth)."
			AND	active = TRUE
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND `ttv_end` IS NULL";
		}
	#	echo $query;
		$pagelookup = db::link()->query($query);
		if ($pagelookup->num_rows) {
			$result = $pagelookup->fetch_assoc();
			self::$id[0] = $result['id']; // ID of current page
			self::$id[$result['depth']] = $result['id']; // ID of the different depths
			self::$parent = $result['parent'];
			self::$title = $result['title'];
			self::$content = $result['content'];
			self::$page_type = $result['page_type'];
			self::$page_depth = $result['depth'];
			self::$crumbs[] = $result['title'];
			self::$crumbs_url[] = $result['url'];
			Meta::setTag('title',$result['title']);
			Meta::setTag('description',$result['metadesc']);
			Meta::setTag('keywords',$result['metakeys']);
			if (Module::$name == '') {
				self::$module = FALSE;
				Module::$name = 'default';
			}
			if ($result['use_module'] != '') {
				self::$module = TRUE;
				Module::$offset = $depth - 1;
				Module::$name = $result['use_module'];
				Module::$id = $result['id'];
				Module::$page = $result['slug'];
			}
			return TRUE;
		} elseif ($slug[1] == ADMIN) {
			self::$id = 0;
			self::$parent = 0;
			self::$title = 'Admin dashboard';
			self::$content = '';
			self::$page_depth = 1;
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	/*
	* Return various page elements via get functions
	*/
	public static function getElement($element_name,$module_name = '',$module_task = '') {

		if($module_name != '') {
			$module_name = strtolower($module_name);
			$allow_multi_include = TRUE;
		} else {
			$module_name = strtolower(Module::$name);
		}
		
		$element_name = strtolower($element_name);
		
		if($module_task != '') {
			$module_task_name = strtolower($module_task);
		} else {
			$module_task_name = strtolower(Core::$module_task);
		}
		
		Security::checkModuleList($module_name,__FILE__.' on line '.__LINE__);
	#	Security::checkTaskList($module_task_name,__FILE__.' on line '.__LINE__);
	#	Security::checkElementList($element_name,__FILE__.' on line '.__LINE__);
		
		// Check if application.element file exists in the current module
		if (file_exists(SYSTEM_ROOT.'modules/'.$module_name.'/front_end/'.$module_task_name.'.'.$element_name.'.php')) {
			if ($allow_multi_include) {
				require(SYSTEM_ROOT.'modules/'.$module_name.'/front_end/'.$module_task_name.'.'.$element_name.'.php');
			} else {
				require_once(SYSTEM_ROOT.'modules/'.$module_name.'/front_end/'.$module_task_name.'.'.$element_name.'.php');
			}
			
		// if not, then check if default.element file exists in the current module
		} elseif (file_exists(SYSTEM_ROOT.'modules/default/front_end/'.$module_task_name.'.'.$element_name.'.php')) {
			require_once(SYSTEM_ROOT.'modules/default/front_end/'.$module_task_name.'.'.$element_name.'.php');
			
		// if not, then check if default.element file exists in the default module
		} elseif (file_exists(SYSTEM_ROOT.'modules/default/front_end/default.'.$element_name.'.php')) {
			require_once(SYSTEM_ROOT.'modules/default/front_end/default.'.$element_name.'.php');
			
		// if still not then error
		} else {
			die(SYSTEM_ROOT.'modules/'.$module_name.'/front_end/'.Core::$module_task.'.'.$element_name.'.php not found');
		}
	}
	
	
	/*
	*
	*/
	public static function getTitle() {
		return self::$title;
	}
	
	
	/*
	*
	*/
	public static function getContent($module_name='',$item_id=0,$field_name='') {
		/*
		$module, $item_id and $field_name could be used to get content from a specific place rather than the default page tree
		$module = Security::checkModuleList($module_name,__FILE__.' on line '.__LINE__);
		*/
		if (User::isAdmin()) {
			//$output = '<div class="editme"><a href="/admin/pages/edit/'.self::$id[0].'/">edit</a></div>'.self::$content;
			$output = '<div class="edit_page admin-edit"><a href="#" rel="'.self::$id[0].'"><span>edit</span></a></div>';
			$output .= '<div id="page_'.self::$id[0].'">'.self::$content.'</div>';
		} else {
			$output = self::$content;
		}
		return $output;
	}
	
	
	/*
	*
	*/
	public static function getModuleUrl($module_name) {
		$query = "
			SELECT
				*
			FROM
				pages
			WHERE
				use_module = '".db::link()->real_escape_string($module_name)."'
			AND site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id'])."
			AND	ttv_end IS null
			LIMIT 1
		";
		$results = db::link()->query($query);
		if($results->num_rows > 0) {
			while ($result = $results->fetch_object()) {
				return $result->url;
			}
		}
		return NULL;
	}
	
	
	/*
	*
	*/
	public static function getPageUrl($page_id) {
		$query = "
			SELECT
				*
			FROM
				pages
			WHERE
				id = ".(int)db::link()->real_escape_string($page_id)."
			AND	ttv_end IS null
			LIMIT 1
		";
		$results = db::link()->query($query);
		if($results->num_rows > 0) {
			while ($result = $results->fetch_object()) {
				return $result->url;
			}
		}
		return NULL;
	}
	
	
	/*
	* This function simply checks to see if we are deeper than we should be
	* and if we are and this check is present in the controller file, then we 404
	* and thus prventing any child pages to exist.
	*/
	public static function disallowChildren() {
		if (self::$depth > self::$page_depth) {
			if (DEBUG_MODE == 2 && USER_IP == OFFICE_IP) {
				die('404: Page not found - reason: Child pages not permitted according to Pages::disallowChildren()');
			} else {
				Error::type(404);
			}
		}
	}
	
	
	/*
	* 
	*/
	public static function showErrors($clear_sessions_vars = TRUE) {
		if (isset($_SESSION['form']['form-success'])) {
			echo '<div class="successmsg">'.$_SESSION['form']['form-success'].'</div>';
			if ($clear_sessions_vars) {
				unset($_SESSION['form']['form-item']);
				unset($_SESSION['form']['form-success']);
			}
		}
		if (isset($_SESSION['form']['form-error'])) {
			echo '<div class="errormsg">'.$_SESSION['form']['form-error'].'</div>';
			if ($clear_sessions_vars) {
				unset($_SESSION['form']['form-item']);
				unset($_SESSION['form']['form-error']);
			}
		}
	}
	
	
	/*
	* Bread crumbs
	*/
	public static function getBreadCrumbs($IGNORE = NULL, $APPEND = NULL, $SEPARATE = '&nbsp; &raquo; &nbsp;'){
		//SET DEFAULTS
		$i = 0;
		$breadCrumbs = array();
		$count = count(self::$crumbs);
		//BUILD IGNORE LIST
		// The '1,' is to ignore the home page crumb.
		$ignoreList = explode(',', '1,'.$IGNORE);// <- IGNORE IS CURRENTLY MANUALLY PASSED THROUGH THE FUNCTION 
	    //DECIDE IF HOME CRUMB SHOULD BE A LINK
		$breadCrumbs[$i] = ($count>0 && self::$id[0] != 1)?'<a href="/">Home</a>':'Home';
		//CYCLE CRUMBS
		foreach(self::$crumbs AS $key => $name) {
			if(!in_array(self::$crumbs_id[$key], $ignoreList)) {
				$i++;
				$breadCrumbs[$i] = ($count == $key+1 && empty($APPEND))?$name:'<a href="'.self::$crumbs_url[$key].'" >'.$name.'</a>';
			}
		}
		//RUN THROUGH APPEND LIST
		//$APPEND should be a simple array of page names and urls. e.g. "foo" => "/bar/"
		if(!empty($APPEND)) {
			$appendI = 0;
			$appendCount = count($APPEND);
			foreach($APPEND AS $name => $url) {
				$i++;
				$appendI++;
				$breadCrumbs[$i] = ($appendCount == $appendI)?$name:'<a href="'.$url.'" >'.$name.'</a>';
			}
		}
		return $breadCrumbs = implode($SEPARATE,$breadCrumbs);
	}
	
	
	/*
	* Get content by ID
	*/
	public static function getContentById($id){
	 $query = "
			SELECT
				content
			FROM
				pages
			WHERE
				id='".db::link()->real_escape_string($id)."'
				AND	active = TRUE
				AND ttv_end IS NULL";
		
		$pagelookup = db::link()->query($query);
		if ($pagelookup->num_rows) { 
			return $pagelookup->fetch_object()->content;
		}
		else
		return NULL;
	}
	
	/*
	* Save page content
	*/
	public static function savePageContent($id,$content){
		$update_pages = array(
			'id' => $id,
			'module' => 'pages',
			'action' => 'edit',
			'content' => $content
		);
		Form::setAuditAction('update');
		Form::processData('pages',$update_pages);
		return $content = Page::getContentById($id);
	}
}