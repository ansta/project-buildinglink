<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* This class does very little at the moment but the plan is to hold here everything that 
* relates to security:
* 
* Another thing that this has found a use for is to verify that $_REQUEST['action'] and $_REQUEST['module'] are permitted
* I think the plan for this is to move it into each individual module config and
* thus tightening it down even more in order to try to reduce the risk of form hacking.
*
*/
class Security {
					
	private static	$list_of_allowed_tables = array(),
					$list_of_allowed_modules = array(),
					$list_of_allowed_actions = array(
													0 => 'new',
													1 => 'copy',
													2 => 'edit',
													3 => 'delete',
													4 => 'send',
													5 => 'submit',
													6 => 'post',
													7 => 'update',
													8 => 'process',
													9 => 'reorder',
													10 => 'password',
													11 => 'accept',
													12 => 'invite',
													13 => 'filter',
													14 => 'approve',
													15 => 'reject',
													16 => 'disable',
													17 => 'activate',
													18 => 'orgremove',
													19 => 'orgadd',
													20 => 'group',
													21 => 'regroup',
													22 => 'reset',
													23 => 'unlink',
													24 => 'relink',
													25 => 'bulk',
													26 => 'crop',
													27 => 'switch',
													28 => 'add',
													29 => 'save',
													30 => 'insert',
													31 => 'import',
													32 => 'export'
													); 
	

	public static function initialise() {
		self::cacheTableList();
		self::cacheModuleList();
		self::createLoginFields();
	}
	
	/*
	* Create randomised login field names to prevent autocomplete from working
	* TODO: Need to check what happens to non-human users where a session wouldn't
	* be started and therefor the login form would have empty names.
	*/
	public static function createLoginFields() {
		if (!isset($_SESSION['loginfield_username'])) {
			$_SESSION['loginfield_username'] = 'username';
			$_SESSION['loginfield_password'] = 'password';
		}
	}
	
	/*
	* This currently just creates login hashes and not much else.
	*/
	public static function encrypt($item,$method = 'md5',$additional_salt = '') {
		if ($method == 'md5') {
			$output = md5(MD5SALT . $item . $additional_salt);
		} else {
			die('no ecryption method specified');
		}
		return $output;
	}
	
	/*
	* Cache the list of tables that exist in the database
	* 
	* This is important when querying the database with dynamic table names
	* such as for example: SELECT * FROM $my_table WHERE id = 1
	* To implement, call checkTableList($my_table) before the query.
	* If the table name is valid it will execute, otherwise the process will die();
	*/
	private static function cacheTableList() {
		$tbls = db::link()->query('SHOW TABLES');
		while($tbl = mysqli_fetch_array($tbls)) {
			$table_list[] = $tbl[0];
		}
		self::$list_of_allowed_tables = $table_list;
	}
	public static function checkTableList($name) {
		if (in_array($name, self::$list_of_allowed_tables)) {
			return $name;
		} else {
			if (DEBUG_MODE > 0) {
				echo 'Table name <strong>\''.$name.'\'</strong> does not appear to exist';
			} else {
				echo $name;
				var_dump(self::$list_of_allowed_tables);
				echo 'There appears to be a system mis-configuration, please contact the website administrator. #table';
			}
			if (isset($_SESSION['id']) && $_SESSION['id'] == 1) {
				echo '<a href="'.Page::$slug[0].'create/">Click here to create it now</a>';
			}
		#	die();
		}
	}
	
	/*
	* Cache the list of modules that exist in the modules directory
	* 
	* This allows us to prevent hings such as form submission to action files belonging
	* modules other than those that exist on this site
	* 
	* TODO:
	* - Maybe cache these in a database table instead of directory scanning each time.
	*   Would require an option to reload/refresh the list held in the database
	*   as it would be even worse to refresh the database list on every page request.
	*/
	private static function cacheModuleList() {
		
		//path to directory to scan
		$path_to_module = SYSTEM_ROOT . 'modules/';

		//get all files in specified directory
		$files = glob($path_to_module . "*");
		
		//loop through all records
		foreach($files as $file) {
			//check to see if the file is a folder/directory
			if(is_dir($file)) {
				$module_paths[] = $file;
			}
		}

		foreach($module_paths as $module_path)
		{
			$slugs = explode("/", $module_path);
			$module_depth = count($slugs) - 1;
			$module_list[] = $slugs[$module_depth];
		}
		
		self::$list_of_allowed_modules = $module_list;
	}
	/* Returns the tested module name if it exists otherwise dies */
	public static function checkModuleList($name,$location = '') {
		if (in_array($name, self::$list_of_allowed_modules)) {
			return $name;
		} else {
			return FALSE;
		}
	}
	/* Returns a list of all the available modules in the form of an array */
	public static function getModuleList() {
		return self::$list_of_allowed_modules;
	}
	
	
	/*
	* Cache the list of permitted actions
	*/
	public static function checkActionList($name) {
		if (in_array($name, self::$list_of_allowed_actions)) {
			return $name;
		} else {
			if (DEBUG_MODE > 0) {
				die($name.' is an invalid action!');
			} else {
				die('There appears to be a system mis-configuration, please contact the website administrator. #action');
			}
		}
	}
	
	
	/*
	* TODO: Check that this record can be edited
	*/
	public static function checkCanEdit($data) {
		# The idea behind this yet-to-implement function is
		# from the _REQUEST array, check for starters that there is an ID
		# and that this ID does indeed exist in the module table (also
		# supplied in the array).
		#
		# Then also check if this record is locked or if there are any
		# edit restrictions for the current user. (make sure that it works for
		# modules that don't require any login such as the contact form)
		#
		# If it passes the 2 tests above return TRUE and go ahead with the UPDATE.
	}
	
	
	/*
	* Check that a particular column exists in a table config
	*/
	public static function checkColumn($module,$name) {
		foreach (Config::$fields[$module] AS $key => $value) {
			if ($value['name'] == $name) {
				return $name;
			}
		}
		if (DEBUG_MODE > 0) {
			die($name.' is an invalid column name!');
		} else {
			die('There appears to be a system mis-configuration, please contact the website administrator. #column-name');
		}
	}
	
}
