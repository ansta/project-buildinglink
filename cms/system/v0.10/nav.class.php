<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
 * Static Class Nav
 * Version: 1.0
 * Author: Loki Wijnen
 *
 * Available by default are:
 * - getPrimary:	Lists any Pages marked to show in Nav1
 * - getFooter:	Lists any Pages marked to show in Footer
 * - getSecondary:	Lists all Pages relating to the current page depth 
 *   including any direct child pages
 *
 * Todo:
 * - 
 * - 
 * - 
 *
 */
class Nav {
	
	/*
	 * Primary nav:
	 * - typically would be any top level pages that should appear in the main navigation
	 * - special characteristics: can show pages from any depth rather than only top level pages
	 */
	public static function getList($depth = 1,$parent_id = 0) {
		
		$version_control = Config::$settings['pages']['version_control'];
		
		$query = "
			SELECT
				`id`
			,	`slug`
			,	`url`
			,	`title`
			,	`label`
			,	pos
			FROM
				`pages`
			WHERE
				`nav1` = true
			AND `active` = true
			AND `depth` = ".db::link()->real_escape_string($depth)."
		";
		if ($parent_id > 0) {
			$query .= "
				AND `parent` = ".db::link()->real_escape_string($parent_id)."
			";
		}
		
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
		if (isset(Config::$settings['pages']['filter_by_site']) && Config::$settings['pages']['filter_by_site'] == true) {
			$query .= " AND site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id']);
		}
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$query .= "
			UNION ALL
			
			SELECT
				id
			,	slug
			,   url
			,	title
			,	label
			,	pos
			FROM
				pages_common
			WHERE
				`nav1` = true
			AND `active` = true
			AND `depth` = ".db::link()->real_escape_string($depth)."
		";
		if ($parent_id > 0) {
			$query .= "
				AND `parent` = ".db::link()->real_escape_string($parent_id)."
			";
		}
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$query .= " ORDER BY `pos` ASC";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return FALSE;
	}
	
	/*
	 * Footer nav:
	 * - typically would be any  pages that should appear in the footer of the page
	 * - special characteristics: can show pages from any depth
	 */
	public static function getFooter($depth = 1,$parent_id = 0) {
		
		$version_control = Config::$settings['pages']['version_control'];
		
		$query = "
			SELECT
				`id`
			,	`slug`
			,	`url`
			,	`title`
			,	`label`
			,	pos
			FROM
				`pages`
			WHERE
				`footer1` = true
			AND `active` = true
			AND `depth` = ".db::link()->real_escape_string($depth)."
		";
		if ($parent_id > 0) {
			$query .= "
				AND `parent` = ".db::link()->real_escape_string($parent_id)."
			";
		}
		
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
		if (isset(Config::$settings['pages']['filter_by_site']) && Config::$settings['pages']['filter_by_site'] == true) {
			$query .= " AND site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id']);
		}
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$query .= "
			UNION ALL
			
			SELECT
				id
			,	slug
			,   url
			,	title
			,	label
			,	pos
			FROM
				pages_common
			WHERE
				`footer1` = true
			AND `active` = true
			AND `depth` = ".db::link()->real_escape_string($depth)."
		";
		if ($parent_id > 0) {
			$query .= "
				AND `parent` = ".db::link()->real_escape_string($parent_id)."
			";
		}
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$query .= " ORDER BY `pos` ASC";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return FALSE;
	}
	
	/*
	 * Secondary nav:
	 * - currently this would not be suitable for top level navs with hover menus
	 * - will show all child pages of the selected top level page
	 * - when selected, child pages will show their direct children
	 */
	public static function getSecondary() {
	#	return Page::$id[1].'x';
		return self::_getSub((int)Page::$id[1]);
	}
	
	/*
	 * _getSub:
	 * - this gets the list of child pages based on a given ID matching their parent ID value
	 */
	private static function _getSub($parent_id) {
		if (!is_int($parent_id)) { die('Value passed to Nav::getForParent() is not valid. Must be of type INT. '.$parent_id.' is not an INT.'); }
		
		$version_control = Config::$settings['pages']['version_control'];
		
		$query = "
			SELECT
				id
			,	title
			,	url
			,	label
			,	depth
			,	slug
			FROM
				`pages`
			WHERE
				`parent` = ".(int)$parent_id."
			AND `nav` = true
			AND `active` = true
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$query .= " ORDER BY `pos` ASC";
		
		$nav = db::link()->query($query);
		if ($nav->num_rows) {
			while( $item = $nav->fetch_object() ) {
				$i++;
				$current_depth = $item->depth;
				if (Page::$slug[$item->depth] == $item->slug) { $linkstate = 'active'; } else { $linkstate = 'inactive'; }
				$output .= '<li class="'.$linkstate.'"><a href="'.$item->url.'" title="'.$item->title.'"><span>'.$item->label.'</span></a>';
				$output .= self::_getSub((int)$item->id);
				$output .= '</li>';
			}
			$output = '<ul class="nav'.$current_depth.'">'.$output.'</ul>';
		} else {
			$output = '';
		}
		return $output;
	}
	
	/*
	 * Generates 2 levels of admin nav
	 * 1: top level nav, only modules that don't have a parent_module specified in the settings config.
	 * 2: sub level nav, only items that do have a parent_module specified and if we are currently viewing the parent.
	 *    When viewing child module, a back link appears instead and linking back to the parent module.
	 */
	public static function admin($depth = 1) {
		if ($depth == 1) {
			$output .= '<h2>CMS Navigation</h2>';
			$output .= '<ul>';
			$output .= '<li class="admin-nav-dashboard" title="dashboard"><a href="/admin/dashboard/"><img src="/images/admin/icon-dashboard.png" alt="Dashboard" class="icon" />Dashboard</a></li>';
		#	if(Page::$slug[1] == 'admin') {
				$output .= '<li class="admin-nav-frontend right" title="'.$navitem.'"><a href="/"><img src="/images/admin/icon-frontend.png" alt="Front End" class="icon" />Front End</a></li>';
		#	} else {
		#		$output .= '<li class="admin-nav-backend right" title="'.$navitem.'"><a href="/admin/"><img src="/images/admin/icon-frontend.png" alt="Admin Area" class="icon" />Admin Area</a></li>';
		#	}
			$output .= '<li class="admin-nav-logout right" title="'.$navitem.'"><a href="/admin/dashboard/?logout=true"><img src="/images/admin/icon-logout.png" alt="Logout" class="icon" />Logout</a></li>';
			$output .= '</ul>';
		}
		
		$output .= '<h2>Modules</h2>';
		$output .= '<ul>';
		foreach (Config::$settings AS $k => $v) {
			if ($v['in_admin_menu']) {
				if ($v['protected']) {
					$style_protected = ' is_protected';
				} else {
					$style_protected = '';
				}
				if ($v['protected'] && $_SESSION['id'] > 1) {
					$show = FALSE;
				} else {
					$show = TRUE;
				}
				if ($show) {
					$permitted_admin_slug[] = $k;
					if ($depth == 2 && Page::$slug[2] == $v['parent_module']) {
						$output .= '<li class="admin-nav-'.$k.$style_protected.'" title="'.$v['plural'].'"><a href="/admin/'.$k.'/">'.$v['plural'].'</a></li>';
					
					} elseif ($depth == 2 && $v['parent_module'] != '' && Page::$slug[2] != $v['parent_module'] && Page::$slug[2] == $k) {
						$output .= '<li class="admin-nav-'.$k.$style_protected.'" title="'.$v['plural'].'"><a href="/admin/'.$v['parent_module'].'/">Back to '.Config::$settings[$v['parent_module']]['plural'].'</a></li>';
					
					} elseif ($depth == 1 && $v['parent_module'] == '') {
						$output .= '<li class="admin-nav-'.$k.$style_protected.'" title="'.$v['plural'].'"><a href="/admin/'.$k.'/"><img src="/image.php?i=/'.$k.'/images/icon-20.png" alt="'.$k.'" class="icon">'.$v['plural'].'</a></li>';
					}
				}
			}
		}
		$output .= '</ul>';
		
		return $output;
	}
	
	/*
	 * Builds a list of Child modules for a given parent module
	 * If there are no children, this section is hidden from the user.
	 */
	public static function showChildModulesList() {
		
		if (Page::$slug[2] != '') {
			
			foreach (Config::$settings AS $k => $v) {
				if ($v['in_admin_menu']) {
					$permitted_admin_slug[] = $k;
					if (Page::$slug[2] == $v['parent_module']) {
						$itemlist .= '<li class="admin-nav-'.$k.'" title="'.$v['plural'].'"><a href="/admin/'.$k.'/">'.$v['plural'].'</a></li>';
					
					} elseif ($v['parent_module'] != '' && Page::$slug[2] != $v['parent_module'] && Page::$slug[2] == $k) {
						$itemlist .= '<li class="admin-nav-'.$k.'" title="'.$v['plural'].'"><a href="/admin/'.$v['parent_module'].'/">Back to '.Config::$settings[$v['parent_module']]['plural'].'</a></li>';
					
					}
				}
			}
			
			if ($itemlist != '') {
				$output  = '<h2>Modules</h2>';
				$output .= '<ul>'.$itemlist.'</ul>';
			}
			
			return $output;
		}
	}
	
	/*
	 * Provides a default central location for the title of the help text in SECONDARY elements of the template,
	 */
	public static function getTipsTitle() {
		return '<h2>Tips</h2>';
	}

}