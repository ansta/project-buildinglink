<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* A (currently not very large) collection of useful functions
*/
class Tool {
	
	
	/*
	* Returns a human friendly date from a timestamp field
	* $input_format is not yet used
	*/
	public static function makeDate($input,$format = 'd/m/Y',$input_format = 'TIMESTAMP') {
		return date($format,strtotime($input));
	}
	
	
	/*
	* Cleans a string and if necessary truncates it to a chosen length
	*/
	public static function cleanString($content,$text_length_limit = 0,$dotdotdot = '...',$strip_tags = TRUE,$clense_array = NULL) {
		
		// Count how many characters are in the supplied string (need this later)
		$input_length = strlen($content);
		
		// Clean the string of any bad characters or snippets
		if ($strip_tags == TRUE) {
			$content = strip_tags($content);
		}
		
		// Clean the string of any bad characters or snippets
		if ($clense_array == NULL) {
			$content = str_replace('&nbsp;',' ',$content);
			$content = str_replace('&ndash;','-',$content);
			$content = str_replace('&ldquo;','"',$content);
			$content = str_replace('&rdquo;','"',$content);
			$content = str_replace('&nbsp;',' ',$content);
		} else {
			foreach ($clense_array AS $k => $v) {
				$content = str_replace($k,$v,$content);
			}
		}
		
		// Trim the string to the desired length
		if ($text_length_limit > 0) {
			$content = substr($content, 0, $text_length_limit);
		}
		
		// If the input length is greater than the trim length then add ... to the end of the string
		if ($input_length > $text_length_limit) {
			$content = $content.$dotdotdot;
		}
		
		// Output the processed string
		return $content;
	}
	
	
	/*
	*
	*/
	public static function cleanUrl($input) {
		
		// Strip out multiple forward slashes and replace with a single one
		$output = str_replace(array('////////','///////','//////','/////','////','///','//'), '/', $input);
		
		return $output;
	}
	
	
	/*
	*
	*/
	public static function my_dump($var) {
		ob_start();
		var_dump($var);
		return ob_get_clean();
	}
	
	/*
	*
	*/
	public static function pre_dump($var) {
		ob_start();
		var_dump($var);
		echo '<pre>'.ob_get_clean().'</pre>';
	}
	
	/*
	*
	*/
	public static function formatFilesize($file) {
		$bytes = filesize($file);
		$kbytes = $bytes / 1024;
		$mbytes = $bytes / 1048576;
		$gbytes = $bytes / 1073741824;
		if ($bytes < 1024) {
			$size = $bytes.'b';
		} elseif ($bytes >= 1024 && $bytes < 1048576) {
			$size = number_format($kbytes,2).'Kb';
		} elseif ($bytes >= 1048576 && $bytes < 1073741824) {
			$size = number_format($mbytes,2).'Mb';
		} elseif ($bytes >= 1073741824 && $bytes < 1099511627776) {
			$size = number_format($gbytes,2).'Gb';
		}
		return $size;
	}
	
	
	
#	Tool::backup_tables();


	
	/*
	* Backup the whole database OR just a set of table(s)
	*/
	public static function backup_tables($tables = '*') {
		
		// get all or selected list of tables
		if($tables == '*') {
			$filename = 'all';
			$tables = array();
			$query = 'SHOW TABLES';
			$result = db::link()->query($query);
			while($row = $result->fetch_assoc()) {
				$tables[] = $row[0];
			}
		} else {
			if (!is_array($tables)) {
				$tables = explode(',',$tables);
			}
			$tables_count = count($tables);
			if ($tables_count == 1) {
				$filename = $tables[0].'table';
			} else {
				$filename = $tables_count.'-tables';
			}
		}
		
		$return  = "\n-- CMS SQL Dump";
		$return .= "\n-- version 1.0.0";
		$return .= "\n-- http://ansta.co.uk";
		$return .= "\n--";
		$return .= "\n-- Written by: Loki Wijnen";
		$return .= "\n-- Edited by: - your name here -";
	#	$return .= "\n--";
	#	$return .= "\n-- Host: localhost";
	#	$return .= "\n-- Generation Time: Sep 14, 2013 at 06:27 PM";
	#	$return .= "\n-- Server version: 5.1.67";
	#	$return .= "\n-- PHP Version: 5.3.3";
	#	$return .= "\n";
	#	$return .= "\n".'SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO';
	#	$return .= "\n".'\nSET time_zone = "+00:00";';
		$return .= "\n";
		$return .= "\n--";
		$return .= "\n-- Database: `".DB_NAME."`";
		$return .= "\n--";
		$return .= "\n";
			
		// cycle through the list of tables
		foreach($tables as $table) {
			
			$creates = db::link()->query('SHOW CREATE TABLE '.$table);
			
			if ($creates->num_rows > 0) {
				$create = $creates->fetch_assoc();
				$drop = 'DROP TABLE IF EXISTS '.$table.';';
				$create = $create["Create Table"].';';
				$column_array = array();
				
				$query = 'SHOW COLUMNS FROM '.$table;
				$columns = db::link()->query($query);
				if ($columns->num_rows > 0) {
					$num_columns = $columns->num_rows;
					while ($column = $columns->fetch_assoc()) {
						$column_array['name'][]		= $column['Field'];
						$column_array['type'][]		= $column['Type'];
						$column_array['null'][]		= $column['Null'];
						$column_array['key'][]		= $column['Key'];
						$column_array['default'][]	= $column['Default'];
						$column_array['extra'][]	= $column['Extra'];
					}
				}
				
				$query = 'SELECT * FROM '.$table;
				$rows = db::link()->query($query);
				if ($rows->num_rows > 0) {
					$num_rows = $rows->num_rows;
					
					$insert = "INSERT INTO ".$table." (`".implode('`,`',$column_array['name'])."`) VALUES ".PHP_EOL;
					$i = 0;
					while ($row = $rows->fetch_assoc()) {
						$i++;
						$line = '';
						foreach ($column_array['name'] AS $num => $field) {
							
							// Check and apply if quotes are needed around the field
							if (substr($column_array['type'][$num], 0, 3) == 'int' || substr($column_array['type'][$num], 0, 3) == 'tin') {
								$quotes = '';
							} else {
								$quotes = '"';
							}
							
							// Escape the data
							$cell_data = addslashes($row[$field]);
							$cell_data = preg_replace("/\n/","\\n",$cell_data);
							
							// Wrap the value of the field in quotes if it's not an INT or TINYINT
							$cell_output = ','.$quotes.$cell_data.$quotes;
							
							// If a field is set as null and the value is empty then replace the empty "" with the word null
							if (strtolower($column_array['null'][$num]) == 'yes') {
								$cell_output = str_replace('""','null',$cell_output);
							}
							
							// If a field has a default value specified and data is cell_output is null then replace null with the default value
							if ($column_array['default'][$num] != NULL) {
								$cell_output = str_replace('null',$quotes.$column_array['default'][$num].$quotes,$cell_output);
							}
							
							// Now save the cell to $line and move on to the next one
							$line .= $cell_output;
						}
						// Build the full line and wrap in brackets
						if ($i < $num_rows) {$line .= '),';} else {$line .= ');';}
						$insert .= '('.substr($line,1).PHP_EOL;
					}
				}
			}
			$return .= "\n-- --------------------------------------------------------";
			$return .= "\n";
			$return .= "\n--";
			$return .= "\n-- Dropping table `".$table."`";
			$return .= "\n--";
			$return .= "\n";
			$return .= "\n".$drop;
			$return .= "\n";
			$return .= "\n-- --------------------------------------------------------";
			$return .= "\n";
			$return .= "\n--";
			$return .= "\n-- Creating table `".$table."`";
			$return .= "\n--";
			$return .= "\n";
			$return .= "\n".$create;
			$return .= "\n";
			$return .= "\n-- --------------------------------------------------------";
			$return .= "\n";
			$return .= "\n--";
			$return .= "\n-- Inserting data in table `".$table."`";
			$return .= "\n--";
			$return .= "\n";
			$return .= "\n".$insert;
			$return .= "\n";
			$return .= "\n";
			$return .= "\n";
		}
		$return .= "\n--";
		$return .= "\n-- End of SQL file";
		$return .= "\n--";
		
		//save file
		$filename = date('Ymd-His').'-'.$filename;
		$full_path = SYSTEM_ROOT.'/modules/data/uploads/database_backups/'.$filename.'.sql';
		$handle = fopen($full_path,'w+');
		fwrite($handle,$return);
		fclose($handle);
		
	#	return $return;
		return true;
	}

			
			
			
			
			
			
			/*
			
			$query = 'SHOW COLUMNS FROM '.$table;
			$columns = db::link()->query($query);
			if ($columns->num_rows > 0) {
				$num_columns = $columns->num_rows;
				while ($column = $columns->fetch_assoc()) {
					$column[] = $column['Field'];
				}
			}
			var_dump($column);
			
			$query = 'SELECT * FROM '.$table;
			echo '<br>query 2: '.$query;
			$result = db::link()->query($query);
			$num_rows = $result->num_rows;
		#	$num_fields = 2;
			
			$return .= 'DROP TABLE IF EXISTS '.$table.';';
		#	$row2 = mysql_fetch_row(db::link()->query('SHOW CREATE TABLE '.$table));
			$query = 'SHOW CREATE TABLE '.$table;
			echo '<br>query 3: '.$query;
			$r2 = db::link()->query($query);
			if ($r2->num_rows > 0) {
				$row2 = $r2->fetch_assoc();
				$return .= "\n\n".$row2["Create Table"].";\n\n";
				
				for ($i = 0; $i < $num_rows; $i++) 
				{
					while($drow = $result->fetch_assoc())
					{
						$return.= 'INSERT INTO '.$table.' VALUES(';
						for($j=0; $j<$num_columns; $j++) 
						{
							$rowX[$j] = addslashes($rowX[$j]);
						#	$rowX[$j] = preg_replace("/\n/","\\n",$rowX[$j]);
							if (isset($rowX[$j])) { $return.= '"'.$rowX[$j].'"' ; } else { $return.= '""'; }
							if ($j<($num_fields-1)) { $return.= ','; }
						}
						$return.= ");\n";
					}
				}
				$return.="\n\n\n";
			}
		}
		echo $return;
		echo '</pre>';
		//save file
	#	$handle = fopen(SYSTEM_ROOT.'/modules/data/uploads/database_backups/db-backup-'.time().'.sql','w+');
	#	fwrite($handle,$return);
	#	fclose($handle);
	}
	
	*/
	
	
}