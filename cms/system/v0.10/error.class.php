<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
*
*
*/
class Error {

	/*
	*
	*
	*/
	public static 	$fastcgi = false;
	private static 	$debug_msg = '';


	/*
	*
	*
	*/
	public static function type($error_type = 0) {
		# When using FastCGI, wwap the header tags below to:
		# header("Status: 404 Not Found");
		# 
		# See the following pages for more information:
		# http://php.net/manual/en/function.header.php
		# http://www.jonasjohn.de/snippets/php/headers.htm
		
		if ($error_type == 404) {
			header("HTTP/1.0 404 Not Found");
			require_once(SYSTEM_ROOT.'modules/default/front_end/error404.php');
			die();
		} elseif ($error_type == 403) {
			header('HTTP/1.1 403 Forbidden');
			require_once(SYSTEM_ROOT.'modules/default/front_end/error403.php');
			die();
		} elseif ($error_type == 500) {
			header('HTTP/1.1 500 Internal Server Error');
			require_once(SYSTEM_ROOT.'modules/default/front_end/error500.php');
			die();
		} else {
			die('unspecified error type');	
		}
	}

	/*
	*
	* NOT YET READY TO USE BUT DO NOT DELETE
	*
	*/
	public static function storeMsg($name,$message = '') {
		if ($message != '') {
	#		$_SESSION['error'] = $message;
		} else {
	#		$_SESSION['error'] = 'Field is required';
		}
		return FALSE;
	}
	
	public static function addDebugMessage($message = '') {
		self::$debug_msg = self::$debug_msg."<p>$message</p>";
	}
	public static function getDebugMessage() {
		echo self::$debug_msg;
	}
	public static function logError($message) {
		$clean_message = str_replace ("\n" , '' , $message );
		$clean_message = str_replace ("\t" , '' , $clean_message );
		error_log($clean_message);
	}
	
	
	
}