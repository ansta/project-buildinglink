<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Provides core setup of the site and pulls in all the relevant and necessary
* files and funtionalities together.
*/
class Core {

	/*
	* - $module_folder is actually the module that should be used 
	* - $module_task is in sort the task that is being carried out
	* 
	* Example: /modules/default/default.header.php
	* 	Where  /modules/$module_folder/$module_task.element.php
	*/
	public static 	$module_folder = 'default',
					$module_task = 'default',
					$template = 'default';
	
	/*
	* Initialise is the starting point
	* 
	* 
	* 
	* Todo:
	* - Should this maybe not be a static class and instead do: new Core();
	* - If yes to todo point 1, maybe put this in Page so we can do new Page();
	* - If yes to todo point 1, maybe put this also in Admin so we can do new Admin();
	* 
	*/
	public static function initialise() {
		
		// Import SuperUsers if the appropriate file exists
		User::importSuperUsers();
		// Perform some security task (caching table and module list)
		Security::initialise();
		// Get available data from URL and assign it to variables for use later on
		self::setSlugs();
		// some last bits that should go elsewhere at some point
		Sites::initialise();
		// setup the table structures
		Config::tableData();
		
		// Detect a LOGOUT request
		if ($_REQUEST['logout'] == 'true') {
			User::logOut();
		}
		// Detect a LOGIN request
		if ($_REQUEST['login'] == 'true') {
			User::getUser();
		}
		
		// Do any redirects if applicable
	#	Redirects::check();
			
		// decide what page/file to show
		if(Page::$slug[1] == ADMIN) {
			// Allow login lockout for "static" sites, only allow login from the Ansta Office
			if (CLIENT_ADMIN == false && $_SERVER['REMOTE_ADDR'] != OFFICE_IP) {
				if (DEBUG_MODE == 2 && USER_IP == OFFICE_IP) {
					die('404: Page not found - reason: this site is not set to have a public accessible admin area.');
				} else {
					Error::type(404);
				}
			}
			// As we are in Admin, check to see if we are logged in
			if (User::isLoggedIn()) {
				// If user is of type 1 (administrator) allow access
				if ($_SESSION['type'] == 1) {
					Admin::resolve();
				// If user is NOT of type 1 (such as members or subscribers) deny admin access
				} else {
					Error::type(403);	
				}
			} else {
				Error::type(403);
			}
		} else if (Page::$slug[1] == 'editor'){
			if (isset($_REQUEST['action'])) {
				// Check if any action requests have been made
				self::checkActions();
			}
			return;
		} else {
			// TODO: At some point it would be nice to restrict any page to a logged in user by ticking a box against a page record
			// We then need to check if the user is logged in before displaying the page.
			// TODO: It would also be nice to be able to set permission levels such as for example:
			// guest (not loggued in), member (registered user such as free members), vip (member with extra rights such as paying members), admin (administrator such as the client), super (a super user such as web dev team staff)
			if (Site::$sitedata['is_live'] != 1) {
				// If website is not yet live, show a maintenance holding page
				Sites::showHoldingPage();
				die();
				
			} else {
				// But currently: all front end pages are accessible to anyone
				Page::resolve();
				
				// Handle Shopping basket cookies and sessions
				Baskets::manageCookies();
			}
		}
		// Check if any action requests have been made
		self::checkActions();
		// Move on to the controller file for the current module
		Module::controller();
		// Get the chosen template
		self::getTemplate();
	}
	
	/*
	* Actions is a way for a form to access processing script for a given module
	* 
	* Module is the name of the module that owns the action file.
	* Action is the specific action available in that module.
	* In the case of the Admin, if a given action file is not found within a custom module,
	* the system will use the action file of the same name if it is present in the default
	* module folder.
	*/
	public static function checkActions() {
		if ($_REQUEST['action'] != '' && $_REQUEST['module'] != '') {
			
			// IMPORTANT TODO:
			// For security reasons, we need to make sure that $_POST['action'] matches regex: [a-z] only (no . or / or - or _ should be permitted)
			// If the validation fails, the process must continue as if all went well from the users point of view IF in production mode.
			// If in Test or Development mode, the process must display an error.
			// In either case a full log of what happened must be made and an email to the developer must be sent.
			// Caution: if someone is hammering the process only a single email to the developer must be sent (or maybe 2 at max) while
			// the rest of the log is stored somewhere.
			// In order to prevent DB flooding, whether successful or failed actions took place, if the number of hits per minute
			// goes beyond a certain level, the process must deny access to the users IP address for a time period and therefore
			// giving the developer time to investigate the issue.
			
			$action = Security::checkActionList($_REQUEST['action']);
			$module = Security::checkModuleList($_REQUEST['module'],__FILE__.' on line '.__LINE__);
			
			// Front end actions (for front end only)
			if (file_exists(SYSTEM_ROOT.'modules/'.$module.'/front_end/'.$action.'.action.php') && Page::$slug[1] != ADMIN) {
				require_once(SYSTEM_ROOT.'modules/'.$module.'/front_end/'.$action.'.action.php');
				return;
			// Custom back end actions (for back end only)
			} elseif (file_exists(SYSTEM_ROOT.'modules/'.$module.'/back_end/'.$action.'.action.php') && Page::$slug[1] == ADMIN) {
				require_once(SYSTEM_ROOT.'modules/'.$module.'/back_end/'.$action.'.action.php');
				return;
			
			// Default back end actions (for back end only)
			} elseif (file_exists(SYSTEM_ROOT.'modules/default/back_end/'.$action.'.action.php') && Page::$slug[1] == ADMIN) {
				require_once(SYSTEM_ROOT.'modules/default/back_end/'.$action.'.action.php');
				return;
			}
		}
	}
	
	/*
	* Reads the URL and stores the ondividual parts into an array.
	* 
	* This array is found in the Page class and has the following charateristics:
	* Given this example URL: http://www.domain.com/xxx/yyy/zzz/?a=1&b=2
	* - Page::$slug[0] = the full URL after the domain name and excluding any query string
	* - Page::$slug[1] = is the first slug of the URL (without slashes) eg: xxx
	* - Page::$slug[2] = is the second slug of the URL (without slashes) eg: yyy
	* - Page::$slug[3] = is the third slug of the URL (without slashes) eg: zzz
	* - Page::$slug[4] = is the forth slug of the URL (without slashes). Slug 4 is empty as there are only 3 slugs.
	* 
	* In addition to the above, additional data is made available:
	* - Page::$scheme = http or https
	* - Page::$host = www.domain.com
	* - Page::$path = http://www.domain.com/xxx/yyy/zzz/
	* - Page::$query = a=1&b=2
	* - Page::$fulluri = Page::$scheme.Page::$host.Page::$path.Page::$query
	* - Page::$depth = is the current URL depth so in this case: 3
	* 
	* Todo:
	* - There is a todo in the function to maybe array the query string but that might be pointless as
	*   that data is actually in a the GET array already so it would be duplicating which is kind of pointless.
	*   So plan is to keep this in mind and see if it is ever needed or not and probably remove the idea sometime.
	*/
	public static function setSlugs() {
		$slugs = explode("/", $_SERVER['REQUEST_URI']);
		$noqry = explode("?", $_SERVER['REQUEST_URI']);
		
		Page::$scheme = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on") ? "https://" : "http://";
		Page::$host = $_SERVER['SERVER_NAME'];
		Page::$path = $noqry[0];
		Page::$query = (isset($noqry[1]) && $noqry[1] != '') ? '?'.$noqry[1] : '';
		Page::$fulluri = Page::$scheme.Page::$host.Page::$path.Page::$query;
		
		// Count how many elements there are and set the physical depth of this page by substracting 2
		Page::$depth = count($slugs)-2;
		
		// Set the value of slug[0] to the full URL but without any query string
		Page::$slug[0] = $noqry[0];
		
		// Maybe create an array of all the query string parameters?
		// echo $noqry[1];
		
		// Loop through the URL from left to right
		$i = 1;
		while ($i < (Page::$depth + 1)) {
			// Store the slugs into the slug array
			Page::$slug[$i] = $slugs[$i];
			$i++;
		}
		
		return;
	}
	
	/*
	* setModuleTask is used to set and specify the module_task which
	* is then used by getElement() to include the requested files
	*/
	public static function setModuleTask($module_task_name) {
		self::$module_task = $module_task_name;
	}
	
	/*
	* setModuleName is used to set and specify the module name
	*/
	public static function setModuleName($module_name) {
		Module::$name = $module_name;
	}
	
	/*
	* Most of the time we will be using the default or the admin template
	* but sometimes we will need to use a different template file.
	* To do so, we simply need to SET the new template and when the time comes
	* GET the template, this new template will be called instead.
	*/
	public static function getTemplate() {
		if (Page::$slug[1] == ADMIN) {
			$interface = 'back_end';
		} else {
			$interface = 'front_end';
		}
		
		if (file_exists(SYSTEM_ROOT.'modules/'.Module::$name.'/'.$interface.'/'.self::$template.'.template.php')) {
			require_once(SYSTEM_ROOT.'modules/'.Module::$name.'/'.$interface.'/'.self::$template.'.template.php');
			
		} elseif (file_exists(SYSTEM_ROOT.'modules/default/'.$interface.'/'.self::$template.'.template.php')) {
			require_once(SYSTEM_ROOT.'modules/default/'.$interface.'/'.self::$template.'.template.php');
			
		} elseif (file_exists(SYSTEM_ROOT.'modules/default/'.$interface.'/'.'default.template.php')) {
			require_once(SYSTEM_ROOT.'modules/default/'.$interface.'/'.'default.template.php');
			
		} else {
			die('Template '.self::$template.' not found.'. SYSTEM_ROOT.'modules/'.Module::$name.'/'.$interface.'/'.self::$template.'.template.php');
		}
	}
	public static function setTemplate($name) {
		if (Page::$slug[1] == ADMIN) {
			$interface = 'back_end';
		} else {
			$interface = 'front_end';
		}
		
		if (file_exists(SYSTEM_ROOT.'modules/'.Module::$name.'/'.$interface.'/'.$name.'.template.php')) {
			self::$template = $name;
		} else {
			die('Template '.$name.' not found in '.$interface.' of '.Module::$name);
		}
	}
}

