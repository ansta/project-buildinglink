<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* This provides as much functionality as possible for easy creation of forms
* including form validation as well as form field hint and error display.
*/
class Form {
	
	public static 	$required_marker = '*',
					$options = array(),
					$values = array(),
					$method = 'post',
					$audit_action = '',
					$error_message_position = 'after';
	
	
	/*
	* Process edit or insert requests
	* 
	* This automatically builds the queries for inserts and updates 
	* according to the module config
	* 
	* Todo:
	* - when doing an update, do checks to make sure that the ID we
	*   are attempting to update actually exists and also ensure
	*   that we are permitted to do so.
	*   - it could be locked
	*   - we may not have permission to edit this record
	*   - think if there maybe be some other reasons?
	* 
	*/
	public static function processData($table_name = '',$data_source = array()) {
		
		// Check to see that no table name has been supplied along with an array of data.
		// This is used for non-REQUEST type processing
		if($table_name == '' || empty($data_source)) {
			$data = $_REQUEST;
		} else {
			$data = $data_source;
		}
		
		$result['query_status'] = 'OK';
		
		if($table_name != '') {
			$data['module'] = Security::checkTableList($table_name);
		}
		
		# Ensure that the module and action requested are valid
		$module = Security::checkModuleList($data['module']);
		$action = Security::checkActionList($data['action']);
		
		// Make sure that we use the correct ID column depending on whether or not this module has version control or not
		$version_control = Config::$settings[$module]['version_control'];
		if($version_control == 'audit' || $version_control == 'full') {
			$item_id = $data['id'];
			$has_version = TRUE;
		} else {
			if (in_array('auto_id', Config::$fields[$module])) {
				$item_id = $data['auto_id'];
				$has_version = FALSE;
			} else {
				$item_id = $data['id'];
				$has_version = FALSE;
			}
		}
		
		# Determin what action is requested
		if (($action == 'edit' || $action == 'update') && $item_id > 0) {
			$result['action_type'] = 'update';
			if (self::getAuditAction() == '') {
				self::setAuditAction('update');
			}
		} else {
			$result['action_type'] = 'insert';
			if (self::getAuditAction() == '') {
				self::setAuditAction('insert');
			}
		}
		
		# Initialise the title for the success or failure message to make sure it's empty
		$_SESSION['form']['form-item'] = '';
		
		# From the list of fields in the config...
		foreach (Config::$fields[$module] as $key => $value) {
			if ($value['name'] == 'auto_id' || $value['name'] == 'id' || $value['name'] == 'attachment') {
			#	echo 'skip: '.$value['name'];
			# For all columns listed in this IF, don't auto populate the query
			} elseif ($value['formtype'] == 'attachment') {
			#	echo 'skip: '.$value['name'];
			# For all columns listed in this IF, don't auto populate the query
			} else {
				# ...only process fields present in the REQUEST or FILES arrays (and not in the above IF list)
				if (isset($data[$value['name']]) OR isset($_FILES[$value['name']])) {
					
					$insert_column[$value['name']] = $key;
					$insert_values[$value['name']] = db::link()->real_escape_string($data[$value['name']]);
					
					$types .= $value['name'].': '.$value['formtype'].'<br>';
					
					# Prepare the title for the success of failure message
					if ($value['istitle'] == 'true') {
						$_SESSION['form']['form-item'] .= $_REQUEST[$value['name']].' ';
					}
					
					$insert_col_count = $key;
					# But ignore password fields if the password value submitted is blank
					if ($value['formtype'] == 'password' && $data[$value['name']] == '') {
						# If field is of type password and value submitted is blank
						# then ignore this field as this means we don't want to over-write
						# the current value of the password field.
						
						# If however this table is version controlled, we need to copy the original password over
						if($version_control == 'audit' || $version_control == 'full') {
							$password_copy_query = "SELECT password FROM `".$module."` WHERE id = ".$data['id']." AND ttv_start <= NOW() AND ttv_end IS NULL";
							$get_passwords = db::link()->query($password_copy_query);
							if ($get_passwords->num_rows) {
								$get_password = $get_passwords->fetch_assoc();
								$this_password = $get_password['password'];
								$insert_column[$value['name']] = $key;
								$insert_values[$value['name']] = db::link()->real_escape_string($this_password);
							}
						}
						
					# If password field is submitted and is NOT blank, change the password
					} elseif ($value['formtype'] == 'password' && $data[$value['name']] != '') {
						$make_password = User::makePassword($data[$value['name']]);
						$insert_column[$value['name']] = $key;
						$insert_values[$value['name']] = $make_password['hash'];
						$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($make_password['hash'])."'";
						
					# If Multi-Select field
					} elseif ($value['formtype'] == 'multi') {
						
						foreach ($data[$value['name']] as $multi_key => $multi_value) {
							if (!isset($multi_data)) {
								$multi_data = $multi_value;
							} else {
								$multi_data .= ','.$multi_value;
							}
						}
						
						$insert_column[$value['name']] = $key;
						$insert_values[$value['name']] = $multi_data;
						$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($multi_data)."'";
						
					# If tags field
					} elseif ($value['formtype'] == 'tags' && $data[$value['name']] == '') {
						$has_tags = TRUE;
						$insert_column[$value['name']] = $key;
						$insert_values[$value['name']] = '';
					} elseif ($value['formtype'] == 'tags' && $data[$value['name']] != '') {
						$has_tags = TRUE;
						$insert_column[$value['name']] = $key;
						$insert_values[$value['name']] = db::link()->real_escape_string($data[$value['name']]);
						$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($data[$value['name']])."'";
						
			#			if (file_exists(SYSTEM_ROOT.'/modules/'.$value['autocomplete_module'].'/'.$value['autocomplete_module'].'.config.php')) {
			#				$has_tags = TRUE;
			#				$tags_item_id = $data['id'];
			#				$tags_array = explode(' ',$data[$value['name']]);
			#				$tags_classname = ucfirst($value['autocomplete_module']);
			#			#	die('found');
			#			} else {
			#			#	die('missing');
			#			}
						
					# If image field
					} elseif ($value['formtype'] == 'img') {
						
						if (isset($_FILES[$value['name']]['name']) && $_FILES[$value['name']]['name'] != '') {
							
							$groupby = Security::checkColumn($value['root'],$value['groupby']);
							
							if ($result['action_type'] == 'update') {
								$imagedata = self::processImageUpload($value,$data[$groupby],$result,'process');
							} else {
								$imagedata = self::processImageUpload($value,$data[$groupby],$result,'prepare');
							}
							
							$insert_column[$value['name']] = $key;
							$insert_values[$value['name']] = db::link()->real_escape_string($imagedata['image_filename_for_db']);
							$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($imagedata['image_filename_for_db'])."'";
							
						}
						
					# If document field
					} elseif ($value['formtype'] == 'file' && $result['action_type'] == 'update') {
						
						if (isset($_FILES[$value['name']]['name']) && $_FILES[$value['name']]['name'] != '') {
							
							$groupby = Security::checkColumn($value['root'],$value['groupby']);
							$filedata = self::processFileUpload($value,$_REQUEST[$groupby]);
							$insert_column[$value['name']] = $key;
							$insert_values[$value['name']] = db::link()->real_escape_string($filedata['filename']);
							$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($filedata['filename'])."'";
							
						}
						
					# For all other fields, prepare the data in a normal manner
					} elseif ($value['formtype'] == 'file' && $result['action_type'] == 'insert') {
						
						if (isset($_FILES[$value['name']]['name']) && $_FILES[$value['name']]['name'] != '') {
							
							$filename = self::clenseFilename($_FILES[$value['name']]['name']);
							
							$insert_column[$value['name']] = $key;
							$insert_values[$value['name']] = db::link()->real_escape_string($filename);
							$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($filename)."'";
							
						}
						
					# For all other fields, prepare the data in a normal manner
					} else {
						$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($data[$value['name']])."'";
					}
					
#	if ($module == 'attachments') {
#		if ($data[$value['name']] == '' || $data[$value['name']] == 0 || $data[$value['name']] == NULL ) {
#			echo '<pre>Form::processData ';
#			var_dump($data);
#			die();
#		}
#	}
				} else {
					if (isset($data['slug'])) {
						if ($value['name'] == 'url' || $value['name'] == 'depth') {
							$url_n_depth = self::setUrlField($module);
							$new_data = $url_n_depth['new_'.$value['name']];
							
							$insert_column[$value['name']] = $key;
							$insert_values[$value['name']] = db::link()->real_escape_string($new_data);
							$update_colvals[] = "`".$value['name']."` = '".db::link()->real_escape_string($new_data)."'";
						}
					}
					# Otherwise ignore from this query
				}
				
			}
		}
						
		if ($result['action_type'] == 'update') {
			if ($has_version) {
				
				
				# Create the first query
				# This is to get the auto_id of the record that we are going to copy and then expire
				$query1 = " /* q1 */
					SELECT
						*
					FROM
						`".$module."`
					WHERE
						`ttv_start` <= NOW()
					AND `ttv_end` IS NULL
					AND `id` = ".db::link()->real_escape_string($item_id)."
				";
				if ($results1 = db::link()->query($query1)) {
					$r1 = $results1->fetch_assoc();
	#				if (DEBUG_MODE > 0) {$_SESSION['processes'] .= ' 1 '; $_SESSION['qry1'] = $query1; }
				} else {
					if (DEBUG_MODE > 0) {$_SESSION['err1'] = 'Query 1 failed: '.$query1.' - ERROR: '.db::link()->error;}
					$result['query_status'] = 'ERROR';
					$result['error_location'] = 'err1';
				}
				
				if ($result['query_status'] == 'OK') {
					# Create the second query
					# From the list of fields in the config...
					foreach (Config::$fields[$module] as $key => $value) {
						if ($value['name'] == 'auto_id' || $value['name'] == 'attachment') {
							# For all columns listed in this IF, don't auto populate the query
						} elseif ($value['formtype'] == 'attachment') {
							# For all columns listed in this IF, don't auto populate the query
						} else {
							$insert2_columni[$value['name']] = $key;
							$insert2_columns[$value['name']] = $key;
						}
					}
					# Copies the current data and inserts it into a new row
					$query2 = " /* q2 */
						INSERT INTO
							`".$module."`
							(
								`".implode("`,`", array_keys($insert2_columni))."`
							)
						SELECT
							`".implode("`,`", array_keys($insert2_columns))."`
						FROM
							`".$module."` d
						WHERE
							`d`.`auto_id` = ".$r1['auto_id']."
					";
					if ($results2 = db::link()->query($query2)) {
						$r2 = db::link()->query("SELECT LAST_INSERT_ID() AS insert_id");
						$last_id = mysqli_fetch_assoc($r2);
						if (DEBUG_MODE > 0) {
#							$_SESSION['processes'] .= ' 2 ';
#							$_SESSION['processes'] .= ' (lid: '.(int)$last_id.') ';
#							$_SESSION['qry2'] = $query2;
						}
					} else {
						if (DEBUG_MODE > 0) {
							$_SESSION['err2'] = 'Query 2 failed: '.$query2.' - ERROR: '.db::link()->error;
						}
						$result['query_status'] = 'ERROR';
						$result['error_location'] = 'err2: '.$query2;
					}
				}
			
			
				if ($result['query_status'] == 'OK') {
					# Create the third query
					# To expire the old record
					$query3 = " /* q3 */
						UPDATE 
							`".$module."`
						SET
							`ttv_end` = NOW(),
							`modify_by` = '".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
							`modify_id` = ".(int)db::link()->real_escape_string($_SESSION['id']).",
							`modify_action` = '".db::link()->real_escape_string(self::getAuditAction())."'
						WHERE
							`auto_id` = ".(int)$r1['auto_id']."
						AND `id` = ".(int)db::link()->real_escape_string($item_id)."
						AND `ttv_end` IS NULL
					";
					if ($results3 = db::link()->query($query3)) {
						if (DEBUG_MODE > 0) {
#							$_SESSION['processes'] .= ' 3 ';
#							$_SESSION['qry3'] = $query3;
						}
					} else {
						if (DEBUG_MODE > 0) {
							$_SESSION['err3'] = 'Query 3 failed: '.$query3.' - ERROR: '.db::link()->error;
						}
						$result['query_status'] = 'ERROR';
						$result['error_location'] = 'err3';
					}
				}
			
			
				if ($result['query_status'] == 'OK') {
					# Create the forth query
					# To enter the new data into the new record
					$query4 = " /* q4 */
						UPDATE 
							`".$module."`
						SET
							".implode(", ", $update_colvals).",
							`ttv_start` = NOW(),
							`create_by` = '".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
							`create_id` = ".(int)db::link()->real_escape_string($_SESSION['id']).",
							`create_action` = '".db::link()->real_escape_string(self::getAuditAction())."'
						WHERE
							`ttv_start` IS NULL
						AND `ttv_end` IS NULL
						AND `id` = ".(int)db::link()->real_escape_string($item_id)."
					";
					if ($results4 = db::link()->query($query4)) {
						if (DEBUG_MODE > 0) {
#							$_SESSION['processes'] .= ' 4 ';
#							$_SESSION['qry4'] = $query4;
						}
					} else {
						if (DEBUG_MODE > 0) {
							$_SESSION['err4'] = 'Query 4 failed: '.$query4.' - ERROR: '.db::link()->error;
						}
						$result['query_status'] = 'ERROR';
						$result['error_location'] = 'err4';
					}
				}
				
				
			} else {
				# Create the query
				$result['query0'] = " /* q0 */
					UPDATE
						".$module."
					SET
						".implode(", ", $update_colvals).",
						`modify_by` = '".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
						`modify_id` = ".(int)db::link()->real_escape_string($_SESSION['id']).",
						`modify_action` = '".db::link()->real_escape_string(self::getAuditAction())."'
					WHERE
						
				";
				if (in_array('auto_id', Config::$fields[$module])) {
					$result['query0'] .= "`auto_id` = ".db::link()->real_escape_string($item_id);
				} else {
					$result['query0'] .= "`id` = ".db::link()->real_escape_string($item_id);
				}
			}
			
		} elseif ($result['action_type'] == 'insert') {

			$result['queryi'] = " /* qi */
				INSERT INTO `".$module."`
					(
						`id`,
						`".implode("`,`", array_keys($insert_column))."`,
						`ttv_start`,
						`ttv_end`,
						`create_by`,
						`create_id`,
						`create_action`
					) VALUES (
						0,
						'".implode("','",$insert_values)."',
						NOW(),
						NULL,
						'".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
						".(int)db::link()->real_escape_string($_SESSION['id']).",
						'".db::link()->real_escape_string(self::getAuditAction())."'
					)
			";
			
			
			if(db::link()->query($result['queryi'])) {
				$result['query_status'] = 'OK';
				$get_id = db::link()->query("SELECT LAST_INSERT_ID() AS insert_id");
				$last_id = mysqli_fetch_assoc($get_id);
				
				// Get new ID for the tags process
				$tags_item_id = $last_id['insert_id'];
				
				if(db::link()->query("UPDATE `".$module."` SET id = ".(int)$last_id['insert_id']." WHERE auto_id = ".(int)$last_id['insert_id']." AND id = 0")) {
					$result['query_status'] = 'OK';
				} else {
					$result['query_status'] = 'ERROR';
					$result['query_err_msg'] = db::link()->error;
					if (DEBUG_MODE > 0) {
						die('errori2: '.db::link()->error.' - '.$result['queryi2']);
					}
					$result['error_location'] = 'err5';
				}

				###################################################################################
				// From the list of fields in the config...
				foreach (Config::$fields[$module] as $key => $value) {
					if ($value['name'] == 'auto_id' || $value['name'] == 'id') {
						// For all columns listed in this IF, don't auto populate the query
					} else {
						// ...only process fields present in the REQUEST or FILES arrays (and not in the above IF list)
						if (isset($_FILES[$value['name']])) {
							
					#		$insert_column[$value['name']] = $key;
					#		$insert_values[$value['name']] = db::link()->real_escape_string($data[$value['name']]);
							
					#		$types .= $value['name'].': '.$value['formtype'].'<br>';
							
							// If image field
							if ($value['formtype'] == 'img') {
								if (isset($_FILES[$value['name']]['name']) && $_FILES[$value['name']]['name'] != '' && $result['action_type'] == 'insert') {
									$imagedata = self::processImageUpload($value,$tags_item_id,$result,'process');
								}
								
							// If document field
							} elseif ($value['formtype'] == 'file' && $result['action_type'] == 'insert') {
								if (isset($_FILES[$value['name']]['name']) && $_FILES[$value['name']]['name'] != '') {
									$filedata = self::processFileUpload($value,$tags_item_id);
								}
							}
						}
					}
				}
				###################################################################################
				
			} else {
				$result['query_status'] = 'ERROR';
				$result['query_err_msg'] = db::link()->error;
				if (DEBUG_MODE > 0) {
					die('errori: '.db::link()->error.' - '.$result['queryi']);
				}
				$result['error_location'] = 'err6';
			}
			
		}
		
		
		// Providing that the query is not blank, execute the query
		if ($result['query0'] != '') {
			if(db::link()->query($result['query0'])) {
		#		die('success0: '.$result['query0'].' - '.db::link()->affected_rows);
			} else {
				$result['query_status'] = 'ERROR';
				$result['query_err_msg'] = db::link()->error;
				if (DEBUG_MODE > 0) {
					die('error0: '.db::link()->error.' - '.$result['query0']);
				}
				$result['error_location'] = 'err7';
			}
		}
		
		// Providing that the query is not blank, execute the query
		if ($result['query1'] != '') {
			if(db::link()->query($result['query1'])) {
		#		die('success1: '.$result['query1'].' - '.db::link()->affected_rows);
			} else {
				$result['query_status'] = 'ERROR';
				$result['query_err_msg'] = db::link()->error;
				if (DEBUG_MODE > 0) {
					die('error1: '.db::link()->error.' - '.$result['query1']);
				}
				$result['error_location'] = 'err8';
			}
		}
		
		// Providing that the query is not blank, execute the query
		if ($result['query2'] != '') {
			if(db::link()->query($result['query2'])) {
	#			echo $result['query2'];
			} else {
				$result['query_status'] = 'ERROR';
				$result['query_err_msg'] = db::link()->error;
				if (DEBUG_MODE > 0) {
					die('error2: '.db::link()->error.' - '.$result['query2']);
				}
				$result['error_location'] = 'err9';
			}
		}
		
	
		// If the process went OK and NEXT was set to CONTINUE EDITING then redirect back to the edit page
		if ($result['query_status'] == 'OK' && $data['next'] == 1) {
			$result['redirect'] = true;
			$result['item_id'] = $item_id;
			$result['destination'] = Page::$slug[0];
			$_SESSION['next'] = 1;
			
		// If the process went OK and NEXT was not set to CONTINUE EDITING then redirect back to the listing page
		} elseif ($result['query_status'] == 'OK') {
			if (isset($data['next']) && $data['next'] == 0) {
				$_SESSION['next'] = 0;
			}
			$result['redirect'] = true;
			if (Page::$slug[1] == ADMIN) {
				$result['item_id'] = $item_id;
				if ($result['action_type'] == 'update') {
					$result['destination'] = '../../?done';
				} else {
					if (Page::$slug[4] != '') {
						$result['destination'] = '../../?done';
					} else {
						$result['destination'] = '../?done';
					}
				}
			} else {
				$result['destination'] = Page::$slug[0];
			}
			
		// otherwise redirect back to the edit page
		} else {
			$result['redirect'] = false;
			$result['destination'] = '';
		}
		
		if ($result['item_id'] == NULL) {
			$result['item_id'] = $tags_item_id;
		}
		
		
	#	if ($last_id['insert_id'] == NULL || $last_id['insert_id'] == 0 || $last_id['insert_id'] == '') {
	#	
	#	}
		
	#		echo '<pre>';
	#		echo '$last_id[insert_id]: '.$last_id['insert_id'].PHP_EOL;
	#		echo '$result[item_id]: '.$result['item_id'].PHP_EOL;
	#		echo '$tags_item_id: '.$tags_item_id.PHP_EOL;
	#	die();
		
		// If appropriate, process TAGS (note: relies on ATTACHMENTS module being present)
		if ($has_tags && $module != 'tags') {
			Tags::processData($last_id['insert_id'],$data); 
		}
		
	#	echo '<pre>';
	#	var_dump($result);
		
		// If appropriate, process ATTACHMENTS
		if($module != 'attachments') {
			Attachments::processData($last_id['insert_id'],$data); 			
		}
		
		if ($result['item_id'] == NULL) {
			$result['item_id'] = $tags_item_id;
		}
		
	#	echo '<pre>';
	#	var_dump($result);
	#	die();
		
		
		return $result;
	}
	
	
	
	
	/*
	* Clense a filename of all bad characters and return a substitute filename
	*/
	public static function clenseFilename($filename) {
		$filename = basename($filename);
		$filename = strtolower($filename);
		$filename = str_replace('  ', ' ', $filename);
		$filename = str_replace('(', '', $filename);
		$filename = str_replace(')', '', $filename);
		$filename = str_replace(' ', '_', $filename);
		$filename = str_replace('&', '_and_', $filename);
		return $filename;
	}
	
	/*
	* Process a file upload
	*/
	private static function processFileUpload($value,$item_id = 0) {
		if ($item_id == 0) {
			
			echo '<pre>';
			var_dump($value);
			echo '</pre>';
			
			die();
		}
		$groupby = Security::checkColumn($value['root'],$value['groupby']);
		$uploaddir = SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/';
		if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/')) {
			mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/',0775);
		}
		if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/')) {
			mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/',0775);
		}
		if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/')) {
			mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/',0775);
		}
		
		$filename = self::clenseFilename($_FILES[$value['name']]['name']);
		$uploadfile = $uploaddir . $filename;
		
		if (!move_uploaded_file($_FILES[$value['name']]['tmp_name'], $uploadfile)) {
			die('<br> file upload failed during insert: '.$uploadfile);
		}
		
		$output = array(
			'filename'=>$filename
		);
		return $output;
	}
	
	/*
	* Get image data from form submittion
	*/
	public static function getImageData($filename) {
		$output['raw'] = $filename;
		$i = strrpos($filename,".");
		if (!$i) { return ""; } 
		$l = strlen($filename) - $i;
		$output['ext'] = strtolower(substr($filename,$i+1,$l));
		$filenam_wash = strtolower(substr($filename,0,$i));
		$filenam_wash = str_replace(' ', '-', $filenam_wash);
		$filenam_wash = str_replace('.', '-', $filenam_wash);
		$filenam_wash = str_replace('(', '', $filenam_wash);
		$filenam_wash = str_replace(')', '', $filenam_wash);
		$output['name'] = $filenam_wash;
		return $output;
	}
	
	/*
	* Process a image upload
	*/
	private static function processImageUpload($value,$item_id,$result,$image_action = 'process') {
		// Initialise
		$groupby = Security::checkColumn($value['root'],$value['groupby']);
		$file_data_array = self::getImageData(stripslashes($_FILES[$value['name']]['name']));
		$uploadedimage = $_FILES[$value['name']]['tmp_name'];
		$uploaddir = SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/';
		$output['uploaddir'] = $uploaddir;
		
		// Set the name of the image as we want it saved on the server
		$image_random_hash = '';	// Maybe add a hash in front? Is this necessary?
		$image_filename_for_db = $image_random_hash.$file_data_array['name'].'.'.$file_data_array['ext'];
		$output['image_filename_for_db'] = $image_filename_for_db;
		
		$output['action_type'] = $result['action_type'];
		$output['image_action'] = $image_action;
		
		if ($result['action_type'] == 'insert' && $image_action == 'prepare') {
			return $output;
		}
		
		// Start by preparing the save location
		if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/')) {
			mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/',0775);
		}
		if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/')) {
			mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/',0775);
		}
		if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/')) {
			mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/',0775);
		}
		if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/original/')) {
			mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/original/',0775);
		}
		
		// Upload the original
		$uploadfile = 'original/'.$image_filename_for_db;
		$output['uploadfile'] = $uploadfile;
		$no_duplicate_count1 = 0;
		
		while (file_exists($uploaddir.$uploadfile)) {
			$no_duplicate_count1 = $no_duplicate_count1+1;
			$image_filename_for_db = $file_data_array['name'].'-'.$no_duplicate_count1.'.'.$file_data_array['ext'];
			$output['image_filename_for_db'] = $image_filename_for_db;
			$uploadfile = 'original/'.$image_filename_for_db;
			$output['uploadfile'] = $uploadfile;
		}
		
		// Save the file info in preparation to save it into the database
		$image_filename_array['original'] = $uploadfile;
		
		// Now do thumbnail versions according to the Settings
		if (Config::$settings[$value['root']][$value['name']]) {
			if ($file_data_array['ext'] == "jpg" ||
				$file_data_array['ext'] == "jpeg" ) {
				$newimagesrc = imagecreatefromjpeg($uploadedimage);
			} else if($file_data_array['ext'] == "png") {
				$newimagesrc = imagecreatefrompng($uploadedimage);
			} else {
				$newimagesrc = imagecreatefromgif($uploadedimage);
			}
			list($width,$height) = getimagesize($uploadedimage);
		}
		
		// Now that we have finished with the temp file we can move it to its final location
		// This is the original image (non-resized)
		if (!move_uploaded_file($_FILES[$value['name']]['tmp_name'], $uploaddir.$uploadfile)) {
			/*
			if (file_exists($_FILES[$value['name']]['tmp_name'])) {
				$output['file_exists'] = 'YES';
			} else {
				$output['file_exists'] = 'NO';
			}
			$output['upload_path'] = $uploaddir.$uploadfile;
			$output['tmp_name'] = $_FILES[$value['name']]['tmp_name'];
			
			echo '<pre>';
			var_dump($output);
			echo '</pre>';
			*/
			die('Image upload failed while "'.$result['action_type'].'"-ing.');
		} else {
			
			$uploaddir_private = $uploaddir;
		
			foreach (Config::$settings[$value['root']][$value['name']] AS $type => $arr) {
			
				if (Config::$settings[$value['root']][$value['name']][$type]['public']) {
					$uploaddir_public = PUBLIC_ROOT.'/userfiles/'.$value['root'].'/'.$value['name'].'/'.$item_id.'/';
					// Start by preparing the save location
					if (!is_dir(PUBLIC_ROOT.'/userfiles/')) {
						mkdir(PUBLIC_ROOT.'/userfiles/',0775);
					}
					if (!is_dir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/')) {
						mkdir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/',0775);
					}
					if (!is_dir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/'.$value['name'].'/')) {
						mkdir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/'.$value['name'].'/',0775);
					}
					if (!is_dir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/'.$value['name'].'/'.$item_id.'/')) {
						mkdir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/'.$value['name'].'/'.$item_id.'/',0775);
					}
					if (!is_dir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/'.$value['name'].'/'.$item_id.'/'.$type.'/')) {
						mkdir(PUBLIC_ROOT.'/userfiles/'.$value['root'].'/'.$value['name'].'/'.$item_id.'/'.$type.'/',0775);
					}
					$uploaddir = $uploaddir_public;
					
				} else {
					if (!is_dir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/'.$type.'/')) {
						mkdir(SYSTEM_ROOT.'/modules/'.$value['root'].'/uploads/'.$value['name'].'/'.$item_id.'/'.$type.'/',0775);
					}
					$uploaddir = $uploaddir_private;
				}
				if (Config::$settings[$value['root']][$value['name']][$type]['method'] == 'crop') {
					$thumb_width = $arr['width'];
					$thumb_height = $arr['height'];
					$width = imagesx($newimagesrc);
					$height = imagesy($newimagesrc);
					$original_aspect = $width / $height;
					$thumb_aspect = $thumb_width / $thumb_height;
					if ( $original_aspect >= $thumb_aspect ) {
					   // If image is wider than thumbnail (in aspect ratio sense)
					   $new_height = $thumb_height;
					   $new_width = $width / ($height / $thumb_height);
					} else {
					   // If the thumbnail is wider than the image
					   $new_width = $thumb_width;
					   $new_height = $height / ($width / $thumb_width);
					}
					$tmpcanvas = imagecreatetruecolor( $thumb_width, $thumb_height );
					// Resize and crop
					imagecopyresampled($tmpcanvas,
									   $newimagesrc,
									   0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
									   0 - ($new_height - $thumb_height) / 2, // Center the image vertically
									   0, 0,
									   $new_width, $new_height,
									   $width, $height);
				} else {
					$newheight = ($height/$width)*$arr['width'];
					$tmpcanvas = imagecreatetruecolor($arr['width'],$newheight);
					imagecopyresampled($tmpcanvas,
										$newimagesrc,
										0,
										0,
										0,0,
										$arr['width'],$newheight,
										$width,$height);
				}
				imagejpeg($tmpcanvas,$uploaddir.$type.'/'.$image_filename_for_db,100);
				imagedestroy($tmpcanvas);
			}
			
			return $output;
		}
	}
	
	
	/*
	* Set and get the form method
	*/
	public static function setMethod($method) {
		self::$method = $method;
	}
	public static function getMethod() {
		return self::$method;
	}
	
	/*
	* Create a form field block including label and input via a single funtion
	* 
	* Todo:
	* - reduce the number of params and probably accept a query string in a
	*   similar way as to how wordpress does it. The query string is then
	*   put into an array which then looks for specific terminology to perform
	*   tasks.
	* - Add in display of hints and error messages
	*/
	public static function makeField($field) {
		
		// If the form field is of type img or file and the admin process is not NEW then ignore the REQUIRED flag
		if (($field['formtype'] == 'img' || $field['formtype'] == 'file') && Page::$slug[3] != 'new') {$field['required'] = false;}
		
		$error_info = self::makeErrorMessage($field);
		if (isset($error_info['status']) && $error_info['status']) {
			$error_css_class = ' error-true';
		} else {
			$error_css_class = ' error-false';
		}
		
		if (isset($field['protected']) && $field['protected'] == true) {
			$super_user_only = ' for-super-user';
		} else {
			$super_user_only = '';
		}
		
		if (isset($field['separator']) && $field['separator'] == 'before') {
			$separator = ' separator-before';
		} elseif (isset($field['separator']) && $field['separator'] == 'after') {
			$separator = ' separator-after';
		} elseif (isset($field['separator']) && $field['separator'] == 'both') {
			$separator = ' separator-both';
		} else {
			$separator = '';
		}
		
		if (isset($field['formtype']) && $field['formtype'] == 'attachment') {
			// Do nothing
			
		} elseif (isset($field['formtype']) && $field['formtype'] == 'hidden') {
			$output .= self::makeInput($field,$error_info);
			if (self::$error_message_position == 'after') {
				$output .= $error_info['message'];
			}
			
		} else {
			$output  = '<div class="field name-'.$field['name'].' type-'.$field['formtype'].$error_css_class.$super_user_only.$separator.'">'.PHP_EOL;
			$output .= self::makeLabel($field,$error_info);
			$output .= self::makeInput($field,$error_info);
			if (self::$error_message_position == 'after') {
				$output .= $error_info['message'];
			}
			$output .= self::makeHint($field,$error_info);
			$output .= '</div>';
		}
		$output .= PHP_EOL;
		$output .= PHP_EOL;
		
		return $output;
	}
	
	public static function makeLabel($field,$error_info = '') {
		if ($field['formtype'] == 'hidden' || $field['formtype'] == 'submit') {
			# Don't create a label for these field types
			$output  = '';
		} else {
			$output  = "\t";
			$output .= '<label for="in_'.$field['name'].'">';
			$output .= $field['label'].self::getRequiredMarker($field['required']);
			if (self::$error_message_position == 'inline') {
				$output .= $error_info['message'];
			}
			$output .= '</label>'.PHP_EOL;
		}
		return $output;
	}
	
	public static function makeHint($field,$error_info = '') {
		if ($field['formtype'] == 'yesno' ||
			$field['formtype'] == 'yes1no2' ||
			$field['formtype'] == 'radio' ||
			$field['formtype'] == 'hidden' ||
			$field['protected'] == 'true' ||
			$field['formtype'] == 'checkbox') {
			# Don't display hint for these field types
			$output  = '';
		} else {
			$output .= "\t".'<span class="hint hint-'.$field['formtype'].'">'.$field['hint'].'</span>'.PHP_EOL;
		}
		return $output;
	}
	
	/*
	* Specify where in the HTML the error message will appear.
	* - after: default, appears after for form element and before the hint text
	* - inline: appears within the label tag
	*/
	public static function setErrorMessagePosition($position) {
		self::$error_message_position = $position;
	}
	
	public static function makeErrorMessage($field) {
		$output['status'] = FALSE;
		$output['message'] = '';
		if (isset($_SESSION['form']['field-error'][$field['name']])) {
			$output['status'] = TRUE;
			$output['message'] = "\t".'<span class="error error-'.$field['formtype'].'">'.$_SESSION['form']['field-error'][$field['name']].'</span>'.PHP_EOL;
			unset($_SESSION['form']['field-error'][$field['name']]);
		}
		return $output;
	}
	
	public static function buildDatalist($field) {
		$output = '';
		if (isset($field['list']) && isset($field['datalist'])) {
			$output .= '<datalist id="'.$field['list'].'">';
			foreach ($field['datalist'] AS $dl_k => $dl_v) {
				$output .= '<option>'.$dl_v.'</option>';
			} 
			$output .= '</datalist>';
		}
		return $output;
	}
	
	public static function makeInput($field,$error_info = '') {
		if(isset($field['autocomplete']) && $field['autocomplete'] === 'false'){
			$autocomplete = ' autocomplete="off"';
		} elseif(isset($field['autocomplete']) && $field['autocomplete'] === 'true'){
			$autocomplete = ' autocomplete="on"';
		} else {
			$autocomplete = '';
		}
		
		$field_value = htmlentities(self::$values[$field['name']]);
		
		if (isset($field['maxlength']) && $field['maxlength'] > 0) { $maxlength = ' maxlength="'.$field['maxlength'].'"'; } else { $maxlength = ''; }
		
		if (isset($field['required']) && $field['required'] == true) { $required = ' required="required"'; } else { $required = ''; }
		
		if (isset($field['min'])) { $min = ' min = "'.$field['min'].'"'; } else { $min = ''; }
		if (isset($field['max'])) { $max = ' max = "'.$field['max'].'"'; } else { $max = ''; }
		if (isset($field['step'])) { $max = ' step = "'.$field['step'].'"'; } else { $step = ''; }
		if (isset($field['list'])) { $max = ' list = "'.$field['list'].'"'; } else { $list = ''; }
		if (isset($field['class'])) { $css_class = $field['class']; } else { $list = ''; }
		
		if (isset($field['placeholder'])) { $placeholder = ' placeholder = "'.$field['placeholder'].'"'; } else { $placeholder = ''; }
		
		if ($field['formtype'] == 'text') {
			$output = '<input type="'.$field['formtype'].'" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.$field_value.'" class="'.$css_class.'"'.$autocomplete.$maxlength.$required.$list.$placeholder.' />';
			$output .= self::buildDatalist($field);
		
		// Edit by Steve 09/09/13	
		} elseif( $field['formtype'] == 'color' ) {
			$output = '<input type="text" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.$field_value.'" class="'.$css_class.' color-picker"'.$autocomplete.$maxlength.$required.$list.$placeholder.' />';
			
		} elseif ($field['formtype'] == 'date'
			||	$field['formtype'] == 'datetime'
			||  $field['formtype'] == 'datetime-local'
			||  $field['formtype'] == 'email'
			||  $field['formtype'] == 'month'
			||  $field['formtype'] == 'number'
			||  $field['formtype'] == 'range'
			||  $field['formtype'] == 'search'
			||  $field['formtype'] == 'tel'
			||  $field['formtype'] == 'time'
			||  $field['formtype'] == 'url'
			||  $field['formtype'] == 'week') {
					  
			$output = '<input type="'.$field['formtype'].'" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.$field_value.'" class="'.$css_class.'"'.$autocomplete.$maxlength.$required.$min.$max.$step.$list.$placeholder.' />';
			$output .= self::buildDatalist($field);
			
			
		} elseif ($field['formtype'] == 'tags') {
			$output = '<input type="text" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.$field_value.'" class="'.$css_class.'"'.$autocomplete.$maxlength.$required.$placeholder.' />';
			
			
		} elseif ($field['formtype'] == 'password') {
			$output = '<input type="password" name="'.$field['name'].'" id="in_'.$field['name'].'" class="'.$css_class.'"'.$autocomplete.$required.$placeholder.' />';
			
			
		} elseif ($field['formtype'] == 'img') {
			$output  = '<input type="file" name="'.$field['name'].'" id="in_'.$field['name'].'" class="'.$css_class.'"'.$required.$placeholder.' />';
			
			if ($field_value != '') {
				$output .= '<input type="hidden" name="'.$field['name'].'_check" id="in_'.$field['name'].'_check" value="'.$field_value.'" />';
			}
			
			if (self::$values[$field['name']] != '') {
				
				if (Page::$slug[3] == 'crop') {
					$crop_link = '';
				} else {
					$crop_link = ' <a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/crop/'.Page::$slug[4].'/'.$field['name'].'/">Perform Manual Crop</a>';
				}
				
				$version_1 = $field['root'].'/uploads/'.$field['name'].'/'.self::$values[$field['groupby']].'/admin/'.$field_value;
				$version_2 = $field['root'].'/uploads/'.$field['name'].'/'.self::$values[$field['groupby']].'/thumb/'.$field_value;
				
				$output .= $crop_link;
				
				if (file_exists(SYSTEM_ROOT.'/modules/'.$version_1)) {
					$output .= '<span class="thumbnail image-'.$field['name'].'"><img src="/image.php?i=/'.$version_1.'" /></span>';
				} elseif (file_exists(SYSTEM_ROOT.'/modules/'.$version_2)) {
					$output .= '<span class="thumbnail image-'.$field['name'].'"><img src="/image.php?i=/'.$version_2.'" /></span>';
				} else {
					$output .= '<span class="thumbnail image-'.$field['name'].'">Error: image file appears to be missing!</span>';
				}
				
			} else {
				$output .= '<span class="thumbnail image-'.$field['name'].'">No image has been uploaded yet.</span>';
		#		$output .= '<span class="image-'.$field['name'].' noimage"><img src="/images/admin/trans.png" style="border:1px solid red;" /></span>';
			}
			
			
		} elseif ($field['formtype'] == 'file') {
			$output  = '<input type="file" name="'.$field['name'].'" id="in_'.$field['name'].'" class="'.$css_class.' '.$required.$placeholder.' />';
			
			if ($field_value != '') {
				$output .= '<input type="hidden" name="'.$field['name'].'_check" id="in_'.$field['name'].'_check" value="'.$field_value.'" />';
			}
			
			if (self::$values[$field['name']] != '') {
			#	$output .= '<span> '.strtoupper(substr(self::$values[$field['name']], -3)).': <a href="'.self::$values[$field['name']].'" target="_blank">'.self::$values[$field['name']].'</a></span>';
			
			
			#	http://tdmc.ansta.co.uk/download_file.php?	r=user_types	&f=tnc		&gb=1	&i=pdf11_30k.pdf = bad
			#	http://tdmc.ansta.co.uk/download_file.php?	r=users			&f=clinical	&gb=1	&i=pdf11_30k.pdf = good
					#	die($field['root']);
				$file_path = '/download_file.php?r='.$field['root'].'&f='.$field['name'].'&gb='.self::$values[$field['groupby']].'&i='.$field_value;
				$output .= '<span> '.strtoupper(substr(self::$values[$field['name']], -3)).': <a href="'.$file_path.'">'.$field_value.'</a></span>';
			#	$output .= '<span>'.strtoupper(substr(self::$values[$field['name']], -3)).': <a href="/download_file.php?m='.Module::$name.'&f='.urlencode(self::$values[$field['name']]).'">'.self::$values[$field['name']].'</a></span>';
			} else {
				$output .= '<span> no file uploaded yet</span>';
			}
			
			
		} elseif ($field['formtype'] == 'select' || $field['formtype'] == 'multi') {
			
			if($field['options']) {
				foreach($field['options'] AS $key => $value) {
					self::setOption($field['name'],$key,$value);
				}
				
			} elseif(is_array(Config::$settings[$field['settings']][$field['name']])) {
				foreach(Config::$settings[$field['settings']][$field['name']] AS $key => $value) {
					self::setOption($field['name'],$key,$value);
				}
				
			} elseif(is_array(Config::$settings[Module::$name][$field['name']])) {
				foreach(Config::$settings[Module::$name][$field['name']] AS $key => $value) {
					self::setOption($field['name'],$key,$value);
				}
				
			} else {
				if($field['source']) {
					$query = "
						SELECT
							`id`
						,	`title`
						FROM
							`".$field['source']."`
						ORDER BY
							".$field['orderby']."
					";
					$nav = db::link()->query($query);
					if ($nav->num_rows) {
						while( $item = $nav->fetch_object() ) {
							self::setOption($field['name'],$item->id,$item->title);
						}
					} else {
						self::setOption($field['name'],'0','ERROR! Unable to fetch data from '.$field['source']);
					}
				}
			}
			if ($field['formtype'] == 'multi') {
				$output = '<select name="'.$field['name'].'[]" id="in_'.$field['name'].'" multiple="multiple"  size="'.$field['size'].'" class="'.$css_class.'>'.self::getOptions($field['name'],$field['formtype']).'</select>';
			} else {
				$output = '<select name="'.$field['name'].'" id="in_'.$field['name'].'">'.self::getOptions($field['name'],$field['formtype']).'</select>';
			}
			
			
		} elseif ($field['formtype'] == 'textarea') {
			if ($field['editor'] == true) {
				$output = '<textarea name="'.$field['name'].'" id="in_'.$field['name'].'" class="editor '.$css_class.'">'.$field_value.'</textarea>';
			} else {
				$output = '<textarea name="'.$field['name'].'" id="in_'.$field['name'].'" class="'.$css_class.'">'.$field_value.'</textarea>';
			}
			
	#	} elseif ($field['formtype'] == 'date') {
	#		$date_array = explode('-',self::$values[$field['name']]);
	#		$output  = '<input type="text" name="'.$field['name'].'" id="in_'.$field['name'].'_d" value="'.$date_array[2].'"'.$autocomplete.' />';
	#		$output .= '<input type="text" name="'.$field['name'].'" id="in_'.$field['name'].'_m" value="'.$date_array[1].'"'.$autocomplete.' />';
	#		$output .= '<input type="text" name="'.$field['name'].'" id="in_'.$field['name'].'_y" value="'.$date_array[0].'"'.$autocomplete.' />';
	#		$output .= '<input type="hidden" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.self::$values[$field['name']].'" />';
			
			
		} elseif ($field['formtype'] == 'submit') {
			if ($field['image'] != '') {
				$output = '<input type="image" src="'.$field['image'].'" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.$field['value'].'" />';
			} else {
				$output = '<input type="submit" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.$field['value'].'" />';
			}
			
			
		} elseif ($field['formtype'] == 'hidden') {
			if (isset($field['value']) && $field['value'] != '') {
				$value = $field['value'];
			} else {
				$value = self::$values[$field['name']];
			}
			$output = '<input type="hidden" name="'.$field['name'].'" id="in_'.$field['name'].'" value="'.$value.'" />';
			
			
		} elseif ($field['formtype'] == 'checkbox') {
	#		$output = '<input type="checkbox" name="'.$field['name'].'" id="in_'.$field['name'].'" value="1" />';
			$output = 'checkbox support is not complete yet, please use a select until it is.';
			
			
		} elseif ($field['formtype'] == 'yesno') {
			if ($field['default'] == true && Page::$slug[3] == 'new') {
				$field_value = 1;
			}
			if ($field_value == 0) {
				$iszero = ' checked="checked"';
			} elseif ($field_value == 1) {
				$isone = ' checked="checked"';
			} else {
				# nothing
			}
			$output  = '<label for="in_'.$field['name'].'_yes" class="sublabel yesno-yes">Yes</label>';
			$output .= '<input type="radio" name="'.$field['name'].'" id="in_'.$field['name'].'_yes" value="1"'.$isone.' class="subfield" />';
			$output .= '&nbsp;&nbsp;&nbsp;';
			$output .= '<input type="radio" name="'.$field['name'].'" id="in_'.$field['name'].'_no" value="0"'.$iszero.' class="subfield" />';
			$output .= '<label for="in_'.$field['name'].'_no" class="sublabel yesno-no">No</label>';
			if($field['formtype'] == 'yesno' || $field['formtype'] == 'radio' || $field['formtype'] == 'checkbox') {
				$output .= '<span class="hint hint-'.$field['name'].'">'.$field['hint'].'</span>';
			} else {
				# Don't display hint here
			}
			
		} elseif ($field['formtype'] == 'yes1no2') {
			if ($field['default'] == true && Page::$slug[3] == 'new') {
				$field_value = 1;
			}
			if ($field_value == 2) {
				$iszero = ' checked="checked"';
			} elseif ($field_value == 1) {
				$isone = ' checked="checked"';
			} else {
				# nothing
			}
			$output  = '<label for="in_'.$field['name'].'_yes" class="sublabel yesno-yes">Yes</label>';
			$output .= '<input type="radio" name="'.$field['name'].'" id="in_'.$field['name'].'_yes" value="1"'.$isone.' class="subfield" />';
			$output .= '&nbsp;&nbsp;&nbsp;';
			$output .= '<input type="radio" name="'.$field['name'].'" id="in_'.$field['name'].'_no" value="2"'.$iszero.' class="subfield" />';
			$output .= '<label for="in_'.$field['name'].'_no" class="sublabel yesno-no">No</label>';
			if($field['formtype'] == 'yes1no2' || $field['formtype'] == 'radio' || $field['formtype'] == 'checkbox') {
				$output .= '<span class="hint hint-'.$field['name'].'">'.$field['hint'].'</span>';
			} else {
				# Don't display hint here
			}
			
		} elseif ($field['formtype'] == 'readonly') {
			$output = '<input type="text" name="readonly_'.$field['name'].'" id="in_'.$field['name'].'" value="'.$field_value.'" readonly="readonly" />';
			
			
		} else {
			$output = '<p>Field type "'.$field['formtype'].'" is invalid!</p>';
			
			
		}
		
		if ($field['formtype'] == 'hidden') {
			return "\t".$output.PHP_EOL;
		} else {
			return "\t".'<div class="input">'.$output.'</div>'.PHP_EOL;
		}
	}
	
	/**
	 * This is called just before a form that needs to be populated with existing data
	 * 
	 * Todo:
	 * - Secure the query by checking it against the config array of tables
	 */
	public static  function prefillValues($item_id=0,$table='') {
		
		// Todo: validate and make sure this is an INT otherwise throw an error
		if ($item_id > 0) {
			$id = $item_id;
		} else {
			$id = Page::$slug[4];
		}
		
		// Todo: validate that table exists in config otherwise throw an error
		if ($table != '') {
			$tbl = $table;
		} else {
			$tbl = Page::$slug[2];
		}
		
		$table_name = Security::checkTableList($tbl,__FILE__.' on line '.__LINE__);
		$version_control = Config::$settings[$table_name]['version_control'];
		
		if (isset($_REQUEST['action']) && isset($_REQUEST['module'])) {
			
			Security::checkActionList($_REQUEST['action']);
			Security::checkModuleList($_REQUEST['module'],__FILE__.' on line '.__LINE__);
			
			self::$values = $_REQUEST;
			return TRUE;
		} else {
			$query = "
				SELECT
					*
				FROM
					`".$table_name."`
				WHERE
			";
			if($version_control == 'audit' || $version_control == 'full') {
				$query .= "
					`id` = '".(int)db::link()->real_escape_string($id)."'
					AND `ttv_start` <= NOW()
					AND `ttv_end` IS NULL
				";
			} else {
				if (in_array('auto_id', Config::$fields[$table_name])) {
					$query .= "
						`auto_id` = '".(int)db::link()->real_escape_string($id)."'
					";
				} else {
					$query .= "
						`id` = '".(int)db::link()->real_escape_string($id)."'
					";
				}
			}
			$query .= " LIMIT 1";
	#		echo $query;
			$pagelookup = db::link()->query($query);
			if ($pagelookup->num_rows) {
				$result = $pagelookup->fetch_assoc();
				self::$values = $result;
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	/*
	* Setting the value of a field
	*/
	public static function setValue($fieldname,$value = '') {
		if ($value == '') {
			self::$values[$fieldname] = $_REQUEST[$fieldname];
		} else {
			self::$values[$fieldname] = $value;
		}
		
	}
	
	/*
	* 
	*/
	public static function setAuditAction($autit_action_text) {
		self::$audit_action = $autit_action_text;
	}
	
	
	/*
	* 
	*/
	public static function getAuditAction() {
		return self::$audit_action;
	}
	
	
	/*
	* Handle SELECT fields in forms
	*/
	public static function setOption($name,$key,$value) {
		self::$options[$name][$key] = $value;
	}
	public static function getOptions($name,$formtype = '') {
		$output = '';
		foreach(self::$options[$name] as $key => $value ) {
			if ($formtype == 'multi') {
				$arr = explode(',',self::$values[$name]);
				if (in_array($key,$arr)) {
					$selected = ' selected="selected"';
				} else {
					$selected = '';
				}
			} else {
				if (self::$values[$name] == $key) {
					$selected = ' selected="selected"';
				} else {
					$selected = '';
				}
			}
			$output .= '<option value="'.$key.'"'.$selected.'>'.$value.'</option>'.PHP_EOL;
		}
		return $output;
	}
	
	/*
	* Handle the required marker that gets applied to field labels
	*/
	public static function setRequiredMarker($value) {
		self::$required_marker = $value;
	}
	public static function getRequiredMarker($required) {
		if ($required) {
			return '<span>'.self::$required_marker.'</span>';
		} else {
			return '';	
		}
	}
	
	
	/*
	* Form field Validation
	*/
	public static function validate($field,$error_message = '') {
		
		// If no default error message is provided, use the system default one.
		if ($error_message == '') {
			$error_message = 'This field is mandatory.';
		}
		
		// Do advanced validation
		if ($field['regex'] != '') {
			
			if ($field['required'] == true  && $_REQUEST[$field['name']] == '') {
				$_SESSION['form']['field-error'][$field['name']] = 'This field is mandatory.';
				return FALSE;
				
			} elseif ($field['required'] == false && $_REQUEST[$field['name']] == '') {
				return TRUE;
				
			} elseif (($field['required'] == false || $field['required'] == true) && $_REQUEST[$field['name']] != '') {
				// If it has regex...
				if (preg_match($field['regex'], $_REQUEST[$field['name']])) {
					// even if regex matches, if required and field is an int then check if zero
					if ($field['format'] == 'i' && $_REQUEST[$field['name']] == 0) {
						$_SESSION['form']['field-error'][$field['name']] = 'This field is mandatory and cannot be set to 0.';
						return FALSE;
					} else {
						return TRUE;
					}
				} else {
					$_SESSION['form']['field-error'][$field['name']] = 'Data does not appear to be valid as it doesn\'t match our validations criteria.';
					return FALSE;
				}
				
			}
		
		// Do Image and File validation
		} elseif ($field['formtype'] == 'img' || $field['formtype'] == 'file') {
			// If required...
			if ($field['required']) {
				// If required, check if there is already data saved, because...
				if ($_REQUEST['action'] == 'edit') {
					$query = "SELECT `".$field['name']."` FROM `".$field['root']."` WHERE `id` = ".db::link()->real_escape_string($_REQUEST['id'])." AND `ttv_end` IS NULL";
					$fieldlookup = db::link()->query($query);
					if ($fieldlookup->num_rows) {
						$fielddata = $fieldlookup->fetch_assoc();
						// ...if there is then there is no need to force an update.
						if ($fielddata[$field['name']] != '' && $_FILES[$field['name']]['size'] <= 10) {
							return TRUE;
						} elseif ($fielddata[$field['name']] != '' && $_FILES[$field['name']]['size'] > 10) {
							// ...make sure that the extension is valid
							$this_file_type = substr(strrchr($_FILES[$field['name']]['type'], '/'), 1);
					#		die($_FILES[$field['name']]['type']);
							$permitted_type_array = explode(',',$field['filetype']);
							if (!in_array($this_file_type,$permitted_type_array)) {
								$_SESSION['form']['field-error'][$field['name']] = 'File type not permitted.';
								return FALSE;
							} else {
								if ($_FILES[$field['name']]['size'] > $field['maxsize'] && $field['maxsize'] > 0) {
									$_SESSION['form']['field-error'][$field['name']] = 'File size too large.';
									return FALSE;
								}
								return TRUE;
							}
						}
						// If however there is no data stored, then we need to carry on validating...
					}
				}
				
				// ...make sure the size is worthy of upload
				if ($_FILES[$field['name']]['size'] <= 10) {
					$_SESSION['form']['field-error'][$field['name']] = 'Error uploading the chosen file.';
					return FALSE;
				} else {
					// ...make sure that the extension is valid
					$this_file_type = substr(strrchr($_FILES[$field['name']]['type'], '/'), 1);
					$permitted_type_array = explode(',',$field['filetype']);
					if (!in_array($this_file_type,$permitted_type_array)) {
						$_SESSION['form']['field-error'][$field['name']] = 'File type not permitted.';
						return FALSE;
					} else {
						if ($_FILES[$field['name']]['size'] > $field['maxsize'] && $field['maxsize'] > 0) {
							$_SESSION['form']['field-error'][$field['name']] = 'File size too large.';
							return FALSE;
						}
						return TRUE;
					}
				}
				
			} else {
				// ...make sure the size is worthy of upload
				if ($_FILES[$field['name']]['size'] > 10) {
					// ...make sure that the extension is valid
					$this_file_type = substr(strrchr($_FILES[$field['name']]['type'], '/'), 1);
					$permitted_type_array = explode(',',$field['filetype']);
					if (!in_array($this_file_type,$permitted_type_array)) {
						$_SESSION['form']['field-error'][$field['name']] = 'File type not permitted.';
						return FALSE;
					} else {
						if ($_FILES[$field['name']]['size'] > $field['maxsize'] && $field['maxsize'] > 0) {
							$_SESSION['form']['field-error'][$field['name']] = 'File size too large.';
							return FALSE;
						}
						return TRUE;
					}
				} else {
					return TRUE;
				}
				
			}
			
		// Do basic validation
		} else {
			
			if ($field['required'] && ($field['formtype'] == 'file' || $field['formtype'] == 'img')) {
				if ($_FILES[$field['name']]['name'] == '') {
					# If no file is submitted but the hidden check field is populated then allow blank value
					if ($_REQUEST[$field['name'].'_check'] != '') {
						return TRUE;
					}
					$_SESSION['form']['field-error'][$field['name']] = $error_message;
					return FALSE;
				} else {
					return TRUE;
				}
				
			} elseif ($field['required'] && $_REQUEST[$field['name']] == '') {
				$_SESSION['form']['field-error'][$field['name']] = $error_message;
				return FALSE;
				
			} elseif ($field['required'] && $_REQUEST[$field['name']] != '') {
				// Check the value submitted matches the data type specified in the config
				if ($field['format'] == 'i' && $_REQUEST[$field['name']] == 0) {
					$_SESSION['form']['field-error'][$field['name']] = $error_message;
					return FALSE;
				} else {
					return TRUE;
				}
				
			} else {
				return TRUE;
				
			}
			
		}
		
	}
	
	
	
	
	
	
	/*
	* Get the details of a parent record
	* 
	* Todo: 
	* - Not sure if this function belongs in here
	* - Error properly
	* - validate and make sure this is an INT otherwise throw an error
	* - validate that table exists in config otherwise throw an error
	*/
	public static  function getParent($this_id,$table) {
		$pagelookup = db::link()->query("SELECT
											slug
										,	parent
										,	depth
										FROM
											`".$table."`
										WHERE
											`id` = ".db::link()->real_escape_string($this_id)."
										AND	ttv_end IS null
										");
		if ($pagelookup->num_rows) {
			$result = $pagelookup->fetch_assoc();
			$output = array(
							'slug'  =>$result['slug'],
							'parent'=>$result['parent'],
							'depth' =>$result['depth']
							);
			return $output;
		} else {
			return 'parent-of-'.$this_id.'-not-found';
		}
	}
	
	
	
	
	/*
	* This stuff needs sorting out
	* 
	* This all probably belongs in a separate class
	* Maybe set the validation in config so they are easy to edit or maybe
	* store them in a db table for easy admin via the back-end?
	* 
	*/
	private static  function validateEmail($value) {
		
		if (!preg_match('/^[a-z0-9.-_]+$/', $value)) {
			$output = array(
						'ok' => false, 
						'msg' => "Email does not appear to be valid"
						);
		} else {
			$output = array(
						'ok' => true, 
						'msg' => "Email appears to be valid"
						);
		}
		return $output;
	}
	public static function ajaxDataCheck($type,$value) {
		if ($type == 'email') {
			$output = Form::validateEmail($value);
		} elseif ($type == 'username') {
	#		$output = Form::validateUsername($value);
		} elseif ($type == 'password') {
	#		$output = Form::validatePassword($value);
		} else {
			return FALSE;
		}
		return json_encode($output);
	}



	
	/*
	* generates the URL for caching purposes as well as determine the depth value
	* 
	* Todo:
	* - Rename this to a more appropriate name
	* - Remove the depth limit (other than maybe hard code a depth limit that
	*   can easily be changed, maybe in config
	* - Separate out depth calculation from url generation or at lease make the
	*   function more efficient
	* - Ensure (and implement) the ability to use this function to auto-correct
	*   URLs for all child pages when a parent page changes URL
	*/
	public static function setUrlField($module_name = 'pages') {
		if ($_REQUEST['parent'] == 0) {
			$this_depth = 1;
			$this_parent_id = 0;
			$this_parent_slug = '';
		} else {
			$data = Form::getParent($_REQUEST['parent'],$module_name);
			$this_depth = $data['depth']+1;
			$this_parent_id = $data['parent'];
			$this_parent_slug = $data['slug'];
		}
		
		$_REQUEST['depth'] = $this_depth;
		$post['depth'] = $this_depth;
		$duplicate['depth'] = '`depth` = \''.$this_depth.'\'';
							
		if ($this_depth == 1) {
			$newvalue = '/'.$_REQUEST['slug'].'/';
			if ($newvalue == '//') {
				$newvalue = '/';
			}
		
		} elseif ($this_depth == 2) {
			$newvalue = '/'.$data['slug'].'/'.$_REQUEST['slug'].'/';
		
		} elseif ($this_depth == 3) {
			$data = Form::getParent($_REQUEST['parent'],$module_name);
			$data2 = Form::getParent($data['parent'],$module_name);
			$newvalue = '/'.$data2['slug'].'/'.$data['slug'].'/'.$_REQUEST['slug'].'/';
		
		} elseif ($this_depth == 4) {
			$data = Form::getParent($_REQUEST['parent'],$module_name);
			$data2 = Form::getParent($data['parent'],$module_name);
			$data3 = Form::getParent($data2['parent'],$module_name);
			$newvalue = '/'.$data3['slug'].'/'.$data2['slug'].'/'.$data['slug'].'/'.$_REQUEST['slug'].'/';
		
		} elseif ($this_depth == 5) {
			$data = Form::getParent($_REQUEST['parent'],$module_name);
			$data2 = Form::getParent($data['parent'],$module_name);
			$data3 = Form::getParent($data2['parent'],$module_name);
			$data4 = Form::getParent($data3['parent'],$module_name);
			$newvalue = '/'.$data4['slug'].'/'.$data3['slug'].'/'.$data2['slug'].'/'.$data['slug'].'/'.$_REQUEST['slug'].'/';
		
		} elseif ($this_depth == 6) {
			$data = Form::getParent($_REQUEST['parent'],$module_name);
			$data2 = Form::getParent($data['parent'],$module_name);
			$data3 = Form::getParent($data2['parent'],$module_name);
			$data4 = Form::getParent($data3['parent'],$module_name);
			$data5 = Form::getParent($data4['parent'],$module_name);
			$newvalue = '/'.$data5['slug'].'/'.$data4['slug'].'/'.$data3['slug'].'/'.$data2['slug'].'/'.$data['slug'].'/'.$_REQUEST['slug'].'/';
		
		}
		$post['url'] = $newvalue;
		$duplicate['url'] = '`url` = \''.db::link()->real_escape_string($newvalue).'\'';
		
		$output['new_depth'] = $this_depth;
		$output['new_url'] = $newvalue;
		
		
		return $output;
	}
	
	
	
	
	public static function getChildren($module_name,$id=0) {
		$version_control = Config::$settings[$module_name]['version_control'];
		$query = "
			SELECT
				*
			FROM
				`".$module_name."`
			WHERE
				parent = ".db::link()->real_escape_string($id)."
			AND	slug != ''
			AND	slug != '".ADMIN."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS NULL";
		}
		$query .= " ORDER BY `pos` ASC";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows) {
			while( $item = $items->fetch_object() ) {
				if ($item->depth == 1) {
					$prefix = '';
				} elseif ($item->depth == 2) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($item->depth == 3) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($item->depth == 4) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($item->depth == 5) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($item->depth == 6) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} else {
					$prefix = 'depth missing! - ';
				}
				Form::setOption('parent',$item->id,$prefix.$item->title);
				self::getChildren($module_name,$item->id);
			}
		}
	}
	
	

	public static function getItemTitle($module_name = '') {
		if ($module_name != '') {
			// This mthod doesn't work yet as it is incomplete
			$module = Security::checkModuleList($module_name);
		} else {
			$module = Security::checkModuleList(Page::$slug[2]);
		}
		$my_array = Config::$fields[$module];
		foreach ($my_array AS $k => $v) {
		#	echo '<br>'.$v['name'].' -> '.$v['istitle'];
			if ($v['istitle'] == 1) {
				$title .= ' '.Form::$values[$v['name']];
			}
		}
		if (trim($title) == '') {
			$title = 'n/a';
		}
		return $title;
	}
	
	
	public static function displayField($field,$item,$nodata='-') {
		
		if ($field['formtype'] == 'text' || $field['formtype'] == 'textarea') {
			$output = nl2br($item->$field['name']);
		
		} elseif ($field['formtype'] == 'select') {
			if ($field['source'] != '') {
				$the_table = Security::checkTableList($field['source']);
				$options_qry = "
					SELECT
						*
					FROM 
						".$the_table."
					WHERE
						`ttv_start` <= NOW()
					AND `ttv_end` IS NULL
					AND `id` = ".(int)db::link()->real_escape_string($item->$field['name'])."
				";
				$options = db::link()->query($options_qry);
				if ($options->num_rows) {
					while( $option = $options->fetch_object() ) {
						if (substr($option->title, 0, 2) != '--') {
							$output = $option->title;
						} else {
							$output = 'n/a';
						}
					}
				}
			} elseif ($field['settings'] != '') {
				$val = Config::$settings[$field['settings']][$field['name']][$item->$field['name']];
				if (substr($val, 0, 2) != '--') {
					$output = $val;
				} else {
					$output = 'n/a';
				}
			}
			
		} elseif ($field['formtype'] == 'yesno' && $item->$field['name'] != '') {
			if ($item->$field['name'] == 1) {
				$output = '<span class="yesno-yes">yes</span>';
			} elseif ($item->$field['name'] == 0) {
				$output = '<span class="yesno-no">no</span>';
			}
			
		} elseif ($field['formtype'] == 'yes1no2' && $item->$field['name'] != '') {
			if ($item->$field['name'] == 0) {
				$output = '<span class="yesno-na">n/a</span>';
			} elseif ($item->$field['name'] == 1) {
				$output = '<span class="yesno-yes">yes</span>';
			} elseif ($item->$field['name'] == 2) {
				$output = '<span class="yesno-no">no</span>';
			}
			
		} elseif ($field['formtype'] == 'img' && $item->$field['name'] != '') {
			$output = '<span class="image-'.$field['name'].'"><img src="/display_image.php?r='.$field['root'].'&f='.$field['name'].'&gb='.$item->$field['groupby'].'&i='.$item->$field['name'].'&s=thumb" /></span>';
			
		} elseif ($field['formtype'] == 'file' && $item->$field['name'] != '') {
			$file_path = '/download_file.php?r='.$field['root'].'&f='.$field['name'].'&gb='.$item->$field['groupby'].'&i='.$item->$field['name'];
		#	$file_type = getFileType($item->$field['name']);
		#	substr($item->$field['name'], -3)
		#	strtoupper($file_type).': ';
			$output = '<span>'.$filetype.'<a href="'.$file_path.'">'.$item->$field['name'].'</a></span>';
			
		}
		if (trim($output) == '') {
			$output = $nodata;
		}
		return $output;
	}
	
	
	public static function makeHumanDate($item) {
		
		if ($item != '') {
			$datetimepop = explode(' ',$item);
			$datepop = explode('-',$datetimepop[0]);
			$timepop = explode(':',$datetimepop[1]);
			if (strlen($datepop[0]) == 4) {
				$date_y = (int)$datepop[0];
				$date_m = (int)$datepop[1];
				$date_d = (int)$datepop[2];
			} elseif (strlen($datepop[2]) == 4) {
				$date_y = (int)$datepop[2];
				$date_m = (int)$datepop[1];
				$date_d = (int)$datepop[0];
			}
			return $date_d.'/'.$date_m.'/'.$date_y.' at '.$timepop[0].':'.$timepop[1];
		#	$cur_d = (int)date('d');
		#	$cur_m = (int)date('m');
		#	$cur_y = (int)date('Y');
		#	if ($dob_m <= $cur_m) {
		#		if ($dob_d <= $cur_d) {
		#			return ($cur_y-$dob_y);
		#		} else {
		#			return ($cur_y-$dob_y-1);
		#		}
		#	} else {
		#		return ($cur_y-$dob_y-1);
		#	}
		}
		return 'n/a';
		
		return $item;
	}
	
}