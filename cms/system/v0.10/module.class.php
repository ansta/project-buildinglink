<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Everything relating to the Module aspect of the system is here
* 
* Todo:
* - This could do with a tidy up
*/
class Module {

	/*
	* Some defaults
	*/
	public static 	$name = 'default',
					$id = -1,
					$page = '',
					$root = '',
					$offset = 0,
					$depth = -1,
					$slug = array(),
					$item_id = 0,
					$view_item_object;	//	This holds the data from when the controller check if an item exists
	
	/*
	* The controller is the first file that gets called in ANY module.
	* The purpose of this file is to allow you to perform re-routing or
	* lookups before the HEADERS are sent. So in short you can even do redirects here.
	* One very important role for this file is to perform routing and URL validation 
	* when serving content that is external to the page tree. One example for this could
	* be a NEWS module where you could have a news listing page consisting of
	* a thumbnail and a brief teaser text and upon clicking through visitors are taken
	* to the full article. The Full article page will almost certainly not be part of the
	* page tree so this will need to be enabled and validated in order to only serve
	* the content when the URL pattern is correct otherwise 404. This will typically be
	* done in the controller file of the module.
	*/
	public static function controller() {
		
		$module_name = Security::checkModuleList(self::$name,__FILE__.' on line '.__LINE__);
		
		if (Page::$slug[1] == ADMIN) {
			$interface = 'back_end';
		} else {
			$interface = 'front_end';
		}
		
		// Load the controller:
		// Look for a module specific one if it exists
		if (file_exists(SYSTEM_ROOT.'modules/'.$module_name.'/'.$interface.'/controller.php')) {
			require_once(SYSTEM_ROOT.'modules/'.$module_name.'/'.$interface.'/controller.php');
			
		// Otherwise load the default one (only applies to back_end as front_end modules must always have a controller file)
		} elseif (file_exists(SYSTEM_ROOT.'modules/default/'.$interface.'/controller.php') && Page::$slug[1] == ADMIN) {
			require_once(SYSTEM_ROOT.'modules/default/'.$interface.'/controller.php');
			
		} else {
			if ($module_name == '') {
			#	Error::type(500);
				echo 'Backend CONTROLLER file not found in unspecified module"!<br>';
			} else {
			#	Error::type(500);
				echo 'Backend CONTROLLER file not found in module '.$interface.'/'.$module_name.'!<br>';
			}	
		}
	}
	
	/*
	* Sets up all the data that relates directly to the module in use
	* 
	* Given this example URL: http://www.domain.com/aaa/bbb/xxx/yyy/zzz/?a=1&b=2
	* - Module::$slug[0] = the full URL treating the module page as ROOT eg: /xxx/yyy/zzz/
	* - Module::$slug[1] = is the first slug of the URL (without slashes) eg: xxx
	* - Module::$slug[2] = is the second slug of the URL (without slashes) eg: yyy
	* - Module::$slug[3] = is the third slug of the URL (without slashes) eg: zzz
	* - Module::$slug[4] = is the forth slug of the URL (without slashes). Slug 4 is empty as there are only 3 slugs.
	* 
	* In addition to the above, additional data is made available:
	* - Module::$name = Name of Module eg: bbb
	* - Module::$id = ID of the module page
	* - Module::$page = The slug of the module page eg: bbb
	* - Module::$offset = Depth of the module page eg: 2
	* - Module::$depth = Depth below the module page eg: 3
	* 			
	*/
	public static function getMeta() {
		// Build Module::$slug , the URL array specific to the module which will most likely be
		// different (or rather: offset) to what is currently in the Page::$slug array
		$moduleslug = Page::$slug;
		for( $k=0; $k <= self::$offset; $k++ ) {
			array_shift($moduleslug);
		}
		self::$slug = $moduleslug;
		foreach( self::$slug as $key => $value ) {
			if ($key > 0) {
				$moduleurl = $moduleurl.'/'.$value;
			}
		}
		self::$slug[0] = $moduleurl.'/';
		self::$root = substr(Page::$slug[0], 0, -strlen($moduleurl.'/')).'/';
	}
	/*
	* This simply returns a number relating to how many slugs are after the module ROOT
	* In the above example this would be 3
	* 
	* This allows us to check that the URL is at least correctly formed and helps
	* prevent "runaway URLs" as well as many other things.
	*/
	public static function moduleArgs() {
		return self::$depth;
	}/*
	public static function setModule() {
		return self::$depth;
	}*/
	
	public static function getList($module_name,$limit=0,$order_by='',$where_clause='1=1') {
		
		$query = "
			SELECT
				*
			FROM
				".$module_name."
			WHERE
				".$where_clause."
			AND	ttv_end IS null
		";
		if ($order_by != '') {
			$query .= " ORDER BY ".$order_by;
		}
		if ($limit > 0) {
			$query .= " LIMIT ".$limit;
		}
		
		$results = db::link()->query($query);
		if($results->num_rows>0) {
			return $results;
		}
		
		return NULL;
	}
	
	
	/*
	*
	*/
	public static function checkItemExists($module_name,$item_slug) {
		$query = "
			SELECT
				*
			FROM
				".$module_name."
			WHERE
				slug = '".$item_slug."'
			AND	ttv_end IS null
			LIMIT 1
		";
		$results = db::link()->query($query);
		if($results->num_rows > 0) {
			if ($result = $results->fetch_object()) {
				self::$view_item_object = $item;
				self::$item_id = $item->id;
				Meta::setTag('title',$item->title);
				Meta::setTag('description',$item->metadesc);
				Meta::setTag('keywords',$item->metakeys);
				Meta::setTag('author',$item->create_by);
				return TRUE;
			}
		}
		return FALSE;
	}
	
}
