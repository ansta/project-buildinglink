<?php
#defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Currently only catering for administrators, this will ultimately be relevant to
* website members also in the near future.
*
*/
class Paging {

	/*
	*
	*
	*/
	public static 	$limit = '',
					$items_per_page = 10,
					$first_item = 0,
					$last_item = 0;
	
	/*
	* 
	*/
	public static function setQuery($set_per_page = 0) {
		if ($set_per_page > 0) {
			self::$items_per_page = $set_per_page;
		}
		
		if ($_REQUEST['p'] > 0) {
			$curent_page = $_REQUEST['p'];
		} else {
			$curent_page = 1;
		}
		
		$record_start = ($curent_page*self::$items_per_page)-self::$items_per_page;
		$record_end = ($curent_page*self::$items_per_page);
		
		self::$limit	 = " LIMIT ".$record_start.",".self::$items_per_page;
		self::$first_item= $record_start+1;
		self::$last_item = $record_end;
		return;
	}
	
	
	/*
	* Display Paging Nav links for next and previous
	*/
	public static function getNav($items_count) {
		// Fetch any existing URL query string and add it back into the paging links
		foreach ($_GET as $key => $val) {
			if ($key != 'p') {
				$paging_extra_query .= '&'.$key.'='.$val;
			}
		}
		// Check to see what page we're on and set current_page
		if ($_REQUEST['p'] > 0) {
			$curent_page = $_REQUEST['p'];
		} else {
			$curent_page = 1;
		}
		// Start building the nav list
		$output = '<div class="paging">';
		if ($curent_page > 1) {
			$output .= '&laquo<a href="?p='.($curent_page-1).$paging_extra_query.'">Previous Page</a>';
		} else {
			$output .= '<span class="disabled">&laquoPrevious Page</span>';
		}
		$output .= ' | ';
		if ($items_count == self::$items_per_page) {
			$output .= '<a href="?p='.($curent_page+1).$paging_extra_query.'">Next Page</a>&raquo';
		} else {
			$output .= '<span class="disabled">Next Page&raquo</span>';
		}
		// Make a summary phrase to position the current page relative to the others
		// Might be worth getting the total count and tagging it on the end?
		if ($items_count < self::$last_item && $curent_page == 1) {
			$total_items_on_this_page = $items_count;
		} elseif ($items_count < self::$last_item && $curent_page > 1) {
			$total_items_on_this_page = self::$first_item + $items_count - 1;
		} else {
			$total_items_on_this_page = self::$last_item;
		}
		$output .= ' <span class="paginginfo">Showing items '.self::$first_item.' to '.$total_items_on_this_page.'</span>';
		$output .= '</div>';
		return $output;
	}
	
}







