<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Provides database connectivity
*
* Todo:
* - This class is rubbish, it connects ok but it needs a lot of work!
*   Having looked at how Kohana does it, I might go down a similar path
*   to achieve a sensible method of connecting to a DB
*   See: http://kohanaframework.org/
*/
class db {
	
	private static $mysql;
	public  static $status = array();
	
    // A private constructor; prevents direct creation of object
    private function __construct() {}
	
	public static function link($key = 'default') {
		if (!isset(self::$mysql[$key])) {
			self::$mysql[$key] = new mysqli
										(
											DB_HOST,
											DB_USER,
											DB_PASSWORD,
											DB_NAME
										)
										or
										die
										(
											$db->error
										);
		}
		return self::$mysql[$key];
	}
	
	public static function checkDb() {
		$link = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD) or die('Cannot connect to DB Server');
		if (!$link) {
			return FALSE;
		}
		// make foo the current db
		$db_selected = mysql_select_db(DB_NAME, $link) or die('Cannot connect to DB on this Server');
		if (!$db_selected) {
			return FALSE;
		}
		
		return TRUE;
	}

	
    // Prevent users to clone the instance
    private function __clone() { trigger_error('Clone is not allowed.', E_USER_ERROR); }
	
	private function __destruct() { @session_write_close(); /* database driven session closing before db destructed */ }
	
}

?>