<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");
/*
* Currently only catering for administrators, this will ultimately be relevant to
* website members also in the near future.
* 
* The plan is to replace user login with OpenID login instead or in addition
*
*/
class User {

	/*
	*
	*
	*/
	private static 	$user = array();
	public static 	$hasSuperUsers = FALSE;
	
	
	/*
	* Check if a user is logged in 
	*/
	public static function isLoggedIn() {
		if(self::checkUser()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	/*
	* Check if a user is logged in as an Administrator
	*/
	public static function isAdmin() {
		if(self::checkUser() && $_SESSION['type'] == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	/*
	* Fetch the file containing the list of all super users permitted to access this system
	*/
	public static function importSuperUsers() {
		if (file_exists(		PUBLIC_ROOT.'../../global_admins.php')) {
			require_once 		PUBLIC_ROOT.'../../global_admins.php';
			self::$hasSuperUsers = TRUE;
			
		} elseif (file_exists(	PUBLIC_ROOT.'../../../global_admins.php')) {
			require_once 		PUBLIC_ROOT.'../../../global_admins.php';
			self::$hasSuperUsers = TRUE;
			
		} elseif (file_exists(	PUBLIC_ROOT.'../../../../global_admins.php')) {
			require_once 		PUBLIC_ROOT.'../../../../global_admins.php';
			self::$hasSuperUsers = TRUE;
			
		} elseif (file_exists(	PUBLIC_ROOT.'../../../../../global_admins.php')) {
			require_once 		PUBLIC_ROOT.'../../../../../global_admins.php';
			self::$hasSuperUsers = TRUE;
			
		} elseif (file_exists(	PUBLIC_ROOT.'../../../../../../global_admins.php')) {
			require_once 		PUBLIC_ROOT.'../../../../../../global_admins.php';
			self::$hasSuperUsers = TRUE;
			
		}
	}
	

	/*
	* Super users are able to login and access functionality that normal admins can't
	*/
	private static function getSuperUser() {
		if (self::$hasSuperUsers) {
			foreach (Config::$global_admins AS $superuser) {
				if ($superuser['email'] == $_REQUEST[$_SESSION['loginfield_username']] && $superuser['password'] == $_REQUEST[$_SESSION['loginfield_password']]) {
					$user_hash = Security::encrypt(microtime().$superuser['email'],'md5');
					$_SESSION['user'] = $user_hash;
					$_SESSION['id'] = 1;
					$_SESSION['salutation'] = $user->salutation;
					$_SESSION['firstname'] = $superuser['firstname'];
					$_SESSION['lastname'] = $superuser['lastname'];
					$_SESSION['email'] = $superuser['email'];
					$_SESSION['type'] = 1;
					return TRUE;
				}
			}
		}
		return FALSE;
	}
	
	
	/*
	* Reurns the users IP address
	*/
	public static function getRealIpAddress() {
		//check ip from share internet
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip=$_SERVER['HTTP_CLIENT_IP'];
			
		//to check ip is pass from proxy
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			
		} else {
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	
	/*
	* Look up the requested user and if successfull log them in
	*/
	private static function checkUser() {
		# If we has User session data then we might be logged in, so start checking.
		if (isset($_SESSION['user']) && $_SESSION['user'] != '' && isset($_SESSION['id']) && $_SESSION['id'] != '') {
			# Look for login record
			$check_login = "
				SELECT
					`id`
				,	`user_id`
				,	`user_hash`
				,	`user_type`
				FROM
					`logins`
				WHERE
					`user_hash` = '".db::link()->real_escape_string($_SESSION['user'])."'
				AND `user_id` = ".(int)db::link()->real_escape_string($_SESSION['id'])."
				AND NOW() < `lastaction` + INTERVAL ".INACTIVITY_TIMEOUT." MINUTE
			";
			$checked = db::link()->query($check_login);
			if ($checked->num_rows) {
				$login = $checked->fetch_assoc();
				$update_login = "
					UPDATE
						`logins`
					SET
						`lastaction` = NOW()
					WHERE
						`id` = '".db::link()->real_escape_string($login['id'])."'
				";
				$updated = db::link()->query($update_login);
				
				# Return TRUE to indicate user is logged in
				return TRUE;
			} else {
				self::logOut();
			}
		}
		# If we are still here, something is not right so return FALSE to prevent user from
		# accessing anything restricted.
		return FALSE;
	}
	
	
	/*
	* 
	*/
	private static function isActive() {
		# If we has User session data then we might be logged in, so start checking.
		if (isset($_SESSION['user']) && $_SESSION['user'] != '' && isset($_SESSION['id']) && $_SESSION['id'] != '' && isset($_SESSION['user_type']) && $_SESSION['user_type'] != '') {
			# Look for login record
			$check_login = "
				SELECT
					`user_id`
				,	`user_hash`
				FROM
					`logins`
				WHERE
					`user_hash` = '".db::link()->real_escape_string($_SESSION['user'])."'
				AND `user_id` = ".(int)db::link()->real_escape_string($_SESSION['id'])."
				AND NOW() < `lastaction` + INTERVAL ".INACTIVITY_TIMEOUT." MINUTE
			";
			$checked = db::link()->query($check_login);
			if ($checked->num_rows) {
				# Return TRUE to indicate user is logged in
				return TRUE;
			}
		}
		# If we are still here, something is not right so return FALSE to prevent user from
		# accessing anything restricted.
		return FALSE;
	}
	
	
	/*
	* 
	*/
	public static function getUser($user_id = 0) {
		if ($user_id == 0) {
			self::logUserIn();
		} else {
			#Fetch user details from db with session info
			#while checking login table
			$query = "
				SELECT
					*
				FROM
					users AS u
				WHERE
					u.`ttv_start` <= NOW()
				AND u.`ttv_end` IS NULL
				AND u.`id` = ".db::link()->real_escape_string($user_id)."
			";
			$users = db::link()->query($query);
			if ($users->num_rows) {
				$user = $users->fetch_assoc();
				return $user;
			}
		}
		return FALSE;
	}
	
	
	/*
	* 
	*/
	private static function logUserIn() {
		
		// If the correct REQUEST parameters are provided, then start login process
		if ($_REQUEST[$_SESSION['loginfield_username']] != '' && $_REQUEST['password'] != '') {
			
			// Prepare the data for use here
			$password = Security::encrypt($_REQUEST[$_SESSION['loginfield_password']],'md5','uehdhwe8w9w89sd78udhdew89qw9sisdjsdjx7xdysgwb1vswyw8d8dhsbshs');
			$email = $_REQUEST[$_SESSION['loginfield_username']];
			
			// If all is fine so far, attempt to lookup the user with the credentials provided
			$query = "
				SELECT
					*
				FROM
					`users`
				WHERE
					`email` = '".db::link()->real_escape_string($email)."'
				AND	`password` = '".db::link()->real_escape_string($password)."'
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		#	die($query);
			$users = db::link()->query($query);
			
			// If a user is found, log them in!
			if ($users->num_rows) {
				// Fetch the users details and cache some of the data in the session
				// Typically this will be data that is used repeadily throughout the site.
				$user = $users->fetch_object();
								
				$user_hash = Security::encrypt($user->id,'md5',date('Y-m-d H:i:s'));
				$_SESSION['user'] = $user_hash;
				$_SESSION['id'] = $user->id;
				$_SESSION['salutation'] = $user->salutation;
				$_SESSION['firstname'] = $user->firstname;
				$_SESSION['lastname'] = $user->lastname;
				$_SESSION['email'] = $user->email;
				$_SESSION['type'] = $user->type;
				$finalise_login = TRUE;
			// If no user found then check to see if it matches a superuser
			}
			
			if (self::getSuperUser()) {
				$finalise_login = TRUE;
			}
			
			$user_ip = self::getRealIpAddress();
					
			if ($finalise_login) {
				// Log the fact that this user is now logged in
				// This record will be checked on every page load to ensure that the login
				// details in the session match what is stored in the database
				// This logins record will also serve to trigger a timeout if the user has been inactive.
				$login_qry = "
					INSERT INTO
						logins
						(
							`user_id`
						,	`user_name`
						,	`user_hash`
						,	`user_type`
						,	`user_ip`
						,	`firstaction`
						,	`lastaction`
						)
					VALUES
						(
							".(int)db::link()->real_escape_string($_SESSION['id'])."
						,	'".db::link()->real_escape_string($_SESSION['firstname'].' '.$_SESSION['lastname'])."'
						,	'".db::link()->real_escape_string($_SESSION['user'])."'
						,	'".db::link()->real_escape_string($_SESSION['type'])."'
						,	'".db::link()->real_escape_string($user_ip)."'
						,	NOW()
						,	NOW()
						)
				";
				db::link()->query($login_qry);
				
		#		die('hello2');
				
				// Wait for a short amount of time
				sleep(0.5);
				
				// Redirect
				header("Location: ".Page::$slug[0]);
			
			// If Login fails, the we need to log failed login attempt
			} else {
				// Wait for a short amount of time
				sleep(2);
				
			}
		}
	}
	
	
	/*
	* Encrypt a password string and return the plain and encrypted versions as an array
	*/
	public static function makePassword($password = '') {
		if ($password == '') {
			$auto_password = self::createRandomString(8);
			$output['plain'] = $auto_password;
			$output['hash'] = Security::encrypt($auto_password,'md5','uehdhwe8w9w89sd78udhdew89qw9sisdjsdjx7xdysgwb1vswyw8d8dhsbshs');
		} else {
			$output['plain'] = $password;
			$output['hash'] = Security::encrypt($password,'md5','uehdhwe8w9w89sd78udhdew89qw9sisdjsdjx7xdysgwb1vswyw8d8dhsbshs');
		}
		return $output;
	}
	
	
	/*
	* Random password generator: Creates a sting of length 8 by default but can be set to any length
	* String will be made up of numbers and upper and lower case letters.
	*/
	public static function createRandomString($length = 8) {
		if ($length > 0) { 
			$random_string = "";
			for($i = 1; $i <= $length; $i++) {
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1,82);
				$random_string .= self::getChar($num);
			}
		}
		return $random_string;
	}
	public static function getChar($num) {
		$rand_value = "dd";
		switch($num) {
			case "1"	:	$rand_value = "a";	break;
			case "2"	:	$rand_value = "b";	break;
			case "3"	:	$rand_value = "c";	break;
			case "4"	:	$rand_value = "d";	break;
			case "5"	:	$rand_value = "e";	break;
			case "6"	:	$rand_value = "f";	break;
			case "7"	:	$rand_value = "g";	break;
			case "8"	:	$rand_value = "h";	break;
			case "9"	:	$rand_value = "i";	break;
			case "10"	:	$rand_value = "j";	break;
			case "11"	:	$rand_value = "k";	break;
			case "12"	:	$rand_value = "a";	break;	// lower case letter L
			case "13"	:	$rand_value = "m";	break;
			case "14"	:	$rand_value = "n";	break;
			case "15"	:	$rand_value = "b";	break;	// lower case letter o
			case "16"	:	$rand_value = "p";	break;
			case "17"	:	$rand_value = "q";	break;
			case "18"	:	$rand_value = "r";	break;
			case "19"	:	$rand_value = "s";	break;
			case "20"	:	$rand_value = "t";	break;
			case "21"	:	$rand_value = "u";	break;
			case "22"	:	$rand_value = "v";	break;
			case "23"	:	$rand_value = "w";	break;
			case "24"	:	$rand_value = "x";	break;
			case "25"	:	$rand_value = "y";	break;
			case "26"	:	$rand_value = "z";	break;
			
			case "27"	:	$rand_value = "A";	break;
			case "28"	:	$rand_value = "B";	break;
			case "29"	:	$rand_value = "C";	break;
			case "30"	:	$rand_value = "D";	break;
			case "31"	:	$rand_value = "E";	break;
			case "32"	:	$rand_value = "F";	break;
			case "33"	:	$rand_value = "G";	break;
			case "34"	:	$rand_value = "H";	break;
			case "35"	:	$rand_value = "A";	break;	// upper case letter i
			case "36"	:	$rand_value = "J";	break;
			case "37"	:	$rand_value = "K";	break;
			case "38"	:	$rand_value = "L";	break;
			case "39"	:	$rand_value = "M";	break;
			case "40"	:	$rand_value = "N";	break;
			case "41"	:	$rand_value = "B";	break;	// upper case letter o
			case "42"	:	$rand_value = "P";	break;
			case "43"	:	$rand_value = "Q";	break;
			case "44"	:	$rand_value = "R";	break;
			case "45"	:	$rand_value = "S";	break;
			case "46"	:	$rand_value = "T";	break;
			case "47"	:	$rand_value = "U";	break;
			case "48"	:	$rand_value = "V";	break;
			case "49"	:	$rand_value = "W";	break;
			case "50"	:	$rand_value = "X";	break;
			case "51"	:	$rand_value = "Y";	break;
			case "52"	:	$rand_value = "Z";	break;
			
			case "53"	:	$rand_value = "7";	break;	// zero
			case "54"	:	$rand_value = "1";	break;
			case "55"	:	$rand_value = "2";	break;
			case "56"	:	$rand_value = "3";	break;
			case "57"	:	$rand_value = "4";	break;
			case "58"	:	$rand_value = "5";	break;
			case "59"	:	$rand_value = "6";	break;
			case "60"	:	$rand_value = "7";	break;
			case "61"	:	$rand_value = "8";	break;
			case "62"	:	$rand_value = "9";	break;
			
			case "63"	:	$rand_value = "5";	break;	// zero
			case "64"	:	$rand_value = "1";	break;
			case "65"	:	$rand_value = "2";	break;
			case "66"	:	$rand_value = "3";	break;
			case "67"	:	$rand_value = "4";	break;
			case "68"	:	$rand_value = "5";	break;
			case "69"	:	$rand_value = "6";	break;
			case "70"	:	$rand_value = "7";	break;
			case "71"	:	$rand_value = "8";	break;
			case "72"	:	$rand_value = "9";	break;
			
			case "73"	:	$rand_value = "9";	break;	// zero
			case "74"	:	$rand_value = "1";	break;
			case "75"	:	$rand_value = "2";	break;
			case "76"	:	$rand_value = "3";	break;
			case "77"	:	$rand_value = "4";	break;
			case "78"	:	$rand_value = "5";	break;
			case "79"	:	$rand_value = "6";	break;
			case "80"	:	$rand_value = "7";	break;
			case "81"	:	$rand_value = "8";	break;
			case "82"	:	$rand_value = "9";	break;
		}	
		return $rand_value;
	}
	
	
	/*
	* Clear out any significant session data upon logout
	*/
	public static function logOut() {
		foreach($_SESSION AS $session_key => $session_val) {
		#	echo '<br>'.$session_key.$session_val;
			unset($_SESSION[$session_key]);
		}
		@session_regenerate_id(TRUE);
		@header("Location: ".Page::$slug[0]);
		die();
	}
}




