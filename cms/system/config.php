<?php
defined('PIXXA_EXECUTE') or die("No direct script access allowed");

/**
 * Genero V3.0, build for Ansta Ltd and based on PixxaCMS
 *
 * An open source Content Management System for PHP 5.x.x or newer
 *
 * @package		PixxaCMS
 * @author		Loki Wijnen
 * @copyright	Copyright (c) 2012, PixxaCMS
 * @link		http://www.pixxacms.com/
 * @license		http://www.pixxacms.com/about/licence/
 * @since		Version 0.4
 * @filesource
 */

// -----------------------------------------------------------------------

/**
 * PixxaCMS Config Class
 *
 * This class is where all config values are stored
 *
 * @package		PixxaCMS
 * @subpackage	System
 * @category	Core
 * @author		Loki Wijnen
 * @link		http://www.pixxacms.com/documentation/classes/config/
 */


/*
* Specify the error reporting level that we wish to use
*/
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_WARNING);


/*
* SYSTEM_VERSION:
* Sets the folder name of the version that you want to use, so you can have multiple versions
* available and make switching between them easy for testing compatibility.
*/
define('SYSTEM_VERSION','v0.10');


/* DATABASE:
* MySQL database settings - part 1
* Sets the basic details about your database
* Access credentials will be set further down
*/
define('DB_TYPE',		'MYSQL');	// CURRENTLY NOT IN USE
define('DB_PORT',		'');		// CURRENTLY NOT IN USE
define('DB_CHARSET',	'utf8');	// CURRENTLY NOT IN USE
define('DB_COLLATE',	'');		// CURRENTLY NOT IN USE

define('OFFICE_IP',		'77.76.67.173');

/* TEST ENVIRONMENT SETTINGS */
if ($_SERVER['SERVER_ADDR'] == OFFICE_IP || $_SERVER['SERVER_ADDR'] == '192.168.80.2' || $_SERVER['SERVER_ADDR'] == '192.168.80.5') {
	/* Operating mode */
	define('DEBUG_MODE',	0); // 0=off, 1=discrete, 2=on-screen : Displays system info at the bottom of each page
	define('LOG_LEVEL',		0); // 0=off, 1=on : Puts stuff into the Error Log (unfortunately this has not been added to many sections yet)
	/* Email settings */
	define('EMAIL_FROM',	'buildinglink@ansta.co.uk'); // This is the email address that all emails are sent FROM
	define('EMAIL_TO',		'buildinglink@ansta.co.uk'); // Re-route all emails sent by the system to this address
	define('SERVER_NAME',	$_SERVER['SERVER_NAME']); // This is used whenever there is a need to create a full URL (such as emails)
	/* MySQL database settings */
	define('DB_HOST',		'localhost');
	define('DB_NAME',		'buildinglink');
	define('DB_USER',		'root');
	define('DB_PASSWORD',	'P@ssword');
	define('CLIENT_ADMIN',	false);			// Set to TRUE if this site was sold as a Static site, this will result in /admin/ resolving to 404 if IP is not White listed

	/* ReCAPTCHA keys */
	define('RECAPTCHA_PUBLIC','not-yet-created');	// Get a key from https://www.google.com/recaptcha/admin/create
	define('RECAPTCHA_PRIVATE','not-yet-created');
// RW test
//	define('RECAPTCHA_PUBLIC','6LcP6BcTAAAAAFad0mmGTM4P6RC9EWIg96mFDKU9');	// Get a key from https://www.google.com/recaptcha/admin/create
//	define('RECAPTCHA_PRIVATE','6LcP6BcTAAAAAL5I0lXPCCPG3OaYXx4XYp27ELIp');
// end RW test

	defined('SAGEPAY_MODE') or define('SAGEPAY_MODE', 'test');
	defined('SAGEPAY_CONFIG') or define('SAGEPAY_CONFIG', 'ansta');
	defined('SAGEPAY_URL') or define('SAGEPAY_URL', 'https://test.sagepay.com/gateway/service/vspform-register.vsp');

	/* Hide dev website from public */
#	if (USER_IP != OFFICE_IP) {
#		die('New website coming soon!');
#	}

/* LIVE ENVIRONMENT SETTINGS */
} else {
	/* Operating mode */
	if (USER_IP == OFFICE_IP) {
		define('DEBUG_MODE',	0); // 0=off, 1=discrete, 2=on-screen : Displays system info at the bottom of each page
	} else {
		define('DEBUG_MODE',	0); // 0=off, 1=discrete, 2=on-screen : Displays system info at the bottom of each page
	}
	define('LOG_LEVEL',		0); // 0=off, 1=on : Puts stuff into the Error Log (unfortunately this has not been added to many sections yet)
	/* Email settings */
	define('EMAIL_FROM',	'buildinglink@ansta.co.uk'); // This is the email address that all emails are sent FROM
	define('EMAIL_TO',		'buildinglink@ansta.co.uk'); // Not applicable to the live server
	define('SERVER_NAME', 	$_SERVER['SERVER_NAME']); // This is used when ever there is a need to create a full URL (such as emails)
	/* MySQL database settings */

	define('DB_HOST',		'10.181.129.178');
	define('DB_NAME',		'buildinglink');
	define('DB_USER',		'buildinglink');
	define('DB_PASSWORD',	'b6je892jerhfr738ie9');
	define('CLIENT_ADMIN',	true);			// Set to TRUE if this site was sold as a Static site, this will result in /admin/ resolving to 404 if IP is not White listed

	/* ReCAPTCHA keys */
	define('RECAPTCHA_PUBLIC','6LdlgyQUAAAAAB1yUkDUPn12O2XRYBd-RUfHbK5X');	// Get a key from https://www.google.com/recaptcha/admin/create
	define('RECAPTCHA_PRIVATE','6LdlgyQUAAAAALjzoDq0r1FJzBVfa-ZXAmCGIKDB');
	if (USER_IP == OFFICE_IP) {
		defined('SAGEPAY_MODE') or define('SAGEPAY_MODE', 'test');
		defined('SAGEPAY_CONFIG') or define('SAGEPAY_CONFIG', 'ansta');
		defined('SAGEPAY_URL') or define('SAGEPAY_URL', 'https://test.sagepay.com/gateway/service/vspform-register.vsp');
	} else {
		defined('SAGEPAY_MODE') or define('SAGEPAY_MODE', 'live');
		defined('SAGEPAY_CONFIG') or define('SAGEPAY_CONFIG', 'live');
		defined('SAGEPAY_URL') or define('SAGEPAY_URL', 'https://live.sagepay.com/gateway/service/vspform-register.vsp');
	}
}

/*
* PUBLIC_ROOT and SYSTEM_ROOT
* Sets the path to your public files and your system files. These settings will tell the system where
* to look for the files, so it is important to get them right.
*
* The prefered installation setup will have both a public and a private directory for storing your files
* The public one will be for all your files that can be accessed directly via a web browser whereas
* everything else should be located in your private direcory.
* In some hosting situations, you may not be able do this so in that case set both root items to the same value
* Add or remove the first slash depending on how your server is setup.
*/
define('PUBLIC_ROOT',	$_SERVER['DOCUMENT_ROOT'].'/');
define('SYSTEM_ROOT',	$_SERVER['DOCUMENT_ROOT'].'/../cms/');


// This is very important regarding products:
// If prices in the database are including VAT set to true
// If prices in the database are excluding VAT set to false
define('DB_PRICES_INCLUDE_VAT',	false);


/*
* ADMINDOCS_ROOT defines where the Admin help texts are retireved from by default.
* All information is customisable but by default this content will be displayed.
*/
define('ADMINDOCS_ROOT',$_SERVER['DOCUMENT_ROOT'].'/../../../anstacms-admin-helptext.ansta.co.uk/cms/');


/*
* If a user logs in and remains inactive for a long period of time,
* we recommend that you terminate their session and ask them to login again upon return.
*/
define('INACTIVITY_TIMEOUT',30);		// If a user performs no actions for this amount of time, they are automatically logged out
define('MAX_LOGIN_TIMEOUT',	240);		// Active or not, if a user has been logged in for this amount of time, they are automatically logged out


/*
* If a user tries to login and fails, they have a maximum number of times before they are locked out.
* CURRENTLY NOT IMPLEMENTED BY DEFAULT!!!
*/
define('MAX_LOGIN_ATTEMPS',	3);			// maximum number of times that a user can enter a wrong password before being blocked
define('MAX_LOGIN_ATTEMPS_TIME',30);	// How much time in the past must we check for failed attempts


/*
* If an individual uses a username that is not found and they repeatidly fail to login, their IP automatically gets blocked for a period of time.
* CURRENTLY NOT IMPLEMENTED BY DEFAULT!!!
*/
define('LOGIN_ABUSE_BY_IP',	100);		// Amount of times a login form can be used to attempt to login from a single IP
define('LOGIN_ABUSE_BY_IP_TIME',120);	// How much time in the past must we check for login attack attempts, time in minutes


/*
* Some customers require a password to be changed periodically
* CURRENTLY NOT IMPLEMENTED BY DEFAULT!!!
*/
define('TEMP_PASSWORD_LIFE',72);		// System generated passwords are valid for only 72 hours from the time of generation. In hours.
define('USER_PASSWORD_LIFE',2160);		// System generated passwords are valid for only 90 days from the time of generation. In hours.


/*
* Here are defined some "super modules" which are not defined in PAGES table
*/
define('ADMIN',	'admin');	// This is the top level page under which all back_end admin pages are found
define('USE_MEMBERS_AREA',	FALSE);	// This is the top level page under which all the members only area must be located
define('MEMBERS_ONLY_AREA',	'members');	// This is the top level page under which all the members only area must be located


/* MD5 salt to use when ecrypting a string */
/* Once live DO NOT CHANGE THIS or existing users will not be able to login!!! */
#define('MD5SALT', 'sjd7we9wenw8dgh3hd8d73');
define('MD5SALT', '83hrr6Â£b9n-G(nBtÂ£bv(M%b3jfg4b*ks');

