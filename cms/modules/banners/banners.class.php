<?php

class Banners {
	
	/*
	* Get banners for this particular WEBSITE ID
	*/
	public static function getList() {
		$version_control = Config::$settings['banners']['version_control'];
		$query = "
			SELECT
				`b`.`id`
			,	`b`.`title`
			,	`b`.`image`
			,	'".$module_name."' AS banner_type
			FROM
				banners b
			WHERE
				`b`.`id` > 0
			AND `b`.`site_id` = ".(int)db::link()->real_escape_string(Site::$sitedata['id'])."
			AND	`b`.`active` = 1
		";
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `b`.`ttv_end` IS null";
		}
		
		$query .= " ORDER BY RAND()";
		$query .= " LIMIT 5";
		
		$items = db::link()->query($query);
		
		if ($items->num_rows > 0) {
			// Some records are found, so return them
			return $items;
			
		}
		
		// Otherwise give up
		return NULL;
		
	}
	
}
