<?php
/* 
 * 
 */
self::$settings['banners'] =
array(
	'orderby'=>'pos ASC'
,	'singular'=>'Banner'
,	'plural'=>'Banners'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'filter_by_site'=>true
,	'image'=>array(
		'admin'		=>array('width'=>250,'height'=>88, 'format'=>'jpg','method'=>'crop', 'quality'=>60,	'hint'=>'This image is only used in the admin area of the website.')
	,	'gallery'	=>array('width'=>707,'height'=>250,'format'=>'jpg','method'=>'crop', 'quality'=>100,'hint'=>'This version of the image is used in the banner.')
	)
);

self::$fields['banners'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'ID',					'protected'=>true,	'hint'=>'')
,	array('name'=>'site_id',	'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Site ID',				'protected'=>true,	'hint'=>'')
,	array('name'=>'title',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Title',				'protected'=>false,	'istitle'=>true, 'hint'=>'This will appear as the <abbr title="Alternative text">ALT text</abbr> for the image',	'istitle'=>true)
,	array('name'=>'image',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',		'label'=>'Photo',				'protected'=>false,	'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 707(W) x 250(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'9097152', 'root'=>'banners', 'groupby'=>'id', 'manual_crop'=>true, 'cropratio'=>'gallery')
,	array('name'=>'active',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',		'label'=>'Banner is Active',	'protected'=>false,	'hint'=>'Allows creation of a banner without publishing it immediately by selecting <strong>No</strong>.', 'default'=>true)
,	array('name'=>'pos',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Position',			'protected'=>false,	'hint'=>'')
);

