<?php
/* 
 * 
 * 
 */

self::$settings['shippings'] =
array(
	'orderby'=>'title ASC'
,	'singular'=>'Shipping Details'
,	'plural'=>'Shipping Details'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
,	'admin_access'=>''
,	'max_items'=>''
,	'same_address' => array(
		0 => 'No'
	,	1 => 'Yes'
	)
,	'ship_country' => Countries::getSelect()
,	'bill_country' => Countries::getSelect()
,	'ship_usstate' => Us_states::getSelect()
,	'bill_usstate' => Us_states::getSelect()
);

self::$fields['shippings'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Basket Item ID',	'formset'=>'syst',	'protected'=>true,	'hint'=>'')
,	array('name'=>'order_id',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Order ID',		'formset'=>'syst',	'protected'=>true,	'hint'=>'')
,	array('name'=>'basket_hash',	'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Session ID',		'formset'=>'syst',	'protected'=>false,	'hint'=>'')

,	array('name'=>'ship_firstname',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'First Name',		'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_lastname',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Last Name',		'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_add1',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Delivery Address','formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_add2',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Line 2',			'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_add3',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Line 3',			'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_town',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Town',			'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_zip',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Zip / Postcode',	'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_county',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'County',			'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_country',	'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'select',	'format'=>'s',		'label'=>'Country',			'formset'=>'ship',	'protected'=>false,	'hint'=>'',	'settings'=>'shippings')
,	array('name'=>'ship_usstate',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'State (US only)',	'formset'=>'ship',	'protected'=>false,	'hint'=>'',	'settings'=>'shippings')
,	array('name'=>'ship_tel',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Telephone',		'formset'=>'ship',	'protected'=>false,	'hint'=>'')
,	array('name'=>'ship_mob',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'Mobile',			'formset'=>'ship',	'protected'=>false,	'hint'=>'')

,	array('name'=>'same_address',	'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'select',	'format'=>'i',		'label'=>'The Billing address is the same as the Delivery Address',	'formset'=>'check',	'protected'=>false,	'hint'=>'',	)

,	array('name'=>'bill_firstname',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'First Name',		'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_lastname',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Last Name',		'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_add1',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Billing Address',	'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_add2',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Line 2',			'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_add3',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Line 3',			'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_town',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Town',			'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_zip',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Zip / Postcode',	'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_county',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'County',			'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_country',	'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'select',	'format'=>'s',		'label'=>'Country',			'formset'=>'bill',	'protected'=>false,	'hint'=>'',	'settings'=>'shippings')
,	array('name'=>'bill_usstate',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'State (US only)',	'formset'=>'bill',	'protected'=>false,	'hint'=>'',	'settings'=>'shippings')
,	array('name'=>'bill_tel',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Telephone',		'formset'=>'bill',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_mob',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'Mobile',			'formset'=>'bill',	'protected'=>false,	'hint'=>'')

,	array('name'=>'email',			'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Email',			'formset'=>'email',	'protected'=>false,	'hint'=>'We need your email address so that we can send you a order confirmation email.')
);


/*

CREATE TABLE IF NOT EXISTS `shippings` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,

  `id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `basket_hash` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  
  `ship_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_add1` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_add2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_add3` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_town` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_zip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_county` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_country` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_tel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ship_mob` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  
  `same_address` varchar(255) COLLATE utf8_bin DEFAULT NULL,

  `bill_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_add1` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_add2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_add3` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_town` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_zip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_county` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_country` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_tel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bill_mob` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  
  `ttv_start` timestamp NULL DEFAULT NULL,
  `ttv_end` timestamp NULL DEFAULT NULL,
  `create_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `create_id` int(11) NOT NULL DEFAULT '0',
  `create_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `modify_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `modify_id` int(11) NOT NULL DEFAULT '0',
  `modify_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;

*/
