<?php

class Shippings {

	/*
	*
	*
	*/
	public static 	$view_item_object,
					$item_id = 0;
	
	/*
	* 
	*/
	public static function getItem($basket_hash = '') {
		if (trim($basket_hash) == '') {
			$basket_hash = $_SESSION['basket'];
		}
		$version_control = Config::$settings['shippings']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`shippings` a
		";
		$query .= "
			WHERE
				a.id > 0
			AND	a.basket_hash = '".db::link()->real_escape_string($basket_hash)."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
		$query .= "	LIMIT 1";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
		#	if ($item = $items->fetch_object()) {
				return $items;
		#	}
		}
		return FALSE;
	}
	
	
	/*
	* 
	*/
	public static function getIdFromBasket() {
		$version_control = Config::$settings['shippings']['version_control'];
		$query = "
			SELECT
				b.*
			FROM
				`shippings` b
            WHERE
				b.basket_hash = '".$_SESSION['basket']."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND b.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				b.`id` ASC
			LIMIT 1
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if ($item = $items->fetch_object()) {
				return $item->id;
			}
		}
		return 0;
	}
	
	
	/*
	* 
	*/
	public static function checkExists($basket_hash,$output_type) {
		$version_control = Config::$settings['shippings']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`shippings` a
		";
		$query .= "
			WHERE
				a.id > 0
			AND	a.basket_hash = '".db::link()->real_escape_string($basket_hash)."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
		$query .= "	LIMIT 1";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if ($output_type == 'bool') {
				return TRUE;
			} elseif ($output_type == 'object') {
				if ($item = $items->fetch_object()) {
					self::$view_item_object = $item;
					self::$item_id = $item->id;
					return $item;
				}
			}
		}
		return FALSE;
	}
	
	/*
	*
	*/
	public static function finaliseShipping($decoded) {
		// Update order record with decoded data and more
		$version_control = Config::$settings['shippings']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`shippings` a
            WHERE
				a.basket_hash = '".db::link()->real_escape_string($decoded['basket_hash'])."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				a.`id` DESC
			LIMIT 1
		";
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if ($item = $items->fetch_object()) {
				$shippings_update_array = array(
					'module'		=> 'shippings'
				,	'action'		=> 'update'
				,	'id'			=> $item->id
				,	'order_id'		=> $decoded['order_id']
				);
				$result = Form::processData('shippings',$shippings_update_array);
				if ($result['query_status'] == 'OK') {
					return $item;
				}
			}
		}
		return 0;
	}
	
}
