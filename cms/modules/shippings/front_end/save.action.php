<?php

$submission = TRUE;

foreach (Config::$fields['shippings'] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
	if ($field['name'] == 'email' && ($_REQUEST['email'] != $_REQUEST['email_confirm'])) {
#		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
#		$_SESSION['form']['field-error']["email_confirm"] = 'Please check that you have entered your email correctly in both Email address fields.';
#		$submission = Error::storeMsg($something_needs_to_go_here);
	}
	if ($field['name'] == 'bill_firstname' || $field['name'] == 'bill_lastname' || $field['name'] == 'email') {
		$_SESSION[$field['name']] = $_REQUEST[$field['name']];
	}
	
}


if (strtolower(Countries::getItem($_REQUEST['ship_country'],'code2')) == 'us' && $_REQUEST['ship_usstate'] < 1) {
	$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
	$_SESSION['form']['field-error']['ship_usstate'] = 'For addresses based in the US, please specify a state.';
	$submission = FALSE;
}
if (strtolower(Countries::getItem($_REQUEST['bill_country'],'code2')) == 'us' && $_REQUEST['bill_usstate'] < 1) {
	$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
	$_SESSION['form']['field-error']['bill_usstate'] = 'For addresses based in the US, please specify a state.';
	$submission = FALSE;
}

	
if ($submission) {
	
	$shipping = Shippings::checkExists($_SESSION['basket'],'object');
	if ($shipping->id > 0) {
		$_REQUEST['action'] = 'edit';
		$_REQUEST['id'] = $shipping->id;
		$success_message = 'Shipping details updated';
	} else {
		$_REQUEST['action'] = 'new';
		$_REQUEST['basket_hash'] = $_SESSION['basket'];
		$success_message = 'Shipping details submitted';
	}
	
	$result = Form::processData();
	
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = $success_message;

		if (isset($_REQUEST['back'])) {
			$redirect_to = '/basket/';
		} else {
			$redirect_to = '/order/confirm/';
		}
		header("Location: ".$redirect_to);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}


