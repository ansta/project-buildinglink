<?php

if (Module::$slug[0] == '/') {
	// Prefill the shipping details if any found for this basket
	Form::prefillValues(Shippings::getIdFromBasket(),'shippings');
	
} else {
	Error::type(404);
	
}