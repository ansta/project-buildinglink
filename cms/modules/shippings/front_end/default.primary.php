		<h1><?php echo Page::getTitle(); ?></h1>

		<?php echo Page::showErrors(); ?>
		
		<form action="" method="post">
			<fieldset style="float:left;width:99%">
				<legend>Contact email address</legend>
		<?php	Form::setErrorMessagePosition('inline');
				foreach (Config::$fields['shippings'] AS $field) {
					if ($field['formset'] == 'email') {
						echo Form::makeField($field);
	#					echo Form::makeField(array('name'=>'email_confirm','required'=>true,'formtype'=>'text','label'=>'Confirm your Email address','hint'=>'','value'=>Form::$values['email']));
					}
				}
		?>	</fieldset>
			<fieldset style="float:left;width:48%">
				<legend>Shipping Address</legend>
		<?php	Form::setErrorMessagePosition('inline');
				foreach (Config::$fields['shippings'] AS $field) {
					if ($field['formset'] == 'ship') {
						$field['class'] = 'ship';
						echo Form::makeField($field);
					}
				}
		?>	</fieldset>
			<fieldset style="float:right;width:48%">
				<legend>Billing Address</legend>
		<?php	Form::setErrorMessagePosition('inline');
				foreach (Config::$fields['shippings'] AS $field) {
					if ($field['formset'] == 'bill') {
						$field['class'] = 'bill';
						echo Form::makeField($field);
					}
				}
		?>	</fieldset>
		<?php	
				echo '<div class="field name-check type-checkbox error-false">';
					echo '<input type="checkbox" id="in_check" /> Check this box if billing address is the same shipping address';
				echo '</div>';
		?>
			<fieldset style="clear:both;  border:none; padding: 0;">
		<?php
				echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'save'));
				echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>'shippings'));
				echo Form::makeField(array('formtype'=>'submit','name'=>'back','value'=>'&laquo; Back'));
				echo Form::makeField(array('formtype'=>'submit','name'=>'next','value'=>'Next &raquo;'));
			#	echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Submit','image'=>'/images/btn-send.jpg'));
		?>	</fieldset>
		</form>
		