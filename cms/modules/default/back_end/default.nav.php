<?php if(User::isAdmin()) { ?>
<link rel="stylesheet" href="/css/cmsmenu.css" />
<div id="slideMenu">
	<img src="/images/admin/tab-menu.png" id="cmsMenuTab" class="tabMenu" title="CMS Menu" alt="CMS Menu" />
	<section>
	<?php echo Dashboard::nav(); ?>
	</section>
</div>
<?php } ?>