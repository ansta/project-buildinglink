<?php echo Nav::showChildModulesList(); ?>

<?php echo Nav::getTipsTitle(); ?>

<p>Here you can add as many photos as you like to each of your caravans.</p>

<p>REMEMBER to mark ONE photo as the master image, this one will be used as the cover image in the listing pages such as the homepage. Failing to add one will cause the advert to show as "coming soon".</p>

<p>To change the order in which the photos are listed in the Caravan gallery, simply drag and drop them to the desired location.</p>