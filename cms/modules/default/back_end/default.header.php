<header>
	<div class="container">
		<section>
			<h1><?php echo Site::getTitle(); ?><span>Admin Control Panel</span></h1>
		</section>
		<?php
			if(User::isAdmin()) {
				$sites = Sites::getList();
				if ($sites->num_rows > 1) {
					echo '<div class="siteswitcher"><form>';
						echo '<select name="siteswitcher">';
							while ($site = $sites->fetch_object()) {
								if (isset($_SESSION['active_site_id']) && $_SESSION['active_site_id'] == $site->id) {
									$site_selected = ' selected="selected"';
								} else {
									$site_selected = '';
								}
								echo '<option value="'.$site->id.'"'.$site_selected.'>'.$site->title.'</option>';
							}
						echo '</select>';
						echo '<input type="hidden" name="action" value="switch" />';
						echo '<input type="hidden" name="module" value="sites" />';
						echo '<input type="submit" name="submit" value="Go" />';
					echo '</form></div>';
				}
			}
		?>
		<aside><span>Ansta - Genero</span></aside>
	</div>
</header>
<nav>
	<?php if(User::isAdmin()) { ?>
	<a href="/admin/dashboard/"><img src="/images/admin/tab-dashboard.png" class="tabDashboard" /></a>
	<a href="/"><img src="/images/admin/tab-frontend.png" class="tabFrontend" /></a>
	<a href="#top"><img src="/images/admin/tab-top.png" class="tabTop" /></a>
	<a href="/admin/dashboard/?logout=true"><img src="/images/admin/tab-logout.png" class="tabLogout" /></a>
	<?php } ?>
</nav>