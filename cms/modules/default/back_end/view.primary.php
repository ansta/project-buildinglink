<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
View <?php echo Config::$settings[Page::$slug[2]]['singular']; ?>: <?php echo Form::getItemTitle(); ?></h1>
	
<section>
	<?php
	$item = Admin::getItem(Page::$slug[4]);
	foreach (Config::$fields[Module::$name] AS $field) {
		
		if ($field['formtype'] == 'hidden' || $field['formtype'] == 'protected') {
		} else {
			echo '<div class="field type-'.$field['formtype'].'">';
				echo '<span class="label">'.$field['label'].': </span>';
				if ($field['formtype'] == 'select') {
					echo Config::$settings[Module::$name][$field['name']][$item->$field['name']];
				} else {
					echo '<span class="value">'.$item->$field['name'].'</span>';
				}
			echo '</div>';
		}
	}
	echo '<div class="field"><span class="label">Date</span>: <span class="value">'.Tool::makeDate($item->ttv_start,'d/m/Y \a\t H:i').'</span></div>';
	?>
</section>