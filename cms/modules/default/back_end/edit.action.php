<?php

$submission = TRUE;

foreach (Config::$fields[Page::$slug[2]] as $key => $field) {
	if ($field['formtype'] == 'password' && $_REQUEST[$field['name']] == '' && Page::$slug[3] == 'edit') {
		# If field is of type password and value submitted is blank
		# then ignore this field as this means we don't want to over-write
		# the current value of the password field.
	} else {
		if (!Form::validate($field)) {
			$_SESSION['form']['form-error'] = 'Your form submission contains error, please check your data and try again.';
			$submission = Error::storeMsg($something_needs_to_go_here);
		}
	}
}

if ($submission) {
	$result = Form::processData();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'Item has been updated';
		header("Location: ".$result['destination']);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query_status'].' - '.$result['redirect'];
			echo $result['query'];
		}
	}
}
