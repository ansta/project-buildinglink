<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
Edit <?php echo Config::$settings[Page::$slug[2]]['singular']; ?>: <?php echo Form::getItemTitle(); ?></h1>

<section>
<?php
if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
	echo '<p class="errorbanner">'.$_SESSION['form']['form-error'].'</p>';
	unset($_SESSION['form']['form-error']);
}
if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
	echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
	unset($_SESSION['form']['form-success']);
}
?>
<form action="" method="post" enctype="multipart/form-data" autocomplete="off">
	<fieldset>
		<legend></legend>
		<?php
		foreach (Config::$fields[Module::$name] AS $field) {
			
			# Set the options for the PARENT drop down field
			# TODO: possibly need to find a better place for this
			if ($field['name'] == 'parent' && Module::$name == 'pages') {
				Form::setOption('parent',0,'ROOT');
				Pages::getChildren('pages');
			}
			
			# Loop through all the fields in the config and create the appropriate form field
			if ( ($field['formtype'] == 'hidden') || ($field['protected'] == true && $_SESSION['id'] > 1) ) {
				$field['formtype'] = 'hidden';
				# Ensure that hidden fields are tagged on to the end of the form
				$hidden_fields .= Form::makeField($field);
				
			} elseif ($field['formtype'] == 'attachment' && !$attachments_complete) {
				Attachments::getAttachments(Form::$values);
				$attachments_complete = TRUE;
			} else {
				 echo Form::makeField($field);
			}
		
		}
		echo $hidden_fields;
		if (!$attachments_complete) {
			Attachments::getAttachments(Form::$values);
		}
		?>
		<?php Form::setValue('next',$_SESSION['next']); ?>
		<?php echo Form::makeField(array('formtype'=>'yesno','name'=>'next','label'=>'Continue editing','hint'=>'After successful edit (No = return to listing page) (Yes: continue editing this item)')); ?>
		<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'edit')); ?>
		<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
		<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Update')); ?>
		<p class="btn-cancel"><a href="../../"><span>Cancel</span></a></p>
	</fieldset>
</form>
</section>