<?php
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$i = 0;
		// Loop through each variant and create a copy in the specified sizes
		foreach (Config::$settings[Page::$slug[2]][Page::$slug[5]] AS $type => $arr) {
			$i++;
			$targ_w = $arr['width'];
			$targ_h = $arr['height'];
			$jpeg_quality = (isset($arr['quality'])?$arr['quality']:80);
			$module_name = Page::$slug[2];
			$groupby = Page::$slug[4];
			$field_name = Page::$slug[5];
			$filename = $_POST['filename'];
			
			$src = $_SERVER['DOCUMENT_ROOT'].'/../cms/modules/'.$module_name.'/uploads/'.$field_name.'/'.$groupby.'/original/'.$filename;
			$img_r = imagecreatefromjpeg($src);
			$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
			
			imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
			$targ_w,$targ_h,$_POST['w'],$_POST['h']);
			
			$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/../cms/modules/'.$module_name.'/uploads/'.$field_name.'/'.$groupby.'/';
			
			imagejpeg($dst_r,$uploaddir.$type.'/'.$filename,$jpeg_quality);
			imagedestroy($dst_r);
		}
		$_SESSION['form']['form-success'] = 'Your chosen crop area has been resized into '.$i.' version(s).';
		header("Location: ".Page::$slug[0]);
		exit();
	}
	
	$_SESSION['form']['form-error'] = 'There was a problem during the cropping process, please try again.';
