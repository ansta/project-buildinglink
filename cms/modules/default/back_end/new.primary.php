<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
Create: New <?php echo Config::$settings[Page::$slug[2]]['singular']; ?></h1>

<section>
<?php 
if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
	echo '<p class="errorbanner">'.$_SESSION['form']['form-error'].'</p>';
	unset($_SESSION['form']['form-error']);
}
if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
	echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
	unset($_SESSION['form']['form-success']);
}
?>	
<form action="" method="post" enctype="multipart/form-data">
	<fieldset>
		<legend></legend>
		<?php
		foreach (Config::$fields[Module::$name] AS $field) {
			
		/*	# Set the options for the PARENT drop down field
			# TODO: possibly need to find a better place for this
			if ($field['name'] == 'parent' && Module::$name == 'pages') {
				Form::setOption('parent',0,'ROOT');
				Pages::getChildren('pages');
			}
		*/
			if ($field['name'] == 'id') {
				#nothing
				
			} elseif ($field['name'] == 'site_id') {
				if (isset(Config::$settings['pages']['filter_by_site']) && Config::$settings['pages']['filter_by_site'] == true) {
					if (isset($_SESSION['active_site_id']) && $_SESSION['active_site_id'] > 0) {
						echo Form::makeField(array('formtype'=>'hidden','name'=>'site_id','value'=>$_SESSION['active_site_id']));
					} else {
						die('Missing active site ID!');
					}
				}
				
			} else {
				# Loop through all the fields in the config and create the appropriate form field
				if ( ($field['formtype'] == 'hidden') || ($field['protected'] == true && $_SESSION['id'] > 1) ) {
					if ($field['name'] == 'created' || $field['name'] == 'createdby' || $field['name'] == 'updated' || $field['name'] == 'updatedby') {
						# do nothing
					} else {
						$field['formtype'] = 'hidden';
						# Ensure that hidden fields are tagged on to the end of the form
						$hidden_fields .= Form::makeField($field);
					}
				} elseif ($field['formtype'] == 'attachment' && !$attachments_complete) {
					Attachments::getAttachments(Form::$values);
					$attachments_complete = TRUE;
				} else {
					 echo Form::makeField($field);
				}
			}
		}
		echo $hidden_fields;
		if (!$attachments_complete) {
			Attachments::getAttachments(Form::$values);
		}
		?>
		<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'new')); ?>
		<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
		<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Create')); ?>
	</fieldset>
</form>
</section>
