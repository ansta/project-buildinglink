<?php

Admin::allowNew(1);
Admin::allowEdit(1);
Admin::allowDelete(1);
Admin::allowView(0);

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	Core::setModuleTask('default'); // Optional to specify default as it would be default anyway!
	
	if (isset($_SESSION['filter']) && $_SESSION['filter'] > 0 && Config::$settings[Page::$slug[2]]['parent_module'] != '') {
		header('Location: filter/'.$_SESSION['filter'].'/');
	} elseif (isset($_SESSION['filter']) && $_SESSION['filter'] > 0 && Config::$settings[Page::$slug[2]]['parent_module'] != '') {
		header('Location: filter/'.$_SESSION['filter'].'/');
	} else {
		//header('Location: filter/1/');
	}
	
} elseif (Page::$slug[3] == 'new') {
	Core::setModuleTask('new');
	if (Page::$slug[4] > 0 && Config::$settings[Page::$slug[2]]['parent_module'] != '') {
		Form::$values['parent_id'] = Page::$slug[4];
	} elseif (Page::$slug[4] > 0 && Config::$settings[Page::$slug[2]]['parent_module'] == '') {
		Error::type(404); // If there is no parent module specified then 404
	} else {
		Form::prefillValues();
	}
	
} elseif (Page::$slug[3] == 'edit' && Page::$slug[4] > 0) {
	Core::setModuleTask('edit');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'view' && Page::$slug[4] > 0) {
	if (Admin::checkHasViewUrl()) {
		Error::type(404);
	} else {
		Core::setModuleTask('view');
		if (!Form::prefillValues()) {
			Error::type(404); // If values not found for the requested record then 404
		}
	}
	
} elseif (Page::$slug[3] == 'delete' && Page::$slug[4] > 0) {
	Core::setModuleTask('delete');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'create' && $_SESSION['id'] == 1) {
	Setup::createNewTable(Page::$slug[2]);
	header('Location: /'.Page::$slug[1].'/'.Page::$slug[2].'/');
	
} elseif (Page::$slug[3] == 'crop' && Page::$slug[4] > 0 && Page::$slug[5] != '') {
	Core::setModuleTask('crop');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'filter' && Page::$slug[4] > 0) {
	Core::setModuleTask('filter');
	$_SESSION['filter'] = Page::$slug[4];
	
} else {
	Error::type(404);
}
