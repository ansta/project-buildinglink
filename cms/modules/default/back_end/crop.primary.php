<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
Crop <?php echo Config::$settings[Page::$slug[2]]['singular']; ?>: <?php echo Form::getItemTitle(); ?></h1>

<section>
<?php
	if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
		echo '<p class="errorbanner">'.$_SESSION['form']['form-error'].'</p>';
		unset($_SESSION['form']['form-error']);
	}
	if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
		unset($_SESSION['form']['form-success']);
	}

	foreach (Config::$fields[Module::$name] AS $field) {
		if ($field['formtype'] == 'img' && $field['name'] == Page::$slug[5]) {
			
			// Some variables for use later in the code
			$module_name = Page::$slug[2];
			$groupby = Page::$slug[4];
			$field_name = Page::$slug[5];
			$filename = Form::$values[$field['name']];
			$path_to_original = $field['root'].'/uploads/'.$field['name'].'/'.Form::$values[$field['groupby']].'/original/'.$filename;
			
			// Set the crop ratio
			$_SESSION['crop_width']	= Config::$settings[Page::$slug[2]][Page::$slug[5]][$field['cropratio']]['width'];
			$_SESSION['crop_height']= Config::$settings[Page::$slug[2]][Page::$slug[5]][$field['cropratio']]['height'];
			
			// Loop through all the image variations and if image exists, display it.
			foreach (Config::$settings[Page::$slug[2]][Page::$slug[5]] AS $type => $arr) {
				$path_to_img = $module_name.'/uploads/'.$field_name.'/'.$groupby.'/'.$type.'/'.$filename;
				if (file_exists(SYSTEM_ROOT.'/modules/'.$path_to_img)) {
					echo '<p><strong style="font-size:1.7em;">'.ucfirst($type).'</strong><br /><img src="/image.php?i=/'.$path_to_img.'" style="" /></p>';
				} else {
					echo 'Error: image file appears to be missing: '.$path_to_img;
				}
			}
			
			// Setup the original image in a form to allow user cropping
			if (file_exists(SYSTEM_ROOT.'/modules/'.$path_to_original)) {
				echo '<link rel="stylesheet" href="/js/libs/Jcrop/css/jquery.Jcrop.css" type="text/css" />';
				echo '<div id="cropwrap">';
					echo '<p><strong style="font-size:1.7em;">Original</strong>
					<br />This is the master image from which all others are generated.
					<br />Select a new crop area on the below image and click the <strong>Crop Image</strong> button below it to replace the smaller versions above.<br /><img src="/image.php?i=/'.$path_to_original.'" id="cropbox" /></p>';
					echo '<form action="" method="post" onsubmit="return checkCoords();">';
						echo '<input type="hidden" id="x" name="x" />';
						echo '<input type="hidden" id="y" name="y" />';
						echo '<input type="hidden" id="w" name="w" />';
						echo '<input type="hidden" id="h" name="h" />';
						echo '<input type="hidden" name="id" value="'.$groupby.'" />';
						echo '<input type="hidden" name="name" value="'.$field_name.'" />';
						echo '<input type="hidden" name="filename" value="'.$filename.'" />';
						echo '<input type="hidden" name="module" value="'.$module_name.'" />';
						echo '<input type="hidden" name="action" value="crop" />';
						echo '<input type="submit" value="Crop Image" />';
					#	echo '<p class="extrainfo">Selecting a new crop area on the above image and clicking <strong>Crop Image</strong> will replace the current one(s).</p>';
					echo '</form>';
				echo '</div>';
			} else {
				echo 'Error: Original image file appears to be missing: '.$path_to_img;
			}
		}
	}
?>
</section>
