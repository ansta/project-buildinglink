<h1><?php echo Config::$settings[Page::$slug[2]]['plural']; ?></h1>
<section>
<?php
	//Create a filter drop down menu to easily switch between product photos.
	$parents = call_user_func(Config::$settings[Page::$slug[2]]['parent_module'].'::getAdminFilterList');
	if ($parents->num_rows > 0) {
		
		echo '<form action="#" name="redirect">';
			echo '<select name="jump" class="list-filter redirectSelection showAll">';
				echo '<option value="">-- show items belonging to... --</option>';
				while ($parent = $parents->fetch_object()) {
					$option_selected = '';
					if (isset($_SESSION['filter']) && $parent->id == $_SESSION['filter']) {
						$option_selected = ' selected="selected"';
					}
					echo '<option value="/'.Page::$slug[1].'/'.Page::$slug[2].'/'.Page::$slug[3].'/'.$parent->id.'/"'.$option_selected.'>'.$parent->title.'</option>';
				}
			echo '</select>';
		echo '</form>';
	}
	
	// Check to see if this module relies on a POS column
	if(Admin::fieldCheckExists(Page::$slug[2],'pos')) {
		Admin::$haspos = TRUE;
		$table_js_id = 'sortable';
	} else {
		$table_js_id = 'notsortable';
	}
	
	if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
		unset($_SESSION['form']['form-success']);
	}
	
	$search = 'parent_id = '.Page::$slug[4];
	$items = Admin::getData($search);
	
	// Start building the output
	if ($items->num_rows > 0) {
		
		$columns = Config::$fields[Module::$name];
		// Start with the list table header
		$output = '<table class="tbldata" id="'.$table_js_id.'">';
			$output .= '<tr class="nodrag nodrop">';
			foreach ($columns AS $field) {
				if ($field['list'] == true) {
					$output .= '<th class="tbltext col-'.$field['name'].' col-'.$field['formtype'].'">';
					$output .= $field['label'];
					$output .= '</th>';
				}
			}
			$colspan = 2;
			if (Admin::checkButtonColumn('seo')) {
				$colspan = $colspan + 1;
			}
			if (isset(Config::$settings[Module::$name]['child_module_filter'])) {
				foreach (Config::$settings[Module::$name]['child_module_filter'] AS $child_module_id => $child_module_name) {
					$colspan = $colspan + 1;
				}
			}
			if (in_array('parent', Config::$fields[Module::$name])) {
				$colspan = $colspan + 1;
			}
			$output .= '<th colspan="'.$colspan.'" class="tblaction btn-new">';
			$output .= Admin::makeButton('new',0,Module::$name,'New '.Config::$settings[Module::$name]['singular'],Page::$slug[2].'/new/'.Page::$slug[4].'/');
			
			$output .= '</th>';
		$output .= '</tr>';
		
		// Continue building the data for each row
		while( $item = $items->fetch_object() ) {
			
			// apply active/inactive styling
			if (in_array('parent', Config::$fields[Module::$name])) {
				if ($item->active == 1) {
					$active_css = 'active';
				} else {
					$active_css = 'inactive';
				}
			} else {
				$active_css = 'active';
			}
			
			// apply alternate row colouring
			$i++;
			if($i & 1) {
				$rowcss = ' class="row1 '.$active_css.'"';
			} else {
				$rowcss = ' class="row0 '.$active_css.'"';
			}
			
			// For depth fields (`pages` table only at the moment) visualise the representation of the depth
			if (Page::$slug[2] == 'pages') {
				$indent_string = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				if ($item->depth == 1) {
					$depth_indent = '';
				} elseif($item->depth == 2) {
					$depth_indent = $indent_string.$indent_suffix;
				} elseif($item->depth == 3) {
					$depth_indent = $indent_string.$indent_string.$indent_suffix;
				} elseif($item->depth == 4) {
					$depth_indent = $indent_string.$indent_string.$indent_string.$indent_suffix;
				} elseif($item->depth == 5) {
					$depth_indent = $indent_string.$indent_string.$indent_string.$indent_string.$indent_suffix;
				} else {
					$depth_indent = '';
				}
			}
			
			// Make sure that we use the correct ID column depending on whether or not this module has version control or not
			$version_control = Config::$settings[$module]['version_control'];
			if($version_control == 'audit' || $version_control == 'full') {
				$item_id = $item->id;
			} else {
				if (in_array('auto_id', Config::$fields[Module::$name])) {
					$item_id = $item->auto_id;
				} else {
					$item_id = $item->id;
				}
			}
			
			// output the HTML for the table
			$output .= '<tr'.$rowcss.' id="'.$item_id.'">';
			foreach ($columns AS $field) {
				if ($field['list'] == true) {
					
					// Setting the width of the column if it is an image
					if ($field['formtype'] == 'img') {
						$output .= '<td class="tbltext" style="width:'.(Config::$settings[Module::$name][$field['name']]['admin']['width']+10).'px">';
					} else {
						$output .= '<td class="tbltext">';
					}
					
					if ($field['formtype'] == 'img') {
						if ($item->is_master == 1) {
							$is_master_css = 'style="border:2px dotted red" ';
						} else {
							$is_master_css = 'style="border:2px dotted transparent" ';
						}
						$output .= '<img src="/image.php?i=/'.Module::$name.'/uploads/'.$field['name'].'/'.$item_id.'/admin/'.$item->$field['name'].'" '.$is_master_css.'/>';
					} elseif ($field['name'] == 'title') {
						if ($item->is_master == 1) {
							$output .= $depth_indent.$item->$field['name'].'<br />(Master image)';
						} else {
							$output .= $depth_indent.$item->$field['name'];
						}
					} else {
						$output .= $depth_indent.$item->$field['name'];
					}
					$output .= '</td>';
				}
			}
			
			// SEO
			if (Admin::checkButtonColumn('seo')) {
				$output .= '<td class="tblaction btn-seo">';
				$output .= Admin::makeButton('seo',$item_id,Module::$name,'SEO');
				$output .= '</td>';
			}
			
			// CHILD Module Filters
			if (isset(Config::$settings[Module::$name]['child_module_filter'])) {
				foreach (Config::$settings[Module::$name]['child_module_filter'] AS $child_module_id => $child_module_name) {
					$output .= '<td class="tblaction '.Config::$settings[$child_module_name]['icon_class'].'">';
					$output .= Admin::makeButton(
									Config::$settings[Module::$name]['child_module_filter'][$child_module_id]
								,	$item_id
								,	Config::$settings[$child_module_name]['child_module_filter'][$child_module_id]
								,	Config::$settings[$child_module_name]['short']
								,	Config::$settings[Module::$name]['child_module_filter'][$child_module_id].'/filter/'.$item_id.'/'
								,	1
								);
					$output .= '</td>';
				}
			}
			
			// NEW
			if (in_array('parent', Config::$fields[Module::$name])) {
				$output .= '<td class="tblaction btn-newsub">';
				if (Config::$settings[Module::$name]['max_depth'] <= $item->depth) {
					$output .= '<span class="disabled" title="Maximum page depth reached!">New SubPage</span>';
				} else {
					if ($item->use_module != '') {
						if (Config::$settings[$item->use_module]['allow_subpages']) {
							$output .= Admin::makeButton('new',$item_id,Module::$name,'New SubPage');
						} else {
							$output .= '<span class="disabled" title="Maximum page depth reached!">New SubPage</span>';
						}
					} else {
						$output .= Admin::makeButton('new',$item_id,Module::$name,'New SubPage');
					}
				}
				$output .= '</td>';
			}
			
			// EDIT
			$output .= '<td class="tblaction btn-edit">';
			$output .= Admin::makeButton('edit',$item_id);
			$output .= '</td>';
		
			// DELETE
			$output .= '<td class="tblaction btn-delete">';
			
			// If global delete is permitted, check for row specific permissions
			if (Admin::$allow['delete'] == 1) {
				if($item->locked) {
					Admin::allowDelete(0);
					$output .= Admin::makeButton('delete',$item_id);
					Admin::allowDelete(1);
				} else {
					$output .= Admin::makeButton('delete',$item_id);
				}
			} else {
				$output .= Admin::makeButton('delete',$item_id);
			}
			$output .= '</td>';
			$output .= '</tr>';
		}
		$output .= '</table>';
	} else {
		$output = '<p>No data available yet, please <a href="../../new/'.Page::$slug[4].'/">add a new item</a>.</p>';
		if (DEBUG_MODE > 0) {
			$output .= $query;
		}
	}
	echo $output;
?>
</section>
