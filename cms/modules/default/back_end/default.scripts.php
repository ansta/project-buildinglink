
<!-- START - jQuery & jQuery UI -->
<script type="text/javascript" src="/js/libs/jquery/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/js/libs/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="/js/libs/jquery-ui/css/ui-lightness/jquery-ui-1.10.1.custom.min.css" type="text/css" media="all" /> 
<!-- END - jQuery & jQuery UI -->


<!-- START - Admin -->
<script type="text/javascript" src="/js/libs/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/libs/color.js"></script>
<script type="text/javascript" src="/js/libs/iris.min.js"></script>
<script type="text/javascript" src="/js/mylibs/admin.js"></script>
<script type="text/javascript" src="/js/mylibs/cmsmenu.js"></script>
<!-- END - Admin -->


<!-- START - jQuery MultiSelect -->
<link rel="stylesheet" href="/js/libs/multiselect/jquery.multiselect.css">
<script type="text/javascript" src="/js/libs/multiselect/jquery.multiselect.js"></script>
<script type="text/javascript">
	$(function(){
		$(".type-multi select").multiselect();
	});
</script>
<!-- END - jQuery MultiSelect -->


<!-- START - jQuery DragAndDrop -->
<script type="text/javascript" src="/js/libs/dragndrop_tables/jquery.tablednd.js"></script>
<script type="text/javascript" src="/js/libs/dragndrop_tables/jqueryTableDnDArticle.js"></script>
<!-- END - jQuery DragAndDrop -->


<!-- START - TinyMCY WYSIWYG Editor -->
<script type="text/javascript" src="/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "specific_textareas",
		editor_selector : "editor",
		theme : "advanced",
		cleanup:true,
	//		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,visualblocks",
			plugins : "autolink,lists,style,table,save,advhr,advimage,advlink,inlinepopups,preview,media,searchreplace,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,visualblocks",

		// Theme options
		theme_advanced_buttons1 : "fullscreen,code,removeformat,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,|,link,unlink,anchor,|,image,media,hr,|,bullist,numlist,|,outdent,indent,|,bold,italic,|",
		theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,|,blockquote,styleselect,formatselect,tablecontrols,|",
	//	theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		
		// Custom file browser plugin
		file_browser_callback : "filebrowser",	// More info: http://www.neele.name/filebrowser/
		convert_urls : false,					// More info: http://www.tinymce.com/wiki.php/TinyMCE_FAQ#Paths.2FURLs_are_incorrect.2C_I_want_absolute.2Frelative_URLs.3F
		
		// Example content CSS (should be your site CSS)
	//	content_css : "/css/editor.css",
		content_css : "/css/editor.css?" + new Date().getTime(),
		
		// Drop lists for link/image/media/template dialogs
	//	template_external_list_url : "lists/template_list.js",
	//	external_link_list_url : "lists/link_list.js",
	//	external_image_list_url : "lists/image_list.js",
	//	media_external_list_url : "lists/media_list.js",

		// Style formats
	//	style_formats : [
	//		{title : 'Bold text', inline : 'b'},
	//		{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
	//		{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
	//		{title : 'Example 1', inline : 'span', classes : 'example1'},
	//		{title : 'Example 2', inline : 'span', classes : 'example2'},
	//		{title : 'Table styles'},
	//		{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
	//	],

		// Replace values for the template plugin
	//	template_replace_values : {
	//		username : "Some User",
	//		staffid : "991234"
	//	}
	});
	
	// Custom file browser plugin
	function filebrowser(field_name, url, type, win) {
		fileBrowserURL = "/editor/filebrowser/index.php?editor=tinymce&filter=" + type;
		tinyMCE.activeEditor.windowManager.open({
			title: "PDW File Browser",
			url: fileBrowserURL,
			width: 950,
			height: 650,
			inline: 0,
			maximizable: 1,
			close_previous: 0
			},{
			window : win,
			input : field_name
			}
		);    
	}
</script>
<!-- END - TinyMCY WYSIWYG Editor -->


<?php if (Page::$slug[3] == 'crop') { ?>

	<script type="text/javascript" src="/js/libs/Jcrop/js/jquery.color.js"></script>
	<script type="text/javascript" src="/js/libs/Jcrop/js/jquery.Jcrop.js"></script>
	<script language="Javascript">
		$(function(){
			$('#cropbox').Jcrop({
				aspectRatio: <?php echo isset($_SESSION['crop_width'])?$_SESSION['crop_width']:100; ?>/<?php echo isset($_SESSION['crop_height'])?$_SESSION['crop_height']:100; ?>,
				onSelect: updateCoords
			});
	
		});
		function updateCoords(c)
		{
			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#w').val(c.w);
			$('#h').val(c.h);
		};
		function checkCoords()
		{
			if (parseInt($('#w').val())) return true;
			alert('Please select a crop region then press submit.');
			return false;
		};
		
	</script>

<?php } ?>
