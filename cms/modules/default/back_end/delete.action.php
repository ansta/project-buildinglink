<?php

$submission = TRUE;

if ($submission) {
	
	if ($_POST['id'] > 0 && $_POST['id'] != '') {
		
		$version_control = Config::$settings[$module]['version_control'];
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query = "
				UPDATE 
					`".$module."`
				SET
					`ttv_end` = NOW(),
					`modify_by` = '".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
					`modify_id` = ".(int)db::link()->real_escape_string($_SESSION['id']).",
					`modify_action` = 'delete'
				WHERE
					`id` = ".(int)db::link()->real_escape_string($_POST['id'])."
				AND `ttv_end` IS NULL
			";
		} else {
			$query = "
				DELETE FROM
					`".$module."`
				WHERE
					`auto_id` = ".(int)db::link()->real_escape_string($_POST['id'])."
			";
		}
		
		db::link()->query($query) or die(db::link()->error);
		$_SESSION['form']['form-success'] = 'Item has been deleted';
		header("Location: ../../?sent=yes");
		die();
	}
	
}




