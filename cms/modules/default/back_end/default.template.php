<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?><!doctype html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Admin</title>
	<link rel="stylesheet" href="/css/admin.css">
		
	<!-- Google web fonts -->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
	
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js">IE7_PNG_SUFFIX=".png";</script>
	<![endif]-->
</head>
<body id="<?php echo Module::$name; ?>" class="module-<?php echo Module::$name; ?> action-<?php echo Page::$slug[3]; ?>" lang="en">

	<div class="primary">
		<?php Admin::getElement('Primary'); ?>
	</div><!-- end of .primary -->

	<div class="secondary">
		<section>
			<?php Admin::getElement('Secondary'); ?>
		</section>
	</div><!-- end of .secondary -->
	
	<?php Admin::getElement('Header'); ?>
	<?php Admin::getElement('Nav'); ?>
	<?php Admin::getElement('Scripts'); ?>
</body>
</html>