<?php
if (isset($_POST['sortable'])) {
	if (User::isAdmin()) {
		$version_control = Config::$settings[Module::$name]['version_control'];
		if (DEBUG_MODE > 0) {
			$_SESSION['reorderactionphp'] = '';
		}
		$list = $_POST['sortable'];
		for ($i = 0; $i < count($list); $i++) {
			if (isset($list[$i]) && $list[$i] != '') {
				$update_qry = "
					UPDATE
						`".Module::$name."`
					SET
						`pos`=" . $i . "
					WHERE
						`id`= " . $list[$i] . "
				";
				if($version_control == 'audit' || $version_control == 'full') {
					$update_qry .= "
						AND `ttv_start` <= NOW()
						AND `ttv_end` IS NULL
					";
				}
				$update = db::link()->query($update_qry);
				if (DEBUG_MODE > 0) {
					$_SESSION['reorderactionphp'] .= $update_qry.PHP_EOL;
				}
			}
		}
	} else {
		# User is not logged in so ignore the request to do something
		if (DEBUG_MODE > 0) {
			$_SESSION['reorderactionphp'] .= 'Un-authorised user cannot re-order!';
		}
	}
}