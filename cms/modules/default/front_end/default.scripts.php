



	







<!-- START - jQuery & jQuery UI -->
<?php /*?><script type="text/javascript" src="/js/libs/jquery/jquery-1.9.1.js"></script><?php */?>
<script type="text/javascript" src="/js/libs/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/libs/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<link rel="stylesheet" href="/js/libs/jquery-ui/css/ui-lightness/jquery-ui-1.10.1.custom.min.css" type="text/css" media="all" /> 
<!-- END - jQuery & jQuery UI -->



<!-- START - flexslider -->
<script type="text/javascript" src="/js/libs/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function ($) {
	// Ensure all images are loaded
	$(window).load(function(){
		// Header Slider
		$('.flexslider').flexslider({
			animation: "fade",
			controlNav: false,
			directionNav: false,
			slideshowSpeed: 5000
		});

		$(".shownav").on("click",function(){
			$(".shownav,.primary-menu-bar").toggleClass("active");
		});
		
	});
	
});

</script>
<!-- END - flexslider -->

	

<!-- START - make Billing address same as Shipping address -->
<script type="text/javascript">
jQuery(document).ready(function($) {
	var sameAddress = 0;
	$('#in_check').change(function(){
		if($(this).is(':checked')) {
			sameAddress = 1;
			$(".ship").each(function() {
				var parts = $(this).attr("id").split("_");
				$("#in_bill_" + parts[2]).val($(this).val());
			});
		} else {
			sameAddress = 0;	
		}
	});
	$(".ship").change(function() {
		if(sameAddress == 1) {
			var parts = $(this).attr("id").split("_");
			$("#in_bill_" + parts[2]).val($(this).val());
		}
	});
});
</script>
<!-- END - make Billing address same as Shipping address -->
	
	
<?php /*?>
<!-- START - onSelect redirect -->
<script type="text/javascript">
// get your select element and listen for a change event on it
$('.redirectSelection').change(function() {
  // set the window's location property to the value of the option the user has selected
  window.location = $(this).val();
});
</script>
<!-- END - onSelect redirect -->
<?php */?>

<?php /*?>
<!-- START - Banner -->
<script type="text/javascript" src="/js/libs/rotator/rotator.js"></script>
<script type="text/javascript">
	jQuery(function ($) {
		window.banner = $('#banner').rotator({ transition: 'fade', nav: { next: true, prev: true }, ieFadeFix: true });
		//window.example_1 = $('#example_2').rotator({ transition: 'swipe', nav: { next: true, prev: true }, ieFadeFix: true });
		//window.example_3 = $('#example_3').rotator({ transition: 'push', nav: { next: true, prev: true }, ieFadeFix: true });
	});
</script>
<!-- END - Banner -->
<?php */?>

<?php /*?>
<?php if (Module::$slug[4] == 'gallery') { ?>
<!-- START - Lightbox Gallery -->
<script type="text/javascript" charset="utf-8" src="/js/libs/shadowbox-gallery/js/jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript">
// Lightbox effect generated via <a href="http://leandrovieira.com/projects/jquery/lightbox/
$(function(){
  $('a[rel=lightbox]').lightBox({
    containerResizeSpeed: 250,
    fixedNavigation: true
  });
//  $('a[rel=2ndlightbox]').lightBox({
//    overlayBgColor: '#fff',
//    overlayOpacity: 0.7
//  });
});
</script>
<!-- END - Lightbox Gallery -->
<?php } ?>
<?php */?>

<?php /*?>
<?php if (Module::$name == 'testimonials') { ?>
<!-- START - Reviews widget -->
<script type="text/javascript">
var DVapp = {
     api_key   : 'a6974e0976e7627a27eac9b0dc64a260',
     hide_discussions   : 'true',
     target   : 'listwidget'
};
</script>
<script type="text/javascript" src="http://www.digitalvisitorapps.com/api/_bf.php" defer></script>
<!-- END - Reviews widget -->
<?php } ?>
<?php */?>

<?php if(User::isAdmin()) { ?>
<!-- START - Admin -->
	<script type="text/javascript" src="/js/libs/jquery.cookie.js"></script>
	<script type="text/javascript" src="/js/mylibs/cmsmenu.js"></script>
<!-- END - Admin -->


<!-- START - TinyMCE WYSIWYG Editor -->
	<script type="text/javascript" src="/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.edit_content a').click(function(){
				var rel= $(this).attr('rel');
				$('<div></div>').load('/editor/edit.content.ajax.php', 'id=' + $(this).attr('rel'),  edit_content);	
			});
			$('.edit_page a').click(function(){
				$('<div></div>').load('/editor/edit.page.ajax.php', 'id=' + $(this).attr('rel'),  edit_page);	
			});
			$('.edit_module a').click(function(){
				$('<div></div>').load('/editor/edit.module.ajax.php', 'id=' + $(this).attr('rel'),  edit_module);	
			});
		
		});
		function edit_content(){
			$(this).dialog({width:639, title: "Edit Content", close:onClose , open:onOpen});
		}
		function edit_page(){
			$(this).dialog({width:639, title: "Edit Page Content", close:onClose, open:onOpen});
		}
		function edit_module(){
			$(this).dialog({width:639, title: "Edit Page Content", close:onClose, open:onOpen});
		}
		function onClose(e){
			$(this).remove();
		}
		function onClose1(e){
			$(this).remove();
		}
		function onOpen(e){
			tinyMCE.init({
				// General options
				mode : "specific_textareas",
				editor_selector : "editor",
				theme : "advanced",
		//		width: "500",
				height: "300",
				cleanup:true,
		//		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,visualblocks",
				plugins : "autolink,lists,style,table,save,advhr,advimage,advlink,inlinepopups,preview,media,searchreplace,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,visualblocks",
		
				// Theme options
				theme_advanced_buttons1 : "save,fullscreen,code,removeformat,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,|,link,unlink,anchor,|,image,media,hr,|,bullist,numlist,|,outdent,indent,|,bold,italic,|",
				theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,|,blockquote,styleselect,formatselect,tablecontrols,|",
		//		theme_advanced_buttons3 : "",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				
				// Custom file browser plugin
				file_browser_callback : "pixxaFileBrowser",	// More info: http://www.neele.name/filebrowser/
				convert_urls : false,					// More info: http://www.tinymce.com/wiki.php/TinyMCE_FAQ#Paths.2FURLs_are_incorrect.2C_I_want_absolute.2Frelative_URLs.3F
				
				save_onsavecallback : "ajaxSave",
				// Example content CSS (should be your site CSS)
				content_css : "/css/editor.css?" + new Date().getTime(),
		
				// Drop lists for link/image/media/template dialogs
			//	template_external_list_url	: "lists/template_list.js",
			//	external_link_list_url		: "lists/link_list.js",
			//	external_image_list_url		: "lists/image_list.js",
			//	media_external_list_url		: "lists/media_list.js",
		
				// Style formats
		//		style_formats : [
			//		{title : 'Bold text', inline : 'b'},
			//		{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			//		{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			//		{title : 'Example 1', inline : 'span', classes : 'example1'},
			//		{title : 'Example 2', inline : 'span', classes : 'example2'},
			//		{title : 'Table styles'},
			//		{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
			//		{title : 'Orange Text', inline : 'span', classes : 'orange'},
			//		{title : 'Grey Text', inline : 'span', classes : 'grey'},
			//		{title : '2 Column List', selector : 'ul', classes : 'columns2 clearfix'},
			//		{title : '3 Column List', selector : 'ul', classes : 'columns3 clearfix'},
			//		{title : 'Inline List', selector : 'ul', classes : 'inline'},
			//		{title : 'Make orange tick lists grey', selector : 'li', classes : 'tick-grey'},
			//		{title : 'Heading 1 plain', selector : 'h1', classes : 'noborder'},
			//		{title : 'Table: schedule style', selector : 'table', classes : 'tbl-schedule'},
			//		{title : 'Table: time column', selector : 'td', classes : 'tbl-col-time'},
			//		{title : 'Table: desction column', selector : 'td', classes : 'tbl-col-desc'},
		//		],
		
				// Replace values for the template plugin
			//	template_replace_values : {
			//		username : "Some User",
			//		staffid : "991234"
			//	}
			});
		}
		
		// Ajax Save for content editor
		function ajaxSave() {
			var ed = tinyMCE.get('details');
			var content = ed.getContent();
			var variables = [];
			var variables1 = [];
			var $inputs = $("#ajaxForm :input");
			$inputs.each(function() {
				variables.push(escape(this.name) + "=" + $(this).val());
				variables1[this.name] = $(this).val();
			});
			var status=  $.ajax({
				type:'POST',
				url:'/editor/edit.'+variables1["type"]+'.save.php',
				data: "content="+escape(content) + "&" + variables.join("&"),
				async: false
			}).responseText;
			document.getElementById(variables1["type"]+"_"+variables1["content_id"]).innerHTML=status;
			return false;
		}
		
		function pixxaFileBrowser (field_name, url, type, win) {
			
	//		alert("Field_Name: " + field_name + "\nURL: " + url + "\nType: " + type + "\nWin: " + win); // debug/testing
			
			/* If you work with sessions in PHP and your client doesn't accept cookies you might need to carry
			the session name and session ID in the request string (can look like this: "?PHPSESSID=88p0n70s9dsknra96qhuk6etm5").
			These lines of code extract the necessary parameters and add them back to the filebrowser URL again. */
			
			var cmsURL = '/editor/filebrowser/index.php';    // script URL - use an absolute path!
			if (cmsURL.indexOf("?") < 0) {
				//add the type as the only query parameter
				cmsURL = cmsURL + "?type=" + type;
			}
			else {
				//add the type as an additional query parameter
				// (PHP session ID is now included if there is one at all)
				cmsURL = cmsURL + "&type=" + type;
			}
			
			tinyMCE.activeEditor.windowManager.open({
				file : cmsURL
			,	title : 'My File Browser'
			,	width : 470  // Your dimensions may differ - toy around with them!
			,	height : 400
			,	resizable : "yes"
			,	inline : "yes"  // This parameter only has an effect if you use the inlinepopups plugin!
			,	close_previous : "no"
			}, {
				window : win
			,	input : field_name
		//	,	src : field_name
			});
			return false;
		}
	</script>
<!-- END - TinyMCE WYSIWYG Editor -->
<?php } ?>


<?php if (Site::$sitedata['analytics'] != '' && DEBUG_MODE < 2) { ?>
<!-- START - Google Analytics -->
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  <?php echo '_gaq.push([\'_setAccount\', \''.Site::$sitedata['analytics'].'\']);'; ?>
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
<!-- END - Google Analytics -->
<?php } ?>

<?php // START - Google reCaptcha js part
	echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
	// END - Google reCaptcha
?>
