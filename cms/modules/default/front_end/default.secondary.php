
<!-- .widget -->
<div class="widget widget-nav">
	<h3 class="widget-title">Product Categories</h3>
	<?php Page::getElement('widget','shop_categories','filter'); ?>
</div>
<!-- /.widget -->

<!-- .widget -->
<div class="widget widget-box">
	
	<!-- .box -->
	<div class="box box-contact">
		<?php echo Site::$sitedata['tel1']!=''?'<span>Tel:</span> '.Site::$sitedata['tel1'].'<br />':''; ?>
		<?php echo Site::$sitedata['email_public']!=''?'<span>Email:</span> <a href="mailto:'.Site::$sitedata['email_public'].'" class="btn">Click Here</a><br />':''; ?>
	</div>
	<!-- /.box -->
	
</div>
<!-- /.widget -->

<!-- .widget -->
<div class="widget widget-box">
	
	<!-- .box -->
	<div class="box box-advert quote-advert clearfix">
		
		<h2>Get a Quote</h2>
		<a href="<?php echo Page::getModuleUrl('contacts'); ?>" class="btn">Click Here</a>
		
	</div>
	<!-- /.box -->
	
</div>
<!-- /.widget -->

<?php /*?>
<!-- .widget -->
<div class="widget widget-box">
	
	<!-- .box -->
	<div class="box box-advert clearfix">
		
		<p>Only need 1 or 2 unprinted rulers?</p>
		<a href="#" class="btn">Click Here</a>
		
	</div>
	<!-- /.box -->
	
</div>
<!-- /.widget -->
<?php */?>

<!-- .widget -->
<div class="widget widget-box">
	
	<!-- .box -->
	<div class="box box-paymnt">
		
		<h5>Payment Options</h5>
		<img src="/images/payment-types.png" alt="Payment Options" />
		
	</div>
	<!-- /.box -->
	
</div>
<!-- /.widget -->

				
				
				<?php /*?>
				
				
			<div class="box box-primarynav">
				<?php Page::getElement('PrimaryNav'); ?>
			</div>
			
			<?php #echo (Page::getPageUrl(113) != '' ? '<div class="box box-video"><a href="'.str_replace("//", "/", Page::getPageUrl(113)).'">watch the in:spa video here</a></div>' : ''); ?>
			
			<div class="box box-cta">
				<?php #Page::getElement('ActionNav'); ?>
			</div>
			
			<?php #echo (Page::getModuleUrl('newsletter_subscriptions') != '' ? '<div class="box box-newsletter"><a href="'.str_replace("//", "/", Page::getModuleUrl('newsletter_subscriptions')).'">sign up for the newsletter</a></div>' : ''); ?>
			
			<div class="box box-additional">
				<?php #Page::getElement('AdditionalNav'); ?>
			</div>
			
			<?php
		/*	$reviews2 = Testimonials::getSecondaryList('pages',Page::$id[0],2);
			if ($reviews2->num_rows > 0) {
				echo '<div class="box box-quotes">';
					$i2 = 0;
					while ($review2 = $reviews2->fetch_object()) {
						$i2++;
						if ($i2 == 1) {
							$item_css = 'item-first';
						} elseif ($i2 == $review2->num_rows) {
							$item_css = 'item-last';
						} else {
							$item_css = '';
						}
						echo '<p class="'.$item_css.'">'.$review2->content.' ';
						if ($review2->label != '') {
							echo '<span class="source">'.$review2->label.'</span>';
						}
						echo '</p>';
					}
					echo '<a href="/reviews/press-reviews/" class="more">read more press quotes</a>';
				echo '</div>';
			}
		 */	?>
			
			<?php
	/*		$reviews1 = Testimonials::getSecondaryList('pages',Page::$id[0],1);
			if ($reviews1->num_rows > 0) {
				echo '<div class="box box-testimonials">';
					$i1 = 0;
					while ($review1 = $reviews1->fetch_object()) {
						$i1++;
						if ($i1 == 1) {
							$item_css = 'item-first';
						} elseif ($i1 == $reviews1->num_rows) {
							$item_css = 'item-last';
						} else {
							$item_css = '';
						}
						echo '<p class="'.$item_css.'">'.$review1->content.'</p>';
					#	echo '<p class="'.$item_css.'">'.$review1->content.' <span class="source">'.$review1->title.'</span></p>';
					}
					echo '<a href="/reviews/guest-reviews/" class="more">read more testimonials</a>';
				echo '</div>';
			}*/
			 ?>
<?php
/*
$pageID = (int)Page::$id[0] * 1000;

echo '<h2>Sitewide text block</h2>';
echo Content::getContentByContentId(1);

echo '<h2>Page specific text block '.($pageID + 2).'</h2>';
echo Content::getContentByContentId($pageID + 2);

*/





