<h1>Authentication required</h1>
<p>Please login:</p>
<?php
if (DEBUG_MODE == 2) {
	$autocomplete = '';
} else {
	$autocomplete = 'autocomplete="off"';
}
?>
<form action="" method="post" <?php echo $autocomplete; ?>>
	<fieldset>
		<legend>Login</legend>
		
		<div class="field type-text error-false">
			<label for="in_<?php echo $_SESSION['loginfield_username']; ?>">Email<span>*</span></label>
			<div class="input"><input type="text" name="<?php echo $_SESSION['loginfield_username']; ?>" id="in_<?php echo $_SESSION['loginfield_username']; ?>" value="" <?php echo $autocomplete; ?> /></div>
			<span class="hint hint-text"></span>
		</div>
		
		<div class="field type-password error-false">
			<label for="in_<?php echo $_SESSION['loginfield_password']; ?>">Password<span>*</span></label>
			<div class="input"><input type="password" name="<?php echo $_SESSION['loginfield_password']; ?>" id="in_<?php echo $_SESSION['loginfield_password']; ?>" value="" <?php echo $autocomplete; ?> /></div>
			<span class="hint hint-password"></span>
		</div>
		
		<input type="hidden" name="login" value="true" />
		
		<div class="field type-submit error-false">
			<div class="input"><input type="submit" name="submit" id="in_submit" value="Login" /></div>
			<span class="hint hint-submit"></span>
		</div>
		
	</fieldset>
</form>