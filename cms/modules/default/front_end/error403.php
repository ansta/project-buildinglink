<!doctype html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Admin</title>
	<link rel="stylesheet" href="/css/admin.css">
	<?php #Page::getHeadScripts(); ?>
	
	<!-- Google web fonts -->
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
	<?php
	$login_form_title = 'Login';
	$login_form_message = '';
	if (CLIENT_ADMIN == false && $_SERVER['REMOTE_ADDR'] == OFFICE_IP) {
		$login_form_title = 'Strictly Ansta Access Only!';
		$login_form_message = '<p style="text-align:center;color:#F00;font-weight:bold;font-size:1.3em;">Admin is unavailable to anyone outside of the Ansta office!</p>';
	}
	?>
</head>
<body id="<?php echo Module::$name; ?>" class="<?php echo Page::$slug[3]; ?>" lang="en">

	<div class="login">
		<?php if (isset($_SESSION['id']) && $_SESSION['id'] > 0) { ?>
		<h1>Access Denied</h1>
		<?php } else { ?>
		<h1><?php echo $login_form_title; ?></h1>
		<?php } ?>
		
		<section>
			<?php if (isset($_SESSION['id']) && $_SESSION['id'] > 0) { ?>
				<div class="mainlogin">
					<form action="?logout=true" method="post">
						<fieldset>
							<legend>Access denied</legend>
							<div class="field type-submit error-false" style="padding-left:0;">
								<div class="input"><input type="submit" name="submit" id="in_submit" value="Logout" /></div>
								<span class="hint hint-submit"></span>
							</div>
							<p style="padding:5px 0 0 15px;color:#E00;font-weight:bold;">You do not have sufficient privileges to access this page.</p>
						</fieldset>
					</form>
					<p style="text-align:center;"><a href="/">return to the homepage</a>.</p>
				</div>
			<?php } else { ?>
				<div class="mainlogin">
					<form action="<?php echo Page::$slug[0]; ?>" method="post" <?php echo $autocomplete; ?>>
						<fieldset>
							<legend><?php echo $login_form_title; ?></legend>
							<?php echo $login_form_message; ?>
							<div class="field type-text error-false">
								<label for="in_<?php echo $_SESSION['loginfield_username']; ?>">Email<span>*</span></label>
								<div class="input"><input type="text" name="<?php echo $_SESSION['loginfield_username']; ?>" id="in_<?php echo $_SESSION['loginfield_username']; ?>" value="" <?php echo $autocomplete; ?> /></div>
								<span class="hint hint-text"></span>
							</div>
							<div class="field type-password error-false">
								<label for="in_<?php echo $_SESSION['loginfield_password']; ?>">Password<span>*</span></label>
								<div class="input"><input type="password" name="<?php echo $_SESSION['loginfield_password']; ?>" id="in_<?php echo $_SESSION['loginfield_password']; ?>" value="" <?php echo $autocomplete; ?> /></div>
								<span class="hint hint-password"></span>
							</div>
							<input type="hidden" name="login" value="true" />
							<div class="field type-submit error-false">
								<div class="input"><input type="submit" name="submit" id="in_submit" value="Login" /></div>
								<span class="hint hint-submit"></span>
							</div>
						</fieldset>
					</form>
				</div>
			<?php } ?>
		</section>
	</div><!-- end of .login -->

	<?php Admin::getElement('Header'); ?>
	
	<?php #Page::getFootScripts(); ?>
	<script type="text/javascript" src="/js/libs/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/js/mylibs/admin.js"></script>
	<script src="/js/libs/dragndrop_tables/jquery.tablednd.js" type="text/javascript"></script>
	<script src="/js/libs/dragndrop_tables/jqueryTableDnDArticle.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script><?php /*?>
	<script src="/ckeditor/_samples/sample.js" type="text/javascript"></script><?php */?>
	<script src="/ckeditor/config.js" type="text/javascript"></script>
</body>
</html>