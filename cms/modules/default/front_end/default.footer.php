
<!-- .widget -->
<div class="widget widget-other-sites">
	
	<h4 class="widget-title">Other Sites</h4>
	
	<!-- .widget-body -->
	<div class="widget-body">
	
		<ul class="site-nav clearfix">
			<?php
			$sites = Sites::getActiveList(Site::$sitedata['id']);
			if ($sites->num_rows > 0) {
				while ($site = $sites->fetch_object()) {
					
					// Retrieve the homepage URL
					if (trim($site->domain_live1) != '') {
						$site_url = $site->domain_live1;
					} elseif (trim($site->domain_live2) != '') {
						$site_url = $site->domain_live2;
					} else {
						$site_url = $site->domain_dev;
					}
					
					// Render the list item
					echo '<li>';
						echo '<a href="http://'.$site_url.'?referer='.$_SESSION["basket"].'">';
							echo '<img src="/image.php?i=/sites/uploads/image/'.$site->id.'/original/'.$site->image.'" alt="'.$site->title.'" style="width:29px;height:29px;" />';
							echo $site->title;
						echo '</a>';
					echo '</li>';
				}
			}
			?>
			<?php /*?>
			<li><a href="#"><img src="/images/icon-hivis.png" alt="hiviswaistcoats">hiviswaistcoats.com</a></li>
			<li><a href="#"><img src="/images/icon-tapemeasures.png" alt="promotionaltapemeasures">promotional<br />tapemeasures.co.uk</a></li>
			<li><a href="#"><img src="/images/icon-carpenterpencils.png" alt="carpenterspencils">carpenterspencils.co.uk</a></li>
			<li><a href="#"><img src="/images/icon-golf.png" alt="corporategolfday">corporategolfday.com</a></li>
			<li><a href="#"><img src="/images/icon-pencilsdirect.png" alt="pencilsdirect">pencilsdirect.co.uk</a></li>
			<li><a href="#"><img src="/images/icon-quickpens.png" alt="quickpens">reallyquickpens.co.uk</a></li>
			<li><a href="#"><img src="/images/icon-slidecharts.png" alt="slidecharts">slidecharts.co.uk</a></li>
			<li><a href="#"><img src="/images/icon-scalerulersdark.png" alt="scalerulers">scalerulers.com</a></li>
			<li><a href="#"><img src="/images/icon-scalerulersred.png" alt="scalerulers">scalerulers.com</a></li>
			<li><a href="#"><img src="/images/icon-scalerulersblack.png" alt="scalerulers">scalerulers.co.uk</a></li>
			<?php */?>
		</ul>
	
	</div>
	<!-- /.widget-body -->
	
</div>
<!-- /.widget -->

<!-- .widget -->
<div class="widget widget-quick-links">
	
	<h4 class="widget-title">Quick Links</h4>
	
	<!-- .widget-body -->
	<div class="widget-body">
		
		<?php
		$footer_nav = Nav::getFooter();
		if ($footer_nav->num_rows > 0) {
			while( $item = $footer_nav->fetch_object() ) {
				$i++;
				$linkstate = 'navitem-'.$i;
				if ($item->slug == '') { $linkstate .= ' nav-home'; } else { $linkstate .= ' nav-'.$item->slug; }
				if (Page::$slug[0] == $item->url) { $linkstate = 'active'; } else { $linkstate = 'inactive'; }
				if ($i == 1) {$linkstate .= ' first';}
				if ($i == $footer_nav->num_rows) {$linkstate .= ' last';}
				$output .= '<li class="'.$linkstate.'"><a href="'.$item->url.'" title="'.$item->title.'"><span>'.$item->label.'</span></a></li>';
			}
			$output = '<ul class="nav nav-footer">'.$output.'</ul>';
		}
		echo $output;
		?>
		
	</div>
	<!-- /.widget-body -->

</div>
<!-- /.widget -->

<!-- .widget -->
<div class="widget widget-contact-us">
	
	<h4 class="widget-title">Contact Us</h4>
	
	<!-- .widget-body -->
	<div class="widget-body">
		
		<div class="vcard">
			<p class="adr">
				<?php echo Site::$sitedata['add1']!=''?Site::$sitedata['add1'].'<br />':''; ?>
				<?php echo Site::$sitedata['add2']!=''?Site::$sitedata['add2'].'<br />':''; ?>
				<?php echo Site::$sitedata['add3']!=''?Site::$sitedata['add3'].'<br />':''; ?>
				<?php echo Site::$sitedata['add4']!=''?Site::$sitedata['add4'].'<br />':''; ?>
				<?php echo Site::$sitedata['town'].', '.Site::$sitedata['county'].', '.Site::$sitedata['postcode']; ?>
			</p>
			
			<p class="tel">
				<?php echo Site::$sitedata['tel1']!=''?'Tel: '.Site::$sitedata['tel1'].'<br />':''; ?>
				<?php echo Site::$sitedata['tel2']!=''?'Tel: '.Site::$sitedata['tel2'].'<br />':''; ?>
				<?php echo Site::$sitedata['fax']!=''?'Fax: '.Site::$sitedata['fax']:''; ?>
			</p>
			<p class="email">
				<?php echo Site::$sitedata['email_public']!=''?'Email: <a href="mailto:'.Site::$sitedata['email_public'].'">'.Site::$sitedata['email_public'].'</a><br />':''; ?>
				<?php echo Site::$sitedata['domain_live1']!=''?'Web: <a href="http://'.Site::$sitedata['domain_live1'].'">'.Site::$sitedata['domain_live1'].'</a>':''; ?>
				<?php echo Site::$sitedata['domain_live1']==''?'Web: <a href="http://'.Site::$sitedata['domain_dev'].'">'.Site::$sitedata['domain_dev'].'</a>':''; ?>
			</p>
		</div>
		
	</div>
	<!-- /.widget-body -->

</div>
<!-- /.widget -->

<!-- .widget -->
<div class="widget widget-social-media">
	
	<h4 class="widget-title">Social Media</h4>
	
	<!-- .widget-body -->
	<div class="widget-body">
		
		<ul>
		  <?php echo Site::$sitedata['facebook']!=''?'<li><a href="'.Site::$sitedata['facebook'].'"><img src="/images/icon-facebook.png" alt="Facebook">Like Us</a></li>':''; ?>
		  <?php echo Site::$sitedata['twitter']!=''?'<li><a href="'.Site::$sitedata['twitter'].'"><img src="/images/icon-twitter.png" alt="Follow Us">Follow Us</a></li>':''; ?>
		  <?php echo Site::$sitedata['google']!=''?'<li><a href="'.Site::$sitedata['google'].'"><img src="/images/icon-google.png" alt="Google Plus">Google Plus Us</a></li>':''; ?>
		  <?php echo Site::$sitedata['linkedin']!=''?'<li><a href="'.Site::$sitedata['linkedin'].'"><img src="/images/icon-linkedin.png" alt="LinkedIn">Link with Us</a></li>':''; ?>
		</ul>
		
	</div>
	<!-- /.widget-body -->

</div>
<!-- /.widget -->




<?php
/*

		<span style="float:right;font-size:0.75em;padding-right:25px;">website by <a href="http://ansta.co.uk/" target="_blank" style="color:#777777;">Ansta Ltd</a></span>
		
		
		
<div class="border"></div>
<section class="information">
	<p>Tel: <?php echo Site::$sitedata['tel']; ?> (please leave a message on the answerphone out of hours)<br />Our hours of business are Mon-Fri, 8.30 am - 4 pm.</p>
	<p>Trade enquiries are welcome - please contact us on your company letterhead.</p>
	<p>Visitors are welcome, but please telephone first, as we do have to be away from the workshop on occasion.</p>
</section>
<section class="contact">
	<p><?php echo Site::$sitedata['title']; ?>
	<?php echo (Site::$sitedata['add1'] != '' ? '<br />'.Site::$sitedata['add1'] : ''); ?>
	<?php echo (Site::$sitedata['add2'] != '' ? '<br />'.Site::$sitedata['add2'] : ''); ?>
	<?php echo (Site::$sitedata['add3'] != '' ? '<br />'.Site::$sitedata['add3'] : ''); ?>
	<?php echo (Site::$sitedata['add4'] != '' ? '<br />'.Site::$sitedata['add4'] : ''); ?>
	<?php echo (Site::$sitedata['town'] != '' ? '<br />'.Site::$sitedata['town'] : ''); ?>
	<?php echo ((Site::$sitedata['county'] != '' && Site::$sitedata['town'] != '') ? ', '.Site::$sitedata['county'] : ''); ?>
	<?php echo (Site::$sitedata['postcode'] != '' ? '<br />'.Site::$sitedata['postcode'] : ''); ?>
	</p>
	<p><a href="http://ansta.co.uk/" title="eCommerce Website Design">eCommerce Website Design</a> by Ansta</p>
</section>


	$footer_nav = Nav::getFooter();
	if ($footer_nav->num_rows > 0) {
		while( $item = $footer_nav->fetch_object() ) {
			$i++;
			$linkstate = 'navitem-'.$i;
			if ($item->slug == '') { $linkstate .= ' nav-home'; } else { $linkstate .= ' nav-'.$item->slug; }
			if (Page::$slug[0] == $item->url) { $linkstate = 'active'; } else { $linkstate = 'inactive'; }
			if ($i == 1) {$linkstate .= ' first';}
			if ($i == $footer_nav->num_rows) {$linkstate .= ' last';}
			$output .= '<li class="'.$linkstate.'"><a href="'.$item->url.'" title="'.$item->title.'"><span>'.$item->label.'</span></a></li>';
		}
		$output = '<ul class="nav-footer">'.$output.'</ul>';
	}
	echo $output;
	
	
	
	
				$nav1 = Pages::getCustomNav('footer2');
				if ($nav1->num_rows > 0) {
					while( $item = $nav1->fetch_object() ) {
						$i++;
						$linkstate = 'item navitem-'.$i;
						if ($item->slug == '') { $linkstate .= ' nav-home'; } else { $linkstate .= ' nav-'.$item->slug; }
						if (Page::$slug[1] == $item->slug) { $linkstate .= ' active'; } else { $linkstate .= ' inactive'; }
						if ($i == 1) {$linkstate .= ' item-first';}
						if ($i == $nav1->num_rows) {$linkstate .= ' item-last';}
						$output .= '<li class="'.$linkstate.'"><a href="'.$item->url.'" title="'.$item->title.'"><span>'.strtolower($item->label).'</span></a></li>';
					}
					echo '<ul class="nav">'.$output.'</ul>';
				}
				
				
				
*/




