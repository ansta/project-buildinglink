<?php

echo '<h1 class="page-title category-title">'.Page::$title.'</h1>';

// Content related to PAGES_COMMON module
if (Page::$page_type == 'common') {
	$zone_1 = Content::getZoneContent(Page::$id[0],1,'COMMON');
	if (strlen($zone_1) > 0) {
		echo ''.$zone_1.'';
	}
	
// Content related to PAGES module
} else {
	$zone_1 = Content::getZoneContent(Page::$id[0],1);
	if (strlen($zone_1) > 0) {
		echo ''.$zone_1.'';
	}
}

