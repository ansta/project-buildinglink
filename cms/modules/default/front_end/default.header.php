<div class="top-bar">
	<span class="shownav"><i class="fa fa-navicon"></i> Menu</span>
	<a class="tel" href="tel:+441206272020"><i class="fa fa-phone"></i> 01206 272020</a>
	<a class="email" href="mailto:sales@buildinglink.co.uk"><i class="fa fa-envelope"></i> sales@buildinglink.co.uk</a>
</div>

<!-- .header -->
<div class="header">
	
	<!-- .header-inner -->
	<div class="header-inner clearfix">
		
		<!-- .logo -->
		<div class="logo">
			<?php 
				echo '<a href="/">';
					echo '<span class="logo-icon"><img src="/image.php?i=/sites/uploads/image/'.Site::$sitedata['id'].'/original/'.Site::$sitedata['image'].'" alt="'.Site::$sitedata['title'].'" style="width:70px;height:70px;"></span>';
					echo '<span class="logo-text">'.Site::$sitedata['title'].'</span>';
				echo '</a>';
			?>
		</div>
		<!-- /.logo -->
		
		<!-- .cart -->
		<div class="cart">
			<?php Page::getElement('widget','baskets','status'); ?>
		</div>
		<!-- /.cart -->
		
	</div>
	<!-- /.header-inner -->
	
	<!-- .header-bottom -->
	<div class="header-bottom clearfix">
		
		<!-- .testimonial -->
		<div class="testimonial">
			<?php Page::getElement('widget','testimonials','default'); // Testimonials has slightly changes, check the widget file for details ?>
		</div>
		<!-- /.testimonial -->
		
		<!-- .banner -->
		<div class="banner">
			<?php Page::getElement('Banner'); ?>
		</div>
		<!-- /.banner -->
		
	</div>
	<!-- /.header-bottom -->
	
	<!-- .primary-menu-ba -->
	<div class="primary-menu-bar clearfix">
		
		<?php Page::getElement('Primarynav'); ?>
		
		<?php Page::getElement('Search'); ?>
		
	</div>
	<!-- /.primary-menu-bar -->

</div>
<!-- /.header -->