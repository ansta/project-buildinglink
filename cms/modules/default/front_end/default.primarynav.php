
<?php
	$nav1 = Nav::getList();
	if ($nav1->num_rows > 0) {
		while( $item = $nav1->fetch_object() ) {
			$i++;
			$linkstate = 'item navitem-'.$i;
			if ($item->slug == '') { $linkstate .= ' nav-home'; } else { $linkstate .= ' nav-'.$item->slug; }
			if (Page::$slug[1] == $item->slug) { $linkstate .= ' active'; } else { $linkstate .= ' inactive'; }
			if ($i == 1) {$linkstate .= ' item-first';}
			if ($i == $nav1->num_rows) {$linkstate .= ' item-last';}
			$output .= '<li class="'.$linkstate.'"><a href="'.$item->url.'" title="'.$item->title.'"><span>'.strtolower($item->label).'</span></a></li>';
		}
		echo '<ul class="nav">'.$output.'</ul>';
	}
	
	
/*
<ul class="nav">
	<li class="item navitem-1 nav-home active item-first"><a href="#" ><span>Home</span></a></li>
	<li class="item navitem-2 nav-dates-and-prices inactive"><a href="#"><span>Special Offers</span></a></li>
	<li class="item navitem-3 nav-destinations inactive"><a href="#" ><span>Quote</span></a></li>
	<li class="item navitem-4 nav-programme inactive"><a href="#"><span>How to Order</span></a></li>
	<li class="item navitem-5 nav-expert-team inactive"><a href="#"><span>Blog</span></a></li>
	<li class="item navitem-6 nav-reviews inactive item-last"><a href="#"><span>Contact Us</span></a></li>
</ul>
*/