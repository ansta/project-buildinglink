<?php

$items = Banners::getList();
if ($items->num_rows > 0) {
	$banner_list = '';
	while ($item = $items->fetch_object()) {
		$banner_list .= PHP_EOL;
		$banner_list .= '<li class="slide '.$css_count_text_rows.' banner-'.$item->banner_type.'">';
			$banner_list .= '<img src="/image.php?i=/banners/uploads/image/'.$item->id.'/gallery/'.$item->image.'" alt="'.$item->title.'">';
		$banner_list .= '</li>';
	}
	echo PHP_EOL.'<div class="flexslider carousel">';
		echo PHP_EOL.'<ul id="banner" class="slides">';
			echo $banner_list;
		echo PHP_EOL.'</ul>';
	echo PHP_EOL.'</div>';
}
