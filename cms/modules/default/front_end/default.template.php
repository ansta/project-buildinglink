<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en-US" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if ( IE 7 )&!( IEMobile )]><html lang="en-US" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if ( IE 8 )&!( IEMobile )]><html lang="en-US" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"><!--<![endif]-->

<head>

	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" /> 
	<?php Meta::getTag('title','tag',true,' - '.Site::$sitedata['title']); ?>
	<?php Meta::getTag('description'); ?>
	<?php Meta::getTag('keywords'); ?>
	<?php Meta::getTag('canonical'); ?>
	
	<?php
		if (Module::$name == 'blogs' && Module::$depth == 1) {
			$item = Blogs::$view_item_object; 
			// Prepare the product image
			if (file_exists(SYSTEM_ROOT.'modules/blogs/uploads/image/'.$item->id.'/list/'.$item->image)) {
				$item_image = '/image.php?i=/blogs/uploads/image/'.$item->id.'/list/'.$item->image.'';
				echo '<meta property="og:image" content="'.$item_image.'" />';
			}
		}
		
		if (Module::$name == 'products' && Module::$depth == 1) {
			$item = Products::$view_item_object; 
			// Prepare the product image
			if (file_exists(SYSTEM_ROOT.'modules/products/uploads/image/'.$item->id.'/gallery/'.$item->image)) {
				$item_image = '/image.php?i=/products/uploads/image/'.$item->id.'/gallery/'.$item->image.'';
				echo '<meta property="og:image" content="'.$item_image.'" />';
			}
		}
	?>
	
	<!-- google chrome frame for ie -->
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<?php
	echo '<link rel="stylesheet" id="normalize-css" href="/css/normalize.css?v='.filemtime(PUBLIC_ROOT.'/css/normalize.css').'" type="text/css" media="all" />'.PHP_EOL;
	echo '<link rel="stylesheet" id="style-css" href="/css/font-awesome.min.css?v='.filemtime(PUBLIC_ROOT.'/css/font-awesome.min.css').'" type="text/css" media="all" />'.PHP_EOL;
	echo '<link rel="stylesheet" id="style-css" href="/css/style.css?v='.filemtime(PUBLIC_ROOT.'/css/style.css').'" type="text/css" media="all" />'.PHP_EOL;
	?>
	
	<style type="text/css">
		body { background: <?php echo Site::$sitedata['color2']!=''?Site::$sitedata['color2']:'#000'; ?>; }
		
		.quote:after {
			border-top-color: <?php echo Site::$sitedata['color1']!=''?Site::$sitedata['color1']:'#B21A2D'; ?>;
		}
		
		.widget-nav .children a { border-left-color: <?php echo Site::$sitedata['color1']!=''?Site::$sitedata['color1']:'#B21A2D'; ?>; }
			
		.btn,
		.button,
		.quote-bg,
		.primary-menu-bar,
		.content-sidebar-bg,
		.widget-nav a span:before,
		.widget-nav .active > a,
		.widget-nav .active > a:hover,
		.info-box.alt .overlay,
		.box-icon,
		.widget-quick-links li:before,
		.btn:hover,
		input[type="submit"] {
			background: <?php echo Site::$sitedata['color1']!=''?Site::$sitedata['color1']:'#B21A2D'; ?>;
		}
		
		.recent-posts .post .entry-title,
		.primary-menu-bar .nav .active a,
		.primary-menu-bar .nav a:hover,
		.icon-search,
		h1,
		.content-inner h1,
		.content-inner h2,
		.content-inner h3,
		.content-inner h4,
		.content-inner h5,
		.content-inner h6,
		.content-inner a,
		.content a,
		.page-title,
		.content-sidebar .widget-title,
		.recent-posts .post .entry-title,
		.entry-title a,
		.more-link,
		.widget-title,
		.box span,
		.blog-comments h2,
		.comment-meta strong,
		.commentform legend,
		.price-break  {
			color: <?php echo Site::$sitedata['color1']!=''?Site::$sitedata['color1']:'#B21A2D'; ?>;
		}
	</style>

</head>
<?php
if(isset($_GET["referer"])){
	//$_SESSION["link_hash"] = $_GET["referer"];
	//db::link()->query("INSERT INTO baskets_linked(basket1, basket2) VALUES('".$_GET["referer"]."','".session_id()."')"); 
	//session_id($_GET["referer"]);
	$_SESSION["basket"] = $_GET["referer"];
} 

$body_class = '';
if (Module::$name != '') { $body_class .= Module::$name; }
if (User::isAdmin()) { $body_class .= ' is_admin'; }
?>
<body class="<?php echo $body_class; ?>">
	
	<!-- .container -->
	<div class="container">
		
		<?php Page::getElement('Header'); ?>
		
		<!-- .main -->
		<div class="main clearfix">
			
			<!-- .content -->
			<div class="content">
				<?php Page::getElement('Primary'); ?>
			</div>
			<!-- /.content -->

			<!-- .sidebar -->
			<div class="sidebar">
				<?php Page::getElement('Secondary'); ?>
			</div>
			<!-- /.sidebar -->
			
		</div>
		<!-- /.main -->
		
		<!-- .footer -->
		<div class="footer clearfix">
			<?php Page::getElement('Footer'); ?>
		</div>
		<!-- /.footer -->
		
	</div>
	<!-- /.container -->
	
	<?php Admin::getElement('Nav'); ?>
	<?php Page::getElement('Scripts'); ?>
	
</body>
</html>
