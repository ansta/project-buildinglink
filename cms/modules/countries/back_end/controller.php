<?php

Admin::allowView(0);
Admin::allowEdit(1);

if ($_SESSION['id'] == 1) {
	Admin::allowNew(1);
	Admin::allowDelete(1);
} else {
	Admin::allowNew(0);
	Admin::allowDelete(0);
}

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	Core::setModuleTask('default'); // Optional to specify default as it would be default anyway!
	
} elseif (Page::$slug[3] == 'new') {
	Core::setModuleTask('new');
	Form::prefillValues();
	
} elseif (Page::$slug[3] == 'edit' && Page::$slug[4] > 0) {
	Core::setModuleTask('edit');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'view' && Page::$slug[4] > 0) {
	Core::setModuleTask('view');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'delete' && Page::$slug[4] > 0) {
	Core::setModuleTask('delete');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'create' && $_SESSION['id'] == 1) {
	Setup::createNewTable(Page::$slug[2]);
	header('Location: /'.Page::$slug[1].'/'.Page::$slug[2].'/');
	
} else {
	Error::type(404);
}