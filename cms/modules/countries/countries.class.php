<?php

class Countries {
	
	public static 	$vat_notice = array();
	
	/*
	* 
	*/
	public static function getSelect() {
		
		$version_control = Config::$settings['countries']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`countries` a
		";
		$query .= "
			WHERE
				a.active = 1
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND	a.ttv_end IS null";
		}
		$query .= "
			ORDER BY
				a.`pos` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			while ($item = $items->fetch_object()) {
				$output[$item->id] = $item->title;
			}
			return $output;
		} else {
			return FALSE;
		}
	}
	
	/*
	* 
	*/
	public static function getItem($item_id,$column_name) {
		$version_control = Config::$settings['countries']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`countries` a
			WHERE
				a.id > 0
			AND a.id = ".db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS NULL";
		}
		$query .= " LIMIT 1";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if($item = $items->fetch_object()) {
				return $item->$column_name;
			}
		}
		return FALSE;
	}
	
	/*
	* 
	*/
	public static function getDeliveryCost($country_id,$country_vat) {
		// Ignore the 2 parameters sent through to this function for now.
		// Shipping charges are Site based rather than Country based.
		// But rather than create a new function, we are doing the calculation here
		// in order to make it easy to change at a later date if necessary.
		
		// We were just pulling the delivery charge straight out of the Sites table
		// but because all the sites share a same basket, this gave shoppers the opportunity
		// to checout on a site with free carriage even if the items they ordered were set
		// to have delivery charges applied because of that sites setings.
		#$delivery_charge = Site::$sitedata['delivery_charge'];
		
		// So instead:
		// Read the list of products in the shopping basket and retrieve a list of SITE_ID's
		// Then check each site and look for the one with the most expensive delivery charge:
		// This is the delivery charge to be applied to the checkout.
		
		$query = "
			SELECT
				s.incvat
			,   s.delivery_charge
			FROM
				`baskets` b
			LEFT JOIN
				`products` p ON p.id = b.item_id AND p.ttv_end IS null
			LEFT JOIN
				`sites` s ON s.id = p.site_id AND s.ttv_end IS null
			WHERE
				b.`ttv_end` IS null
			AND	b.basket_hash = '".db::link()->real_escape_string($_SESSION['basket'])."'
			GROUP BY
				p.site_id
			ORDER BY 
				/* Multiplying by 100 to make sure it's an INT and to make the order-by easier) */
				(s.delivery_charge * 100) DESC
			LIMIT
				1
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if($item = $items->fetch_object()) {
				$delivery_charge= $item->delivery_charge;
				$incvat			= $item->incvat;
			}
		}
		
		
		if (DB_PRICES_INCLUDE_VAT == true) {
			$calculated_vat = ($delivery_charge - ($delivery_charge / $country_vat['divider']));
			$delivery['exc_vat'] = $delivery_charge - $calculated_vat;
			$delivery['vat'] = $calculated_vat;
			$delivery['inc_vat'] = $delivery_charge;
		} else {
			$delivery['exc_vat'] = $delivery_charge;
			$delivery['vat'] = (($delivery_charge / 100) * $country_vat['percent']);
			$delivery['inc_vat'] = ($delivery_charge + $delivery['vat']);
		}
		
		return $delivery;
	}
	
	
	/*
	public static function getDeliveryCost($country_id) {
		
		// Get VAT rate based on shipping data
		$master_vat = self::getMasterVatRate($country_id);
		
		// Get VAT rate based on shipping data
		$vat_rate = self::getVatRate($country_id);
		
		// Count how many items are in the basket
		$basket_items = Baskets::getItems();
		$total_number_of_items_in_basket = 0;
		if ($basket_items->num_rows > 0) {
			while ($basket_item = $basket_items->fetch_object()) {
				$total_number_of_items_in_basket = $total_number_of_items_in_basket + $basket_item->quantity;
			}
		}
		
		// Lookup the delivery country
		$version_control = Config::$settings['countries']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`countries` a
			WHERE
				a.`id` = ".db::link()->real_escape_string($country_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS NULL";
		}
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if($item = $items->fetch_object()) {
				// Get price to default destination (UK)
				$unit_delivery_cost = $item->delivery;
				
				// Calculate the VAT included in the above price
				$unit_delivery_master_vat	= $unit_delivery_cost - ($unit_delivery_cost / $master_vat['divider']);
				
				// Calculate the delivery price excluding VAT
				$unit_delivery_excluding_vat	= $unit_delivery_cost - $unit_delivery_master_vat;
				
				// Add back on any VAT applicable to the destination country
				if ($vat_rate['percent'] > 0) {
					$unit_delivery_local_vat	= ($unit_delivery_excluding_vat / 100) * $vat_rate['percent'];
				} else {
					$unit_delivery_local_vat	= 0;
				}
				
				// Multiply the resulting delivery charge by the number of items in the basket
				$total_delivery_cost_excluding_local_vat = ($unit_delivery_excluding_vat * $total_number_of_items_in_basket);
				$total_delivery_cost_including_local_vat = (($unit_delivery_excluding_vat + $unit_delivery_local_vat) * $total_number_of_items_in_basket);
				
				//Return the data combinations as an array
				$output = array(
					'exc_vat'	=> $total_delivery_cost_excluding_local_vat
				,	'vat'		=> $total_delivery_cost_including_local_vat - $total_delivery_cost_excluding_local_vat
				,	'inc_vat'	=> $total_delivery_cost_including_local_vat
				);
				return $output;
			}
		}
	}
	*/
	
	
	/*
	* 
	*/
	public static function getMasterVatRate() {
		$version_control = Config::$settings['countries']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`countries` a
			WHERE
				a.`basecountry` = 1
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS NULL";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if($item = $items->fetch_object()) {
				$output['percent'] = $item->tax;
				$output['divider'] = ($item->tax + 100) / 100;
				return $output;
			}
		}
	}
	
	/*
	* 
	*/
	public static function getVatRate($country_id) {
		$version_control = Config::$settings['countries']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`countries` a
			WHERE
				a.`id` = ".db::link()->real_escape_string($country_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS NULL";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if($item = $items->fetch_object()) {
				$output['percent'] = $item->tax;
				$output['divider'] = ($item->tax + 100) / 100;
				return $output;
			}
		}
	}
	
	/*
	* 
	*/
	public static function getDisplayVatRate() {
		// Get VAT rate for the UK which is currently ID 256
		return self::getVatRate(256);
	}
	
	/*
	* 
	*/
	public static function getDisplayPrice($price) {
		
		// Check to see if prices should include VAT or not when displayed in the shop
		if (Site::$sitedata['incvat'] == 1) {
			$vat_array = self::getDisplayVatRate();
			$vat_amount = (($price / 100) * $vat_array['percent']);
			$price = ($price + $vat_amount);
			self::$vat_notice['standard'] = 'Prices include VAT at UK standard rate of '.$vat_array['percent'].'%';
			self::$vat_notice['inline'] = 'include VAT at UK standard rate of '.$vat_array['percent'].'%';
			self::$vat_notice['short'] = 'inc. VAT';
		} else {
			self::$vat_notice['standard'] = 'Excluding VAT';
			self::$vat_notice['inline'] = 'excluding VAT';
			self::$vat_notice['short'] = 'exc. VAT';
		}
		
		return $price;
	}
	
	/*
	* 
	*/
	public static function getVatNotice($format = 'standard') {
		if ($format == 'inline') {
			return self::$vat_notice['inline'];
		} elseif ($format == 'short') {
			return self::$vat_notice['short'];
		}
		return self::$vat_notice['standard'];
	}
	
	/*
	* 
	*/
	public static function getCurrencySymbol($country_id = 256) {
		return '&pound;';
	}
	
	#	getCurrencySymbol();'&pound;'
	/*
	* 
	*/
	public static function getNav() {
		
	}
	
	/*
	* 
	*/
	public static function getTags() {
		
	}
}