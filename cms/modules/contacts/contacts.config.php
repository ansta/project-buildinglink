<?php
/* 
 * 
 * 
 * 
 * 
 * 
 * 
 */
self::$settings['contacts'] =
array(
	'orderby'=>'auto_id DESC'
,	'singular'=>'Contact message'
,	'plural'=>'Contact messages'
,	'version_control'=>'audit'
,	'title'=>array(
		0	=>	'-- please select --'
	,	1	=>	'Mr.'
	,	2	=>	'Mrs.'
	,	3	=>	'Miss'
	,	4	=>	'Ms'
	)
,	'subject'=>array(
	#	0	=>	'-- please select --'
		2	=>	'Get a Quote'
	,	1	=>	'General Enquiry'
	)
,	'in_admin_menu'=>true
);

self::$fields['contacts'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'ID',						'protected'=>false,	'hint'=>'')
,	array('name'=>'title',			'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Title',					'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'firstname',		'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'First Name',				'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'lastname',		'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Last Name',				'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'email',			'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Email address',			'protected'=>false,	'hint'=>'')
,	array('name'=>'subject',		'required'=>false,	'list'=>true,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'select',	'format'=>'i',		'label'=>'Subject',					'protected'=>false,	'hint'=>'')
,	array('name'=>'message',		'required'=>true,	'list'=>false,	'unique'=>false,	'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',		'label'=>'Message',					'protected'=>false,	'hint'=>'',	'editor'=>false)
);


#,	'regex'=>'/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/'



