<?php

class Contacts {
	
	/*
	* 
	public static function getList() {
		
	}
	*/
	
	
	
	/*
	* This function produces the default data list for the module.
	* Whether it be pages or documents or news or anything, by default
	* this function will produce a table style list allowing admins to
	* Create new and edit, copy or delete existing records.
	* 
	* Simply copy this funtion into your custom module to create your own version.
	* 
	* Todo:
	* - Make it simpler and more flexible
	* - improve the drag and drop reordering to cater for depth and parent
	*/
	public static function getData($search = '') {
		
		// Validate the table (ie: module) name
		$table_name = Security::checkTableList(Page::$slug[2],__FILE__.' on line '.__LINE__);
		
		if ($table_name != '') {
			// Start building the SELECT for this list
			$query = "
				SELECT
					*
				FROM
					`".$table_name."`
				WHERE
					id > 0
			";
			
			// Hide record 1 from anyone but superusers in the users table
			if ($_SESSION['id'] > 1 && $table_name == 'users') {
				$query .= " AND id != 1";
			}
			
			// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
			$filter_by_site_id = false;
			foreach (Config::$fields[$table_name] AS $field) {
				if ($field['name'] == 'site_id') {
					$filter_by_site_id = true;
				}
			}
			if ($filter_by_site_id) {
				$query .= " AND site_id = ".(int)db::link()->real_escape_string($_SESSION['active_site_id']);
			}
			
			// If version control is in place for this table then we must only show the current records
			$version_control = Config::$settings[$table_name]['version_control'];
			if($version_control == 'audit' || $version_control == 'full') {
				$query .= " AND ttv_end IS NULL";
			}
			
			if(trim($search) != '') {
				$query .= " AND ".$search;
			}
			
			// List ordering
			$query_orderby = '';
			if(Config::$settings[$table_name]['orderby'] && !isset($_GET['sort'])) {
				$query_orderby = Config::$settings[$table_name]['orderby'];
			}
			if (isset($_GET['sort']) && isset($_GET['dir'])) {
				if($_GET['dir'] == 'asc' OR $_GET['dir'] == 'desc') {
					$query_orderby = $_GET['sort'].' '.$_GET['dir'];
				}
			}
			if ($query_orderby != '') {
				$query = $query.' ORDER BY '.$query_orderby; 
			}
			
			$query = $query.' LIMIT 100 '; 
			
			// Query the database
			$items = db::link()->query($query);
			if ($items->num_rows > 0) {
				return $items;
			}
		}
		return FALSE;
	}
	
}

