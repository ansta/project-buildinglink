<?php

Admin::allowNew(0);
Admin::allowEdit(0);
Admin::allowDelete(0);
Admin::allowView(1);

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	
} elseif (Page::$slug[3] == 'new') {
	Error::type(404);
		
} elseif (Page::$slug[3] == 'edit' && Page::$slug[4] > 0) {
	Error::type(404);
	
} elseif (Page::$slug[3] == 'view' && Page::$slug[4] > 0) {
	Core::setModuleTask('view');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'create' && $_SESSION['id'] == 1) {
	Setup::createNewTable(Page::$slug[2]);
	header('Location: /'.Page::$slug[1].'/'.Page::$slug[2].'/');
	
} elseif (Page::$slug[3] == 'delete' && Page::$slug[4] > 0) {
	Core::setModuleTask('delete');
	
} else {
	Error::type(404);
}
