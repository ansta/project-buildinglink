<?php

$submission = TRUE;

$response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_PRIVATE."&response=".$_REQUEST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']));

if ($response->success == false) {
	$_SESSION['form']['form-error'] = "reCaptcha error, please click on the check box and follow instructions if asked.";
	$submission = FALSE;
}

foreach (Config::$fields['contacts'] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}

//akismet content filter
require SYSTEM_ROOT.'libraries/akismet.class.php';
$comment = array(
                'comment_author' => Config::$settings['contact']['title'][(int)$_REQUEST['title']].' '.filter_var($_REQUEST['firstname'], FILTER_SANITIZE_STRING).' '.filter_var($_REQUEST['lastname'], FILTER_SANITIZE_STRING),//'viagra-test-123' will always return as spam
                'comment_email' => filter_var($_REQUEST['email'], FILTER_SANITIZE_STRING),
                'comment_body' => $_POST['message'],
                'permalink' => $_SERVER['SERVER_NAME'].Page::$slug[0]
);
$akismet = new Akismet('http://buildinglink.co.uk/','36a150f07f3d',$comment);
if($akismet->errorsExist()) { // Returns true if any errors exist.
        die('Akismet returned error with error: '.$akismet->getError());
}

if ($submission) {
	$result = Form::processData();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		
		$_SESSION['form']['form-success'] = 'Thank you, your message has been sent. We will respond to your query shortly.';
		
		$subject = 'Building Link website contact form message';
		$html  = '<p>You have been sent a message via the contact form on the '.Site::$sitedata['title'].' website.';
		$html .= PHP_EOL.'<br />The content of this message is as follows:</p>';
		$html .= PHP_EOL.'<p>Name: <strong>'.Config::$settings['contact']['title'][(int)$_REQUEST['title']].' '.filter_var($_REQUEST['firstname'], FILTER_SANITIZE_STRING).' '.filter_var($_REQUEST['lastname'], FILTER_SANITIZE_STRING).'</strong>';
		$html .= PHP_EOL.'<br>Email: <strong>'.filter_var($_REQUEST['email'], FILTER_SANITIZE_STRING).'</strong>';
		$html .= '</p>'.PHP_EOL."\r\n";
		$html .= nl2br(strip_tags($_POST['message']));
		
		if($akismet->isSpam()) {
			//do something if its spam
			#	Email::sendEmail('mat@ansta.co.uk','SPAM:'.$subject,$html);
		}
		else {
			Email::send(Site::$sitedata['email_contact'], $subject, $html, array('neilbeech14@outlook.com'));
//			Email::send('radek@ansta.co.uk', $subject, $html);
		};
		
	#	Email::sendEmail('loki.ansta@gmail.com',$subject,$html);
		
		header("Location: ".Page::$slug[0]);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}


