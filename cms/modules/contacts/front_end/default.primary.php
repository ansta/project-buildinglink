<?php
	echo '<h1>'.Page::$title.'</h1>';
	Page::showErrors();
?>	
	
	<form action="" method="post" class="page-form">
		<fieldset>
			<legend>Contact Form</legend>
	<?php	#Form::setErrorMessagePosition('inline');
			foreach (Config::$fields['contacts'] AS $field) {
			#	if ($field['name'] == 'subject') {
			#		echo Form::makeField(array('formtype'=>'hidden','name'=>$field['name'],'value'=>'2'));
			#	} else {
					Form::setValue($field['name']);
					echo Form::makeField($field);
			#	}
			}
			echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'new'));
			echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>'contacts'));
			echo '<div class="g-recaptcha" data-sitekey="'.RECAPTCHA_PUBLIC.'"></div>';
			echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Send'));
		#	echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Submit','image'=>'/images/btn-send.jpg'));
	?>	</fieldset>
	</form>
