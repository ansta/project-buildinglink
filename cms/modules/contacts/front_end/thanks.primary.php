<div class="inner-main">
<?php
	$zone_11 = Content::getZoneContent(Page::$id[0],11);
	if (strlen($zone_11) > 0) {
		echo '<div class="box box-before box-wysiwyg clearfix">'.$zone_11.'</div>';
	}
	$zone_12 = Content::getZoneContent(Page::$id[0],12);
	if (strlen($zone_12) > 0) {
		echo '<div class="box box-before box-wysiwyg clearfix">'.$zone_12.'</div>';
	}
?>
</div>

<div class="inner-side">
	<?php
	$zone_6 = Content::getZoneContent(Page::$id[0],16);
	if (strlen($zone_6) > 0) {
		echo '<div class="minibox first-minibox box-wysiwyg clearfix"><div class="inner">'.$zone_6.'</div></div>';
	}
	?>
	<?php
	$zone_7 = Content::getZoneContent(Page::$id[0],17);
	if (strlen($zone_7) > 0) {
		echo '<div class="minibox box-wysiwyg clearfix"><div class="inner">'.$zone_7.'</div></div>';
	}
	?>
	<?php
	$zone_8 = Content::getZoneContent(Page::$id[0],18);
	if (strlen($zone_8) > 0) {
		echo '<div class="minibox box-wysiwyg clearfix"><div class="inner">'.$zone_8.'</div></div>';
	}
	?>
</div>