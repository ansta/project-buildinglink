<?php
class Attachments {
	
	public static $attached;
	public static $values;
	
	/*
	*
	*/
	public static function getAttachments($data = ''){
		self::$values = $data;	
		$arr = Config::$settings[Module::$name];
		self::$values['module'] = Module::$name;
		$i = 0;
		// Check if something is attached
		if (is_array($arr['attachments'])) {
			foreach($arr['attachments'] as $attachment) {
				echo self::makeField($attachment,$i++);
			}
		}
	}
	
	/*
	*
	*/
	public static function makeField($attachment_arr,$i = 0) {  
		$error_css_class = ' error-false';
		$output  = '<div class="field type-'.$attachment_arr['formtype'].$error_css_class.'">'.PHP_EOL;
		$output .= self::makeLabel($attachment_arr);
		$output .= self::makeInput($attachment_arr,$i);
		$output .= "\t".'<span class="hint hint-'.$field['formtype'].'">'.$attachment_arr['hint'].'</span>'.PHP_EOL;
		$output .= '</div>';
		$output .= PHP_EOL;
		$output .= PHP_EOL;
		return $output;
	}
	
	/*
	*
	*/
	public static function makeLabel($field) { 
		$output  = "\t";
		$output .= '<label for="in_'.$field['name'].'">';
		$output .= $field['label'];
		$output .= '</label>'.PHP_EOL;
		return $output;
	}
	
	/*
	*
	*/
	public static function makeInput($field,$i) { 
		if(self::$values['id'] != '') {
			self::$attached = self::getAttachedIds($field['name']);
		}
		if ($field['formtype'] == 'multi') {
			$output = '<select name="'.$field['name'].'[]" id="in_'.$field['name'].'" multiple="multiple">';
		} elseif ($field['formtype'] == 'single') {
			$output = '<select name="'.$field['name'].'" id="in_'.$field['name'].'">';
		}
		$output .= self::getOptions($field,$i).'</select>';
		return "\t".'<div class="input">'.$output.'</div>'.PHP_EOL;
	}
	
	/*
	*
	*/
	public static function getAttachedIds($attached_name){
		$query = "
			SELECT
				attachment_id
			FROM
				attachments
			WHERE
				ttv_end IS null
				AND module_id='".db::link()->real_escape_string(self::$values['id'])."'
				AND module_name ='".db::link()->real_escape_string(self::$values['module'])."'
				AND attachment_name ='".db::link()->real_escape_string($attached_name)."'
			";
		$items = array();
		$qry =db::link()->query($query);
		if($qry->num_rows>0) {
			while( $result = $qry->fetch_object()) {
				$items[] = $result->attachment_id;
			}
			return $items;
		}
		return NULL;
	}
	
	/*
	*
	*/
	public static function getOptions($field,$i){
		// Set the name of the database table to query
		$source_table = Config::$settings[Module::$name]['attachments'][$i]['source'];
		$source_tbl = ucfirst(strtolower(Security::checkTableList($source_table)));
	#	echo $source_tbl;
		if (method_exists($source_tbl,'getAdminAttachmentOptions')) {
			return call_user_func_array(array($source_tbl, 'getAdminAttachmentOptions'), array($field,$i));
		} else {
			return self::getDefaultAdminAttachmentOptions($field,$i);
		}
	}
	
	/*
	*
	*/
	public static function getDefaultAdminAttachmentOptions($field,$i){
		
		// Set the name of the database table to query
		$source_table = Config::$settings[Module::$name]['attachments'][$i]['source'];
		$source_tbl = Security::checkTableList($source_table);
		
		// Get any custome WHERE statement if specififed
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['where'])) {
			$where = Config::$settings[Module::$name]['attachments'][$i]['where'];
		} else {
			$where = '';
		}
		
		// Get any custom ORDER BY statement if specififed
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['orderby'])) {
			$hasdepth = Config::$settings[Module::$name]['attachments'][$i]['orderby'];
		} else {
			$hasdepth = '';
		}
		
		// Check to see if hasdepth has been specified
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['hasdepth'])) {
			$hasdepth = Config::$settings[Module::$name]['attachments'][$i]['hasdepth'];
		} else {
			$hasdepth = false;
		}
		
		// Prepare the columns to fetch in the query
		if ($hasdepth) {
			$columns = "
				id
			,	title
			,	depth
			";
		} else {
			$columns = "
				id
			,	title
			";
		}
		
		// Start building the SQL query
		$query = "
			SELECT
				".$columns."
			FROM
				".$source_tbl."
			WHERE
		";
		if ($where != '') {
			$query .= $where;
		} else {
			$query .= "	ttv_end IS null";
		}
		if ($orderby != '') {
			$query .= "	ORDER BY ".$orderby;
		} else {
			$query .= "	ORDER BY title ASC";
		}
		
		//Query the database and return the data
		$items = db::link()->query($query);
		$output = '';
		if ($items->num_rows > 0) {
			while ($item = $items->fetch_object()) {
				// Mark any that need to be pre-selected
				if(is_array(self::$attached) && in_array($item->id, self::$attached)) {
					$sel[$item->id] = 'selected="selected"';
				} else {
					$sel[$item->id] = '';
				}
				// Handle indenting for nested items
				if ($hasdepth) {
					$depth = '';
					for ($d=2;$d<=$item->depth;$d++) {
						$depth .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
				} else {
					$depth = '';
				}
				// Build up the list of options
				$output .= '<option value="'.$item->id.'" '.$sel[$item->id].'>&nbsp;&nbsp;'.$depth.$item->title.'</option>';
			}
		}
		return $output;
	}
	
	/*
	*
	*/
	public static function processData($id, $array_data) {
		if($array_data['id'] == NULL) {
			$array_data['id'] = $id;
		}
		$arr = Config::$settings[Module::$name];
		// Check if something is attached
		$i = 0;
		if(is_array($arr['attachments'])){
			foreach($arr['attachments'] as $attachment){
				self::saveAttachments($attachment, $array_data, $i++);
			}
		}
	}
	
	
	/*
	*
	*/
	public static function saveAttachments($attachments, $data, $i = 0) {
		// Only process the delete function on the first round of attachments
	#	if ($i == 0) {
			self::removeAttachments($data,$attachments);
	#	}
	#	if ($i > 0) {
	#		self::removeAttachments($data,$attachments);
	#	}
			
		$attachments_array = array();
		if(is_array($data[$attachments['name']])) {
			$attachments_array = $data[$attachments['name']];
		} else {
		   $attachments_array[$i] = $data[$attachments['name']];
		}
		foreach($attachments_array as $attachment) {
			if ($attachment > 0) {
				$attachment_data = array(
					'module' => 'attachments',
					'action' => 'new',
					'module_id' => $data['id'],
					'module_name' => $data['module'],
					'attachment_id' => $attachment,
					'attachment_name' => $attachments['name']
				);
#				echo '<pre>saveAttachments: '.$i;
#				var_dump($attachments);
#				var_dump($data);
#				var_dump($attachment_data);
#				
				$result = Form::processData('attachments',$attachment_data);
#				
#				
#				var_dump($result);
#				if ($attachments['name'] == 'tags') {
#					die();
#				}
			} else {
				// skip as without $attachment > 0 there is no point adding
			}
		}
	}
	
	
	/*
	*
	*/
	public static function removeAttachments($data,$attachments){
		$query = "
			UPDATE
				`attachments`
			SET
				ttv_end = NOW()
			,	modify_by = '".$_SESSION['firstname']."'
			,	modify_id = ".$_SESSION['id']."
			,	modify_action = 'update'
			WHERE
				module_id = '".db::link()->real_escape_string($data['id'])."'
			AND	module_name = '".db::link()->real_escape_string($data['module'])."'
			AND	attachment_name = '".db::link()->real_escape_string($attachments['name'])."'
		";
	#	echo $query;
	#	if($attachments['name'] != 'blogs_categories') {
	#		var_dump($data);
	#		var_dump($attachments);
	#		die();
	#	}
		db::link()->query($query); 
	}

}