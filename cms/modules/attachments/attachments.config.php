<?php
/* 
 * 
 * 
 * 
 * 
 * 
 * 
 */
self::$settings['attachments'] =
array(
	'orderby'=>'auto_id DESC'
,	'singular'=>'Attachment'
,	'plural'=>'Attachments'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
);

self::$fields['attachments'] =
array(
	array('name'=>'id',					'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'ID',				'protected'=>false,	'hint'=>'')
,	array('name'=>'module_id',			'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'s',		'label'=>'Module ID',		'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'module_name',		'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Module Name',		'protected'=>false,	'hint'=>'')
,	array('name'=>'attachment_id',		'required'=>true,	'list'=>false,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'s',		'label'=>'Attachment ID',	'protected'=>false,	'hint'=>'')
,	array('name'=>'attachment_name',	'required'=>true,	'list'=>false,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Attachment Name',	'protected'=>false,	'hint'=>'')
);

/*

CREATE TABLE attachments (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  `module_id` INT(11) DEFAULT NULL,
  `module_name` VARCHAR(255) DEFAULT NULL,
  `attachment_id` INT(11) DEFAULT NULL,
  `attachment_name` VARCHAR(255) DEFAULT NULL,
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;

*/