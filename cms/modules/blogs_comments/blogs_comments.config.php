<?php
/* 
 * 
 * 
 * 
 * 
 * 
 * 
 */
self::$settings['blogs_comments'] =
array(
	'orderby'=>'approved DESC, id DESC'
,	'singular'=>'Blog Comment'
,	'plural'=>'Blog Comments'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'approved'=>array(
		1=>'Approved'
	,	2=>'Pending'
	,	3=>'Rejected'
	)
,	'dashboard_count'=>Blogs_comments::newCommentsAwaitingApproval()
);

self::$fields['blogs_comments'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Comment ID',		'protected'=>false,	'hint'=>'')
,	array('name'=>'blog_id',	'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Post ID',			'protected'=>false,	'hint'=>'')
,	array('name'=>'title',		'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'Title',			'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'content',	'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',		'label'=>'Content',			'protected'=>false,	'hint'=>'')
,	array('name'=>'approved',	'required'=>false,	'list'=>true,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'select',	'format'=>'i',		'label'=>'Approval Status',	'protected'=>false,	'hint'=>'Do you approve the content of this comment and allow it to be visible to the public?', 'settings'=>'blogs_comments')
);

/*

CREATE TABLE blogs_comments (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  `blog_id` INT(11) NOT NULL DEFAULT 0,
  `user_id` INT(11) NOT NULL DEFAULT 0,
  `title` VARCHAR(255) DEFAULT NULL,
  `content` TEXT DEFAULT NULL,
  `approved` TINYINT(0) NOT NULL DEFAULT 0,
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;








,	'type'=>Articles_types::getSelectList()
,	array('name'=>'type',		'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'select',	'format'=>'i',		'label'=>'Article type',	'protected'=>false,	'hint'=>'',	'settings'=>'articles')

,	'avatar'=>array(
		'thumb'=>array('width'=>60,'height'=>60,'format'=>'jpg')
	,	'large'=>array('width'=>90,'height'=>90,'format'=>'jpg')
	)
,	'comments'=>array(
		2=>'Comments are not permitted'
	,	1=>'Users can post comments'
	)


*/