<?php

class Blogs_comments {

	private static 	$filter_query = '',
					$item_id = 0;
	public static 	$view_item_object;
	
	/*
	* 
	*/
	public static function getList($item_id) {
		$version_control = Config::$settings['blogs_comments']['version_control'];
		$query = "
			SELECT
				c.*
			FROM
				`blogs_comments` c
			WHERE
				c.`blog_id` = ".db::link()->real_escape_string($item_id)."
			AND	c.`approved` = 1
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND	c.ttv_end IS null";
		}
		$query .= "
			ORDER BY
				c.`id` DESC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* 
	*/
	public static function newCommentsAwaitingApproval() {
		$version_control = Config::$settings['blogs_comments']['version_control'];
		$query = "
			SELECT
				*
			FROM
				`blogs_comments`
			WHERE
				`approved` = 2
		";
	#	if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND	ttv_end IS null";
	#	}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items->num_rows.$version_control;
		} else {
			return 0;
		}
	}
	
	/*
	* 
	*/
	public static function commentCount($item_id,$separator) {
		$version_control = Config::$settings['blogs_comments']['version_control'];
		$query = "
			SELECT
				c.*
			,	u.firstname
			,	u.lastname
			FROM
				`blogs_comments` c
			LEFT JOIN
				`users` u ON c.`user_id` = u.`id` AND u.`ttv_end` IS null
			WHERE
				c.`blog_id` = ".db::link()->real_escape_string($item_id)."
			AND	c.`approved` = 1
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND	c.ttv_end IS null";
		}
		$query .= "
			ORDER BY
				c.`id` DESC
			LIMIT
				10
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			if ($items->num_rows > 1) { $plural = 's'; } else { $plural = ''; }
			return $items->num_rows.' comment'.$plural.'<span class="separator">'.$separator.'</span>';
		} else {
			return FALSE;
		}
	}

	
	/*
	* This function produces the default data list for the module.
	* Whether it be pages or documents or news or anything, by default
	* this function will produce a table style list allowing admins to
	* Create new and edit, copy or delete existing records.
	* 
	* Simply copy this funtion into your custom module to create your own version.
	* 
	* Todo:
	* - Make it simpler and more flexible
	* - improve the drag and drop reordering so that it activates when a POS column exists
	* - improve the drag and drop reordering to cater for depth and parent
	*/
	public static function getData($search = '') {
		
		// Validate the table (ie: module) name
		$table_name = Security::checkTableList(Page::$slug[2],__FILE__.' on line '.__LINE__);
		
		// Check to see if this module relies on a POS column
		if(Admin::fieldCheckExists($table_name,'pos')) {
			Admin::$haspos = TRUE;
			$table_js_id = 'sortable';
		} else {
			$table_js_id = 'notsortable';
		}
		
		// Start building the SELECT for this list
		$query = "SELECT * FROM `".$table_name."`";
		
		// If version control is in place for this table then we must only show the current records
		$version_control = Config::$settings[$table_name]['version_control'];
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " WHERE ttv_start <= NOW() AND ttv_end IS NULL";
		}
		
		// List ordering
		$query_orderby = '';
		if(Config::$settings[$table_name]['orderby'] && !isset($_GET['sort'])) {
			$query_orderby = Config::$settings[$table_name]['orderby'];
		}
		if (isset($_GET['sort']) && isset($_GET['dir'])) {
			if($_GET['dir'] == 'asc' OR $_GET['dir'] == 'desc') {
				$query_orderby = $_GET['sort'].' '.$_GET['dir'];
			}
		}
		if ($query_orderby != '') {
			$query = $query.' ORDER BY '.$query_orderby; 
		}
		
		// Query the database with the resulting query
		$items = db::link()->query($query);
		
		// Start building the output
		if ($items->num_rows) {
			
			$columns = Config::$fields[Module::$name];
			// Start with the list table header
			$output = '<table class="tbldata" id="'.$table_js_id.'">';
				$output .= '<tr class="nodrag nodrop">';
				foreach ($columns AS $field) {
					if ($field['list'] == true) {
						$output .= '<th class="tbltext">';
							$output .= $field['label'];
						$output .= '</th>';
					}
				}
				$header_colspan = 0;
				if (Admin::$allow['view'] == 1) { $header_colspan = $header_colspan + 1; }
				if (Admin::$allow['edit'] == 1) { $header_colspan = $header_colspan + 1; }
				if (Admin::$allow['delete'] == 1) { $header_colspan = $header_colspan + 1; }
				
				$output .= '<th colspan="'.$header_colspan.'" class="tblaction btn-new">';
					if (Admin::$allow['new'] == 1) {
						$output .= Admin::makeButton('new',0,Module::$name,'New '.Config::$settings[Module::$name]['singular']);
					}
				$output .= '</th>';
			$output .= '</tr>';
			
			// Continue building the data for each row
			while( $item = $items->fetch_object() ) {
				
				// used for tracking the row count
				$i++;
				
		#		// Specific to the USERS module: to prevent the SuperUser record from being edited by non-SuperUsers
		#		if ($item->id == 1 && Page::$slug[2] == 'users' && $_SESSION['id'] != 1) {
		#			self::allowEdit(0);
		#		} else {
		#			self::allowEdit(1);
		#		}
		
				// apply alternate row colouring
				if($i & 1) {
					$rowcss = 'row1';
				} else {
					$rowcss = 'row0';
				}
				
				// For depth fields (`pages` table only at the moment) visualise the representation of the depth
				if (Page::$slug[2] == 'pages') {
					$indent_string = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					if ($item->depth == 1) {
						$depth_indent = '';
					} elseif($item->depth == 2) {
						$depth_indent = $indent_string.$indent_suffix;
					} elseif($item->depth == 3) {
						$depth_indent = $indent_string.$indent_string.$indent_suffix;
					} elseif($item->depth == 4) {
						$depth_indent = $indent_string.$indent_string.$indent_string.$indent_suffix;
					} elseif($item->depth == 5) {
						$depth_indent = $indent_string.$indent_string.$indent_string.$indent_string.$indent_suffix;
					} else {
						$depth_indent = '';
					}
				}
				
				// Make sure that we use the correct ID column depending on whether or not this module has version control or not
				#$item_id = Core::getItemId($table_name,$item);
				$version_control = Config::$settings[$module]['version_control'];
				if($version_control == 'audit' || $version_control == 'full') {
					$item_id = $item->id;
				} else {
					if (in_array('auto_id', Config::$fields[$table_name])) {
						$item_id = $item->auto_id;
					} else {
						$item_id = $item->id;
					}
				}
				
				if ($item->approved == 1) {
					$rowcss .= ' comment-approved';
				} elseif ($item->approved == 2) {
					$rowcss .= ' comment-pending';
				} elseif ($item->approved == 3) {
					$rowcss .= ' comment-rejected';
				} else {
					$rowcss .= ' comment-unknown';
				}
				
				// output the HTML for the table
				$output .= '<tr class="'.$rowcss.'" id="'.$item_id.'">';
					foreach ($columns AS $field) {
						if ($field['list'] == true) {
							$output .= '<td class="tbltext">';
								if ($field['settings'] != '') {
									$output .= $depth_indent.Config::$settings[$field['settings']][$field['name']][$item->$field['name']];
								} else {
									$output .= $depth_indent.substr(strip_tags($item->$field['name']), 0, 80).'...';
								}
							$output .= '</td>';
						}
					}
					if (Admin::$allow['view'] == 1) {
						$output .= '<td class="tblaction btn-view">';
							$output .= Admin::makeButton('view',$item_id);
						$output .= '</td>';
					}
					if (Admin::$allow['edit'] == 1) {
						$output .= '<td class="tblaction btn-edit">';
							$output .= Admin::makeButton('edit',$item_id);
						$output .= '</td>';
					}
					if (Admin::$allow['delete'] == 1) {
						$output .= '<td class="tblaction btn-delete">';
						// If global delete is permitted, check for row specific permissions
						if (Admin::$allow['delete'] == 1) {
							if($item->locked) {
								Admin::allowDelete(0);
								$output .= Admin::makeButton('delete',$item_id);
								Admin::allowDelete(1);
							} else {
								$output .= Admin::makeButton('delete',$item_id);
							}
						} else {
							$output .= Admin::makeButton('delete',$item_id);
						}
						$output .= '</td>';
					}
				$output .= '</tr>';
			}
			$output .= '</table>';
		} else {
			$output = '<p>No data available yet.</p>';
			if (Admin::$allow['new'] == 1) {
				$output .= '<a href="new/">Click here to add your first item</a>';
			}
		}
		return $output;
	}
}

























