<?php

$submission = TRUE;

$_REQUEST['approved'] = 2;

// Standard form validation
foreach (Config::$fields['blogs_comments'] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}

if (trim($_REQUEST['title']) == '') {
	$_REQUEST['title'] = 'Anonymous';
}


// Implement reCAPTCHA form validation
require_once(SYSTEM_ROOT.'/libraries/recaptchalib.php');
$resp = null;
$error = null;
# was there a reCAPTCHA response?
if ($_POST["recaptcha_response_field"]) {
	$resp = recaptcha_check_answer (RECAPTCHA_PRIVATE,
									$_SERVER["REMOTE_ADDR"],
									$_POST["recaptcha_challenge_field"],
									$_POST["recaptcha_response_field"]);
	if ($resp->is_valid) {
		// All clear, reCAPTCHA data entered into the form is correct
	} else {
		// reCAPTCHA data entered into the form is incorrect so we must abort the submission
		$_SESSION['form']['field-error']['recaptcha'] = $resp->error;
		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}


if ($submission) {
	$result = Form::processData();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'Thank you, your comments has been sumbitted and will appear here shortly.';
		header("Location: ".str_replace('#form', '', Page::$slug[0]));
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}
