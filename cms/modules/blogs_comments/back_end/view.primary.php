<h1>View <?php echo Config::$settings[Page::$slug[2]]['singular']; ?><?php /*?>: <?php echo Form::getItemTitle(); ?><?php */?></h1>
	
<section>
	<?php
	$item = Admin::getItem(Page::$slug[4]);
	foreach (Config::$fields[Module::$name] AS $field) {
		
		if ($field['formtype'] == 'hidden' || $field['formtype'] == 'protected') {
		} else {
			// Cache the data as we need some items outside of this foreach loop
			$viewdata[$field['name']] = $item->$field['name'];
			
			// Display the record data
			echo '<p class="field">';
				echo '<span class="label">';
					echo $field['label'];
				echo '</span>: ';
				echo '<span class="value">';
					if ($field['settings'] != '') {
						echo Config::$settings[$field['settings']][$field['name']][$item->$field['name']];
					} else {
						echo htmlspecialchars($item->$field['name']);
					}
				echo '</span>';
			echo '</p>';
		}
	}
	// Can't look up ttv_start for the comment as that date changes when a comment is approved or rejected.
	// Need to look it up.
#	echo '<div class="field"><span class="label">Received</span>: <span class="value">'.Form::makeHumanDate($item->ttv_start).'</span></div>';
	
	if ($viewdata['approved'] == 1) { ?>
		<form action="" method="post">
			<fieldset>
				<legend></legend>
				<?php
					foreach (Config::$fields[Module::$name] AS $field) {
						if ($field['formtype'] == 'hidden' || $field['formtype'] == 'protected') {
						} else {
							echo Form::makeField(array('formtype'=>'hidden','name'=>$field['name'],'value'=>$item->$field['name']));
						}
					}
				?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'approved','value'=>3)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'edit')); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'id','value'=>Page::$slug[4])); ?>
				<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Reject')); ?>
			</fieldset>
		</form>
<?php } elseif ($viewdata['approved'] == 3) { ?>
		<form action="" method="post">
			<fieldset>
				<legend></legend>
				<?php
					foreach (Config::$fields[Module::$name] AS $field) {
						if ($field['formtype'] == 'hidden' || $field['formtype'] == 'protected') {
						} else {
							echo Form::makeField(array('formtype'=>'hidden','name'=>$field['name'],'value'=>$item->$field['name']));
						}
					}
				?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'approved','value'=>1)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'edit')); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'id','value'=>Page::$slug[4])); ?>
				<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Approve')); ?>
			</fieldset>
		</form>
<?php } else { ?>
		<form action="" method="post">
			<fieldset>
				<legend></legend>
				<?php
					foreach (Config::$fields[Module::$name] AS $field) {
						if ($field['formtype'] == 'hidden' || $field['formtype'] == 'protected') {
						} else {
							echo Form::makeField(array('formtype'=>'hidden','name'=>$field['name'],'value'=>$item->$field['name']));
						}
					}
				?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'approved','value'=>1)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'edit')); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'id','value'=>Page::$slug[4])); ?>
				<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Approve')); ?>
			</fieldset>
		</form>
		<form action="" method="post">
			<fieldset>
				<legend></legend>
				<?php
					foreach (Config::$fields[Module::$name] AS $field) {
						if ($field['formtype'] == 'hidden' || $field['formtype'] == 'protected') {
						} else {
							echo Form::makeField(array('formtype'=>'hidden','name'=>$field['name'],'value'=>$item->$field['name']));
						}
					}
				?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'approved','value'=>3)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'edit')); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
				<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'id','value'=>Page::$slug[4])); ?>
				<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Reject')); ?>
			</fieldset>
		</form>
<?php } ?>
<a href="../../delete/<?php echo Page::$slug[4]; ?>/">delete this comment completely</a>
<?php /*?>
	<form action="../../delete/<?php echo Page::$slug[4]; ?>/" method="post">
		<fieldset>
			<legend></legend>
			<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Delete')); ?>
		</fieldset>
	</form>
<?php */?>
</section>