<?php

class Blogs_categories {
	
	public static 	$filter_query = '',
					$item_id = 0;
	public static 	$view_item_object;
	
	
	/*
	* 
	*/
	public static function getFeaturedList() {
		$query = "
			SELECT
				*
			FROM
				blogs_categories t
			WHERE
				t.ttv_end IS null
            AND t.featured IS true
			ORDER BY
				t.title ASC
			LIMIT 3
		";
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return FALSE;
	}
	
	/*
	* 
	*/
	public static function getList() {
		$query = "
			SELECT
				c.*
			FROM
				blogs_categories c
	/*		LEFT JOIN
				attachments x ON x.attachment_id = c.id AND x.attachment_name = 'blogs_categories' AND x.ttv_end IS null
	*/		WHERE
				c.ttv_end IS null
			AND	c.site_id = ".db::link()->real_escape_string(Site::$sitedata['id'])."
    /*      AND x.id IS NOT null
	*/	";
		$query .= "
			GROUP BY
				c.id
			ORDER BY
				c.title ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return FALSE;
	}
	
	/*
	* 
	*/
	public static function checkItemExists($item_slug) {
		$version_control = Config::$settings['blogs_categories']['version_control'];
		$query = "
			SELECT
				t.*
			FROM
				`blogs_categories` t
			WHERE
				t.`slug` = '".db::link()->real_escape_string($item_slug)."'
			AND t.site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id'])."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND t.`ttv_end` IS NULL";
		}
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if($item = $items->fetch_object()) {
				self::$item_id = $item->id;
				self::$view_item_object = $item;
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/*
	* 
	*/
	public static function getArticleList($article_id) {
		$query = "
			SELECT
				b.id
			,   b.slug
			,   c.id
			,   c.title
			,   c.slug
			FROM
				blogs b
			LEFT JOIN
				attachments x ON x.module_name = 'blogs' AND x.attachment_name = 'blogs_categories' AND x.module_id = b.id AND x.ttv_end IS null
			LEFT JOIN
				blogs_categories c ON c.id = x.attachment_id AND c.ttv_end IS null
			where
				b.ttv_end is null
			AND b.id = ".(int)db::link()->real_escape_string($article_id)."
			GROUP BY
				c.id
			ORDER BY
				c.title ASC
		";
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return FALSE;
	}
	
	/*
	*
	*/
	public static function getAdminAttachmentOptions($field,$i){
		
		// Set the name of the database table to query
		$source_table = Config::$settings[Module::$name]['attachments'][$i]['source'];
		$source_tbl = Security::checkTableList($source_table);
		
		// Get any custome WHERE statement if specififed
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['where'])) {
			$where = Config::$settings[Module::$name]['attachments'][$i]['where'];
		} else {
			$where = '';
		}
		
		// Get any custom ORDER BY statement if specififed
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['orderby'])) {
			$hasdepth = Config::$settings[Module::$name]['attachments'][$i]['orderby'];
		} else {
			$hasdepth = '';
		}
		
		// Check to see if hasdepth has been specified
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['hasdepth'])) {
			$hasdepth = Config::$settings[Module::$name]['attachments'][$i]['hasdepth'];
		} else {
			$hasdepth = false;
		}
		
		// Prepare the columns to fetch in the query
		if ($hasdepth) {
			$columns = "
				`a`.`id`
			,	`a`.`title`
			,	`a`.`depth`
			,	`s`.`title` AS prefix
			";
		} else {
			$columns = "
				`a`.`id`
			,	`a`.`title`
			,	`s`.`title` AS prefix
			";
		}
		
		// Start building the SQL query
		$query = "
			SELECT
				".$columns."
			FROM
				".$source_tbl." `a`
			LEFT JOIN
				sites s
			ON	`s`.`id` = `a`.`site_id`
			AND	`s`.`ttv_end` IS null
			WHERE
		";
		if ($where != '') {
			$query .= $where;
		} else {
			$query .= "	`a`.`ttv_end` IS null";
		}
		if ($orderby != '') {
			$query .= "	ORDER BY ".$orderby;
		} else {
			$query .= "	ORDER BY a.title ASC";
		}
		
		//Query the database and return the data
		$items = db::link()->query($query);
		$output = '';
	#	echo $query;
		if ($items->num_rows > 0) {
			while ($item = $items->fetch_object()) {
				// Mark any that need to be pre-selected
				if(is_array(Attachments::$attached) && in_array($item->id, Attachments::$attached)) {
					$sel[$item->id] = 'selected="selected"';
				} else {
					$sel[$item->id] = '';
				}
				// Handle indenting for nested items
				if ($hasdepth) {
					$depth = '';
					for ($d=2;$d<=$item->depth;$d++) {
						$depth .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
				} else {
					$depth = '';
				}
				// Build up the list of options
				$output .= '<option value="'.$item->id.'" '.$sel[$item->id].'>&nbsp;&nbsp;'.$depth.$item->title.' ('.$item->prefix.')</option>';
			}
		}
		return $output;
	}
	
}