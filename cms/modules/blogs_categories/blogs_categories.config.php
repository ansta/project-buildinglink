<?php
/* 
 * 
 * 
 * 
 * 
 * 
 * 
 */
self::$settings['blogs_categories'] =
array(
	'orderby'=>'title ASC'
,	'singular'=>'Blog Category'
,	'plural'=>'Blog Categories'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'parent_module'=>'blogs'
,	'image'=>array(
		'admin'		=>array('width'=> 70,'height'=> 70,'format'=>'jpg','method'=>'crop')
	,	'list' 		=>array('width'=>230,'height'=>230,'format'=>'jpg','method'=>'crop')
	,	'gallery'	=>array('width'=>300,'height'=>300,'format'=>'jpg','method'=>'crop')
	)
,	'featured'=>array(
		0=>'-'
	,	1=>'Yes'
	)
);

self::$fields['blogs_categories'] =
array(
	array('name'=>'id',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'User ID',			'protected'=>false,	'hint'=>'')
,	array('name'=>'site_id','required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Site ID',			'protected'=>true,	'hint'=>'')
,	array('name'=>'title',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',			'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'slug',	'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'URL identifier',	'protected'=>false,	'regex'=>'/^[a-z0-9\-]*$/', 'hint'=>'Identifies this category within the URL structure.<br /><strong style="color:#955;">Once set, it is strongly advised NOT to change it in the future as this can affect your <abbr title="Search Engine Optimisation">SEO</abbr> page ranking.</strong>')
,	array('name'=>'featured','required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Featured',		'protected'=>true, 	'default'=>false,	'hint'=>'Display this category on the homepage as a featured Category (remember to upload an image!)', 'settings'=>'blogs_categories')
,	array('name'=>'image',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Photo',			'protected'=>true,	'hint'=>'Accepted formats: jpg, gif, png', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'2097152', 'root'=>'blogs_categories', 'groupby'=>'id')
);

/*

CREATE TABLE blogs_categories (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  `title` VARCHAR(255) DEFAULT NULL,
  `slug` VARCHAR(255) DEFAULT NULL,
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;

*/