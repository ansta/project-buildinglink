<?php

class Products {
	
	/*
	*
	*
	*/
	public static 	$id = 0,
					$title = '',
					$url = '',
					$newwin = '',
					$view_item_object,
					$item_id = 0;
	
	
	/*
	* This generates a data object with purpose of generating a list of categories in the front end of the website
	*/
	public static function getList($category_id = 0) {
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				p.*
			,	c.id		AS	cat_id
			,	c.title	AS	cat_title
			,	c.label	AS	cat_label
			,	c.slug	AS	cat_slug
			,	c.active	AS	cat_active
			,	c.url		AS	cat_url
			FROM
				`products` p
			LEFT JOIN
				`attachments` x ON x.module_id = p.id AND x.module_name = 'products' AND x.ttv_end IS null 
			LEFT JOIN
                `shop_categories` c ON c.id = x.attachment_id AND x.attachment_name = 'shop_categories' AND c.ttv_end IS null AND c.active IS true
            WHERE
				p.active IS true
		";
		if($category_id > 0) {
			$query .= "	AND	c.id = ".(int)db::link()->real_escape_string($category_id)."";
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND p.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				p.`title` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* This generates a data object with purpose of generating a list of categories in the front end of the website
	*/
	public static function getSearchResults() {
		$search_phrase = $_REQUEST['q'];
		if (trim($search_phrase) == '') {
			return NULL;
		}
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				p.*
			FROM
				`products` p
            WHERE
				p.active IS true
			AND
			(	sku LIKE '%".db::link()->real_escape_string($search_phrase)."%'
			OR	title LIKE '%".db::link()->real_escape_string($search_phrase)."%'
			)
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND p.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				p.`title` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return NULL;
	}
	
	
	/*
	* This generates a data object with purpose of generating a list of related products list based on a CSV list of SKUs supplied
	*/
	public static function getRelatedList($supplied_list) {
		
		// Process the comma separated list of SKUs and put them together in a list suitable for SQL
		$pop_related = explode(',',$supplied_list);
		$related_items = '[0]';
		foreach ($pop_related AS $related_sku) {
			if (trim($related_sku) != '') {
				$related_items .= ",'".trim($related_sku)."'";
			}
		}
		$related_items = str_replace('[0],','',$related_items);
		$processed_list = str_replace('[0]','',$related_items);
		
		// Make sure that there are related products to be added
		if (trim($supplied_list) == '' || trim($processed_list) == '') {
			return NULL;
		}
		
		// Query the products list
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				p.*
			FROM
				`products` p
            WHERE
				p.active IS true
			AND p.sku IN (".$processed_list.")
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND p.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				RAND()
			LIMIT 3
		";
	#	echo $query;
		
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		
		return NULL;
	}
	
	
	/*
	* This generates a data object with purpose of generating a list of categories in the front end of the website
	*/
	public static function getAdminFilterList() {
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`products` a
            WHERE
				a.active IS true
		";
		$query .= "	AND a.`ttv_end` IS null";
		$query .= "	AND a.`site_id` = ".(int)db::link()->real_escape_string($_SESSION['active_site_id'])."";
		$query .= "
			ORDER BY
				a.`title` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* This generates a data object with purpose of generating a list of categories in the front end of the website
	*/
	public static function getLatest($limit = 3) {
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				p.*
			FROM
				`products` p
            WHERE
				p.active IS true
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND p.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				p.`id` DESC
			LIMIT
				".(int)$limit."
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* This generates a data object with purpose of generating a list of categories in the front end of the website
	*/
	public static function getSomeItemsAtRandom($limit = 3) {
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				p.*
			FROM
				`products` p
            WHERE
				p.active IS true
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND p.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				RAND()
			LIMIT
				".(int)$limit."
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* This generates a data object containing all the product details
	*/
	public static function checkItemExists($item_slug) {
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`products` a
		";
		$query .= "
			WHERE
				a.id > 0
			AND	CONCAT(a.slug,'-',a.id) = '".db::link()->real_escape_string($item_slug)."'
			AND a.active IS true
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			while($item = $items->fetch_object()) {
				self::$view_item_object = $item;
				self::$item_id = $item->id;
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/*
	* 
	*/
	public static function checkItemExistsById($item_id) {
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`products` a
		";
		$query .= "
			WHERE
				a.id > 0
			AND	a.id = ".(int)db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			while($item = $items->fetch_object()) {
				self::$view_item_object = $item;
				self::$item_id = $item->id;
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/*
	* This generates the list of subcategories: categories to which products can be associated with
	* This generates an array and is used in the admin only
	*/
	public static function getAdminSelect() {
		$output = array();
		$version_control = Config::$settings['products']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`products` a
			WHERE
				a.id > 0
		";
#		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
#		}
			$query .= "	AND a.`site_id` = ".(int)db::link()->real_escape_string($_SESSION['active_site_id'])."";
		$query .= "
			ORDER BY
				a.title ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			while($item = $items->fetch_object()) {
				$output[$item->id] = $item->title;
			}
		}
		return $output;
	}
	
	
	/*
	* 
	*/
	public static function getDisplayPrice($price,$show_currency = true) {
		$price = Countries::getDisplayPrice($price);
		$price = number_format($price,2);
		if ($show_currency) {
			$currency_symbol = Countries::getCurrencySymbol();
			$price = $currency_symbol.$price;
		}
		return $price;
	}
	
	
	
	/*
	* This generates an array of data of all products and feeds it to the CSV download file
	*/
	public static function getListForCsvExport() {
		$version_control = Config::$settings['products']['version_control'];
		
		$query = "
			SELECT
				`p`.`site_id`
			,	`p`.`sku`			AS `product_no`
			,	`p`.`title`
			,	`p`.`related_products`
			,	GROUP_CONCAT(
					(
					SELECT
						GROUP_CONCAT(`x`.`attachment_id`)
					FROM
						`attachments` x
					WHERE
						`x`.`module_id` = `p`.`id`
					AND `x`.`module_name` = 'products'
					AND `x`.`attachment_name` = 'shop_categories'
					AND `x`.`ttv_end` IS null
					)
				) AS `categories`
			,	`p`.`artwork`
			,	`p`.`summary`		AS `product_overview`
			,	`p`.`content`		AS `product_description`
			,	`p`.`active`		AS `status`
			,	`p`.`metakeys`		AS `meta_keywords`
			,	`p`.`metadesc`		AS `meta_description`
			FROM
				`products` p
			WHERE
				`p`.`ttv_end` is null
			GROUP BY
				p.`id`
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `p`.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				`p`.`site_id` ASC
			,	`p`.`sku` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return NULL;
		
	}
	
	
	
}
