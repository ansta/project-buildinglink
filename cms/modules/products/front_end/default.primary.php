<?php
	// Display any messages
	echo Page::showErrors();
	
	// Fetch the data about this product
	$product = Products::$view_item_object;
	
	$related_items = Products::getRelatedList($product->related_products);
	
	// Prepare the product image
	if (file_exists(SYSTEM_ROOT.'modules/products/uploads/image/'.$product->id.'/gallery/'.$product->image)) {
		$product_image = '/image.php?i=/products/uploads/image/'.$product->id.'/gallery/'.$product->image.'';
	} else {
		$product_image = '/images/no-image-large.jpg" alt="'.$product->title.'';
	}
	
	// Get the cheapest price to display from what price this item can be purchased
	$cheapest_price = Products_variants::getCheapestPrice($product->id);
	
	// Get the minimum order quantity that a shopper can add to basket
	$minimum_order_quantity = Products_variants::getMinimumOrderQuantity($product->id);
	
	// Prepare the data for the variants
	$variants = Products_variants::getList($product->id);
	if ($variants->num_rows > 0) {
		
		$variant_select_options = array();
		$variant_table_row		= array();
		$variant_table_header	= array();
		
		while ($variant = $variants->fetch_object()) {
			$variant_select_options[] = array(
											'label' => $variant->title
										,	'value' => $variant->id
										,	'out_of_stock' => $variant->out_of_stock
										);
			
			$variant_table_row[] = array(
											'label' => $variant->title
										,	'out_of_stock' => $variant->out_of_stock
										,	1 => $variant->value_1
										,	2 => $variant->value_2
										,	3 => $variant->value_3
										,	4 => $variant->value_4
										,	5 => $variant->value_5
										,	6 => $variant->value_6
										);
			
			if (trim($variant_table_header[1]) == '') {
				$variant_table_header[1]= $variant->label_1;
				$variant_table_header[2]= $variant->label_2;
				$variant_table_header[3]= $variant->label_3;
				$variant_table_header[4]= $variant->label_4;
				$variant_table_header[5]= $variant->label_5;
				$variant_table_header[6]= $variant->label_6;
			}
			
		}
		
		// Prepare some variant table data
		$variant_table_header = array_filter($variant_table_header);
		$variant_table_columns = count($variant_table_header);
	}
?>
<div>

	<h1 class="page-title category-title"><?php echo $product->title; ?> <span class="price"><span class="from">From</span> <?php echo Products::getDisplayPrice($cheapest_price).' <small>'.Countries::getVatNotice('short').'</small>'; ?></span></h1>
	
	<!-- .product-single -->
	<?php echo ($product->special_offer?'<div class="product-single special-offer">':'<div class="product-single">') ?>
		
		<!-- .clearfix -->
		<div class="clearfix">

			<!-- .product-left -->
			<div class="product-left">
				<div class="product-image">
					<?php echo ($product->special_offer?'<div class="special-offer-ribbon"></div>':'') ?>
					<?php echo '<img src="'.$product_image.'" alt="'.$product->title.'" />'; ?>
				</div>
				<!-- .social -->
				<ul class="social clearfix">
					<?php
					echo '
					<li><span class="title">Share:</span></li>
					<li><a target="_blank" href="//www.facebook.com/sharer.php?u='.urlencode(SERVER_NAME.Page::$slug[0]).'"><i class="icon-facebook-sign"></i></a></li>
					<li><a target="_blank" href="//twitter.com/share?url='.urlencode(SERVER_NAME.Page::$slug[0]).'&text='.urlencode($product->title).'"><i class="icon-twitter-sign"></i></a></li>
					<li><a target="_blank" href="//plus.google.com/share?url='.urlencode(SERVER_NAME.Page::$slug[0]).'"><i class="icon-google-plus-sign"></i></a></li>
					<li><a href="//www.linkedin.com/shareArticle?mini=true&url='.urlencode(SERVER_NAME.Page::$slug[0]).'" target="_blank"><i class="icon-linkedin-sign"></i></a></li>
					<!--<li><a target="_blank" href="//pinterest.com/pin/create/button/?url='.urlencode(SERVER_NAME.Page::$slug[0]).'&media='.urlencode(SERVER_NAME.$product_image).'&description='.urlencode($product->title).'"><i class="icon-pinterest-sign"></i></a></li>-->
					';
					?>
				</ul>
				<!-- /.social -->
		
			</div>
			<!-- /.product-left -->
			
			<!-- .product-right -->
			<div class="product-right">
				
				<div class="product-description">
					<h4 class="label">Product Overview</h4>
					<p><?php echo strip_tags($product->summary); ?></p>
				</div>
				
				<form action="" method="post" enctype="multipart/form-data">
					<!-- .product-variants -->
					<ul class="product-variants">
						<li>
							<span class="part_no">Product number:</span>
							<?php echo $product->sku; ?>
						</li>
						<?php
						if ($variants->num_rows > 0) {
							echo '
								<li>
									<span class="label">Variant:</span>
									<select name="variant" id="">
										<option value="0" selected="selected">Choose a Variant</option>
									';
									foreach ($variant_select_options AS $variant_option) {
										/*Edited 03/5/2014*/
										if($variant_option['out_of_stock']){ 
											continue; 
										}
										/*end Edit*/
										echo '<option value="'.$variant_option['value'].'">'.$variant_option['label'].'</option>';
									}
									echo '
									</select>
								</li>
								<li>
									<span class="label">Qty:</span>
									<input name="quantity" type="text" value="'.$minimum_order_quantity.'" />
									&nbsp;<span class="small">Min Order: '.$minimum_order_quantity.'</span>
								</li>
							';
							if ($product->artwork) {
								echo '
									<li>
										<span class="label">Custom Artwork: <small style="font-weight:normal;">(optional)</small></span>
										<input name="artwork" type="file" />
										<br /><small>Vector EPS file required. If you choose not to upload your artwork now, you can contact us later to discuss it with us.</small>
									</li>
								';
							}
						} else {
							echo '<li>Please contact us for pricing information and to order.</li>';
						}
						?>
					</ul>
					<!-- /.product-variants -->
					<?php if ($variants->num_rows > 0) { ?>
					<input type="hidden" name="item" value="<?php echo $product->id; ?>" />
					<input type="hidden" name="module" value="baskets" />
					<input type="hidden" name="action" value="add" />
					<input type="submit" class="add-to-cart" name="submit" value="Add to Cart" />
					<?php } ?>
				</form>
			
			</div>
			<!-- /.product-right -->
		
		</div>
		<!-- /.clearfix -->
		
		<div class="product-description-full">
			<h2 class="category-title price-breakdown-title">Product Description</h2>
			<?php echo $product->content; ?>
		</div>
		
		<?php
		
		if ($variants->num_rows > 0) {
			echo '
				<h2 class="category-title price-breakdown-title">Prices <span>(per unit in '.Countries::getCurrencySymbol().' '.Countries::getVatNotice('inline').')</span></h2>
				<table class="price-breakdown">
					<tbody>
						<tr>
							<th class="label"><strong>'.$product->title.'</strong></th>
							';
							// Print each variant on a row below the corresponding quantity breaks
							for ($col=1; $col<=$variant_table_columns; $col++) {
								echo '<th class="price-break">'.number_format($variant_table_header[$col],0).'</th>';
							}
							echo '
						</tr>
						';
						// Print each variant on a row below the corresponding quantity breaks
						foreach ($variant_table_row AS $variant_row) {
							echo '<tr>';
								/*Edited 03/5/2014*/
								$label=$variant_row['label'];
								if($variant_row['out_of_stock']){ 
									$label.=' - out of stock'; 
								}
								/*end Edit*/
								echo '<th class="label">'.$label.'</th>';
								for ($col=1; $col<=$variant_table_columns; $col++) {
									echo '<td>'.Products::getDisplayPrice($variant_row[$col],false).'</td>';
								}
							echo '</tr>';
						}
						
						// Print the one off setup fee if one has been set
						if ((int)$product->setup_fee > 0) {
							echo '<tr>';
								echo '<td class="label" colspan="'.($variant_table_columns + 1).'"><strong>Setup Fee (one off cost)</strong> '.Products::getDisplayPrice($product->setup_fee).' '.Countries::getVatNotice('short').'</small>'.'</td>';
							echo '</tr>';
						}
						echo '
					</tbody>
				</table>
			';
		}
		?>
	</div>
	<!-- /.product-single -->
	
	<?php
	if ($related_items->num_rows > 0) {
		
		echo '
			<h2 class="related-title category-title">Related Products</h2>
			<!-- .row -->
			<div class="row product-list related-list">
				';
				
				while ($related_item = $related_items->fetch_object()) {
					// Prepare the product image
					if (file_exists(SYSTEM_ROOT.'modules/products/uploads/image/'.$related_item->id.'/list/'.$related_item->image)) {
						$product_image = '/image.php?i=/products/uploads/image/'.$related_item->id.'/list/'.$related_item->image.'';
					} else {
						$product_image = '/images/no-image-small.jpg" alt="'.$related_item->title.'';
					}
					
					// Get the cheapest price to display from what price this item can be purchased
					$cheapest_price = Products_variants::getCheapestPrice($related_item->id);
					
					// Prepare the view page URL
					$related_product_url = str_replace("//", "/", Page::getModuleUrl('products').$related_item->slug.'-'.$related_item->id.'/');
					
					echo '
						<!-- .column three -->
						<div class="column three">
							<!-- .info-box -->
							<div class="info-box product-box">
								<div class="thumb">
									<img src="'.$product_image.'" alt="'.$related_item->title.'" style="width:230px;height:230px;">
									<div class="overlay">
										<h3>'.$related_item->title.'</h3>
										<!-- .product-box-bottom -->
										<div class="product-box-bottom clearfix">
											<div class="price">'.($cheapest_price!='n/a'?'<span>From</span> '.Products::getDisplayPrice($cheapest_price):'&nbsp;').'</div>
											<div class="btn-wrap"><span class="btn">More info</span></div>
										</div>
										<!-- /.product-box-bottom -->
									</div>
									<a class="box-link" href="'.$related_product_url.'"></a>
								</div>
							</div>
							<!-- /.info-box -->
						</div>
						<!-- /.column three -->
					';
				}
				
				echo '
			</div>
			<!-- /.row-->
		';
	}
	?>
			<?php /*?>
			
			
			
			// Prepare the product image
			if (file_exists(SYSTEM_ROOT.'modules/products/uploads/image/'.$product->id.'/list/'.$product->image)) {
				$product_image = '/image.php?i=/products/uploads/image/'.$product->id.'/list/'.$product->image.'';
			} else {
				$product_image = '/images/no-image.png" alt="'.$product->title.'';
			}
			
			// Get the cheapest price to display from what price this item can be purchased
			$cheapest_price = Products_variants::getCheapestPrice($product->id);
			
			echo '
				<li>
					<!-- .info-box -->
					'.($product->special_offer?'<div class="info-box product-box special-offer">':'<div class="info-box product-box">').'
						<div class="thumb">
							'.($product->special_offer?'<div class="special-offer-ribbon"></div>':'').'
							<img src="'.$product_image.'" alt="Rulers" style="width:230px;height:230px;" />
							<div class="overlay">
								<h3>'.$product->title.'</h3>
								<!-- .product-box-bottom -->
								<div class="product-box-bottom clearfix">
									<div class="price">'.($cheapest_price!='n/a'?'<span>From</span> '.Products::getDisplayPrice($cheapest_price):'&nbsp;').'</div>
									
									
									
									
			<!-- .column three -->
			<div class="column three">
				<!-- .info-box -->
				<div class="info-box product-box special-offer">
					<div class="thumb">
						<div class="special-offer-ribbon"></div>
						<img src="/images/thumb-ruler.jpg" alt="Rulers">
						<div class="overlay">
							<h3>Amazing Ruler</h3>
							<!-- .product-box-bottom -->
							<div class="product-box-bottom clearfix">
								<div class="price"><span>From</span> £20.00</div>
								<div class="btn-wrap"><span class="btn">Customise</span></div>
							</div>
							<!-- /.product-box-bottom -->
						</div>
						<a class="box-link" href="#"></a>
					</div>
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.column three -->
			
			<!-- .column three -->
			<div class="column three">
				<!-- .info-box -->
				<div class="info-box product-box">
					<div class="thumb">
						<img src="/images/thumb-ruler.jpg" alt="Rulers">
						<div class="overlay">
							<h3>Amazing Ruler</h3>
							<!-- .product-box-bottom -->
							<div class="product-box-bottom clearfix">
								<div class="price"><span>From</span> £20.00</div>
								<div class="btn-wrap"><span class="btn">Customise</span></div>
							</div>
							<!-- /.product-box-bottom -->
						</div>
						<a class="box-link" href="#"></a>
					</div>
				</div>
				<!-- /.info-box -->
			</div>
			<!-- /.column three -->
		</div>
		<!-- /.row-->
			<?php */?>
		
</div>
			
			
<?php
/*
	if ($_SESSION['form']['form-success']) {
		if ($_SERVER['REMOTE_ADDR'] == OFFICE_IP) {
			echo '<div class="basket-arrow"><img src="/images/arrow.png" class="basket-pointer" /></div>';
		}
	}
	echo Page::showErrors();
	$product = Products::$view_item_object;
	$currency_symbol = '&pound;';
	echo '<h1>'.$product->title.'</h1>';
	
	// Fetch and display the product image
	if (file_exists(SYSTEM_ROOT.'modules/products/uploads/image/'.$product->id.'/gallery/'.$product->image)) {
		echo '<img src="/image.php?i=/products/uploads/image/'.$product->id.'/gallery/'.$product->image.'" alt="'.$product->title.'" title="'.$product->title.'" style="float:right;border:2px solid #736357;width:330px;" />';
	} else {
		echo '<img src="/images/no-image.png" alt="'.$product->title.'" title="'.$product->title.'" style="float:right;border:2px solid #736357;width:330px;">';
	}
	
	// Fetch all the price band columns available so that we can then check to see which ones to display
	foreach (Config::$fields['price_bands'] AS $pbk => $pbv) {
		if (substr($pbv['name'], 0, 5) == 'price') {
			$price_template_array[substr($pbv['name'], 5, 5)] = $pbv['name'];
		}
	}
	
	echo '<div class="product_info">';
		echo '<form action="" method="post">';

			// List all the available sizes
		#	echo '<div>Please select your size: <a href="'.Page::getPageUrl(152).'" class="whatisthis"><small>What\'s this?</small></a><br />';
			echo '<div>Please select: <a href="'.Page::getPageUrl(152).'" class="whatisthis"><small>What\'s this?</small></a><br />';
			echo '<select name="size" id="in_size" class="field name-size type-select">';
				$i = 0;
				foreach ($price_template_array AS $column_num => $price_column_name) {
					$pb_getinfo = Price_bands::getPriceDetails($product->price_bands_id,$column_num);
					$category_id = $pb_getinfo->category_id;
					$i++;
					if (trim($pb_getinfo->label) != '' && $pb_getinfo->price > 0) {
						echo '<option value="'.$price_column_name.'" id="'.$column_num.'" rel="'.$pb_getinfo->price.'">'.$pb_getinfo->label.': '.$currency_symbol.$pb_getinfo->price.'</option>';
						$extras_title[$column_num] = $pb_getinfo->label;
					}
				}
			echo '</select>';
			echo '</div>';
			
			// List all the options available for each of the sizes
			// Each options list will be hidden and only the option set for the selected price band should show.
			echo '<div id="productextras">';
			echo 'Please select any optional extras: ';
			echo '<a href="'.Page::getPageUrl(172).'" class="whatisthis"><small>What\'s this?</small></a><br />';
			for ($j=1; $j<=$i; $j++) {
				$price_extra[$j] = '';
				$extras = Products_extras::getList($category_id);
				if ($extras->num_rows >0) {
					while ($extra = $extras->fetch_assoc()) {
						if ($extra['price'.$j] > 0) {
							$price_extra[$j] .= '<li class="'.$extra['label'.$extra['id']].'"><label><span class="title">'.$extra['title'].'</span><input type="checkbox" id="in_extra'.$extra['id'].'" name="extra'.$extra['id'].'" rel="'.$extra['price'.$j].'" class="opt"></label> '.$currency_symbol.$extra['price'.$j].'</li>';
						} else {
							$price_extra[$j] .= '<li class="'.$extra['label'.$extra['id']].' disabled"><label><span class="title">'.$extra['title'].'</span><input type="checkbox" id="in_extra'.$extra['id'].'" name="extra'.$extra['id'].'" rel="0" class="opt" disabled="disabled"></label> <span class="faded">not available</span></li>';
						}
					}
					echo '<span class="extras_label">Options available only if you have chosen <span class="extras_title">'.$extras_title[$j].':</span></span>';
					echo '<ul id="options'.$j.'" class="extras_list">'.$price_extra[$j].'</ul>';
				}
			}
			echo '</div>';
			
			// If this item is a weathervane then display fixing options
			if (Products::requiresFixingsOption($product->id)) {
				echo '<div id="productextras">';
					echo 'Please select the fixing: ';
					echo '<a href="'.Page::getPageUrl(164).'" class="whatisthis"><small>What\'s this?</small></a><br />';
					echo '<select name="fixings_id" id="in_fixings_id" class="field name-fixings_id type-select">';
						echo '<option value="0">-- please select a fixing type --</option>';
						foreach (Config::$settings['baskets']['fixings_id'] AS $fix_key => $fix_value) {
							if (isset($_REQUEST['fixings_id']) && $_REQUEST['fixings_id'] == $fix_key) {
								$selected_fixing = ' selected="selected"';
							} else {
								$selected_fixing = '';
							}
							echo '<option value="'.$fix_key.'"'.$selected_fixing.'>'.$fix_value.'</option>';
						}
					echo '</select>';
				echo '</div>';
			}
			echo '<span class="total_price"><span class="title"><strong>Total:</span>'.$currency_symbol.'<span class="total">000.00</span></strong></span>';
	
			echo '<input type="hidden" name="item" value="'.$product->id.'">';
			echo '<input type="hidden" name="qty" value="1">';
			echo '<input type="hidden" name="module" value="baskets">';
			echo '<input type="hidden" name="action" value="add">';
			echo '<input type="submit" name="submit" id="in_submit" value="Add to basket">';
			
		echo '</form>';
	echo '</div>';
	
			*/
	




#	if (trim($product->content) != '') {
#		echo '<h2>Description:</h2>';
#		echo $product->content;
#	}
