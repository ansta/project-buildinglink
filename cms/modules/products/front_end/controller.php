<?php

if (Module::$slug[0] == '/') {
	// Default landing page: show latest few post summaries
#	Page::addBreadCrumb('#'.$_REQUEST['tag']);
	Error::type(404);
	
} elseif (Products::checkItemExists(Module::$slug[1]) && trim(Module::$slug[2]) == '') {
	
	// SSet up some meta tags
	Meta::setTag('title',Products::$view_item_object->title);
	Meta::setTag('keywords',Products::$view_item_object->metakeys);
	Meta::setTag('description',Products::$view_item_object->metadesc);
	
	// Setup the Canonical URL of this product
	$canonical_domain = Sites::getDomain(Products::$view_item_object->site_id);
	$canonical_url = Page::getModuleUrl('products').Products::$view_item_object->slug.'-'.Products::$view_item_object->id.'/';
	Meta::setTag('canonical',$canonical_domain.Tool::cleanUrl($canonical_url));

} else {
	Error::type(404);
	
}

