<?php
echo '<h1>';
	echo '<img src="/image.php?i=/'.Page::$slug[2].'/images/icon-20.png" alt="'.Config::$settings[Page::$slug[2]]['plural'].'" class="icon">';
	echo Config::$settings[Page::$slug[2]]['plural'];
echo '</h1>';
?>
<section>
<?php
	
	// Check to see if this module relies on a POS column
	if(Admin::fieldCheckExists(Page::$slug[2],'pos')) {
		Admin::$haspos = TRUE;
		$table_js_id = 'sortable';
	} else {
		$table_js_id = 'notsortable';
	}
	
	if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
		unset($_SESSION['form']['form-success']);
	}
	
	$search = '';
	$items = Admin::getData($search);
	
	// Start building the output
	if ($items->num_rows > 0) {
		
		$columns = Config::$fields[Module::$name];
		// Start with the list table header
		$output = '<table class="tbldata" id="'.$table_js_id.'">';
			$output .= '<tr class="nodrag nodrop">';
			foreach ($columns AS $field) {
				if ($field['list'] == true) {
					$output .= '<th class="tbltext col-'.$field['name'].' col-'.$field['formtype'].'">';
					$output .= $field['label'];
					$output .= '</th>';
				}
			}
			$colspan = 0;
			if (Admin::$allow['delete'] == 1) {
				$colspan = $colspan + 1;
			}
			if (Admin::$allow['edit'] == 1) {
				$colspan = $colspan + 1;
			}
			if (Admin::checkButtonColumn('seo')) {
				$colspan = $colspan + 1;
			}
			if (Admin::$allow['view'] == 1) {
				$colspan = $colspan + 1;
			}
			if (isset(Config::$settings[Module::$name]['child_module_filter'])) {
				foreach (Config::$settings[Module::$name]['child_module_filter'] AS $child_module_id => $child_module_name) {
					$colspan = $colspan + 1;
				}
			}
			if (in_array('parent', Config::$fields[Module::$name])) {
				$colspan = $colspan + 1;
			}
			$output .= '<th class="tblaction btn-social">';
				$output .= 'Social';
			$output .= '</th>';
			$output .= '<th colspan="'.$colspan.'" class="tblaction btn-new">';
			if (Admin::$allow['new'] == 1) {
				$output .= Admin::makeButton('new',0,Module::$name,'New '.Config::$settings[Module::$name]['singular']);
			}
			$output .= '</th>';
		$output .= '</tr>';
		
		// Check to see if this table has an ACTIVE column
		$has_active_column = FALSE;
		foreach (Config::$fields[Module::$name] AS $k => $v) {
			if ($v['name'] == 'active') {
				$has_active_column = TRUE;
			}
		}
		
		// Continue building the data for each row
		while( $item = $items->fetch_object() ) {
			
			// apply active/inactive styling
			if (in_array('parent', Config::$fields[Module::$name])) {
				if ($item->active == 1) {
					$active_css = 'active';
				} else {
					$active_css = 'inactive';
				}
			} else {
				if ($has_active_column && $item->active == 1) {
					$active_css = 'active';
				} elseif ($has_active_column && $item->active == 0) {
					$active_css = 'inactive';
				} else {
					$active_css = 'active';
				}
			}
			
			// apply alternate row colouring
			$i++;
			if($i & 1) {
				$rowcss = ' class="row1 '.$active_css.'"';
			} else {
				$rowcss = ' class="row0 '.$active_css.'"';
			}
			
			// For depth fields (`pages` table only at the moment) visualise the representation of the depth
			if (Page::$slug[2] == 'pages') {
				$indent_string = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				if ($item->depth == 1) {
					$depth_indent = '';
				} elseif($item->depth == 2) {
					$depth_indent = $indent_string.$indent_suffix;
				} elseif($item->depth == 3) {
					$depth_indent = $indent_string.$indent_string.$indent_suffix;
				} elseif($item->depth == 4) {
					$depth_indent = $indent_string.$indent_string.$indent_string.$indent_suffix;
				} elseif($item->depth == 5) {
					$depth_indent = $indent_string.$indent_string.$indent_string.$indent_string.$indent_suffix;
				} else {
					$depth_indent = '';
				}
			}
			
			// Make sure that we use the correct ID column depending on whether or not this module has version control or not
			$version_control = Config::$settings[$module]['version_control'];
			if($version_control == 'audit' || $version_control == 'full') {
				$item_id = $item->id;
			} else {
				if (in_array('auto_id', Config::$fields[Module::$name])) {
					$item_id = $item->auto_id;
				} else {
					$item_id = $item->id;
				}
			}
			
			// output the HTML for the table
			$output .= '<tr'.$rowcss.' id="'.$item_id.'">';
			foreach ($columns AS $field) {
				if ($field['list'] == true) {
					$output .= '<td class="tbltext">';
					if ($field['formtype'] == 'select') {
						$output .= $depth_indent.Config::$settings[Module::$name][$field['name']][$item->$field['name']];
					} elseif ($field['formtype'] == 'img') {
						$version_1 = $field['root'].'/uploads/'.$field['name'].'/'.$item_id.'/admin/'.$item->$field['name'];
						$version_2 = $field['root'].'/uploads/'.$field['name'].'/'.$item_id.'/thumb/'.$item->$field['name'];
						if (file_exists(SYSTEM_ROOT.'/modules/'.$version_1)) {
							$output .= '<span class="thumbnail image-'.$field['name'].'"><img src="/image.php?i=/'.$version_1.'" /></span>';
						} elseif (file_exists(SYSTEM_ROOT.'/modules/'.$version_2)) {
							$output .= '<span class="thumbnail image-'.$field['name'].'"><img src="/image.php?i=/'.$version_2.'" /></span>';
						} else {
							$output .= '<span class="thumbnail image-'.$field['name'].'">Error: image file appears to be missing!</span>';
						}
					} else {
						$output .= $depth_indent.$item->$field['name'];
					}
					$output .= '</td>';
				}
			}
			
			// Facebook share link
			$output .= '<td class="tblaction btn-seo">';
				$output .= '<a href="'.Blogs::makeFacebookShareLinkUrl($item_id).'" style="background-image:url(/images/admin/btn-facebook.png);" target="_blank"></a>';
				$output .= '<a href="'.Blogs::makeTwitterShareLinkUrl($item_id).'" style="background-image:url(/images/admin/btn-twitter.png);" target="_blank"></a>';
			$output .= '</td>';
			
			// SEO
			if (Admin::checkButtonColumn('seo')) {
				$output .= '<td class="tblaction btn-seo">';
				$output .= Admin::makeButton('seo',$item_id,Module::$name,'SEO');
				$output .= '</td>';
			}
			
			// CHILD Module Filters
			if (isset(Config::$settings[Module::$name]['child_module_filter'])) {
				foreach (Config::$settings[Module::$name]['child_module_filter'] AS $child_module_id => $child_module_name) {
					$output .= '<td class="tblaction custom-btn">';
					$output .= Admin::makeButton(
									Config::$settings[Module::$name]['child_module_filter'][$child_module_id]
								,	$item_id
								,	Config::$settings[$child_module_name]['child_module_filter'][$child_module_id]
								,	Config::$settings[$child_module_name]['short']
								,	Config::$settings[Module::$name]['child_module_filter'][$child_module_id].'/filter/'.$item_id.'/'
								,	1
								,	Config::$settings[$child_module_name]['icon_class']
								);
					$output .= '</td>';
				}
			}
			
			// NEW
			if (in_array('parent', Config::$fields[Module::$name])) {
				$output .= '<td class="tblaction btn-newsub">';
				if (Config::$settings[Module::$name]['max_depth'] <= $item->depth) {
					$output .= '<span class="disabled" title="Maximum page depth reached!">New SubPage</span>';
				} else {
					if ($item->use_module != '') {
						if (Config::$settings[$item->use_module]['allow_subpages']) {
							$output .= Admin::makeButton('new',$item_id,Module::$name,'New SubPage');
						} else {
							$output .= '<span class="disabled" title="Maximum page depth reached!">New SubPage</span>';
						}
					} else {
						$output .= Admin::makeButton('new',$item_id,Module::$name,'New SubPage');
					}
				}
				$output .= '</td>';
			}
			
			// VIEW
			if (Admin::$allow['view'] == 1) {
				$output .= '<td class="tblaction btn-view">';
				if (Admin::checkHasViewUrl()) {
					$output .= Admin::makeButton('view',$item_id,Module::$name,'View',call_user_func(array(Module::$name,'getAdminViewUrl'),$item->slug,$item->id));
				} else {
					$output .= Admin::makeButton('view',$item_id);
				}
				$output .= '</td>';
			}
			
			// EDIT
			if (Admin::$allow['edit'] == 1) {
				$output .= '<td class="tblaction btn-edit">';
				$output .= Admin::makeButton('edit',$item_id);
				$output .= '</td>';
			}
		
			// DELETE
			// If global delete is permitted, check for row specific permissions
			if (Admin::$allow['delete'] == 1) {
				$output .= '<td class="tblaction btn-delete">';
				if($item->locked) {
					Admin::allowDelete(0);
					$output .= Admin::makeButton('delete',$item_id);
					Admin::allowDelete(1);
				} else {
					$output .= Admin::makeButton('delete',$item_id);
				}
				$output .= '</td>';
			} else {
	#			$output .= '<td class="tblaction btn-delete">';
	#				$output .= Admin::makeButton('delete',$item_id);
	#			$output .= '</td>';
			}
			
			$output .= '</tr>';
		}
		$output .= '</table>';
	} else {
		if (Admin::$allow['new'] == 1) {
			$output = '<p>No data available yet, please <a href="new/">add a new item</a>.</p>';
		} else {
			$output = '<p>No data available yet.</p>';
		}
		
		if (DEBUG_MODE > 0) {
			$output .= $query;
		}
	}
	echo $output;
?>
</section>
