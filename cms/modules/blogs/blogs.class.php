<?php

class Blogs {

	private static 	$filter_query = '';
	public static 	$view_item_object,
				 	$item_id = 0,
					$paging = array(
									'start_point' => 0
								,	'items_per_page' => 6
								),
					$depth = 0,
					$page_depth = 0,	#replace one of these 2 with $uri_length
					$slug = array(),
					$module = FALSE,
					$id = 0,
					$parent = 0,
					$title = '',
					$content = '',
					
					$haspos = FALSE,
					$haseditor = FALSE,
					
					$fulluri,
					$scheme,
					$host,
					$path,
					$query,
					
					$template = 'default',
					$allow = array();	// to store the new, edit, delete buttom permissions;
	
	/*
	*
	public static function getPaging() {
		
		$previous_text = '&laquo; Previous';
		$next_text = 'Next &raquo;';
		
		$previous_text = 'Previous';
		$next_text = 'Next';
		
		$oldest_link = self::countTotal() - (self::$paging['items_per_page'] * $_REQUEST['p']);
		
		$total_num_posts = self::countTotal();
		if ($total_num_posts > 0 && $total_num_posts > self::$paging['items_per_page']) {
			echo '<p class="paging">';
			if ($oldest_link > 0) {
				if ($_REQUEST['p'] > 1) {
					$new_page_number = $_REQUEST['p'] + 1;
				} else {
					$new_page_number = 2;
				}
				echo '<a href="?p='.$new_page_number.'" class="btn-prev">'.$previous_text.'</a>';
			} else {
				echo '<span class="disabled btn-prev">'.$previous_text.'</span>';
			}
			if (isset($_REQUEST['p']) && $_REQUEST['p'] > 1) {
				if ($_REQUEST['p'] > 1) {
					$new_page_number = $_REQUEST['p'] - 1;
				} else {
					$new_page_number = 1;
				}
				echo '<a href="?p='.$new_page_number.'" class="btn-next">'.$next_text.'</a>';
			} else {
				echo '<span class="disabled btn-next">'.$next_text.'</span>';
			}
			echo '</p>';
		}
	}
	*/
	
	
	/*
	* 
	public static function countTotal() {
		$type_id = Blogs_categories::$item_id;
		if ($type_id > 0) {
			if (file_exists(SYSTEM_ROOT.'/modules/blogs_categories/blogs_categories.config.php')) {
				$use_types = TRUE;
			}
		}
		$version_control = Config::$settings['blogs']['version_control'];
		$query = "
			SELECT
				id
			FROM
				`blogs` a
		";
		if ($use_types) {
			$query .= "
				LEFT JOIN
					attachments x ON x.module_id = a.id AND x.module_name = 'blogs' AND x.attachment_name = 'blogs_categories' AND x.ttv_end IS null  
				LEFT JOIN
					blogs_categories t ON x.attachment_id = t.id AND t.ttv_end IS null
			";
		}
		$query .= "
			WHERE
				a.slug != ''
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND	a.ttv_end IS null";
		}
		if ($use_types) {
			$query .= " AND	t.id = ".$type_id."";
		}
		$query .= " AND approved=1
			ORDER BY
				a.`id` DESC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items->num_rows;
		}
		return 0;
	}
	*/
	
	/*
	* Used by listing page (when no category is selected) and blog widget
	*/
	public static function getLatestList($limit = 0) {
		$query = "
			SELECT
				t.site_id as site
			,	t.title as cat
			,	(SELECT d.ttv_start FROM blogs d WHERE d.auto_id = b.id) AS created
			,	b.*
			FROM
				blogs b
			LEFT JOIN
				attachments x
				ON x.module_id = b.id
				AND x.module_name = 'blogs'
				AND x.attachment_name = 'blogs_categories'
				AND x.ttv_end IS null
			LEFT JOIN
				blogs_categories t
				ON x.attachment_id = t.id
				AND t.ttv_end IS null
			WHERE
				b.ttv_end IS null
			AND	b.active = 1
			AND t.site_id = ".db::link()->real_escape_string(Site::$sitedata['id'])."
			ORDER BY
				b.id DESC
		";
		if ($limit > 0) {
			$query .= " LIMIT ".(int)$limit;
		}
		if (USER_IP == OFFICE_IP) {
		#	echo $query;
		}
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		}
		return NULL;
	}
	
	
	/*
	* Used by the listing page when a category is selected
	*/
	public static function getList($category_id) {
		
		$start_point = self::$paging['start_point'];
		$items_per_page = self::$paging['items_per_page'];
	
		if (isset($_REQUEST['p']) && $_REQUEST['p'] > 1) {
			$start_point = ($_REQUEST['p']-1)*$items_per_page;
		}
		
		$version_control = Config::$settings['blogs']['version_control'];
		$query = "
			SELECT
				t.site_id AS site
			,	(SELECT d.ttv_start FROM blogs d WHERE d.auto_id = b.id) AS created
			,	b.*
			FROM
				blogs b
			LEFT JOIN
				attachments x
				ON x.module_id = b.id
				AND x.module_name = 'blogs'
				AND x.attachment_name = 'blogs_categories'
				AND x.ttv_end IS null
			LEFT JOIN
				blogs_categories t
				ON x.attachment_id = t.id
				AND t.ttv_end IS null
			WHERE
				b.ttv_end IS null
			AND	b.active = 1
			AND t.site_id = ".db::link()->real_escape_string(Site::$sitedata['id'])."
			AND t.id = ".$category_id."
			ORDER BY
				b.id DESC
			LIMIT
				".(int)$start_point.",".(int)$items_per_page."
		";
		if (USER_IP == OFFICE_IP) {
		#	echo $query;
		}
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* Used by the VIEW page
	*/
	public static function checkItemExists($item_slug) {
		$version_control = Config::$settings['blogs']['version_control'];
		$query = "
			SELECT
				a.*
			,	(SELECT d.ttv_start FROM blogs d WHERE d.auto_id = a.id) AS created
			FROM
				`blogs` a
			WHERE
				CONCAT(a.slug,'-',a.id) = '".db::link()->real_escape_string($item_slug)."'
			AND a.active = 1
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS NULL";
		}
		if (USER_IP == OFFICE_IP) {
		#	echo $query;
		}
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			while($item = $items->fetch_object()) {
				self::$view_item_object = $item;
				self::$item_id = $item->id;
			#	Meta::setTitle($item->title);
			#	Meta::setDescription($item->metadesc);
			#	Meta::setKeywords($item->metakeys);
			#	Meta::setAuthor($item->create_by);
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/*
	* 
	*/
	public static function getNav() {
		
	}
	
	/*
	* 
	*/
	public static function getTags() {
		
	}
	
	/*
	* Admin Social Share Icons
	* Create a new function for each new social network and use getShareInfo() to get the data
	*/		
	public static function makeFacebookShareLinkUrl($item_id) {
		$share_data = self::getShareInfo($item_id);
		$output = 'http://www.facebook.com/sharer.php?';
		$output .= 's=100';
		$output .= '&amp;p[title]='.$share_data['title'];
		$output .= '&amp;p[url]='.$share_data['url'];
		$output .= '&amp;p[summary]='.$share_data['summary'];
		$output .= '&amp;p[images][0]='.$share_data['image'];
		return $output;
	}		
	public static function makeTwitterShareLinkUrl($item_id) {
		$share_data = self::getShareInfo($item_id);
		$output = 'http://twitter.com/share?';
		$output .= '&amp;url='.$share_data['url'];
		return $output;
	}
	public static function getShareInfo($item_id) {
		$version_control = Config::$settings['blogs']['version_control'];
		$query = "
			SELECT
				`a`.*
			FROM
				`blogs` a
            WHERE
				`a`.`id` = ".(int)db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
		$query .= "
			LIMIT 1
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			if ($item = $items->fetch_object()) {
				// Prepare the item image
			#	$imageobject = Parent_child::getFirst($item->id); // See olivercars.co.uk
				if (file_exists(SYSTEM_ROOT.'modules/blogs/uploads/image/'.$item->id.'/list/'.$item->image)) {
					$product_image = Page::$scheme.Page::$host.'/image.php?i=/blogs/uploads/image/'.$item->id.'/list/'.$item->image;
					$output['image'] = urlencode($product_image);
				}
				// Prepare the item url
				$product_url = Page::$scheme.Page::$host.Tool::cleanUrl(Page::getModuleUrl('blogs').'/'.$item->slug.'-'.$item->id.'/');
				// Send out data in an array
				$output['title'] = urlencode($item->title);
				$output['url'] = urlencode($product_url);
				$output['summary'] = urlencode(substr(strip_tags($item->content), 0, 330));
			}
		} else {
			$output['title'] = '';
			$output['url'] = '';
			$output['summary'] = '';
			$output['image'] = '';
		}
		return $output;
	}
	
}

