
<?php $item = Blogs::$view_item_object; ?>
	
	<?php
		// Prepare the product image
		if (file_exists(SYSTEM_ROOT.'modules/blogs/uploads/image/'.$item->id.'/list/'.$item->image)) {
			$item_image = '/image.php?i=/blogs/uploads/image/'.$item->id.'/list/'.$item->image.'';
		} else {
			$item_image = '/images/no-image-small.jpg" alt="'.$item->title.'';
		}
		
		//Permalink
		$permalink = str_replace("//", "/", Page::getModuleUrl('blogs').$item->slug.'/'.$item->id.'/');
		
	?>
	<!-- .post -->
	<article class="post">
		<!-- .post-header -->
		<header class="post-header">
			<h2 class="entry-title"><a href="<?php echo $permalink; ?>"><?php echo $item->title; ?></a></h2>
				<div class="entry-date">Posted on <?php echo date('F j, Y',strtotime($item->created)); ?></div>
		</header>
		<!-- /.post-header -->
		<div class="post-icon">
			<img src="<?php echo $item_image; ?>" alt="<?php echo $item->title; ?>" style="width:710px;height:230px;">
		</div>
		<!-- .entry-content -->
		<div class="entry-content">
			<?php echo $item->content; ?>
		</div>
		<!-- /.entry-content -->
		
	</article>
	<!-- /.post -->
	
	
	<?php
	// Display Blog Comments and new comments form
	echo '<div class="blog-comments">';
		if ($item->allow_comments == 1) {
			echo '<a name="form"></a>';
			
			// Display Comments
			$comments = Blogs_comments::getList($item->id);
			echo '<h2>Comments</h2>';
			if ($comments->num_rows > 0) {
				while ($comment = $comments->fetch_object()) {
					echo '
						<div class="comment">
							<div class="comment-meta">
								<strong>'.(trim($comment->title)!=''?$comment->title:'anonymous').'</strong> on
								<span class="commentdate">'.Form::makeHumanDate($comment->ttv_start).'</span> said:
							</div>
							<p class="comment-content">'.nl2br(htmlspecialchars(strip_tags($comment->content))).'</p>
						</div>
					';
				}
			} else {
				echo '<p>No one has posted any comments yet, why not be the first.</p>';
			}
			
			// Display New Comment Form
			echo Page::showErrors();
			
			// Prepare the captcha field
			$captcha = '';
			$captcha_error = '';
			require_once(SYSTEM_ROOT.'/libraries/recaptchalib.php');
			if (isset($_SESSION['form']['field-error']['recaptcha'])) {
				$error = $_SESSION['form']['field-error']['recaptcha'];
				$captcha_error = '<span class="error error-textarea">The value that you entered was incorrect. Please try again with this new image.<br />If you are having trouble reading the text in the image, click the top most little red button just to the right of the yellow area.</span>';
				unset($_SESSION['form']['field-error']['recaptcha']);
			}
			$captcha = recaptcha_get_html(RECAPTCHA_PUBLIC, $error);


			echo '
				<form action="'.Page::$slug[0].'#form" method="post" class="commentform">
					<fieldset>
						<legend>Add your comment</legend>
						'.Form::makeField(array('formtype'=>'text','name'=>'title','label'=>'Your Name')).'
						'.Form::makeField(array('formtype'=>'textarea','name'=>'content','label'=>'Your Message','required'=>true)).'
						'.$captcha.$captcha_error.'
						'.Form::makeField(array('formtype'=>'hidden','name'=>'blog_id','value'=>$item->id)).'
						'.Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'new')).'
						'.Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>'blogs_comments')).'
						'.Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Submit')).'
					</fieldset>
				</form>
			';
		}
	echo '</div>';
	
?>
	
	
	
	
	
	
<?php /*?>
	
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',				'protected'=>false,	'hint'=>'')
,	array('name'=>'site_id',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Site ID',			'protected'=>true,	'hint'=>'')
,	array('name'=>'title',			'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',			'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'slug',			'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'URL identifier',	'protected'=>false,	'regex'=>'/^[a-z0-9\-]*$/',	'hint'=>'Identifies this blog post within the URL structure.<br /><strong style="color:#955;">Once set, it is strongly advised NOT to change it in the future as this can affect your <abbr title="Search Engine Optimisation">SEO</abbr> page ranking.</strong>')
,	array('name'=>'content',		'required'=>true,	'list'=>false,	'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',	'label'=>'Content',			'protected'=>false,	'hint'=>'',	'editor'=>true)
,	array('name'=>'metakeys',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Keywords',		'protected'=>false,	'hint'=>'Recommendation: no more than 5 comma separated keywords all of which <strong>MUST</strong> appear within the page content')
,	array('name'=>'metadesc',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Description',		'protected'=>false,	'hint'=>'Recommendation: a short description of approx. 150 characters')
,	array('name'=>'allow_comments',	'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yes1no2',	'format'=>'b',	'label'=>'Allow comments',	'protected'=>false,	'default'=>1, 'hint'=>'Do you want to allow your website visitors to leave comments?')
,	array('name'=>'active',			'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Item is Active',	'protected'=>false,	'hint'=>'Allows creation of an item without publishing it immediately by selecting <strong>No</strong>.', 'default'=>true)
,	array('name'=>'image',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Photo',			'protected'=>false,	'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 330(W) x 150(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'9097152', 'root'=>'blogs', 'groupby'=>'id')
	



<section class="whatToDo">
	<img src="/images/bg-whattodo-top.png" alt="" title="" class="waves" />
	<div class="dashed">
	<?php $item = Blogs::$view_item_object; ?>
		<?php if(User::isAdmin()) { ?>
			<div class="admin-edit">
				<a href="/admin/blogs/">
					<span>Manage Blog</span>
				</a>
				<a href="/admin/blogs/edit/<?php echo $item->id; ?>/">
					<span>Edit this Blog Post</span>
				</a>
			</div>
		<?php } ?>
		<?php
			echo Page::showErrors(FALSE);
			echo Blogs::getNav();
			echo '<h1>'.$item->title.'</h1>';
			echo '<img src="/image.php?i=/blogs/uploads/image/'.$item->id.'/home/'.$item->image.'" alt="'.$item->title.'" title="'.$item->title.'" class="blogpostimg" />';
			echo $item->content;
			
			$categories = Blogs_categories::getArticleList($item->id);
		#	echo '<hr class="comment_divider" />';
			if ($categories->num_rows > 0) {
		#		echo '<h2>Categories</h2>';
				echo '<p class="catlist">This post has been categorised under: ';
				$c = 1;
				while ($category = $categories->fetch_object()) {
					if ($c < $categories->num_rows) { $sep = ', '; } else { $sep = '.'; }
					echo '<a href="/blog/'.$category->slug.'/">'.$category->title.'</a>'.$sep;
					$c++;
				}
				echo '</p>';
			} else {
				echo '<p>No one has posted any comments yet, why not be the first to share your experiences!</p>';
				echo '<hr class="comment_divider" />';
			}
			
			if ($item->allow_comments == 1) {
				echo '<a name="form"></a>';
				Page::getElement('widget','blogs_comments','view');
				
				if (!User::isLoggedIn()) {
					Page::getElement('widget','users','default');
				} else {
					Page::getElement('widget','blogs_comments','new');
				}
			} else {
				echo '<hr class="comment_divider" />';
				echo '<p>User contribution has been disabled for this page.</p>';
			}
		#	Page::getElement('widget','users','login');
			
		#	Page::getElement('widget','users','signup');
			
			
			
		#	Page::getElement('widget','blogs_comments','view');
		#	if ($_SESSION['id'] > 0) {
		#		Page::getElement('widget','blogs_comments','new');
		#	} else {
		#		Page::getElement('widget','blogs_comments','register');
		#	}
		?>
	</div>
</section>
<?php */?>

