<?php

	if (Module::$slug[0] == '/') {
		$articles = Blogs::getLatestList();
		$page_title = Page::getTitle();
	} else {
		
		// Fetch the data about this category
		$blog_category = Blogs_categories::$view_item_object;
		
		$page_title = $blog_category->title;
		
		$articles = Blogs::getList($blog_category->id);
	}
	?>
	
	<div class="recent-posts main-posts">
		
		<h1><?php echo $page_title; ?></h1>
		
		<?php 
		if ($articles->num_rows > 0) { 
			while ($item = $articles->fetch_object()) { 
			
		?>
		
		<?php
			// Prepare the product image
			if (file_exists(SYSTEM_ROOT.'modules/blogs/uploads/image/'.$item->id.'/list/'.$item->image)) {
				$item_image = '/image.php?i=/blogs/uploads/image/'.$item->id.'/list/'.$item->image.'';
			} else {
				$item_image = '/images/no-image-small.jpg" alt="'.$item->title.'';
			}
			
			// Prepare item URL
			$item_url = Tool::cleanUrl(Page::getModuleUrl('blogs').$item->slug.'-'.$item->id.'/');
			
		?>
		<!-- .post -->
		<article class="post">
			<!-- .post-header -->
			<header class="post-header">
				<h2 class="entry-title"><a href="<?php echo $item_url; ?>"><?php echo $item->title; ?></a></h2>
				<div class="entry-date">Posted on <?php echo date('F j, Y',strtotime($item->created)); ?></div>
			</header>
			<!-- /.post-header -->
			<div class="post-icon">
				<a href="<?php echo $item_url; ?>"><img src="<?php echo $item_image; ?>" alt="<?php echo $item->title; ?>" style="width:710px;height:230px;"></a>
			</div>
			<!-- .entry-content -->
			<div class="entry-content">
				<p><?php echo substr(strip_tags($item->content), 0, 500); ?>...
				<a href="<?php echo $item_url; ?>" class="more-link">More <i class="icon-angle-right"></i></a></p>
			</div>
			<!-- /.entry-content -->
			
		</article>
		<!-- /.post -->
	<?php 
		}
	} else {
		echo '<p>No articles found, please check back soon.</p>';
	} 
	?>
		
	</div>
<?php /*
<section class="whatToDo">
	<img src="/images/bg-whattodo-top.png" alt="" title="" class="waves" />
	<div class="dashed">
		
			if (Module::$slug[1] != '') {
				echo '<h1>'.Blogs_categories::$view_item_object->title.'</h1>';
			} else {
				echo '<h1>'.Page::getTitle().'</h1>';
			}
			echo Page::showErrors();
			echo Content::getContentByContentId(Page::$id[1] * 1000  + (int)Blogs_categories::$view_item_object->id)
		
	</div>
</section>
*/ ?>	<?php /*
<div class="latestArticles">
 if(User::isAdmin()) { ?>
		<div class="admin-edit" style="z-index:30;">
			<a href="/admin/blogs/">
				<span>Manage Blog</span>
			</a>
			<a href="/admin/blogs/new/">
				<span>New Blog Post</span>
			</a>
		</div>
	<?php } */ ?>
	<?php 
	/*
	Page::getElement('widget','blogs','list'); 
</div>
*/
?>