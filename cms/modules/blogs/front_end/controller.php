<?php

if (Module::$slug[0] == '/') {
	Core::setModuleTask('default');
	
} elseif (Module::$slug[1] != '' && Module::$slug[2] == '' && Blogs_categories::checkItemExists(Module::$slug[1])) {
	// Fetch the data about this product
	$blog_category = Blogs_categories::$view_item_object;
	
	Core::setModuleTask('default');

} elseif (Module::$slug[1] != '' && Module::$slug[2] == '' && Blogs::checkItemExists(Module::$slug[1])) {
	$blog = Blogs::$view_item_object;
	Core::setModuleTask('view');
	
	// SSet up some meta tags
	Meta::setTag('title',Blogs::$view_item_object->title);
	Meta::setTag('keywords',Blogs::$view_item_object->metakeys);
	Meta::setTag('description',Blogs::$view_item_object->metadesc);
	
	// Setup the Canonical URL of this product
	$canonical_domain = Sites::getDomain(Blogs::$view_item_object->site_id);
	$canonical_url = Page::getModuleUrl('blogs').Blogs::$view_item_object->slug.'-'.Blogs::$view_item_object->id.'/';
	Meta::setTag('canonical',$canonical_domain.Tool::cleanUrl($canonical_url));

} else {
	Error::type(404);
	
}

