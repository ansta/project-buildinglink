<?php

$submission = TRUE;

foreach (Config::$fields['blogs'] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
	
	if ($field['name'] == 'title') {
		$new_slug = strtolower($_REQUEST['title']);
		$new_slug = str_replace(' ','-',$new_slug);
		$new_slug = str_replace('_','-',$new_slug);
		$new_slug = str_replace('&','and',$new_slug);
		$new_slug = str_replace('!','',$new_slug);
		$new_slug = str_replace('"','',$new_slug);
		$new_slug = str_replace('£','',$new_slug);
		$new_slug = str_replace('$','',$new_slug);
		$new_slug = str_replace('%','',$new_slug);
		$new_slug = str_replace('^','',$new_slug);
		$new_slug = str_replace('*','',$new_slug);
		$new_slug = str_replace('(','',$new_slug);
		$new_slug = str_replace(')','',$new_slug);
		$new_slug = str_replace('+','',$new_slug);
		$new_slug = str_replace('¬','',$new_slug);
		$new_slug = str_replace('`','',$new_slug);
		$new_slug = str_replace('{','',$new_slug);
		$new_slug = str_replace('}','',$new_slug);
		$new_slug = str_replace('[','',$new_slug);
		$new_slug = str_replace(']','',$new_slug);
		$new_slug = str_replace(':','',$new_slug);
		$new_slug = str_replace('@','',$new_slug);
		$new_slug = str_replace('~','',$new_slug);
		$new_slug = str_replace(';','',$new_slug);
		$new_slug = str_replace("'",'',$new_slug);
		$new_slug = str_replace('#','',$new_slug);
		$new_slug = str_replace(',','',$new_slug);
		$new_slug = str_replace('.','',$new_slug);
		$new_slug = str_replace('/','',$new_slug);
		$new_slug = str_replace('<','',$new_slug);
		$new_slug = str_replace('>','',$new_slug);
		$new_slug = str_replace('?','',$new_slug);
		$new_slug = str_replace('€','',$new_slug);
		$new_slug = str_replace('|','',$new_slug);
		$new_slug = str_replace('¦','',$new_slug);
		$new_slug = str_replace('--','-',$new_slug);
		$new_slug = str_replace('---','-',$new_slug);
		$new_slug = str_replace('----','-',$new_slug);
		$new_slug = str_replace('-----','-',$new_slug);
		$new_slug = str_replace('------','-',$new_slug);
		$new_slug = str_replace('-------','-',$new_slug);
		$_REQUEST['slug'] = $new_slug;
	}
}




if ($submission) {
	$result = Form::processData();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		
		$_SESSION['form']['form-success'] = 'Thank you, your article has been submitted for review and once approved will show on the website.';
		
		header("Location: ".Page::$slug[0]);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}


