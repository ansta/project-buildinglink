<?php
	
	$articles = Blogs::getLatestList(3);
	if ($articles->num_rows > 0) {
		echo '
			<!-- .content-sidebar -->
			<div class="content-sidebar">
				<div class="content-sidebar-bg"></div>
				
				<div class="widget recent-posts">
					
					<h3 class="widget-title">Recent blog posts</h3>
		';
		
					while ($item = $articles->fetch_object()) {
						
					#	// Prepare the product image
					#	if (file_exists(SYSTEM_ROOT.'modules/blogs/uploads/image/'.$item->id.'/list/'.$item->image)) {
					#		$item_image = '/image.php?i=/blogs/uploads/image/'.$item->id.'/list/'.$item->image.'';
					#	} else {
					#		$item_image = '/images/no-image-small.jpg" alt="'.$item->title.'';
					#	}
						$item_url = str_replace("//", "/", Page::getModuleUrl('blogs').$item->slug.'-'.$item->id.'/');
						
						if ($item->site_id == Site::$sitedata['id']) {
							$item_icon = Site::$sitedata['image_path'];
						} else {
							$item_icon = Sites::getSiteIconPath($item->site_id);
						}
						echo '
							<!-- .post -->
							<div class="post">
								<!-- .post-header -->
								<div class="post-header">
									<h2 class="entry-title"><a href="'.$item_url.'">'.$item->title.'</a></h2>
									<div class="entry-date">Posted on '.date('F j, Y',strtotime($item->created)).'</div>
								</div>
								<!-- /.post-header -->
								<div class="post-icon">
									<img src="'.$item_icon.'" alt="'.$item->title.'" style="width:39px;height:39px;">
								</div>
								<!-- .entry-content -->
								<div class="entry-content">
									<p>'.substr(strip_tags($item->content), 0, 100).'... <a href="'.$item_url.'" class="more-link">More <i class="icon-angle-right"></i></a></p>
								</div>
								<!-- /.entry-content -->
								
							</div>
							<!-- /.post -->
						';
					}

		echo '		<a href="'.str_replace("//", "/", Page::getModuleUrl('blogs').'/').'" class="btn">View Blog</a>
					
				</div>
				<!-- /.widget -->
				
			</div>
			<!-- /.content-sidebar -->
		';
		
	} else {
	#	echo '<p>No articles found, please check back soon.</p>';
	}
	





