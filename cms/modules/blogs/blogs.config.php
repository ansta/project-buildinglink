<?php
/* 
 * 
 * 
 * 
 * 
 * 
 * 
 */
self::$settings['blogs'] =
array(
	'orderby'=>'id DESC'
,	'singular'=>'Blog'
,	'plural'=>'Blog'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'attachments'=> array(
		array('name'=>'blogs_categories', 'formtype'=>'multi', 'column'=>'title', 'label'=>'Categories', 'source'=>'blogs_categories', 'hint'=>'To select multiple categories, press and hold down the Ctrl button on your keyboard before clicking with your mouse')
	)
,	'image'=>array(
		'admin'		=>array('width'=> 71,'height'=> 23,'format'=>'jpg','method'=>'crop')
	,	'list' 		=>array('width'=>710,'height'=>230,'format'=>'jpg','method'=>'crop')
	,	'gallery'	=>array('width'=>710,'height'=>230,'format'=>'jpg','method'=>'crop')
	)
);

self::$fields['blogs'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',				'protected'=>false,	'hint'=>'')
,	array('name'=>'site_id',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Site ID',			'protected'=>true,	'hint'=>'')
,	array('name'=>'title',			'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',			'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'slug',			'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'URL identifier',	'protected'=>false,	'regex'=>'/^[a-z0-9\-]*$/',	'hint'=>'Identifies this blog post within the URL structure.<br /><strong style="color:#955;">Once set, it is strongly advised NOT to change it in the future as this can affect your <abbr title="Search Engine Optimisation">SEO</abbr> page ranking.</strong>')
,	array('name'=>'content',		'required'=>true,	'list'=>false,	'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',	'label'=>'Content',			'protected'=>false,	'hint'=>'',	'editor'=>true)
,	array('name'=>'metakeys',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Keywords',		'protected'=>false,	'hint'=>'Recommendation: no more than 5 comma separated keywords all of which <strong>MUST</strong> appear within the page content')
,	array('name'=>'metadesc',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Description',		'protected'=>false,	'hint'=>'Recommendation: a short description of approx. 150 characters')
,	array('name'=>'allow_comments',	'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yes1no2',	'format'=>'b',	'label'=>'Allow comments',	'protected'=>false,	'default'=>1, 'hint'=>'Do you want to allow your website visitors to leave comments?')
,	array('name'=>'active',			'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Item is Active',	'protected'=>false,	'hint'=>'Allows creation of an item without publishing it immediately by selecting <strong>No</strong>.', 'default'=>true)
,	array('name'=>'image',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Photo',			'protected'=>false,	'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 710(W) x 230(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'9097152', 'root'=>'blogs', 'groupby'=>'id')
);

/*

,	array('name'=>'tags',			'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'tags',		'format'=>'s',		'label'=>'Tags',				'protected'=>false,	'hint'=>'May not be supported by all article types. Enter space separated words, each one will be converted to a tag. Autocomplete may suggest tags based on what you start to type.', 'autocomplete_module'=>'articles_tags', 'autocomplete_field'=>'title')
,	array('name'=>'image',			'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',		'label'=>'Image',				'protected'=>false,	'hint'=>'May not be supported by all article types. Accepted formats: jpg, gif, png', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'2097152', 'root'=>'articles', 'groupby'=>'id')
,	array('name'=>'document',		'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'file',		'format'=>'s',		'label'=>'Document',			'protected'=>false,	'hint'=>'May not be supported by all article types. Accepted formats: doc, docx, pdf, txt, rtf', 'filetype'=>'octet-stream,plain,doc,docx,pdf,txt,rtf', 'maxsize'=>'0', 'root'=>'articles', 'groupby'=>'id')


CREATE TABLE blogs (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  `title` VARCHAR(255) DEFAULT NULL,
  `slug` VARCHAR(255) DEFAULT NULL,
  `content` TEXT DEFAULT NULL,
  `metakeys` VARCHAR(255) DEFAULT NULL,
  `metadesc` VARCHAR(255) DEFAULT NULL,
  `tags` VARCHAR(255) DEFAULT NULL,
  `image` VARCHAR(255) DEFAULT NULL,
  `document` VARCHAR(255) DEFAULT NULL,
  `allow_comments` TINYINT(4) NOT NULL DEFAULT 0,
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;








,	'type'=>Articles_types::getSelectList()
,	array('name'=>'type',		'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'select',	'format'=>'i',		'label'=>'Article type',	'protected'=>false,	'hint'=>'',	'settings'=>'articles')

,	'avatar'=>array(
		'thumb'=>array('width'=>60,'height'=>60,'format'=>'jpg')
	,	'large'=>array('width'=>90,'height'=>90,'format'=>'jpg')
	)
,	'comments'=>array(
		2=>'Comments are not permitted'
	,	1=>'Users can post comments'
	)


*/