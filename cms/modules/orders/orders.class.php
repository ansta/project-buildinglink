<?php

class Orders {
	
	/*
	*
	*/
	public static function processSuccessfullPayment() {
		
		// Decode the crypt returned from SagePay
		$decoded = Sagepay::decodeCrypt($_GET['crypt']);
		
		// Add the crypt to the decodad array so that we can store it with the order
		$decoded['crypt'] = $_GET['crypt'];
		
		// Retrieve the $basket_hash from the VendorTxCode returned by the crypt
		// This will be used to link all the baskets, shippings and orders together for this transaction
		$decoded['basket_hash'] = Payments::getBasketHashFromTxCode($decoded['VendorTxCode']);
		
		// In order to store the delivery cost charged into the Orders record we
		// need to look up the billing and shipping countries and add the data to the $decoded array
		$shipping_object = Shippings::checkExists($decoded['basket_hash'],'object');
		$decoded['bill_country'] = $shipping_object->bill_country;
		$decoded['ship_country'] = $shipping_object->ship_country;
		
		// Finalise the orders record by storing all the necessary data
		// and return the unique $order_id for this transaction
		$order_obj = self::finaliseOrder($decoded);
		$decoded['basket_data']	= $order_obj->basket_data;
		$decoded['order_id']	= $order_obj->id;
		
		// Now that we have the $order_id and the $basket_hash, we can link all items together
		$shipping_object = Shippings::finaliseShipping($decoded);
		Baskets::finaliseBasket($decoded);
		
		// Store some data in SESSION so that we can display the order thank you page
		$_SESSION['order_number'] = $decoded['order_id'];
		$_SESSION['vendortxcode'] = $decoded['VendorTxCode'];
		$_SESSION['order_email']  = $shipping_object->email;
		$_SESSION['form']['form-success'] = 'Thank you, your order and payment have been processed successfully.';
		
		// Now send an email to the shop adminimistrators to inform them of the order
		self::sendNotificationEmail($decoded);
		// Now send an email to the shopper to confirm that their order has been placed
		self::sendConfirmationEmail($decoded);
		
	#	echo '<pre>';
	#	var_dump($decoded);
	#	echo '</pre>';
		
		return TRUE;
		
	}
	
	/*
	*
	*/
	public static function finaliseOrder($decoded) {
		// Update order record with decoded data and more
		$version_control = Config::$settings['orders']['version_control'];
		$query_orders = "
			SELECT
				a.*
			FROM
				`orders` a
            WHERE
				a.basket_hash = '".db::link()->real_escape_string($decoded['basket_hash'])."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query_orders .= "	AND a.`ttv_end` IS null";
		}
		$query_orders .= "
			ORDER BY
				a.`id` DESC
			LIMIT 1
		";
		$orders = db::link()->query($query_orders);
#				,	'vat_rate'		=> Sagepay::$vat
		if ($orders->num_rows == 1) {
			if ($order = $orders->fetch_object()) {
				$orders_update_array = array(
					'module'			=> 'orders'
				,	'action'			=> 'update'
				,	'id'				=> $order->id
				,	'amount_paid'		=> number_format($decoded['Amount'],2)
				
				,	'status'			=> $decoded['Status']
				,	'statusdetail'		=> $decoded['StatusDetail']
				,	'vendortxcode'		=> $decoded['VendorTxCode']
				,	'vpstxid'			=> $decoded['VPSTxId']
				,	'txauthno'			=> $decoded['TxAuthNo']
				,	'amount'			=> $decoded['Amount']
				,	'avscv2'			=> $decoded['AVSCV2']
				,	'addressresult'		=> $decoded['AddressResult']
				,	'postcoderesult'	=> $decoded['PostCodeResult']
				,	'cv2result'			=> $decoded['CV2Result']
				,	'giftaid'			=> $decoded['GiftAid']
				,	'threedsecurestatus'=> $decoded['3DSecureStatus']
				,	'cavv'				=> $decoded['CAVV']
				,	'cardtype'			=> $decoded['CardType']
				,	'last4digits'		=> $decoded['Last4Digits']
				,	'crypt'				=> $decoded['crypt']
				);
				$result = Form::processData('orders',$orders_update_array);
				if ($result['query_status'] == 'OK') {
					return $order;
				}
			}
		}
		return 0;
	}

	/*
	* Creates an entry in ORDERS to hold the main order data and provide the system with an order number
	*/
	public static function createOrder($basket_hash) {
		$version_control = Config::$settings['orders']['version_control'];
		$query_orders = "
			SELECT
				a.*
			FROM
				`orders` a
            WHERE
				a.basket_hash = '".db::link()->real_escape_string($basket_hash)."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query_orders .= "	AND a.`ttv_end` IS null";
		}
		$query_orders .= "
			ORDER BY
				a.`id` DESC
			LIMIT 1
		";
	#	echo $query;
		$orders = db::link()->query($query_orders);
		if ($orders->num_rows == 1) {
			if ($order = $orders->fetch_object()) {
				return $order->id;
			}
		} else {
			$orders_item_array = array(
				'module' => 'orders'
			,	'action' => 'insert'
			,	'basket_hash' => $basket_hash
			,	'bill_name' => $_SESSION['bill_firstname'].' '.$_SESSION['bill_lastname']
			,	'bill_email' => $_SESSION['email']
			);
			$result = Form::processData('orders',$orders_item_array);
			if ($result['query_status'] == 'OK') {
				return $result['item_id'];
			}
		}
		return 0;
	}
	
	
	/*
	* 
	*/
	public static function sendNotificationEmail($decoded) {
		$email	= Site::$sitedata['email_contact'];
		$subject= 'Building Link order notification: '.$decoded['VendorTxCode'].'';
		$summary = json_decode($decoded['basket_data'],true);
		$html	.= '<p>Order Number: <strong>'.$decoded['VendorTxCode'].'</strong></p>';
		$html	.= '<p>An order has been received from <strong>'.$summary['shipping']['bill_firstname'].' '.$summary['shipping']['bill_lastname'].'</strong> (email: '.$_SESSION['order_email'].').<br>';
		$html	.= 'Below is a summary of their order:</p>';
		$html	.= self::getOrderDetailsForEmail($decoded,'notification');
		Email::sendEmail($email,$subject,$html);
	}
	
	/*
	* 
	*/
	public static function sendConfirmationEmail($decoded) {
		$email	= $_SESSION['order_email'];
		$subject= 'Building Link order confirmation: '.$decoded['VendorTxCode'].'';
		$summary = json_decode($decoded['basket_data'],true);
		$html	.= '<p>Dear '.$summary['shipping']['bill_firstname'].' '.$summary['shipping']['bill_lastname'].',</p>';
		$html	.= '<p>Thank you, your order has been placed with a reference number of <strong>'.$decoded['VendorTxCode'].'</strong>, please quote this number in the event that you should need to contact us.<br>';
		$html	.= 'Below is a reminder of your order:</p>';
		$html	.= self::getOrderDetailsForEmail($decoded,'confirmation');
		Email::sendEmail($email,$subject,$html);
	}
	
	/*
	* Build up the HTML to go into the email to both the shop owner and the customer
	*/
	public static function getOrderDetailsForEmail($decoded,$type) {
		
		$output = '';
		$summary = json_decode($decoded['basket_data'],true);
		$currency_symbol = '&pound;';
		
		$output .= '<table>';
			$output .= '<tr>';
				$output .= '<td>';
		
					$output .= '<table>';
						$output .= '<tr><th colspan="2" style="text-align:left;">Shipping address</th></tr>';
						foreach (Config::$fields['shippings'] AS $field) {
							if ($field['formset'] == 'ship' && $field['formtype'] != 'hidden' && $summary['shipping'][$field['name']] != '') {
								$output .= '<tr>';
									$output .= '<td>'.$field['label'].':</td>';
									if ($field['name'] == 'ship_country') {
										$output .= '<td>'.Countries::getItem($summary['shipping'][$field['name']],'title').'</td>';
									} elseif ($field['name'] == 'ship_usstate') {
										$output .= '<td>'.Us_states::getItem($summary['shipping'][$field['name']],'title').'</td>';
									} else {
										$output .= '<td>'.$summary['shipping'][$field['name']].'</td>';
									}
								$output .= '</tr>';
							}
						}
					$output .= '</table>';
	
				$output .= '</td>';
				$output .= '<td>';
				
					$output .= '<table>';
						$output .= '<tr><th colspan="2" style="text-align:left;">Billing address</th></tr>';
						foreach (Config::$fields['shippings'] AS $field) {
							if ($field['formset'] == 'bill' && $field['formtype'] != 'hidden' && $summary['shipping'][$field['name']] != '') {
								$output .= '<tr>';
									$output .= '<td>'.$field['label'].':</td>';
									if ($field['name'] == 'bill_country') {
										$output .= '<td>'.Countries::getItem($summary['shipping'][$field['name']],'title').'</td>';
									} elseif ($field['name'] == 'bill_usstate') {
										$output .= '<td>'.Us_states::getItem($summary['shipping'][$field['name']],'title').'</td>';
									} else {
										$output .= '<td>'.$summary['shipping'][$field['name']].'</td>';
									}
								$output .= '</tr>';
							}
						}
					$output .= '</table>';
		
				$output .= '</td>';
			$output .= '</tr>';
		$output .= '</table>';
		
		
		$output .= '<table style="width:700px;border-top:1px solid #000;border-left:1px solid #000;border-collapse:collapse;" border="1">';
		
			$output .= '<tr>';
				$output .= '<th>Product Details</th>';
				$output .= '<th>Product No</th>';
				$output .= '<th>Unit Cost</th>';
				$output .= '<th>Quantity</th>';
				$output .= '<th>Line Value</th>';
			$output .= '</tr>';
			
			foreach ($summary['basket'] AS $line => $item) {
				$total_basket_price = $total_basket_price + ($item["quantity"] * $item["item_price"]);
				$output .= '<tr>';
					$output .= '<td style="border-right:1px solid #000;border-bottom:1px solid #000;">';
						$output .= $item['item_title'].' '.$item["variant_title"];
						if ((int)$item["setup_price"] > 0) {
							$output .= ' (setup fee: '.$currency_symbol.number_format($item["setup_price"], 2).')';
						}
						
						// If artwork has been supplied for this item then provide a link to the shop keeper
						if ($type == 'notification') {
							if ($item["artwork"] != '' && file_exists(SYSTEM_ROOT.'/modules/baskets/uploads/artwork/'.$item["id"].'/')) {
								$field['root'] = 'baskets';
								$field['name'] = 'artwork';
								$field['groupby'] = $item["id"];
								$field_value = $item["artwork"];
								$file_path = Page::$scheme.Page::$host.'/download_file.php?r='.$field['root'].'&f='.$field['name'].'&gb='.$field['groupby'].'&i='.$field_value;
								$output .= ' - <span><a href="'.$file_path.'">'.$field_value.'</a></span>';
							#	$output .= '<span> '.strtoupper(substr($item["artwork"], -3)).': <a href="'.$file_path.'">'.$field_value.'</a></span>';
							}
						}
						
					$output .= '</td>';
					$output .= '<td style="border-right:1px solid #000;border-bottom:1px solid #000;">';
						$output .= $item["item_ref"];
					$output .= '</td>';
					$output .= '<td style="border-right:1px solid #000;border-bottom:1px solid #000;">';
						$output .= $item["item_price"];
					$output .= '</td>';
					$output .= '<td style="border-right:1px solid #000;border-bottom:1px solid #000;">';
						$output .= $item["quantity"];
					$output .= '</td>';
					$output .= '<td style="border-right:1px solid #000;border-bottom:1px solid #000;">';
						$output .= $currency_symbol.number_format(($item["item_price"]*$item["quantity"]),2);
					$output .= '</td>';
				$output .= '</tr>';
			}
			
			$output .= '<tr>';
				$output .= '<td colspan="4" style="width:75%;text-align:right;padding-right:10px;border-right:1px solid #000;border-bottom:1px solid #000;">Delivery Charge</td>';
				$output .= '<td style="width:25%;border-right:1px solid #000;border-bottom:1px solid #000;">'.$currency_symbol.number_format($summary['totals']['delivery_cost'],2).'</td>';
			$output .= '</tr>';
			
			$output .= '<tr>';
				$output .= '<td colspan="4" style="text-align:right;padding-right:10px;border-right:1px solid #000;border-bottom:1px solid #000;">Total (exc. VAT)</td>';
				$output .= '<td style="border-right:1px solid #000;border-bottom:1px solid #000;">'.$currency_symbol.number_format($summary['totals']['total_excluding_vat'],2).'</td>';
			$output .= '</tr>';
			
			$output .= '<tr>';
				$output .= '<td colspan="4" style="text-align:right;padding-right:10px;border-right:1px solid #000;border-bottom:1px solid #000;">VAT ('.number_format($summary['totals']['vat_rate'],2).'%)</td>';
				$output .= '<td style="border-right:1px solid #000;border-bottom:1px solid #000;">'.$currency_symbol.number_format($summary['totals']['total_vat'],2).'</td>';
			$output .= '</tr>';
			
			$output .= '<tr>';
				$output .= '<td colspan="4" style="text-align:right;font-weight:bold;padding-right:10px;border-right:1px solid #000;border-bottom:1px solid #000;">Total</td>';
				$output .= '<td style="font-weight:bold;border-right:1px solid #000;border-bottom:1px solid #000;">'.$currency_symbol.number_format($summary['totals']['total_basket_price'],2).'</td>';
			$output .= '</tr>';
		
		$output .= '</table>';
		
		$output = str_replace('<',PHP_EOL.'<',$output);
	#	$output = str_replace('&',PHP_EOL.'&',$output);
		
	#	$debugging .= Tool::my_dump($summary);
	#	Email::sendEmail('loki.ansta@gmail.com','Building Link - getOrderDetailsForEmail()','<pre>'.$debugging.'</pre><hr>'.PHP_EOL.$output);
		
		return $output;
	}
	
	/*
	*
	*/
	public static function createOrderSummary() {
		
		// Prepare an empty array that weill hold all the data
		$order = array();
		
		// Get shipping data
		$shippings = Shippings::getItem();
		if ($shippings->num_rows == 1) {
			$shipping = $shippings->fetch_assoc();
			foreach($shipping AS $ship_field => $ship_value) {
				$order['shipping'][$ship_field] = $ship_value;
			}
		}
		
		// Get VAT rate based on shipping data
		$master_vat = Countries::getMasterVatRate();
		
		// Get VAT rate based on shipping data
		$country_vat = Countries::getVatRate($order['shipping']['bill_country']);
		
		// Get delivery cost based on shipping data
		$delivery_cost_array = Countries::getDeliveryCost($order['shipping']['ship_country'],$country_vat);
		
		// Get basket items
		$i = 0;
		$total_basket_price = 0;
		$baskets = Baskets::getItems();
		if ($baskets->num_rows > 0) {
			while ($basket_item = $baskets->fetch_assoc()) {
				$i++;
				
				$line_item_price	= $basket_item['quantity'] * $basket_item['item_price'];
				$setup_item_price	= $basket_item['setup_price'];
				
				$line_item_master_vat	= $line_item_price - ($line_item_price / $master_vat['divider']);
				$setup_item_master_vat	= $setup_item_price - ($setup_item_price / $master_vat['divider']);
				
				if ($database_product_prices_include_vat == true) {
					$line_item_price_excluding_vat	= $line_item_price - $line_item_master_vat;
					$setup_item_price_excluding_vat	= $setup_item_price - $setup_item_master_vat;
				} else {
					$line_item_price_excluding_vat	= $line_item_price;
					$setup_item_price_excluding_vat	= $setup_item_price;
				}
				
				if ($country_vat['percent'] > 0) {
					$line_item_local_vat	= ($line_item_price_excluding_vat / 100) * $country_vat['percent'];
					$setup_item_local_vat	= ($setup_item_price_excluding_vat / 100) * $country_vat['percent'];
				} else {
					$line_item_local_vat	= 0;
					$setup_item_local_vat	= 0;
				}
				
				$line_item_including_local_vat	= $line_item_price_excluding_vat + $line_item_local_vat;
				$setup_item_including_local_vat	= $setup_item_price_excluding_vat + $setup_item_local_vat;
	
				$total_basket_vat	= $total_basket_vat + $line_item_local_vat + $setup_item_local_vat;
				$total_basket_price = $total_basket_price + $line_item_including_local_vat + $setup_item_including_local_vat;
				
				
				foreach($basket_item AS $basket_field => $basket_value) {
					$order['basket'][$i][$basket_field] = $basket_value;
					if ($basket_field == 'item_price') {
						$order['basket'][$i]['line_vat'] = $line_item_local_vat;
						$order['basket'][$i]['line_price_inc_vat'] = $line_item_including_local_vat;
						$order['basket'][$i]['setup_vat'] = $setup_item_local_vat;
						$order['basket'][$i]['setup_price_inc_vat'] = $setup_item_including_local_vat;
					}
				}
				$order['basket'][$i]['vat_rate'] = $country_vat['percent'].'%';
			}
		}
		
		// Get totals
		$total_basket_price = $total_basket_price + $delivery_cost_array['inc_vat'];
		
		$order['totals']['discount'] = 0;
		
		$order['totals']['delivery_cost'] = $delivery_cost_array['exc_vat'];
		
		$order['totals']['total_excluding_vat'] = $total_basket_price - $total_basket_vat - $delivery_cost_array['vat'];
		
		$order['totals']['total_vat'] = $total_basket_vat + $delivery_cost_array['vat'];
		
		$order['totals']['vat_rate'] = $country_vat['percent'];
		
		$order['totals']['total_basket_price'] = $total_basket_price;
		
	#	echo '<pre style="color:#FFF;">';
	#	print_r($order);
	#	echo '</pre>';
		
		// Return array
		return $order;
	}

}


















