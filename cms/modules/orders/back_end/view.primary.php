<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
View <?php echo Config::$settings[Page::$slug[2]]['singular']; ?>: <?php echo Form::getItemTitle(); ?></h1>
	
<section>
	<?php
	$order = Admin::getItem(Page::$slug[4]);
	foreach (Config::$fields[Module::$name] AS $field) {
		if ($field['formset'] == 'view') {
			echo '<div class="field type-'.$field['formtype'].'">';
				echo '<span class="label">'.$field['label'].': </span>';
				echo '<span class="value">';
					if ($field['name'] == 'discount'
					||	$field['name'] == 'delivery_cost'
					||	$field['name'] == 'total_excluding_vat'
					||	$field['name'] == 'total_vat'
					||	$field['name'] == 'total_to_pay') {
						echo '&pound;'.number_format(str_replace(',','',$order->$field['name']),2);
					} elseif ($field['name'] == 'amount_paid') {
						echo '&pound;'.number_format(str_replace(',','',$order->amount),2);
					} elseif ($field['name'] == 'vat_rate') {
						echo number_format($order->$field['name'],2).'%';
					} else {
						echo $order->$field['name'];
					}
				echo '</span>';
			echo '</div>';
		}
	}
	echo '<div class="field"><span class="label">Order placed</span>: <span class="value">'.Form::makeHumanDate($order->ttv_start).'</span></div>';
	?>
	
	<hr />
	
	<?php
	$baskets = Baskets::getItems($order->basket_hash);
	if($baskets->num_rows > 0) {
		echo '<table style="font-size:1.3em;width:100%;">';
			echo '<tr style="font-size:1.1em;">';
				echo '<th style="text-align:left">Product Details</th>';
				echo '<th style="text-align:left">Product No</th>';
				echo '<th style="text-align:left">Unit Cost</th>';
				echo '<th style="text-align:left">Quantity</th>';
				echo '<th style="text-align:left">Line Value</th>';
			echo '</tr>';
			
			while($basket = $baskets->fetch_object()) {
				$basket_data = json_decode($basket->basket_data,true);
				echo '<tr>';
					echo '<td>';
						echo $basket->item_title.' '.$basket->variant_title;
						
						// Display ARTWORK status
						$real_path = SYSTEM_ROOT.'/modules/baskets/uploads/artwork/'.$basket->id.'/'.$basket->artwork;
						if ($basket->artwork != '' && file_exists($real_path)) {
							$field['root'] = 'baskets';
							$field['name'] = 'artwork';
							$field['groupby'] = $basket->id;
							$field_value = $basket->artwork;
							$file_path = '/download_file.php?r='.$field['root'].'&f='.$field['name'].'&gb='.$field['groupby'].'&i='.$field_value;
							echo  '<br>Artwork: <a href="'.$file_path.'">'.$field_value.'</a> ('.strtoupper(substr($basket->artwork, -3)).' '.Tool::formatFilesize($real_path).')</span>';
						}
						
						if ($basket->setup_price > 0) {
							echo ' (Setup fee: &pound;'.$basket->setup_price.')';
						}
						
					#	Tool::pre_dump($basket);
					#	Tool::pre_dump($basket_data);
						
					echo '</td>';
					
					echo '<td>';
						echo $basket->item_ref;
					echo '</td>';
					
					echo '<td>';
						echo '&pound;'.number_format($basket->item_price,2);
					echo '</td>';
					
					echo '<td>';
						echo $basket->quantity;
					echo '</td>';
					
					echo '<td>';
						echo '&pound;'.number_format(($basket->quantity * $basket->item_price) + $basket->setup_price,2);
					echo '</td>';
					
				echo '</tr>';
			
			}
		echo '</table>';
	}
	?>
	
	<hr />
	
	<?php
	$shippings = Shippings::getItem($order->basket_hash);
	if($shippings->num_rows > 0) {
		while($shipping = $shippings->fetch_object()) {
			echo '<div style="width:50%;float:left;">';
				echo '<h2>Shipping Address</h2>';
				foreach (Config::$fields['shippings'] AS $ship_field) {
					if ($ship_field['formset'] == 'ship' && $ship_field['formtype'] != 'hidden') {
						if ($shipping->$ship_field['name'] != '') {
							echo '<div class="field type-'.$ship_field['formtype'].'">';
								echo '<span class="label">'.$ship_field['label'].': </span>';
								if ($ship_field['formtype'] == 'select') {
									if ($ship_field['name'] == 'ship_country') {
										echo '   <span class="value">'.Config::$settings['shippings']['ship_country'][$shipping->$ship_field['name']].'</span>';
									} else {
										echo '   <span class="value">'.$shipping->$ship_field['name'].'</span>';
									}
									
								} else {
									echo '<span class="value">'.$shipping->$ship_field['name'].'</span>';
								}
							echo '</div>';
						}
					}
				}
			echo '</div>';
			echo '<div style="width:49%;float:left;">';
				echo '<h2>Billing Address</h2>';
				foreach (Config::$fields['shippings'] AS $ship_field) {
					if ($ship_field['formset'] == 'bill' && $ship_field['formtype'] != 'hidden') {
						if ($shipping->$ship_field['name'] != '') {
							echo '<div class="field type-'.$ship_field['formtype'].'">';
								echo '<span class="label">'.$ship_field['label'].': </span>';
								if ($ship_field['formtype'] == 'select') {
									if ($ship_field['name'] == 'bill_country') {
										echo '   <span class="value">'.Config::$settings['shippings']['bill_country'][$shipping->$ship_field['name']].'</span>';
									} else {
										echo '   <span class="value">'.$shipping->$ship_field['name'].'</span>';
									}
								} else {
									echo '<span class="value">'.$shipping->$ship_field['name'].'</span>';
								}
							echo '</div>';
						}
					}
				}
			echo '</div>';
		}
	}
	?>
	
</section>

<?php
	/*
	
	
	
		#	foreach (Config::$fields['baskets'] AS $field) {
		#	#	if ($field['formset'] == 'view') {
		#			echo '<div class="field type-'.$field['formtype'].'">';
		#				echo '<span class="label">'.$field['name'].': </span>';
		#				echo '<span class="value">'.$basket->$field['name'].'</span>';
		#			echo '</div>';
		#	#	}
		#	}
		#	id: 99
		#	basket_hash: 17f6cdfc8f40ac21a2273eb20d376e27
		#	order_id: 271
		#	item_id: 1
		#	item_title: test product 1
		#	item_size: Petite Deluxe
		#	item_price: 111.50
		#	quantity: 2
		#	options_title: [{"item_id":"1","item_title":"Gold Leaf on NSEW","item_label":"Petite Deluxe","item_price":"37.00"},{"item_id":"4","item_title":"NSEO\/NSOW","item_label":"Petite Deluxe","item_price":"7.50"}]
		#	options_price: 44.50
		
		
		
		
		
		
		
		
	$shippings = Shippings::getItem($order->basket_hash);
	if($shippings->num_rows > 0) {
			$i = 0;
			$j = 0;
			while($shipping = $shippings->fetch_object()) {
				foreach (Config::$fields['shippings'] AS $ship_field) {
					if ($ship_field['formset'] == 'ship') {
						$i++;
						$ship_data[$i] = '<td>';
							$ship_data[$i] .= $ship_field['label'];
						$ship_data[$i] .= '</td>'.PHP_EOL;
						$ship_data[$i] .= '<td>';
							$ship_data[$i] .= $shipping->$ship_field['name'];
						$ship_data[$i] .= '</td>'.PHP_EOL;
					}
				}
				foreach (Config::$fields['shippings'] AS $bill_field) {
					if ($ship_field['formset'] == 'bill') {
						$j++;
						$bill_data[$j] = '<td>';
							$bill_data[$j] .= $bill_field['label'];
						$bill_data[$j] .= '</td>'.PHP_EOL;
						$bill_data[$j] .= '<td>';
							$bill_data[$j] .= $shipping->$bill_field['name'];
						$bill_data[$j] .= '</td>'.PHP_EOL;
					}
				}
			}
		echo '<table width="100%">';
			for ($k=1;$k<$i;$k++) {
				echo '<tr class="field type-'.$field['formtype'].'">'.PHP_EOL;
					echo $ship_data[$k];
					echo $bill_data[$k];
				echo '</tr>'.PHP_EOL;
			}
		echo '</table>';
	}
	*/