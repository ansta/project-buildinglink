<h1>Confirm your order</h1>

<?php echo Page::showErrors(); ?>

<?php
	// Get as much data and settings as we can
	$basket_items		= Baskets::getItems();	// Just used to check that the basket has items in it
	$currency_symbol	= '&pound;';
	$payment_url		= Sagepay::$sage_pay[SAGEPAY_CONFIG]['url'];

	// Calculate some costs
#	$delivery_cost		= Countries::getDeliveryCost($shipping->ship_country);
#	$vat_rate			= Countries::getVatRate($shipping->bill_country);

	$order_id			= Orders::createOrder($_SESSION['basket']);

	// Send some data to the Sagepay class
	Sagepay::$vendortxcode	= Payments::generateTransactionId();
	Sagepay::$shopper_details= Shippings::$view_item_object;
#	Sagepay::$vat			= $vat_rate['divider'];

	// Get order data to display confirmation page
	$summary = Orders::createOrderSummary();

	// Display basket summary (if basket is not empty!)
	if (!empty($summary['basket'])) {

		// Display basket table headers
		echo '<table class="basket" style="width:100%;margin-bottom:20px;">';
			echo '<thead>';
				echo '<tr>';
					echo '<th>Product Details</th>';
					echo '<th>Product No</th>';
					echo '<th>Unit Cost</th>';
					echo '<th>Quantity</th>';
					echo '<th>Line Value</th>';
				echo '</tr>';
			echo '</thead>';

			$country_vat = Countries::getVatRate($summary["shipping"]["bill_country"]);

			// Display each item in the basket
			foreach ($summary['basket'] AS $line => $item) {
				$line_unit_price = ($item["item_price"] / $item["quantity"]);
				echo '<tr>';
					echo '<td>';
			#			$line_total_price = ($item["item_price"] + $item["options_price"]);
						$line_description = $item['item_title'].' '.$item["variant_title"]; # at '.$currency_symbol.number_format($item["item_price"],2).' each (exc.VAT)'
					#	if ((int)$item["setup_price"] > 0) {
					#		$line_description .= ' (setup fee: '.$currency_symbol.number_format($item["setup_price"], 2).')';
					#	}
						echo $line_description;

						// Display ARTWORK status
						$real_path = SYSTEM_ROOT.'/modules/baskets/uploads/artwork/'.$item['id'].'/'.$item['artwork'];
						if ($item['artwork'] != '' && file_exists($real_path)) {
							$field['root'] = 'baskets';
							$field['name'] = 'artwork';
							$field['groupby'] = $item['id'];
							$field_value = $item['artwork'];
							$file_path = SERVER_NAME.'/download_file.php?r='.$field['root'].'&f='.$field['name'].'&gb='.$field['groupby'].'&i='.$field_value;
							echo  '<br><span class="notify">Artwork supplied: Yes ('.strtoupper(substr($item['artwork'], -3)).' '.Tool::formatFilesize($real_path).')</span>';
						}

					echo '</td>';
					echo '<td>';
						echo $item["item_ref"];
					echo '</td>';
					echo '<td>';
						echo $currency_symbol.number_format($item["item_price"],2);
					echo '</td>';
					echo '<td>';
						echo $item["quantity"];
					echo '</td>';
			#		echo '<td>';
			#			$line_total_excluding_vat = $line_total_price;
			#			$line_item_country_vat	= (($line_total_excluding_vat / 100) * $country_vat['percent']);
			#			echo $currency_symbol.number_format($item["line_vat"], 2);
			#		echo '</td>';
					echo '<td>';
						echo $currency_symbol.number_format(($item["line_price_inc_vat"]-$item["line_vat"]),2, '.', ',');
					echo '</td>';
				echo '</tr>';

				// Send each item's data to SagePay to display the basket content in SagePay interface
				$payment['item_price'] = $item["item_price"];
				$payment['line_quantity'] = $item["quantity"];
				$payment['line_tax'] = ($item["line_vat"] / $item["quantity"]);
				$payment['line_desc'] = $item["item_ref"].' '.$item['item_title'].' '.$item["variant_title"];
				Sagepay::addItem($payment['line_desc'],$payment['item_price'],$payment['line_quantity'],$payment['line_tax']);

				if ((int)$item["setup_price"] > 0) {
					echo '<tr>';
						echo '<td colspan="2">';
							$payment['setup_desc'] = 'Setup fee for '.$payment['line_desc'];
							echo $payment['setup_desc'];
						echo '</td>';
						echo '<td>';
							echo $currency_symbol.number_format($item["setup_price"],2);
						echo '</td>';
						echo '<td>';
							echo '1';
						echo '</td>';
						echo '<td>';
							echo $currency_symbol.number_format(($item["setup_price_inc_vat"]-$item["setup_vat"]),2, '.', ',');
						echo '</td>';
					echo '</tr>';

					// Send each item's data to SagePay to display the basket content in SagePay interface
					$setup['setup_price'] = $item["setup_price"];
					$setup['setup_quantity'] = 1;
					$setup['setup_tax'] = $item["setup_vat"];
					$setup['setup_desc'] = $payment['setup_desc'];
					Sagepay::addItem($setup['setup_desc'],$setup['setup_price'],$setup['setup_quantity'],$setup['setup_tax']);
				}
			}

			// Display basket totals
			echo '<tfoot>';
				echo '<tr class="highlight">';
					echo '<td colspan="4" style="text-align:right;padding-right:10px;">';
						echo 'Delivery Charge:';
					echo '</td>';
					echo '<td>';
						echo $currency_symbol.number_format($summary['totals']['delivery_cost'],2, '.', ',');
						Sagepay::addExtraLine('Delivery Charge',$summary['totals']['delivery_cost']);
					echo '</td>';
				echo '</tr>';

				echo '<tr class="highlight">';
					echo '<td colspan="4" style="text-align:right;padding-right:10px;">';
						echo 'Total (exc. VAT):';
					echo '</td>';
					echo '<td>';
						echo $currency_symbol.number_format($summary['totals']['total_excluding_vat'],2, '.', ',');
						Sagepay::addExtraLine('Total (exc. VAT)',$summary['totals']['total_excluding_vat']);
					echo '</td>';
				echo '</tr>';

				echo '<tr class="highlight">';
					echo '<td colspan="4" style="text-align:right;padding-right:10px;">';
						echo 'VAT ('.number_format($summary['totals']['vat_rate'],2).'%):';
					echo '</td>';
					echo '<td>';
						echo $currency_symbol.number_format($summary['totals']['total_vat'],2, '.', ',');
						Sagepay::addExtraLine('VAT ('.number_format($summary['totals']['vat_rate'],2).'%)',$summary['totals']['total_vat']);
					echo '</td>';
				echo '</tr>';

				echo '<tr class="highlight">';
					echo '<td colspan="4" style="text-align:right;padding-right:10px;">';
						echo '<strong>Total</strong>:';
					echo '</td>';
					echo '<td><strong>';
						echo $currency_symbol.number_format($summary['totals']['total_basket_price'],2, '.', ',');
						Sagepay::$total_to_pay	= $summary['totals']['total_basket_price'];
						Sagepay::addExtraLine('Total to pay',$summary['totals']['total_basket_price']);
					echo '</strong></td>';
				echo '</tr>';
			echo '</tfoot>';

		echo '</table>';


		// Send data to Sagepay class
	#	Sagepay::$total_to_pay	= $summary['totals']['total_basket_price'];
	#	Sagepay::addExtraLine('Delivery Charge',number_format($summary['totals']['delivery_cost'],2, '.', ''));
	#	Sagepay::addExtraLine('Total to Pay',Sagepay::$total_to_pay);

		// Update the order record with this data so that we don't have to calculate it again
		$order_item_array = array(
			'module' => 'orders'
		,	'action' => 'update'
		,	'id' => $order_id
		,	'basket_data' => json_encode($summary)
		,	'delivery_cost' => number_format($summary['totals']['delivery_cost'],2)
		,	'total_excluding_vat' => number_format($summary['totals']['total_excluding_vat'],2)
		,	'total_vat' => number_format($summary['totals']['total_vat'],2)
		,	'total_to_pay' => number_format($summary['totals']['total_basket_price'],2)
		,	'vat_rate' => number_format($summary['totals']['vat_rate'],2)
		);
		$orders_update_result = Form::processData('orders',$order_item_array);

	}



	// Display Shipping and Billing details (if basket is not empty!)
	if ($basket_items->num_rows > 0) {
		echo '<div class="confirm-shipping">';
			echo '<table class="confirm-details" style=" border-right: none;">';
				echo '<tr><th colspan="2">Shipping address</th></tr>';
				foreach (Config::$fields['shippings'] AS $field) {
					if ($field['formset'] == 'ship' && $field['formtype'] != 'hidden' && $summary['shipping'][$field['name']] != '') {
						echo '<tr>';
							echo '<td class="col1">'.$field['label'].':</td>';
							if ($field['name'] == 'ship_country') {
								echo '<td class="col2">'.Countries::getItem($summary['shipping'][$field['name']],'title').'</td>';
							} elseif ($field['name'] == 'ship_usstate') {
								echo '<td class="col2">'.Us_states::getItem($summary['shipping'][$field['name']],'title').'</td>';
							} else {
								echo '<td class="col2">'.$summary['shipping'][$field['name']].'</td>';
							}
						echo '</tr>';
					}
				}
			echo '</table>';
		echo '</div>';

		echo '<div class="confirm-shipping">';
			echo '<table class="confirm-details">';
				echo '<tr><th colspan="2">Billing address</th></tr>';
				foreach (Config::$fields['shippings'] AS $field) {
					if ($field['formset'] == 'bill' && $field['formtype'] != 'hidden' && $summary['shipping'][$field['name']] != '') {
						echo '<tr>';
							echo '<td class="col1">'.$field['label'].':</td>';
							if ($field['name'] == 'bill_country') {
								echo '<td class="col2">'.Countries::getItem($summary['shipping'][$field['name']],'title').'</td>';
							} elseif ($field['name'] == 'bill_usstate') {
								echo '<td class="col2">'.Us_states::getItem($summary['shipping'][$field['name']],'title').'</td>';
							} else {
								echo '<td class="col2">'.$summary['shipping'][$field['name']].'</td>';
							}
						echo '</tr>';
					}
				}
			echo '</table>';
		echo '</div>';
	#	echo '<pre>';
	#	var_dump($summary['shipping']);
		?>

		<div style="float:left;width:99%">
			<?php
			echo '<p style="clear:both;padding-top:10px;">Upon order completion, a confirmation email will be sent to: <strong>'.$summary['shipping']['email'].'</strong></p>';
			?>
		</div>

		<form action="/shipping/" method="post" style="clear:both; float: left;">
			<?php echo Form::makeField(array('formtype'=>'submit','name'=>'back','value'=>'&laquo; Back')); ?>
		</form>

		<form style=" float: right;" action="<?php echo $payment_url; ?>" method="post" name="redirectToPayment">
			<input type="hidden" name="VPSProtocol" value="3.0" />
			<input type="hidden" name="TxType" value="PAYMENT" />
			<input type="hidden" name="Vendor" value="<?php echo Sagepay::$sage_pay[SAGEPAY_CONFIG]['username']; ?>" />
			<input type="hidden" name="Crypt" value="<?php echo Sagepay::createCrypt(); ?>" />
			<input type="submit" value="Proceed to Online Payment &raquo;" class="button buttonRight"  />
		</form>

		<?php

		echo '<p style="clear:both;padding-top:10px; font-size: 12px; line-height: 1.5em;">Upon clicking Proceed to Online Payment, you will be taken to our payment gateway powered by SagePay.
		<br />Once you have finished paying, you will be brought back here and will be provided with an order confirmation number and send you an email summarising the order you have just placed.
		<br /><br />In the unlikely event of a problem occuring, please contact us.</p>';

	} else {
		// If basket IS empty, then display an appropriate message instead
		echo '<p style="">Your basket appears to be empty. Why not <a href="/">return to the homepage</a> to begin shopping.</p>';
	}





