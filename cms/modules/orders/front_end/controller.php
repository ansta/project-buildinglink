<?php

// Pre-SagePay order confirmation page
if (Module::$slug[1] == 'confirm' && Module::$slug[2] == '') {
	
	if (Shippings::checkExists($_SESSION['basket'],'object')) {
		Core::setModuleTask('confirm');
		Orders::createOrderSummary();
	} else {
		header('Location: /shipping/');
		exit();
	}
	
// SagePay Success URL
} elseif (Module::$slug[1] == 'process') {
	
	if (Orders::processSuccessfullPayment()) {
		Baskets::resetCookies();
		header('Location: /order/complete/');
		exit();
	}
	
// SagePay Error URL
} elseif (Module::$slug[1] == 'error') {
	Core::setModuleTask('error');
	
// Order thank you page once payment has been completed (appears after: SagePay Success URL)
} elseif (Module::$slug[1] == 'complete' && Module::$slug[2] == '') {
	Core::setModuleTask('complete');
	
// Page not found
} else {
	Error::type(404);
	
}

