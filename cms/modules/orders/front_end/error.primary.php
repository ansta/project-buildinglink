<?php $decoded = Sagepay::decodeCrypt($_GET['crypt']); ?>

<h1>Payment Error</h1>

<p>An error occured while trying to process your payment. Please contact us.</p>

<p>Error details:

<?php
echo '<br>&bull; Status:		'.$decoded["Status"];
echo '<br>&bull; StatusDetail:	'.$decoded["StatusDetail"];
echo '<br>&bull; Amount:		'.$decoded["Amount"];
echo '<br>&bull; CardType:		'.$decoded["CardType"];
#echo '<br>&bull; VendorTxCode: '.$decoded["VendorTxCode"];

/*
array(14) {
  ["Status"]=>
  string(9) "NOTAUTHED"
  ["StatusDetail"]=>
  string(40) "NOTAUTHED message generated by Simulator"
  ["VendorTxCode"]=>
  string(2) "63"
  ["VPSTxId"]=>
  string(38) "{A9FA8888-64A1-42F3-9A8D-4427A97CBF79}"
  ["Amount"]=>
  string(6) "109.49"
  ["AVSCV2"]=>
  string(9) "ALL MATCH"
  ["AddressResult"]=>
  string(7) "MATCHED"
  ["PostCodeResult"]=>
  string(7) "MATCHED"
  ["CV2Result"]=>
  string(7) "MATCHED"
  ["GiftAid"]=>
  string(1) "0"
  ["3DSecureStatus"]=>
  string(2) "OK"
  ["CAVV"]=>
  string(22) "MNT6PM6M3R5PFN3B20FD01"
  ["CardType"]=>
  string(4) "VISA"
  ["Last4Digits"]=>
  string(4) "9309"
}
*/
?>
</p>