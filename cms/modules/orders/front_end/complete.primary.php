<h1>Order Complete</h1>

<?php echo Page::showErrors(); ?>

<p>Your order has been recorder with the number <strong><?php echo $_SESSION['vendortxcode']; ?></strong>.<br />
Please quote this number in any communications with us about this order.</p>

<p>We have sent a confirmation email to <strong><?php echo $_SESSION['order_email']; ?></strong>.<br />
Please remember to check your spam folder if think you have not received it.</p>

<p>Thank you.</p>
