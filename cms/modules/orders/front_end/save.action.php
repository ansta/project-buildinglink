<?php

$submission = TRUE;

foreach (Config::$fields['shippings'] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}

if ($submission) {
	
	$shippings = Shippings::checkExists($_SESSION['basket']);
	if ($shippings->num_rows == 1) {
		$shipping = $shippings->fetch_object();
		$_REQUEST['action'] = 'edit';
		$_REQUEST['id'] = $shipping->id;
		$success_message = 'Shipping details updated';
	} else {
		$_REQUEST['action'] = 'new';
		$_REQUEST['basket_hash'] = $_SESSION['basket'];
		$success_message = 'Shipping details submitted';
	}
	
	$result = Form::processData();
	
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = $success_message;
		
		/*
		echo '<pre>_REQUEST:
';
		var_dump($_REQUEST);
		echo '</pre>';
		echo '<pre>result:
';
		var_dump($result);
		echo '</pre>';
		$_SESSION['form']['form-error'] = '';
		die();
		*/
		if (isset($_REQUEST['back'])) {
			$redirect_to = '/basket/';
		} else {
			$redirect_to = '/confirm/';
		}
		header("Location: ".$redirect_to);
	#	header("Location: ".Page::$slug[0]);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}


