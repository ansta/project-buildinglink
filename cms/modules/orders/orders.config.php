<?php
/* 
 * 
 * 
 */

self::$settings['orders'] =
array(
	'orderby'=>'vendortxcode DESC'
,	'singular'=>'Order'
,	'plural'=>'Orders'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'admin_access'=>''
,	'max_items'=>''
);

self::$fields['orders'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'text',	'format'=>'i',	'label'=>'Order Number',			'formset'=>'data',	'protected'=>true,	'hint'=>'')

,	array('name'=>'vendortxcode',	'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'VendorTxCode',			'formset'=>'gate',	'protected'=>true,	'hint'=>'',	'istitle'=>true)

,	array('name'=>'basket_hash',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Basket Hash',				'formset'=>'data',	'protected'=>true,	'hint'=>'')
,	array('name'=>'basket_data',	'required'=>false,	'list'=>false,	'datatype'=>'text',		'formtype'=>'text',	'format'=>'s',	'label'=>'Basket Data (jSON)',		'formset'=>'data',	'protected'=>true,	'hint'=>'')

,	array('name'=>'bill_name',		'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Bill payers full name',	'formset'=>'view',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_email',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Bill payers email',		'formset'=>'view',	'protected'=>false,	'hint'=>'')
,	array('name'=>'bill_telephone',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Bill payers telephone',	'formset'=>'data',	'protected'=>false,	'hint'=>'')

,	array('name'=>'discount',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden','format'=>'s',	'label'=>'Discount',				'formset'=>'none',	'protected'=>true,	'hint'=>'')
,	array('name'=>'delivery_cost',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Delivery',				'formset'=>'view',	'protected'=>false,	'hint'=>'')
,	array('name'=>'total_excluding_vat','required'=>false,'list'=>false,'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Total Exc VAT',			'formset'=>'view',	'protected'=>false,	'hint'=>'')
,	array('name'=>'total_vat',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Total VAT',				'formset'=>'view',	'protected'=>false,	'hint'=>'')
,	array('name'=>'total_to_pay',	'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Total to Pay',			'formset'=>'view',	'protected'=>false,	'hint'=>'')
	
,	array('name'=>'amount_paid',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Amount Paid',				'formset'=>'view',	'protected'=>false,	'hint'=>'NOTE: ignore this field, check the AMOUNT column instead')
,	array('name'=>'vat_rate',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'VAT rate',				'formset'=>'view',	'protected'=>false,	'hint'=>'')

,	array('name'=>'status',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Status',					'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'statusdetail',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'StatusDetail',			'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'vpstxid',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'VPSTxId',					'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'txauthno',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'TxAuthNo',				'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'amount',			'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Amount Paid',				'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'avscv2',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'AVSCV2',					'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'addressresult',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'AddressResult',			'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'postcoderesult',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'PostCodeResult',			'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'cv2result',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'CV2Result',				'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'giftaid',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'GiftAid',					'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'threedsecurestatus','required'=>false,'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'3DSecureStatus',			'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'cavv',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'CAVV',					'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'cardtype',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'CardType',				'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'last4digits',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',	'format'=>'s',	'label'=>'Last4Digits',				'formset'=>'gate',	'protected'=>true,	'hint'=>'')
,	array('name'=>'crypt',			'required'=>false,	'list'=>false,	'datatype'=>'text',		'formtype'=>'text',	'format'=>'s',	'label'=>'crypt',					'formset'=>'gate',	'protected'=>true,	'hint'=>'')
);

/*

CREATE TABLE IF NOT EXISTS `orders` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,

  `id` int(11) NOT NULL DEFAULT '0',
  `basket_hash` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `amount_paid` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `vat_rate` varchar(8) COLLATE utf8_bin DEFAULT NULL,
  `bill_name` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `bill_email` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `bill_telephone` varchar(70) COLLATE utf8_bin DEFAULT NULL,

  `status` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `statusdetail` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `vendortxcode` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `vpstxid` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `txauthno` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `amount` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `avscv2` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `addressresult` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `postcoderesult` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `cv2result` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `giftaid` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `threedsecurestatus` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `cavv` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `cardtype` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `last4digits` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `crypt` varchar(600) COLLATE utf8_bin DEFAULT NULL,
  
  `ttv_start` timestamp NULL DEFAULT NULL,
  `ttv_end` timestamp NULL DEFAULT NULL,
  `create_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `create_id` int(11) NOT NULL DEFAULT '0',
  `create_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `modify_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `modify_id` int(11) NOT NULL DEFAULT '0',
  `modify_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;

*/
