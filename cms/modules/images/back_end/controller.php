<?php

Admin::allowNew(1);
Admin::allowEdit(1);
Admin::allowDelete(1);
Admin::allowView(0);

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	Core::setModuleTask('default'); // Optional to specify default as it would be default anyway!
	
} elseif (Page::$slug[3] == 'new') {
	Core::setModuleTask('new');
	Form::prefillValues();
	
} elseif (Page::$slug[3] == 'edit' && Page::$slug[4] > 0) {
	Core::setModuleTask('edit');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'delete' && Page::$slug[4] > 0) {
	Core::setModuleTask('delete');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'create' && $_SESSION['id'] == 1) {
	Setup::createNewTable(Page::$slug[2]);
	header('Location: /'.Page::$slug[1].'/'.Page::$slug[2].'/');
	
//} elseif (Page::$slug[3] == 'crop' && Page::$slug[4] > 0 && Page::$slug[5] != '') {
//	Core::setModuleTask('crop');
//	if (!Form::prefillValues()) {
//		Error:type(404);
//	}
} else {
	Error::type(404);
}
