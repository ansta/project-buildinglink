<?php
/* 
 * Users holds the login details of all admins
 * Renaming this table to admins might be a good idea and would leave room to use
 * the name for other purposes such as registered users and members
 * Alternatively, all users, both admins and members could use the same table although
 * the downsite of this would be that there would be less flexibility when it comes to
 * what columns could be used for either situation
 * This table must always contain at minimum 1 record or ID = 1 in order for Super Users to be able to login
 * Should this "special" entry be deleted, the system should automatically re-create it again (currently this is not implemented)
 */
self::$settings['images'] =
array(
	'orderby'=>'auto_id DESC'
,	'singular'=>'Image'
,	'plural'=>'Images'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'image'=>array(
		'admin'		=>array('width'=>100,'height'=> 70,'format'=>'jpg','method'=>'crop')
	,	'list' 		=>array('width'=>200,'height'=>140,'format'=>'jpg','method'=>'crop')
	,	'thumb'		=>array('width'=>100,'height'=>100,'format'=>'jpg','method'=>'resize','public'=>true)
	,	'gallery'	=>array('width'=>600,'height'=>1000,'format'=>'jpg','method'=>'resize','public'=>true)
	)
);

self::$fields['images'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'User ID',		'protected'=>false,		'hint'=>'')
,	array('name'=>'title',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',		'protected'=>false,		'hint'=>'', 'istitle'=>true)
,	array('name'=>'image',		'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Image',		'protected'=>false,		'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 500(W) x 500(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'2097152', 'root'=>'images', 'groupby'=>'id')
);

/*

CREATE TABLE images (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  
  `title` VARCHAR(70) DEFAULT NULL,
  `image` VARCHAR(255) DEFAULT NULL,
  
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;

*/