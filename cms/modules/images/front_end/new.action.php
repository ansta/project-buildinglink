<?php

$submission = TRUE;

foreach (Config::$fields['images'] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Your form submission contains error, please check your data and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}

if ($submission) {
	$result = Form::processData('images');
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'New item has been created.';
		header("Location: ".$result['destination']);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}
