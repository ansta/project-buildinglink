<?php
/* 
 * 
 * 
 */

self::$settings['payments'] =
array(
	'orderby'=>'title ASC'
,	'singular'=>'payment'
,	'plural'=>'payments'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
,	'admin_access'=>''
);

self::$fields['payments'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Payment ID',	'protected'=>true,	'hint'=>'')
,	array('name'=>'basket_hash','required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'Basket Hash',	'protected'=>true,	'hint'=>'')
,	array('name'=>'notification','required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'Notification Email Content',	'protected'=>true,	'hint'=>'')
,	array('name'=>'confirmation','required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',		'label'=>'Confirmation Email Content',	'protected'=>true,	'hint'=>'')
);


/*
		
CREATE TABLE IF NOT EXISTS `payments` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  
  `id` int(11) NOT NULL DEFAULT '0',
  `basket_hash` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `notification` text COLLATE utf8_bin,
  `confirmation` text COLLATE utf8_bin,
  
  `ttv_start` timestamp NULL DEFAULT NULL,
  `ttv_end` timestamp NULL DEFAULT NULL,
  `create_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `create_id` int(11) NOT NULL DEFAULT '0',
  `create_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `modify_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `modify_id` int(11) NOT NULL DEFAULT '0',
  `modify_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;


*/


