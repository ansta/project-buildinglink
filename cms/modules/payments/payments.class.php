<?php

class Payments {
	/*
	*
	*
	*/
	public static 	$item_id = 0;
	
	
	public static function checkExists($basket_hash) {
		$version_control = Config::$settings['shippings']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`payments` a
		";
		$query .= "
			WHERE
				a.id > 0
			AND	a.basket_hash = '".db::link()->real_escape_string($basket_hash)."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			$item = $items->fetch_object();
			self::$item_id = $item->id;
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	/*
	* 
	*/
	public static function generateTransactionId() {
		$basket_hash = $_SESSION['basket'];
		
		if (self::checkExists($basket_hash)) {
			$delete_query = "
				UPDATE
					payments
				SET 
					`ttv_end` = NOW()
				,	`modify_by` = '".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."'
				,	`modify_id` = ".(int)db::link()->real_escape_string($_SESSION['id'])."
				,	`modify_action` = 'delete'
				WHERE
					id = ".(int)self::$item_id."
			";
			db::link()->query($delete_query);
		}
		
		$payments_item_array = array(
			'module' => 'payments'
		,	'action' => 'insert'
		,	'basket_hash' => $basket_hash
		);
		
		$result = Form::processData('payments',$payments_item_array);
		
		if ($result['query_status'] == 'OK' && $result['item_id'] > 0) {
			return $result['item_id'];
		}
		return 0;
	}
	
	
	/*
	* 
	*/
	public static function getBasketHashFromTxCode($txcode) {
		$version_control = Config::$settings['payments']['version_control'];
		$query = "
			SELECT
				a.basket_hash
			FROM
				`payments` a
			WHERE
				a.id = ".(int)db::link()->real_escape_string($txcode)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY auto_id DESC
			LIMIT 1
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			$item = $items->fetch_object();
			return $item->basket_hash;
		}
		return NULL;
	}
	
}