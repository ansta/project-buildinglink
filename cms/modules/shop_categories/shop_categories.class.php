<?php

class Shop_categories {

	/*
	*
	*
	*/
	public static 	$id = 0,
					$title = '',
					$url = '',
					$newwin = '',
					$view_item_object,
					$item_id;
	
	public static  function getChildren($table,$id=0) {
		$version_control = Config::$settings['shop_categories']['version_control'];
		$query = "
			SELECT
				id
			,	label
			,	depth
			FROM
				shop_categories
			WHERE
				id > 0
			AND	parent = ".db::link()->real_escape_string($id)."
		";
		
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
		if (isset(Config::$settings['shop_categories']['filter_by_site']) && Config::$settings['shop_categories']['filter_by_site'] == true) {
			$query .= " AND site_id = ".(int)db::link()->real_escape_string($_SESSION['active_site_id']);
		}
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS NULL";
		}
		$query .= " ORDER BY `pos` ASC";
		$pages = db::link()->query($query);
		if ($pages->num_rows) {
			while( $page = $pages->fetch_object() ) {
				if ($page->depth == 1) {
					$prefix = '';
				} elseif ($page->depth == 2) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 3) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 4) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 5) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 6) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} else {
					$prefix = 'depth missing! - ';
				}
				Form::setOption('parent',$page->id,$prefix.$page->label);
				self::getChildren($table,$page->id);
			}
		}
	}
	
	
	/*
	* Allows us to look up the POS number of a given CATEGORY
	*/
	public static  function getPosFromParentId($item_id = 0) {
		$version_control = Config::$settings['shop_categories']['version_control'];
		$query = "
			SELECT
				pos
			FROM
				shop_categories
			WHERE
				id = ".db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS NULL";
		}
		$results = db::link()->query($query);
		if ($results->num_rows > 0) {
			if($result = $results->fetch_object()) {
				return $result->pos;
			}
		}
		return 0;
	}
	
	
	/*
	* Get items for this particular WEBSITE ID
	*/
	public static function getFeaturedList() {
		$version_control = Config::$settings['shop_categories']['version_control'];
		$query = "
			SELECT
				`a`.id
			,	`a`.title
			,	`a`.label
			,	`a`.image
			,	`a`.url
			FROM
				shop_categories a
			WHERE
				`a`.`id` > 0
			AND `a`.`site_id` = ".(int)db::link()->real_escape_string(Site::$sitedata['id'])."
			AND	`a`.`on_homepage` = 1
		";
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
		
		$query .= " ORDER BY `a`.`pos` ASC";
		$query .= " LIMIT 7";
	#	echo $query;
		$items = db::link()->query($query);
		
		if ($items->num_rows > 0) {
			// Some records are found, so return them
			return $items;
			
		}
		
		// Otherwise give up
		return NULL;
		
	}
	
	/*
	* 
	*/
	public static function getList($depth,$parent = 0) {
		$version_control = Config::$settings['shop_categories']['version_control'];
		$query = "
			SELECT
				c.*
			FROM
				`shop_categories` c
			WHERE
				c.id > 0
			AND c.depth = ".(int)db::link()->real_escape_string($depth)."
			AND c.active = true
		";
		if ($parent > 0) {
			$query .= " AND c.parent = ".(int)db::link()->real_escape_string($parent);
		}
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
		if (isset(Config::$settings['shop_categories']['filter_by_site']) && Config::$settings['shop_categories']['filter_by_site'] == true) {
			$query .= " AND site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id']);
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND c.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				c.`pos` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	/*
	* 
	*/
	public static function getListForDataAdminPage($depth,$parent = 0) {
		$version_control = Config::$settings['shop_categories']['version_control'];
		$query = "
			SELECT
				c.*
			FROM
				`shop_categories` c
			WHERE
				c.id > 0
			AND c.depth = ".(int)db::link()->real_escape_string($depth)."
			AND c.active = true
		";
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
		if ($parent > 0) {
			$query .= " AND c.parent = ".(int)db::link()->real_escape_string($parent);
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND c.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				c.`title` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* This generates a data object with purpose of generating a list of categories in the front end of the website
	*/
	public static function checkItemExists($url) {
		$version_control = Config::$settings['shop_categories']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`shop_categories` a
		";
		$query .= "
			WHERE
				a.id > 0
			AND	a.url = '".db::link()->real_escape_string($url)."'
		";
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
		if (isset(Config::$settings['shop_categories']['filter_by_site']) && Config::$settings['shop_categories']['filter_by_site'] == true) {
			$query .= " AND site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id']);
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			while($item = $items->fetch_object()) {
				self::$view_item_object = $item;
				self::$item_id = $item->id;
				return TRUE;
			}
		} else {
			return FALSE;
		}
	}
	
	/*
	* This generates the list of subcategories: categories to which products can be associated with
	* This generates an array and is used in the admin only
	*/
	public static function getAdminAttachmentsList() {
		$output = array();
		$query = "
			SELECT
				a.*
			,   b.title AS parent_title
			FROM
				shop_categories a
			LEFT JOIN
				shop_categories b ON b.id = a.parent AND b.ttv_end IS null
			WHERE
				a.parent > 0
			AND a.`ttv_end` IS null
			ORDER BY
				a.`pos` ASC
			,	b.`pos` ASC
		";
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			while($item = $items->fetch_object()) {
				$output[$item->id] = $item->parent_title.': '.$item->title;
			}
		}
		return $output;
	}
	
	/*
	* This generates the list of subcategories: categories to which products can be associated with
	* This generates an array and is used in the admin only
	*/
	public static function getParentCategories() {
		$output = array();
		$version_control = Config::$settings['shop_categories']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`shop_categories` a
			WHERE
				a.parent = 0
		";
#		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
#		}
		$query .= "
			ORDER BY
				a.pos ASC
		";
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			while($item = $items->fetch_object()) {
				$output[$item->id] = $item->title;
			}
		}
		return $output;
	}
	
	
	/*
	* Create default Product Categories based on the ID of the new site that has been created in SITES table
	* This function is used by Sites::createDefaultWebsiteContent();
	*/
	public static  function createDefaultCategories($site_id) {
		
		$module_name = 'shop_categories';
		$i = 1;
		
		// Create a sample category
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Sample Category'
		,	'label'=>'Sample Category'
		,	'slug'=>'sample-category'
		,	'parent'=>0
		,	'active'=>true
		,	'locked'=>0
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/sample-category/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		return $result;
	}
	
	/*
	*
	*/
	public static function getAdminAttachmentOptions($field,$i){
		
		// Set the name of the database table to query
		$source_table = Config::$settings[Module::$name]['attachments'][$i]['source'];
		$source_tbl = Security::checkTableList($source_table);
		
		// Get any custome WHERE statement if specififed
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['where'])) {
			$where = Config::$settings[Module::$name]['attachments'][$i]['where'];
		} else {
			$where = '';
		}
		
		// Get any custom ORDER BY statement if specififed
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['orderby'])) {
			$hasdepth = Config::$settings[Module::$name]['attachments'][$i]['orderby'];
		} else {
			$hasdepth = '';
		}
		
		// Check to see if hasdepth has been specified
		if (isset(Config::$settings[Module::$name]['attachments'][$i]['hasdepth'])) {
			$hasdepth = Config::$settings[Module::$name]['attachments'][$i]['hasdepth'];
		} else {
			$hasdepth = false;
		}
		
		// Prepare the columns to fetch in the query
		if ($hasdepth) {
			$columns = "
				`a`.`id`
			,	`a`.`title`
			,	`a`.`depth`
			,	`s`.`title` AS prefix
			";
		} else {
			$columns = "
				`a`.`id`
			,	`a`.`title`
			,	`s`.`title` AS prefix
			";
		}
		
		// Start building the SQL query
		$query = "
			SELECT
				".$columns."
			FROM
				".$source_tbl." `a`
			LEFT JOIN
				sites s
			ON	`s`.`id` = `a`.`site_id`
			AND	`s`.`ttv_end` IS null
			WHERE
		";
	#	if ($where != '') {
	#		$query .= $where;
	#	} else {
			$query .= "	`a`.`ttv_end` IS null";
	#	}
		if ($orderby != '') {
			$query .= "	ORDER BY ".$orderby;
		} else {
			$query .= "	ORDER BY a.title ASC";
		}
		
		//Query the database and return the data
		$items = db::link()->query($query);
		$output = '';
	#	echo $query;
		if ($items->num_rows > 0) {
			while ($item = $items->fetch_object()) {
				// Mark any that need to be pre-selected
				if(is_array(Attachments::$attached) && in_array($item->id, Attachments::$attached)) {
					$sel[$item->id] = 'selected="selected"';
				} else {
					$sel[$item->id] = '';
				}
				// Handle indenting for nested items
				if ($hasdepth) {
					$depth = '';
					for ($d=2;$d<=$item->depth;$d++) {
						$depth .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					}
				} else {
					$depth = '';
				}
				// Build up the list of options
				$output .= '<option value="'.$item->id.'" '.$sel[$item->id].'>&nbsp;&nbsp;'.$depth.$item->title.' ('.$item->prefix.')</option>';
			}
		}
		return $output;
	}
	
	
	/*
	* This function produces the default data list for the module.
	* Whether it be pages or documents or news or anything, by default
	* this function will produce a table style list allowing admins to
	* Create new and edit, copy or delete existing records.
	* 
	* Simply copy this funtion into your custom module to create your own version.
	* 
	* Todo:
	* - Make it simpler and more flexible
	* - improve the drag and drop reordering so that it activates when a POS column exists
	* - improve the drag and drop reordering to cater for depth and parent
	public static function getData($search = '') {
		
		// Validate the table (ie: module) name
		$table_name = 'pages';
		
		// Check to see if this module relies on a POS column
		if(Admin::fieldCheckExists($table_name,'pos')) {
			Admin::$haspos = TRUE;
			$table_js_id = 'sortable';
		} else {
			$table_js_id = 'notsortable';
		}
		
		// Start building the SELECT for this list
		$query = "SELECT * FROM `".$table_name."`";
		
		// If version control is in place for this table then we must only show the current records
		$version_control = Config::$settings[$table_name]['version_control'];
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " WHERE ttv_start <= NOW() AND ttv_end IS NULL";
		}
		
		// List ordering
		$query_orderby = '';
		if(Config::$settings[$table_name]['orderby'] && !isset($_GET['sort'])) {
			$query_orderby = Config::$settings[$table_name]['orderby'];
		}
		if (isset($_GET['sort']) && isset($_GET['dir'])) {
			if($_GET['dir'] == 'asc' OR $_GET['dir'] == 'desc') {
				$query_orderby = $_GET['sort'].' '.$_GET['dir'];
			}
		}
		if ($query_orderby != '') {
			$query = $query.' ORDER BY '.$query_orderby; 
		}
		
		// Query the database with the resulting query
		$items = db::link()->query($query);
		
		if ($items->num_rows) {
			return $items;
		}
		return NULL;
	}
	*/
	
}
