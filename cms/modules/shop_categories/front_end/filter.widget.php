<?php
	
	$cats = Shop_categories::getList(1);
	if ($cats->num_rows > 0) {
		echo '<ul class="nav">';
		$i = 0;
		while ($cat = $cats->fetch_object()) {
			
			$i++;
			
			if ($i == 1) {
				$item_count = 'item-first';
			} elseif ($i == $cats->num_rows) {
				$item_count = 'item-last';
			} else {
				$item_count = '';
			}
			
			
			if ($cat->slug == Module::$slug[1]) {
				$lvl1_active_state_css = 'active';
			#	echo $cat->slug.' || '.Module::$slug[1];
			} else {
				$lvl1_active_state_css = 'inactive';
			#	echo $cat->slug.' || '.Module::$slug[1];
			}
			
			echo '<li class="item navitem-'.$i.' '.$lvl1_active_state_css.' '.$item_count.'"><a href="'.str_replace("//", "/", Page::getModuleUrl('shop_categories').$cat->url).'"><span>'.$cat->title.'</span></a>';
				$subcats = Shop_categories::getList(2,$cat->id);
				if ($lvl1_active_state_css == 'active' && $subcats->num_rows > 0) {
					echo '<ul class="children">';
					while ($subcat = $subcats->fetch_object()) {
						echo '<li><a href="'.str_replace("//", "/", Page::getModuleUrl('shop_categories').$subcat->url).'"><span>'.$subcat->title.'</span></a></li>';
					}
					echo '</ul>';
				}
			echo '</li>';
			
		}
		
		echo '</ul>';
	}

