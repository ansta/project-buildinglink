<?php
if (Module::$slug[0] == '/') {
	// Default landing page: does not exist
	Error::type(404);
	
} elseif (Module::$slug[1] == 'search' && Module::$slug[2] == '') {
	// Search page
	
} elseif (Shop_categories::checkItemExists(Module::$slug[0])) {
	#Meta::setTitle(Shop_categories::$view_item_object->title);
	
	// SSet up some meta tags
	Meta::setTag('title',Shop_categories::$view_item_object->title);
	Meta::setTag('keywords',Shop_categories::$view_item_object->metakeys);
	Meta::setTag('description',Shop_categories::$view_item_object->metadesc);
	
	// Setup the Canonical URL of this product
	$canonical_domain = Sites::getDomain(Shop_categories::$view_item_object->site_id);
	$canonical_url = Page::getModuleUrl('shop_categories').Shop_categories::$view_item_object->slug.'/';
	Meta::setTag('canonical',$canonical_domain.Tool::cleanUrl($canonical_url));

} else {
	Error::type(404);
	
}

