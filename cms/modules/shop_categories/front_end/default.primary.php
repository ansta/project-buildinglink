<?php
if (Module::$slug[1] == 'search') {
	$products = Products::getSearchResults();
	
	$search_phrase = $_REQUEST['q'];
	if (trim($search_phrase) == '') {
		$search_phrase = '';
	}
	$page_title = 'Search';
	if ($products->num_rows == 1) {
		$page_content = '<p>You searched for the term <strong>'.htmlspecialchars($search_phrase).'</strong> which returned <strong>1 result</strong>.</p>';
	} elseif ($products->num_rows > 1) {
		$page_content = '<p>You searched for the term <strong>'.htmlspecialchars($search_phrase).'</strong> which returned <strong>'.$products->num_rows.' results</strong>.</p>';
	} else {
		$page_content = '<p>Sorry, no results found for the search term <strong>'.htmlspecialchars($search_phrase).'</strong></p><p>Try searching again or browse through our categories in the left menu. If you still can\'t find what you are looking for, please contact us.</p>';
	}
	
} else {
	$category = Shop_categories::$view_item_object;
	$page_title = $category->title;
	$page_content = $category->content;
	$products = Products::getList($category->id);
}
?>
<div class="product-columns">
	
	<h1 class="page-title category-title"><?php echo $page_title; ?></h1>
	
	<div class="category-description">
		<?php echo $page_content; ?>
	</div>
	
	<?php
	
	$pagination = '
		<!-- .pagination -->
		<ul class="pagination">
			<li><a href="#">1</a></li>
			<li class="active"><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#"><i class="icon-angle-right"></i></a></li>
			<li><a href="#">Last</a></li>
		</ul>
		<!-- /.pagination -->
	';
	
	if ($products->num_rows > 0) {
		
	#	echo $pagination;
		
		echo '<ul class="large-block-grid-3 product-list">';
		
		while($product = $products->fetch_object()) {
			
			// Prepare the product image
			if (file_exists(SYSTEM_ROOT.'modules/products/uploads/image/'.$product->id.'/list/'.$product->image)) {
				$product_image = '/image.php?i=/products/uploads/image/'.$product->id.'/list/'.$product->image.'';
			} else {
				$what_i_want = '/image.php?i=/products/uploads/image/'.$product->id.'/list/'.$product->image.'';
				$product_image = '/images/no-image-small.jpg" alt="'.$product->title.'';
			}
			
			// Get the cheapest price to display from what price this item can be purchased
			$cheapest_price = Products_variants::getCheapestPrice($product->id);
			
			$product_url = Page::getModuleUrl('products').$product->slug.'-'.$product->id.'/';
			
			echo '
				<li>
					<!-- .info-box -->
					'.($product->special_offer?'<div class="info-box product-box special-offer">':'<div class="info-box product-box">').'
						<div class="thumb">
							'.($product->special_offer?'<div class="special-offer-ribbon"></div>':'').'
							<img src="'.$product_image.'" alt="Rulers" style="width:230px;height:230px;" /> <!-- '.$what_i_want.' -->
							<div class="overlay">
								<h3>'.$product->title.'</h3>
								<!-- .product-box-bottom -->
								<div class="product-box-bottom clearfix">
									<div class="price">'.($cheapest_price!='n/a'?'<span>From</span> '.Products::getDisplayPrice($cheapest_price):'&nbsp;').'</div>
									<div class="btn-wrap"><span class="btn">More info</span></div>
								</div>
								<!-- /.product-box-bottom -->
							</div>
							<a class="box-link" href="'.Tool::cleanUrl($product_url).'"></a>
						</div>
					</div>
					<!-- /.info-box -->
				</li>
			';
		}

		echo '</ul>';
		
	#	echo $pagination;
	}
	
	?>
	
	
	<?php /*
	<!-- .pagination -->
	<ul class="pagination">
		<li><a href="#">1</a></li>
		<li class="active"><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#"><i class="icon-angle-right"></i></a></li>
		<li><a href="#">Last</a></li>
	</ul>
	<!-- /.pagination -->
	
	<!-- .row -->
	<div class="row product-list">
	
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box special-offer">
				<div class="thumb">
					<div class="special-offer-ribbon"></div>
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler, this is a stupidly long title that will break my layout</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
	</div>
	<!-- /.row-->
	
	<div class="row product-list">
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box special-offer">
				<div class="thumb">
					<div class="special-offer-ribbon"></div>
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
	
	</div>
	<!-- /.row-->
	
	<div class="row product-list">
	
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box special-offer">
				<div class="thumb">

					<div class="special-offer-ribbon"></div>
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
	
	</div>
	<!-- /.row-->
	
	<div class="row product-list">
	
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box special-offer">
				<div class="thumb">
					<div class="special-offer-ribbon"></div>
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box product-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers">
					<div class="overlay">
						<h3>Amazing Ruler</h3>
						<!-- .product-box-bottom -->
						<div class="product-box-bottom clearfix">
							<div class="price"><span>From</span> £20.00</div>
							<div class="btn-wrap"><span class="btn">Customise</span></div>
						</div>
						<!-- /.product-box-bottom -->
					</div>
					<a class="box-link" href="#"></a>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
	  
	</div>
	<!-- /.row-->
	
	<!-- .pagination -->
	<ul class="pagination">
		<li><a href="#">1</a></li>
		<li class="active"><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#"><i class="icon-angle-right"></i></a></li>
		<li><a href="#">Last</a></li>
	</ul>
	<!-- /.pagination -->
	*/ ?>
</div>


<?php
/*
	$currency_symbol = '&pound;';
	$category = Shop_categories::$view_item_object;
	echo '<h1>'.$category->title.'</h1>';
	
	$items = Products::getList($category->id);
	$i = 0;
	if($items->num_rows) {
		echo '<section class="products clearfix">';
		while ($item = $items->fetch_object()) {
			if ($i == 0) {
				$css_last = ' first';
			} elseif ($i == 2) {
				$css_last = ' last';
			} else {
				$css_last = '';
			}
			echo '<div class="product product-00'.++$i.$css_last.'">';
				echo '<div class="inner clearfix">';
					echo '<a href="'.Page::getModuleUrl('products').$item->slug.'/'.$item->id.'/">';
						if (file_exists(SYSTEM_ROOT.'modules/products/uploads/image/'.$item->id.'/list/'.$item->image)) {
							echo '<img src="/image.php?i=/products/uploads/image/'.$item->id.'/list/'.$item->image.'" alt="'.$item->title.'" title="'.$item->title.'" />';
						} else {
							echo '<img src="/images/no-image.png" alt="'.$item->title.'" title="'.$item->title.'">';
						}
						echo '<span class="title">'.$item->title.'</span>';
						echo '<span class="button">View</span>';
					echo '</a>';
					echo '<span class="price"><small>from</small> '.$currency_symbol.Price_bands::getLowestPrice($item).'</span>';
				echo '</div>';
			echo '</div>';
			if ($i == 3) { $i = 0; }
		}
		echo '</section>';
	} else {
		echo 'There are currently no products available under this category, please check back later.';
	}

*/