<?php
echo '<h1>';
	echo '<img src="/image.php?i=/'.Page::$slug[2].'/images/icon-20.png" alt="'.Config::$settings[Page::$slug[2]]['plural'].'" class="icon">';
	echo Config::$settings[Page::$slug[2]]['plural'];
echo '</h1>';
?>
<section>
<?php
	
	// Check to see if this module relies on a POS column
	if(Admin::fieldCheckExists(Page::$slug[2],'pos')) {
		Admin::$haspos = TRUE;
		$table_js_id = 'sortable';
	} else {
		$table_js_id = 'notsortable';
	}
	
	if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
		unset($_SESSION['form']['form-success']);
	}
	
	$search = '';
	$items = Admin::getData($search);

	// Start building the output
	if ($items->num_rows > 0) {
		
		$columns = Config::$fields[Module::$name];
		// Start with the list table header
		$output = '<table class="tbldata" id="'.$table_js_id.'">';
			$output .= '<tr class="nodrag nodrop">';
			foreach ($columns AS $field) {
				if ($field['list'] == true) {
					$output .= '<th class="tbltext">';
					$output .= $field['label'];
					$output .= '</th>';
				}
			}
			if (Admin::makeButton('seo',$item_id,Module::$name,'SEO')) {
				$colspan = 4;
			} else {
				$colspan = 3;
			}
			$output .= '<th colspan="'.$colspan.'" class="tblaction btn-new">';
			$output .= Admin::makeButton('new',0,Module::$name,'New Main Category');
			$output .= '</th>';
		$output .= '</tr>';
		
		// Continue building the data for each row
		while( $item = $items->fetch_object() ) {
			
			// used for tracking the row count
			$i++;
			
			// apply active/inactive styling
			if ($item->active == 1) {
				$active_css = 'active';
			} else {
				$active_css = 'inactive';
			}
			
			// apply alternate row colouring
			if($i & 1) {
				$rowcss = ' class="row1 '.$active_css.'"';
			} else {
				$rowcss = ' class="row0 '.$active_css.'"';
			}
			
			// For depth fields (`pages` table only at the moment) visualise the representation of the depth
			if (Page::$slug[2] == 'shop_categories') {
				$indent_string = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				if ($item->depth == 1) {
					$depth_indent = '';
				} elseif($item->depth == 2) {
					$depth_indent = $indent_string.$indent_suffix;
				} elseif($item->depth == 3) {
					$depth_indent = $indent_string.$indent_string.$indent_suffix;
				} elseif($item->depth == 4) {
					$depth_indent = $indent_string.$indent_string.$indent_string.$indent_suffix;
				} elseif($item->depth == 5) {
					$depth_indent = $indent_string.$indent_string.$indent_string.$indent_string.$indent_suffix;
				} else {
					$depth_indent = '';
				}
			}
			
			// Make sure that we use the correct ID column depending on whether or not this module has version control or not
			$version_control = Config::$settings[$module]['version_control'];
			if($version_control == 'audit' || $version_control == 'full') {
				$item_id = $item->id;
			} else {
				if (in_array('auto_id', Config::$fields[Module::$name])) {
					$item_id = $item->auto_id;
				} else {
					$item_id = $item->id;
				}
			}
			
			// output the HTML for the table
			$output .= '<tr'.$rowcss.' id="'.$item_id.'">';
			foreach ($columns AS $field) {
				if ($field['list'] == true) {
					$output .= '<td class="tbltext">';
					$output .= $depth_indent.$item->$field['name'];
					$output .= '</td>';
				}
			}
			
			// SEO
			if (Admin::$allow['seo'] == 1) {
				$output .= '<td class="tblaction btn-seo">';
				$output .= Admin::makeButton('seo',$item_id,Module::$name,'SEO');
				$output .= '</td>';
			}
			
			// NEW
			$output .= '<td class="tblaction btn-newsub">';
			if (Config::$settings[Module::$name]['max_depth'] <= $item->depth) {
				$output .= '<span class="disabled" title="Maximum page depth reached!">New SubCat.</span>';
			} else {
				if ($item->use_module != '') {
					if (Config::$settings[$item->use_module]['allow_subpages']) {
						$output .= Admin::makeButton('new',$item_id,Module::$name,'New SubCat.');
					} else {
						$output .= '<span class="disabled" title="Maximum page depth reached!">New SubCat.</span>';
					}
				} else {
					$output .= Admin::makeButton('new',$item_id,Module::$name,'New SubCat.');
				}
			}
			$output .= '</td>';
			
			// EDIT
			$output .= '<td class="tblaction btn-edit">';
			$output .= Admin::makeButton('edit',$item_id);
			$output .= '</td>';
		
			// DELETE
			$output .= '<td class="tblaction btn-delete">';
			
			// If global delete is permitted, check for row specific permissions
			if (Admin::$allow['delete'] == 1) {
				if($item->locked) {
					Admin::allowDelete(0);
					$output .= Admin::makeButton('delete',$item_id);
					Admin::allowDelete(1);
				} else {
					$output .= Admin::makeButton('delete',$item_id);
				}
			} else {
				$output .= Admin::makeButton('delete',$item_id);
			}
			$output .= '</td>';
			$output .= '</tr>';
		}
		$output .= '</table>';
	} else {
		$output = '<p>No data available yet, please <a href="new/">add a new item</a>.</p>';
		if (DEBUG_MODE > 0) {
			$output .= $query;
		}
	}
	echo $output;
?>
</section>
