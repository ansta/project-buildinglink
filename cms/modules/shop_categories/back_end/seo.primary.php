<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
Edit SEO data for Page: <?php echo Config::$settings[Page::$slug[2]]['singular']; ?>: <?php echo Form::getItemTitle(); ?></h1>

<section>
<?php 
if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
	echo '<p class="errorbanner">'.$_SESSION['form']['form-error'].'</p>';
	unset($_SESSION['form']['form-error']);
}
if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
	echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
	unset($_SESSION['form']['form-success']);
}
?>	
<form action="" method="post" enctype="multipart/form-data">
	<fieldset>
		<legend></legend>
		<?php
		foreach (Config::$fields[Module::$name] AS $field) {
			
			# Set the options for the PARENT drop down field
			if ($field['name'] == 'parent' && Module::$name == 'shop_categories') {
				Form::setOption('parent',0,'-- Make this page a top level category --');
				Shop_categories::getChildren('shop_categories');
			}
		#	if ($field['name'] == 'title' ||
		#		$field['name'] == 'metakeys' ||
		#		$field['name'] == 'metadesc' ||
		#		$field['name'] == 'metafollow' ||
		#		$field['name'] == 'metaindex' ||
		#		$field['name'] == 'metarevisit') {
		#		echo Form::makeField($field);
		#		
		#	} else
			
			if ( ($field['formtype'] == 'hidden') || ($field['protected'] == true && $_SESSION['id'] > 1) ) {
				# Ensure that hidden fields are tagged on to the end of the form
				$hidden_fields .= Form::makeField($field);
			} else {
				$arrayFormset = explode(',',$field['formset']);
				# If this type of user should have these fields show them
				if (in_array(Page::$slug[3],$arrayFormset)) {
					echo Form::makeField($field);
				} else {
					echo '<div style="display:none;">'.Form::makeField($field).'</div>';
				}
			}
			
		}
		echo $hidden_fields;
		?>
		<?php #Form::setValue('next',$_SESSION['next']); ?>
		<?php #echo Form::makeField(array('formtype'=>'yesno','name'=>'next','label'=>'Continue editing','hint'=>'After successful edit (No = return to listing page) (Yes: continue editing this item)')); ?>
		<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'edit')); ?>
		<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
		<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Update')); ?>
		<p class="btn-cancel"><a href="../../"><span>Cancel</span></a></p>
	</fieldset>
</form>
</section>