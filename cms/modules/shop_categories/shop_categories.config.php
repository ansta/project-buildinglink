<?php
/* 
 * 
 * 
 */

self::$settings['shop_categories'] =
array(
	'orderby'=>'pos ASC'
,	'singular'=>'Product Category'
,	'plural'=>'Product Categories'
,	'version_control'=>'audit'
,	'parent_module'=>'products'
,	'in_admin_menu'=>true
,	'admin_access'=>''
,	'max_items'=>''
,	'max_depth'=>2
,	'filter_by_site' => true // This if for the front-end laft nav
,	'metarevisit'=>array(
		0	=>	'-- optionally select a "revisit after" date --'
	,	1	=>	'1 days'
	,	7	=>	'7 days'
	,	14	=>	'14 days'
	,	30	=>	'30 days'
	,	90	=>	'90 days'
	,	180	=>	'180 days'
	)
,	'image'=>array(
		'admin'	=>array('width'=>113,'height'=> 70,'format'=>'jpg','method'=>'crop')
	,	'small' =>array('width'=>170,'height'=>106,'format'=>'jpg','method'=>'crop')
	,	'large'	=>array('width'=>229,'height'=>143,'format'=>'jpg','method'=>'crop')
	)
);

self::$fields['shop_categories'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,		'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Category ID',			'formset'=>'new,edit',	'protected'=>true,	'hint'=>'')
,	array('name'=>'site_id',	'required'=>false,	'list'=>false,		'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Site ID',				'formset'=>'new,edit',	'protected'=>true,	'hint'=>'')
,	array('name'=>'title',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',				'formset'=>'new,edit',	'protected'=>false,	'hint'=>'This is the title of the page from an <abbr title="Search Engine Optimisation">SEO</abbr> perspective')
,	array('name'=>'label',		'required'=>true,	'list'=>true,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Navigation title',	'formset'=>'new,edit',	'protected'=>false,	'hint'=>'How this item will appear in the website navigation', 'istitle'=>true)
,	array('name'=>'slug',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'URL identifier',		'formset'=>'new,edit',	'protected'=>false,	'regex'=>'/^[a-z0-9\-]*$/',	'hint'=>'Identifies this page within the URL structure.<br />Must only contain lowercase letters, numbers or a minus symbol. All other characters are not permitted. Warning: spaces are not permitted.<br /><strong style="color:#955;">Once set, it is strongly advised NOT to change it in the future as this can affect your <abbr title="Search Engine Optimisation">SEO</abbr> page ranking.</strong>')
,	array('name'=>'metakeys',	'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Keywords',			'formset'=>'new,edit',	'protected'=>false,	'hint'=>'Recommendation: no more than 5 comma separated keywords all of which <strong>MUST</strong> appear within the page content')
,	array('name'=>'metadesc',	'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Description',			'formset'=>'new,edit',	'protected'=>false,	'hint'=>'Recommendation: a short description of approx. 150 characters')
,	array('name'=>'metafollow',	'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Follow',				'formset'=>'new,edit',	'protected'=>false,	'hint'=>'',	'default'=>'1')
,	array('name'=>'metaindex',	'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Index',				'formset'=>'new,edit',	'protected'=>false,	'hint'=>'',	'default'=>'1')
,	array('name'=>'metarevisit','required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Revisit after',		'formset'=>'new,edit',	'protected'=>false,	'hint'=>'',	'config'=>'pages')
,	array('name'=>'content',	'required'=>false,	'list'=>false,		'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',	'label'=>'Category Content',	'formset'=>'new,edit',	'protected'=>false,	'hint'=>'', 'editor'=>true)
,	array('name'=>'parent',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'select',	'format'=>'i',	'label'=>'Parent',				'formset'=>'new,edit',	'protected'=>true,	'hint'=>'Specify which Parent page this page should appear')
,	array('name'=>'active',		'required'=>false,	'list'=>false,		'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Category is Active',	'formset'=>'new,edit',	'protected'=>false,	'hint'=>'Allows creation of a page without publishing it immediately by selecting <strong>No</strong>.', 'default'=>true)
,	array('name'=>'locked',		'required'=>false,	'list'=>false,		'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Category is Locked',	'formset'=>'new,edit',	'protected'=>true,	'hint'=>'Prevent this page from being deleted (requires unlocking before deleting)')
,	array('name'=>'on_homepage','required'=>false,	'list'=>false,		'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Feature on Homepage',	'formset'=>'new,edit',	'protected'=>false,	'hint'=>'Allows this category to be featured on the homepage', 'separator'=>'before')
,	array('name'=>'image',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Homepage Feature Photo','formset'=>'new,edit','protected'=>false,	'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 229(W) x 143(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'9097152', 'root'=>'shop_categories', 'groupby'=>'id', 'manual_crop'=>true, 'cropratio'=>'large', 'separator'=>'after')
,	array('name'=>'pos',		'required'=>false,	'list'=>false,		'datatype'=>'int',		'formtype'=>'text',		'format'=>'i',	'label'=>'Position',			'formset'=>'new,edit',	'protected'=>true,	'hint'=>'Relative to other pages: Small number will put item nearer to the top or left of a list, large number will put nearer to the bottom or right of a list.')
,	array('name'=>'depth',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'readonly',	'format'=>'i',	'label'=>'Category depth',		'formset'=>'new,edit',	'protected'=>false,	'hint'=>'Auto-generated!')
,	array('name'=>'url',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'readonly',	'format'=>'s',	'label'=>'Category URL',		'formset'=>'new,edit',	'protected'=>false,	'hint'=>'Auto-generated!')
);


/*
		
CREATE TABLE IF NOT EXISTS `shop_categories` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `metakeys` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `metadesc` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `metafollow` int(4) NOT NULL DEFAULT '1',
  `metaindex` int(4) NOT NULL DEFAULT '1',
  `metarevisit` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `pos` tinyint(2) NOT NULL DEFAULT '0',
  `depth` tinyint(2) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  
  `ttv_start` timestamp NULL DEFAULT NULL,
  `ttv_end` timestamp NULL DEFAULT NULL,
  `create_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `create_id` int(11) NOT NULL DEFAULT '0',
  `create_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `modify_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `modify_id` int(11) NOT NULL DEFAULT '0',
  `modify_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

*/
