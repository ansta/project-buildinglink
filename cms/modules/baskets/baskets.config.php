<?php
/* 
 * 
 * 
 */

self::$settings['baskets'] =
array(
	'orderby'=>'title ASC, id ASC'
,	'singular'=>'Shopping Basket'
,	'plural'=>'Shopping Baskets'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
,	'admin_access'=>''
,	'max_items'=>''
);

self::$fields['baskets'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Basket Item ID',		'protected'=>true,	'hint'=>'')
,	array('name'=>'basket_hash',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'2',	'label'=>'Basket Hash',			'protected'=>false,	'hint'=>'')
,	array('name'=>'link_hash',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'2',	'label'=>'Link Hash',			'protected'=>false,	'hint'=>'')
,	array('name'=>'order_id',		'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Order ID',			'protected'=>false,	'hint'=>'')

,	array('name'=>'item_id',		'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'i',	'label'=>'Item ID',				'protected'=>false,	'hint'=>'')
,	array('name'=>'item_title',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Item Title',			'protected'=>false,	'hint'=>'')
,	array('name'=>'item_ref',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Item Part Number',	'protected'=>false,	'hint'=>'')
,	array('name'=>'variant_id',		'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'i',	'label'=>'Item ID',				'protected'=>false,	'hint'=>'')
,	array('name'=>'variant_title',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Item Size',			'protected'=>false,	'hint'=>'')
,	array('name'=>'item_price',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Unit Item Price',		'protected'=>false,	'hint'=>'')
,	array('name'=>'setup_price',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Setup Fee',			'protected'=>false,	'hint'=>'')
,	array('name'=>'quantity',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'i',	'label'=>'Item Quantity',		'protected'=>false,	'hint'=>'')
,	array('name'=>'artwork',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'file',		'format'=>'s',	'label'=>'Artwork',				'protected'=>false,	'hint'=>'Accepted formats: doc, docx, pdf, txt, rtf', 'filetype'=>'octet-stream,plain,doc,docx,pdf,txt,rtf', 'maxsize'=>'0', 'root'=>'baskets', 'groupby'=>'id')
);


/*

CREATE TABLE IF NOT EXISTS `baskets` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  
  `id` int(11) NOT NULL DEFAULT '0',
  `basket_hash` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `item_size` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `item_price` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  
  `options_title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `options_price` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  
  `ttv_start` timestamp NULL DEFAULT NULL,
  `ttv_end` timestamp NULL DEFAULT NULL,
  `create_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `create_id` int(11) NOT NULL DEFAULT '0',
  `create_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `modify_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `modify_id` int(11) NOT NULL DEFAULT '0',
  `modify_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;

*/
