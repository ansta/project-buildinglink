<?php

$submission = TRUE;

// Make sure item exists in the basket
$basket_items = Baskets::checkItemExists($_REQUEST['item']);
if ($basket_items->num_rows > 0) {
	if ($basket_item = $basket_items->fetch_object()) {
		$baskets_id = $basket_item->id;
	}
} else {
	$_SESSION['form']['form-error'] = 'Unable to delete this item as it was not found in your basket.';
	$submission = Error::storeMsg($something_needs_to_go_here);
}

// If the request is valid, process the add to basket
if ($submission) {

	$query = "
		UPDATE
			baskets
		SET
			ttv_end = NOW()
		,	modify_by = '".db::link()->real_escape_string($_SESSION['firstname'])." ".db::link()->real_escape_string($_SESSION['lastname'])."'
		,	modify_id = ".(int)db::link()->real_escape_string($_SESSION['id'])."
		,	modify_action = 'delete'
		WHERE
			id = ".$baskets_id."
		AND	ttv_end IS null
	";
#	echo $query;
	if (db::link()->query($query)) {
		$result['query_status'] = 'OK';
		$result['redirect'] = true;
	}
	
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'Notice: <strong>'.$_REQUEST['quantity'].'</strong> x <strong>'.$_REQUEST['title'].'</strong> has been removed from your basket.';
		header("Location: ".Page::$slug[0]);
		exit();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo '<p>ooops!</p>';
			echo $result['query'];
		}
	}
}


