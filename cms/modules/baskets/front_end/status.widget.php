
<!-- .cart-inner -->
<div class="cart-inner">
	
	<div class="cart-icon"></div>
	
	<!-- .description -->
	<div class="description">
		<?php
			$items = Baskets::getItems();
			$total_basket_price = 0;
			$num_rows = $items->num_rows;
			if ($num_rows > 0) {
				while ($item = $items->fetch_object()) {
					$total_basket_price = $total_basket_price + ($item->quantity * $item->item_price) + $item->setup_price;
					$total_basket_quantity = $total_basket_quantity + $item->quantity;
				}
			}
			echo '
				<span class="count">'.number_format($num_rows,0).'</span>
				<p>Product'.($num_rows == 1?'':'s').' in your basket
				<br />Total: <span>&pound;'.number_format($total_basket_price, 2,'.',',').'</span></p>
			';
		?>
	</div>
	<!-- /.description -->
	
</div>
<!-- /.cart-inner -->

<a href="/basket/" class="btn btn-view-cart">View basket</a>
