<h1><?php echo Page::getTitle(); ?></h1>

<?php echo Page::showErrors(); ?>

<?php
$currency_symbol = '&pound;';

// Get VAT rate based on shipping data
$master_vat = Countries::getMasterVatRate();

// Get delivery cost based on shipping data
$delivery_cost_array = Countries::getDeliveryCost($order['shipping']['ship_country'],$master_vat);
		
$items = Baskets::getItems();
$num_rows = $items->num_rows;
if ($num_rows > 0) {
	echo '<form action="" method="get">';
		echo '<table class="basket" width="100%">';
			echo '<thead>';
				echo '<tr>';
					echo '<th>&nbsp;</th>';
					echo '<th>';
						echo 'Product Details';
					echo '</th>';
					echo '<th>';
						echo 'Product No';
					echo '</th>';
					echo '<th>';
						echo 'Unit Cost';
					echo '</th>';
					echo '<th>';
						echo 'Quantity';
					echo '</th>';
				#	echo '<th style="text-align:center;">';
				#		echo 'VAT';
				#	echo '</th>';
					echo '<th>';
						echo 'Line Value';
					echo '</th>';
				echo '</tr>';
			echo '</thead>';
			while ($item = $items->fetch_object()) {
				
				// Calculate the running total
				$total_basket_price = $total_basket_price + ($item->quantity * $item->item_price) + $item->setup_price;
			
				// Prepare the view page URL
				$item_url = str_replace("//", "/", Page::getModuleUrl('products').$item->slug.'-'.$item->item_id.'/');
				
				echo '<tr>';
					echo '<td>';
						echo '<a href="?module=baskets&amp;action=delete&amp;item='.$item->id.'&amp;title='.urlencode($item->item_title).'&amp;quantity='.$item->quantity.'"';
							echo 'onclick="return confirm(\'Are you sure you want to delete '.$item->item_title.' from your basket?\');"> ';
							echo '<i class="icon-remove icon-delete"></i>'; //ete '.$item->item_title.' from your basket?
						echo '</a>';
					echo '</td>';
					echo '<td>';
						echo '<a href="'.$item_url.'">'.$item->item_title.'</a> '.$item->variant_title;
						if ((int)$item->setup_price > 0) {
							echo ' (setup fee: '.$currency_symbol.number_format($item->setup_price, 2).')';
						}
						
						// Display ARTWORK status
						$real_path = SYSTEM_ROOT.'/modules/baskets/uploads/artwork/'.$item->id.'/'.$item->artwork;
						if ($item->artwork != '' && file_exists($real_path)) {
							$field['root'] = 'baskets';
							$field['name'] = 'artwork';
							$field['groupby'] = $item->id;
							$field_value = $item->artwork;
							$file_path = SERVER_NAME.'/download_file.php?r='.$field['root'].'&f='.$field['name'].'&gb='.$field['groupby'].'&i='.$field_value;
							echo  '<br><span class="notify">Artwork supplied: Yes ('.strtoupper(substr($item->artwork, -3)).' '.Tool::formatFilesize($real_path).')</span>';
						}
						
					echo '</td>';
					echo '<td style="text-align:center;">';
						echo $item->item_ref;
					echo '</td>';
					echo '<td style="text-align:center;">';
						echo $currency_symbol.number_format($item->item_price, 2);
					echo '</td>';
					echo '<td style="text-align:center;">';
						echo '<input type="text" name="item['.$item->id.']" value="'.$item->quantity.'" size="4" />';
					echo '</td>';
				#	echo '<td>';
				#		$line_total_excluding_vat = ($item->quantity * $item->item_price);
				#		$line_item_master_vat	= (($line_total_excluding_vat / 100) * $master_vat['percent']);
				#		echo $currency_symbol.number_format($line_item_master_vat, 2);
				#	echo '</td>';
					echo '<td style="text-align:center;">';
						echo $currency_symbol.number_format((($item->quantity * $item->item_price) + $item->setup_price + $line_item_master_vat), 2);
					echo '</td>';
				echo '</tr>';
			}
		#	echo '<tr class="highlight">';
		#		echo '<td colspan="4" style="text-align:right;padding-right:10px;">';
		#			echo 'Delivery:';
		#		echo '</td>';
		#		echo '<td>';
		#			echo $currency_symbol.number_format($delivery_cost_array['inc_vat'], 2);
		#		echo '</td>';
		#	echo '</tr>';
			
			echo '<tfoot>';
			echo '<tr class="highlight">';
				echo '<td colspan="5" style="text-align:right;padding-right:10px;">';
					echo '<strong>Subtotal:</strong>';
				echo '</td>';
				echo '<td style="text-align:center;"><strong>';
					$basket_total_vat = (($total_basket_price / 100) * $master_vat['percent']);
					echo $currency_symbol.number_format(($total_basket_price), 2);
				#	echo $currency_symbol.number_format(($total_basket_price + $basket_total_vat), 2);
				#	echo $currency_symbol.number_format(($total_basket_price + $basket_total_vat + $delivery_cost_array['inc_vat']), 2);
				echo '</strong></td>';
			echo '</tr>';
			echo '</tfoot>';
			
		echo '</table>';
		echo '<input type="hidden" name="module" value="baskets">';
		echo '<input type="hidden" name="action" value="update">';
		echo '<input type="submit" name="update" value="Update Basket" class="button">';
		echo '<input type="submit" name="next" value="Proceed to Checkout &raquo;" class="button buttonRight">';
	echo '</form>';
} else {
	echo 'There are currently no items in your basket. Why not <a href="/">return to the homepage</a> to begin shopping.';
}















/*

?>

<h1><?php echo Page::getTitle(); ?></h1>

<?php echo Page::showErrors(); ?>

<?php
$currency_symbol = '&pound;';
$items = Baskets::getItems();
$num_rows = $items->num_rows;
if ($num_rows > 0) {
	echo '<form action="" method="get">';
		echo '<table class="basket">';
			echo '<tr>';
				echo '<th>';
					echo 'Product details';
				echo '</th>';
				echo '<th style="text-align:center;">';
					echo 'Quantity';
				echo '</th>';
				echo '<th>';
					echo 'Price';
				echo '</th>';
			echo '</tr>';
			while ($item = $items->fetch_object()) {
				$total_basket_price = $total_basket_price + ($item->quantity * ($item->item_price + $item->options_price));
				echo '<tr>';
					echo '<td>';
						echo '<a href="/buy/'.$item->slug.'/'.$item->item_id.'/">'.$item->item_title.'</a> in size '.$item->item_size.' '.$currency_symbol.$item->item_price;
						if (trim($item->options_title) != '') {
							$options_array[$item->id] = json_decode($item->options_title,true);
							foreach ($options_array[$item->id] AS $opt_label => $opt_value) {
								echo '<br>- '.$opt_value['item_title'].' '.$currency_symbol.$opt_value['item_price'];
							}
						}
						if (Products::requiresFixingsOption($item->item_id)) {
							echo '<br>- Fixing: '.str_replace(': ',', ',Config::$settings['baskets']['fixings_id'][$item->fixings_id]);
						}
					echo '</td>';
					echo '<td style="text-align:center;">';
						echo '<input type="text" name="item['.$item->id.']" value="'.$item->quantity.'" size="1" />';
						echo '<a href="?module=baskets&amp;action=delete&amp;item='.$item->id.'&amp;title='.urlencode($item->item_title).'&amp;quantity='.$item->quantity.'"';
							echo 'onclick="return confirm(\'Are you sure you want to delete '.$item->item_title.' from your basket?\');"> ';
							echo '<img src="/images/icon-basket-delete-item.png" alt="Delete '.$item->item_title.' from your basket?" title="Delete '.$item->item_title.' from your basket?" />';
						echo '</a>';
					echo '</td>';
					echo '<td>';
						echo $currency_symbol.number_format($item->quantity * ($item->item_price + $item->options_price), 2);
					echo '</td>';
				echo '</tr>';
			}
			echo '<tr class="highlight">';
				echo '<td colspan="2" style="text-align:right;">';
					echo 'Subtotal:';
				echo '</td>';
				echo '<td>';
					echo $currency_symbol.number_format($total_basket_price, 2);
				echo '</td>';
			echo '</tr>';
		echo '</table>';
		echo '<input type="hidden" name="module" value="baskets">';
		echo '<input type="hidden" name="action" value="update">';
		echo '<input type="submit" name="update" value="Update Basket" class="button">';
		echo '<input type="submit" name="next" value="Proceed to Checkout &raquo;" class="button buttonRight">';
	echo '</form>';
} else {
	echo 'There are currently no items in your basket. Why not <a href="/">return to the homepage</a> to begin shopping.';
}


*/


