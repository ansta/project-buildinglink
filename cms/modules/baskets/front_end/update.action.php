<?php

$submission = TRUE;

// Loop through each basket item and update accordingly
foreach ($_REQUEST['item'] AS $item_id => $new_quantity) {
	
	$basket_items = Baskets::checkItemExists($item_id);
	
	if ($basket_items->num_rows == 1) {
		$basket_item = $basket_items->fetch_object();
		
		$minimum_quantity_permitted = Products_variants::getMinimumOrderQuantity($basket_item->item_id);
		
		if ($new_quantity > 0 && $new_quantity != $basket_item->quantity && $new_quantity >= $minimum_quantity_permitted) {
			// Quantity is different from the existing and still above the minimum required quantity
			$basket_item_array = array(
				'module' => 'baskets'
			,	'action' => 'edit'
			,	'id' => $basket_item->id
			,	'quantity' => $new_quantity
			,	'item_price' => Products_variants::getUnitPriceBasedOnQuantity($basket_item->variant_id,$new_quantity)
			);
			Form::setAuditAction('update');
			$result = Form::processData('baskets',$basket_item_array);
			$_SESSION['form']['form-success'] = 'Notice: Your basket has been updated.';
		
		} elseif ($new_quantity == 0) {
			// Quantity is set to zero so (SOFT) delete this item
			$delete_query = "
				UPDATE
					baskets
				SET
					ttv_end = NOW()
				WHERE
					id = ".$basket_item->id."
				AND	ttv_end IS null
			";
			if (db::link()->query($delete_query)) {
				# silent success
			}
			
		} elseif ($new_quantity < $minimum_quantity_permitted) {
			// Prevent shoppers from reducing the quantity below the minimum permitted quantity
			$_SESSION['form']['form-error'] .= '<br />There is a minimum order quantity of '.$minimum_quantity_permitted.' for this item: '.$basket_item->item_title.'.';
			
		} elseif ($new_quantity >= 0 && $new_quantity == $basket_item->quantity) {
			// Quantity is the same so do nothing
			
		} else {
			// Unable to determine the various quantities so throw back an error
			$_SESSION['form']['form-error'] .= '<br />'.$new_quantity.' is an invalid quantity chosen for item '.$basket_item_array['item_title'].'.';
		}
		
	} else {
		// Unable to determine what items are in the basket so throw back an error
		$_SESSION['form']['form-error'] .= '<br />Could not update quantity of one of your items as it was not found in your basket.';
	}
}


if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
	$_SESSION['form']['form-error'] = 'There was a problem updating your basket:'.$_SESSION['form']['form-error'];
}


if (isset($_REQUEST['next'])) {
	$redirect_to = '/shipping/';
	$submission = TRUE;
} else {
	$redirect_to = Page::$slug[0];
}


// Redirect off to the appropriate page
if ($submission) {
	header("Location: ".$redirect_to);
	exit();
}



