<?php

if (Module::$slug[0] == '/') {
	// Default landing page: show latest few post summaries
#	Page::addBreadCrumb('#'.$_REQUEST['tag']);
	
} elseif (Products::checkItemExists(Module::$slug[1],Module::$slug[2])) {
	// View page: display the article in full
	Core::setModuleTask('view');

} else {
	Error::type(404);
	
}

