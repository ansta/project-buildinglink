<?php

#echo '<pre>';

#var_dump($_REQUEST);




$submission = TRUE;

$default_error_message = 'There was a problem adding your item to the basket, please try again. If the problem persists, please contact us.';

// Start preparing the array that holds the insert data
$basket_item_array = array(
	'module' => 'baskets'
,	'action' => 'new'
,	'basket_hash' => $_SESSION['basket']
,	'link_hash' => $_SESSION['link_hash']
);

// Check to see that the PRODUCT ID is valid
if ($submission && Products::checkItemExistsById($_REQUEST['item'])) {
	$product = Products::$view_item_object;
	$basket_item_array['item_id'] = $product->id;
	$basket_item_array['item_title'] = $product->title;
	$basket_item_array['item_ref'] = $product->sku;
} else {
	$_SESSION['form']['form-error'] = $default_error_message;
	$submission = Error::storeMsg($something_needs_to_go_here);
}

// Check to make sure that the VARIANT ID is valid for this PRODUCT ID
if ($submission && Products_variants::checkItemIsValid($_REQUEST['variant'],$basket_item_array['item_id'])) {
	$variant = Products_variants::$view_item_object;
	$basket_item_array['variant_id'] = $variant->id;
	$basket_item_array['variant_title'] = $variant->title;
	$basket_item_array['item_price'] = Products_variants::getUnitPriceBasedOnQuantity($variant->id,$_REQUEST['quantity']);
	$basket_item_array['setup_price'] = $product->setup_fee;

	// Check to make sure that the QUANTITY is greater than or equal to the minimum permitted order quantity for this PRODUCT ID
	if ($submission && $_REQUEST['quantity'] >= Products_variants::getMinimumOrderQuantity($product->id)) {
		$basket_item_array['quantity'] = $_REQUEST['quantity'];
	} else {
		$basket_item_array['quantity'] = 'error quantity';
	#	$_SESSION['form']['form-error'] = $default_error_message;
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
	
} else {
	$basket_item_array['variant_id'] = 'error variant';
	$_SESSION['form']['form-error'] = 'Please select a variant from the list of available options before adding this item to your cart.';
	$submission = Error::storeMsg($something_needs_to_go_here);
}


#echo '<hr><pre>';

#var_dump($_REQUEST);
#var_dump($basket_item_array);

#die();

/*
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Basket Item ID',		'protected'=>true,	'hint'=>'')
,	array('name'=>'basket_hash',	'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'i',	'label'=>'Basket Hash',			'protected'=>false,	'hint'=>'')
,	array('name'=>'order_id',		'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Order ID',			'protected'=>false,	'hint'=>'')

,	array('name'=>'variant_id',		'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'i',	'label'=>'Item ID',				'protected'=>false,	'hint'=>'')
,	array('name'=>'variant_title',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Item Size',			'protected'=>false,	'hint'=>'')
,	array('name'=>'item_price',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Unit Item Price',		'protected'=>false,	'hint'=>'')
,	array('name'=>'quantity',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'text',		'format'=>'i',	'label'=>'Item Quantity',		'protected'=>false,	'hint'=>'')
,	array('name'=>'artwork',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'file',		'format'=>'s',	'label'=>'Artwork',				'protected'=>false,	'hint'=>'Accepted formats: doc, docx, pdf, txt, rtf', 'filetype'=>'octet-stream,plain,doc,docx,pdf,txt,rtf', 'maxsize'=>'0', 'root'=>'baskets', 'groupby'=>'id')




// Check that the item requested is a valid number
// TODO: check that it is a valid product ID
if (!isset($_REQUEST['item']) || (int)$_REQUEST['item'] < 1) {
	$_SESSION['form']['form-error'] = $default_error_message;
	$submission = Error::storeMsg($something_needs_to_go_here);
} else {
	$product = Products::checkItemById($_REQUEST['item']);
	$basket_item_array['item_id'] = $product->id;
	$basket_item_array['item_title'] = $product->title;
}


// Check that the size requested is a valid number
// TODO: check that it is a valid size
if (!isset($_REQUEST['size'])) {
	// If size (which determines the price band) is not set
	$_SESSION['form']['form-error'] = $default_error_message;
	$submission = Error::storeMsg($something_needs_to_go_here);
} else {
	// From $_REQUEST['size'] extract the number and prepare for the next steps
	$price_num_check = explode('price',$_REQUEST['size']);
	$price_band_number = $price_num_check[1];
	$price_band = 'price'.$price_band_number;
	// Error if the number extracted from the price band (size) is not valid
	if ((int)$price_band_number < 1) {
		$_SESSION['form']['form-error'] = $default_error_message;
		$submission = Error::storeMsg($something_needs_to_go_here);
	} else {
#		This is the OLD way
#		// Fetch all the different available sizes that the shop offers
#		foreach (Config::$settings['price_bands']['sizes'] AS $price_band_column_name => $price_band_label) {
#			$i++;
#			$extras_title[$i] = $price_band_label;
#		}
#		// Prepare the data that will be added to the baskets table record
#		$basket_item_array['item_size'] = $extras_title[$price_band_number];
#		$basket_item_array['item_price'] = number_format(Price_bands::getPrice($product->price_bands_id,$price_band), 2);
#
#
#		This is the NEW way
		// Retrieve all the price columns available in price_bands for use in the next step
		foreach (Config::$fields['price_bands'] AS $pbk => $pbv) {
			if (substr($pbv['name'], 0, 5) == 'price') {
				$price_template_array[substr($pbv['name'], 5, 5)] = $pbv['name'];
			}
		}
		// For each price column, fetch the price label and the price value
		foreach ($price_template_array AS $column_num => $price_column_name) {
			$pb_getinfo = Price_bands::getPriceDetails($product->price_bands_id,$column_num);
			if (trim($pb_getinfo->label) != '' && $pb_getinfo->price > 0) {
				$size_label[$column_num] = $pb_getinfo->label;
				$size_price[$column_num] = $pb_getinfo->price;
				$category_id = $pb_getinfo->category_id;
			}
		}
		// Now store the values for the chosen size into the basket array
		$basket_item_array['item_size'] = $size_label[$price_band_number];
		$basket_item_array['item_price'] = number_format($size_price[$price_band_number], 2);
		
	}
}


// Prepare to extract the options details to store in the DB
$i = 0;
$temp_options_price = 0;
$extras = Products_extras::getList($category_id);
if ($extras->num_rows >0) {
	while ($extra = $extras->fetch_assoc()) {
		if (isset($_REQUEST['extra'.$extra['id']]) && $_REQUEST['extra'.$extra['id']] == 'on' && (int)$extra['price'.$price_band_number] > 0) {
			// Fetch the option details in preparation for storing in DB as json format
			$basket_item_options[$i]['item_id'] = $extra['id'];
			$basket_item_options[$i]['item_title'] = $extra['title'];
			$basket_item_options[$i]['item_label'] = $extra['label'.$price_band_number];
			$basket_item_options[$i]['item_price'] = $extra['price'.$price_band_number];
			// Add this option price to the options running total
			$temp_options_price = $temp_options_price + $extra['price'.$price_band_number];
			$i++;
		}
	}
}
if (json_encode($basket_item_options) != 'null') {
	$basket_item_array['options_title'] = json_encode($basket_item_options);
}
$basket_item_array['options_price'] = number_format($temp_options_price, 2);


// Check that the quantity requested is a valid number and is greater than 0
if (!isset($_REQUEST['qty']) || (int)$_REQUEST['qty'] < 1) {
	$_SESSION['form']['form-error'] = 'There was a problem adding your item to the basket, please try again while checking that the quantity requested is greater than 0.';
	$submission = Error::storeMsg($something_needs_to_go_here);
} else {
	$basket_item_array['quantity'] = $_REQUEST['qty'];
}


// If this item is a weathervane
if (Products::requiresFixingsOption($product->id)) {
	// Check if a fixing type has been selected
	if (!isset($_REQUEST['fixings_id']) || (int)$_REQUEST['fixings_id'] < 1) {
		$_SESSION['form']['form-error'] = 'There was a problem adding your item to the basket, please make sure that you have selected a fixing type.';
		$submission = Error::storeMsg($something_needs_to_go_here);
		
	} else {
		if (($basket_item_array['item_size'] == "Petite Economy" || $basket_item_array['item_size'] == "Petite Deluxe") && ($_REQUEST['fixings_id'] == 1 || $_REQUEST['fixings_id'] == 2)) {
			// Size and Fixing match
			$basket_item_array['fixings_id'] = $_REQUEST['fixings_id'];
			
		} elseif (($basket_item_array['item_size'] == "Medium Economy" || $basket_item_array['item_size'] == "Medium Deluxe") && ($_REQUEST['fixings_id'] == 3 || $_REQUEST['fixings_id'] == 4 || $_REQUEST['fixings_id'] == 5 || $_REQUEST['fixings_id'] == 6 || $_REQUEST['fixings_id'] == 11)) {
			// Size and Fixing match
			$basket_item_array['fixings_id'] = $_REQUEST['fixings_id'];
			
		} elseif (($basket_item_array['item_size'] == "Large Economy" || $basket_item_array['item_size'] == "Large Deluxe") && ($_REQUEST['fixings_id'] == 7 || $_REQUEST['fixings_id'] == 8 || $_REQUEST['fixings_id'] == 9 || $_REQUEST['fixings_id'] == 10 || $_REQUEST['fixings_id'] == 12)) {
			// Size and Fixing match
			$basket_item_array['fixings_id'] = $_REQUEST['fixings_id'];
			
		} else {
			$_SESSION['form']['form-error'] = 'There was a problem adding your item to the basket, the size you have chosen is unsuitable for the chosen fixing';
			$submission = Error::storeMsg($something_needs_to_go_here);
		}
	}
}

*/

#	echo '<pre>';
#	var_dump($basket_item_array);
#	echo '</pre>';
#	$submission = FALSE;
#	die();

// If the request is valid, process the add to basket
if ($submission) {
	
	Form::setAuditAction('insert');
	$result = Form::processData('baskets',$basket_item_array);
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		#Thank you: '.$basket_item_array['quantity'].' x '.$basket_item_array['item_title'].' in '.$basket_item_array['item_size'].' has been added to your basket.
		$_SESSION['form']['form-success'] = 'Product added to basket<br /><a href="/basket/">Click here to go to your basket</a>.';
		
	#	var_dump($result);
		
		header("Location: ".Page::$slug[0]);
		die();
		
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo '<p>ooops!</p>';
			echo $result['query'];
		}
	}
}


