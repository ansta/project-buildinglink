<?php

class Baskets {

	/*
	*
	*
	*/
	public static 	$id = 0,
					$title = '',
					$url = '',
					$newwin = '',
					$view_item_object,
					$item_id = 0;
					
	/*
	* This handles the creating of SESSION and COOKIE to hold the basket hash
	* This will allow a previously created basket to be restored (automatically) upon 
	* return to the website.
	*/
	public static function manageCookies() {
		$cookie_expire = 30 * 60 * 24 * 60 + time(); // Basket cookie life: 1 month
		
		// If there is no basket session set...
		if (!isset($_SESSION['basket'])) {
			
			// ...check to see if there is a basket cookie.
			
			// If there is a basket cookie
			if (isset($_COOKIE['basket']) && !empty($_COOKIE['basket']) && trim($_COOKIE['basket']) != '') {
				// Create a basket session with the old basket cookie code
				$_SESSION['basket'] = $_COOKIE['basket'];
				// Update the cookie with the new expiry and the same basket code
				setcookie('basket', $_COOKIE['basket'], $cookie_expire, '/');
				if (DEBUG_MODE > 0) {
					header("Location: ".Page::$slug[0].'?basket-restored-from-cookie');
				} else {
					header("Location: ".Page::$slug[0]);
				}
				exit();
				
				
			// If there is no basket cookie
			} else {
				// Create a new basket session
				$_SESSION['basket'] = session_id();
				// Create a new basket cookie
				setcookie('basket', session_id(), $cookie_expire, '/');
			#	if (DEBUG_MODE > 0) {
			#		header("Location: ".Page::$slug[0].'?basket-cookie-missing-make-new');
			#	} else {
			#		header("Location: ".Page::$slug[0]);
			#	}
			#	exit();
				
#				echo '<p>Basket Cookie not found, so creating a new one</p>';

			}
			
		} else {
			// If there is already a basket session, then only check to see if a basket cookie exists
			if (!isset($_COOKIE['basket']) || empty($_COOKIE['basket']) || trim($_COOKIE['basket']) == '') {
				// If cookie doesn't exist, try to create it
				setcookie('basket', $_SESSION['basket'], $cookie_expire, '/');
				if (DEBUG_MODE > 0) {
					echo '<p>Strange, Basket Cookie was missing</p>';
				}
			} else {
				// Do nothing if a cookie exists
			}
		}
	}
	
	
	/*
	* This handles the creating of SESSION and COOKIE to hold the basket hash
	* This will allow a previously created basket to be restored (automatically) upon 
	* return to the website.
	*/
	public static function resetCookies() {
		
		$cookie_expire = 30 * 60 * 24 * 60 + time(); // Basket cookie life: 1 month
		$new_basket_hash = md5(microtime());
		
		setcookie('basket', $new_basket_hash, $cookie_expire, '/');
		$_SESSION['basket'] = $new_basket_hash;
		
	}


	/*
	* This generates a data object with purpose of generating a list of items
	*/
	public static function getItems($basket_hash = '') {
		if (trim($basket_hash) == '') {
			$basket_hash = $_SESSION['basket'];
		}
		$version_control = Config::$settings['baskets']['version_control'];
		$query = "
			SELECT
				b.*
			,	p.slug
			,	o.basket_data
			FROM
				`baskets` b
			LEFT JOIN
				`products` p ON p.id = b.item_id AND p.ttv_end IS null
			LEFT JOIN
				`orders` o ON o.id = b.order_id AND o.ttv_end IS null
            WHERE
				b.basket_hash = '".db::link()->real_escape_string($basket_hash)."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND b.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				b.`id` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* Check to see if an item exists in this shoppers basket
	*/
	public static function checkItemExists($baskets_id) {
		$version_control = Config::$settings['baskets']['version_control'];
		$query = "
			SELECT
				b.*
			FROM
				`baskets` b
            WHERE
				b.basket_hash = '".db::link()->real_escape_string($_SESSION['basket'])."'
			AND	b.id = '".db::link()->real_escape_string($baskets_id)."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND b.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				b.`id` ASC
			LIMIT 1
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return NULL;
		}
	}
	
	/*
	*
	*/
	public static function finaliseBasket($decoded) {
		// Update order record with decoded data and more
		$version_control = Config::$settings['baskets']['version_control'];
		$query_orders = "
			SELECT
				a.*
			FROM
				`baskets` a
            WHERE
				a.basket_hash = '".db::link()->real_escape_string($decoded['basket_hash'])."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query_orders .= "	AND a.`ttv_end` IS null";
		}
		$orders = db::link()->query($query_orders);
		if ($orders->num_rows > 0) {
			while ($order = $orders->fetch_object()) {
				$orders_update_array = array(
					'module'		=> 'baskets'
				,	'action'		=> 'update'
				,	'id'			=> $order->id
				,	'order_id'		=> $decoded['order_id']
				);
				$result = Form::processData('baskets',$orders_update_array);
			#	if ($result['query_status'] == 'OK') {
			#		return $order->id;
			#	}
			}
		}
		return $orders->num_rows;
	}
}
