<?php
/* 
 * 
 * 
 */

self::$settings['us_states'] =
array(
	'orderby'=>'pos ASC'
,	'singular'=>'US State'
,	'plural'=>'US States'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'parent_module'=>'countries'
,	'admin_access'=>''
);

self::$fields['us_states'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,		'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Page ID',				'protected'=>true,	'hint'=>'')
,	array('name'=>'title',		'required'=>true,	'list'=>true,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Title',				'protected'=>false,	'istitle'=>true,	'hint'=>'This is the name that appears on the front-end of the website on the shipping form.')
,	array('name'=>'code2',		'required'=>true,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'2 Digit Country Code','protected'=>true,	'regex'=>'/^[A-Z]{2}$/',	'hint'=>'Must be 2 uppercase letter - Needed for SagePay')
,	array('name'=>'code3',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'3 Digit Country Code','protected'=>true,	'regex'=>'/^[A-Z]{3}$/',	'hint'=>'Must be 3 uppercase letter - Not currently used by anything')
,	array('name'=>'pos',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'i',		'label'=>'Position',			'protected'=>true,	'hint'=>'')
,	array('name'=>'active',		'required'=>false,	'list'=>false,		'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',		'label'=>'Country is Active',	'protected'=>true,	'hint'=>'Allows you to only display countries that you are willing to deliver to', 'default'=>true)
);



/*
		
CREATE TABLE IF NOT EXISTS `us_states` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `code2` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `code3` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `pos` tinyint(2) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',

  `ttv_start` timestamp NULL DEFAULT NULL,
  `ttv_end` timestamp NULL DEFAULT NULL,
  `create_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `create_id` int(11) NOT NULL DEFAULT '0',
  `create_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `modify_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `modify_id` int(11) NOT NULL DEFAULT '0',
  `modify_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


// Data for ISO 3166-2 US State names and codes
// https://code.google.com/p/sagepay/source/browse/old/2.23/PHPFormKit/scripts/statecodes.js?r=5

	AL	Alabama	
	AK	Alaska	
	AZ	Arizona	
	AR	Arkansas	
	CA	California	
	CO	Colorado	
	CT	Connecticut	
	DE	Delaware	
	FL	Florida	
	GA	Georgia	
	HI	Hawaii	
	ID	Idaho	
	IL	Illinois	
	IN	Indiana	
	IA	Iowa	
	KS	Kansas	
	KY	Kentucky	
	LA	Louisiana	
	ME	Maine	
	MD	Maryland	
	MA	Massachusetts	
	MI	Michigan	
	MN	Minnesota	
	MS	Mississippi	
	MO	Missouri	
	MT	Montana	
	NE	Nebraska	
	NV	Nevada	
	NH	New Hampshire	
	NJ	New Jersey	
	NM	New Mexico	
	NY	New York	
	NC	North Carolina	
	ND	North Dakota	
	OH	Ohio	
	OK	Oklahoma	
	OR	Oregon	
	PA	Pennsylvania	
	RI	Rhode Island	
	SC	South Carolina	
	SD	South Dakota	
	TN	Tennessee	
	TX	Texas	
	UT	Utah	
	VT	Vermont	
	VA	Virginia	
	WA	Washington	
	WV	West Virginia	
	WI	Wisconsin	
	WY	Wyoming


INSERT INTO `us_states` (`code2`,`title`) VALUES ('AL','Alabama');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('AK','Alaska');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('AZ','Arizona');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('AR','Arkansas');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('CA','California');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('CO','Colorado');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('CT','Connecticut');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('DE','Delaware');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('FL','Florida');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('GA','Georgia');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('HI','Hawaii');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('ID','Idaho');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('IL','Illinois');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('IN','Indiana');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('IA','Iowa');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('KS','Kansas');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('KY','Kentucky');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('LA','Louisiana');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('ME','Maine');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('MD','Maryland');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('MA','Massachusetts');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('MI','Michigan');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('MN','Minnesota');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('MS','Mississippi');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('MO','Missouri');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('MT','Montana');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('NE','Nebraska');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('NV','Nevada');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('NH','New Hampshire');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('NJ','New Jersey');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('NM','New Mexico');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('NY','New York');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('NC','North Carolina');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('ND','North Dakota');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('OH','Ohio');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('OK','Oklahoma');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('OR','Oregon');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('PA','Pennsylvania');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('RI','Rhode Island');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('SC','South Carolina');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('SD','South Dakota');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('TN','Tennessee');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('TX','Texas');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('UT','Utah');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('VT','Vermont');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('VA','Virginia');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('WA','Washington');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('WV','West Virginia');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('WI','Wisconsin');
INSERT INTO `us_states` (`code2`,`title`) VALUES ('WY','Wyoming');

*/


