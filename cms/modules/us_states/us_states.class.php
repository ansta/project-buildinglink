<?php

class Us_states {
	
	/*
	* 
	*/
	public static function getSelect() {
		
		$version_control = Config::$settings['us_states']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`us_states` a
		";
		$query .= "
			WHERE
				a.active = 1
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= " AND	a.ttv_end IS null";
		}
		$query .= "
			ORDER BY
				a.`pos` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			$output[0] = 'select a state (US only)';
			while ($item = $items->fetch_object()) {
				$output[$item->id] = $item->title;
			}
			return $output;
		} else {
			return FALSE;
		}
	}
	
	/*
	* 
	*/
	public static function getItem($item_id,$column_name) {
		$version_control = Config::$settings['us_states']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`us_states` a
			WHERE
				a.id > 0
			AND a.id = ".db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS NULL";
		}
		$query .= " LIMIT 1";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			if($item = $items->fetch_object()) {
				return $item->$column_name;
			}
		}
		return FALSE;
	}
	
	
	/*
	* 
	*/
	public static function getNav() {
		
	}
	
	/*
	* 
	*/
	public static function getTags() {
		
	}
}