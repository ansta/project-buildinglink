<?php

Admin::allowNew(0);
Admin::allowEdit(1);
Admin::allowDelete(0);

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	header('Location: /admin/sites/edit/1/');
	
} elseif (Page::$slug[3] == 'new') {
#	Core::setModuleTask('new');
#	Form::prefillValues();
	Error::type(404);
		
} elseif (Page::$slug[3] == 'edit' && Page::$slug[4] > 0) {
	Core::setModuleTask('edit');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'delete' && Page::$slug[4] > 0) {
#	Core::setModuleTask('delete');
#	if (!Form::prefillValues()) {
#		Error::type(404); // If values not found for the requested record then 404
#	}
	Error::type(404);
	
} else {
	Error::type(404);
}
