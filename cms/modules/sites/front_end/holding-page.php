<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en-US" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if ( IE 7 )&!( IEMobile )]><html lang="en-US" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if ( IE 8 )&!( IEMobile )]><html lang="en-US" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"><!--<![endif]-->

<head>

	<meta charset="UTF-8" />
	<meta name="rotbots" content="noindex,nofollow" />
	
	<style type="text/css">
		html, body { height:100%; }
		body { 
			background: <?php echo (Site::$sitedata['color2']!=''?Site::$sitedata['color2']:'#444'); ?>;
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		}
		.table {
			display:table;
			width: 100%;
			height:100%;
		}
		
		.cell {
			display: table-cell;
			width:100%;
			vertical-align: middle;
			height:100%;
			color:#fff;
			text-align: center;
		}
		
		.container {
			background: <?php echo (Site::$sitedata['color1']!=''?Site::$sitedata['color1']:'#48a8d0'); ?>;
			width:500px;
			margin:0 auto;
			padding:20px;
			border-radius: 5px;
			border:5px solid rgba(255,255,255, 0.2);
		}
		
		.logo {
			margin-bottom: 30px; 
			margin-top: -104px;
		}
	</style>

</head>
<body class="maintenance">
	
	<div class="table">
		
		<div class="cell">
			<!-- .container -->
			
			<?php if( Site::$sitedata['image_path'] != '' ) { ?>
			<div class="logo"><img src="<?php echo Site::$sitedata['image_path']; ?>" alt=""></div>
			<?php }  ?>
			<div class="container">
				<h1><?php echo Site::$sitedata['title']; ?></h1>
				<p><strong>Website down for maintenance, we will be back soon!</strong></p>
				<p>In the mean time, please feel free to contact us on: <?php echo Site::$sitedata['tel1']; ?></p>
			</div>
			<!-- /.container -->
		</div>
	
	</div>
	
</html>


<?php /*?>

Site::$sitedata['auto_id'] -> 17
Site::$sitedata['id'] -> 1
Site::$sitedata['title'] -> Scalerulers.co.uk
Site::$sitedata['image'] -> roundel-logo-scalerulers-co-2.png
Site::$sitedata['domain_live1'] -> 
Site::$sitedata['domain_live2'] -> 
Site::$sitedata['domain_live3'] -> 
Site::$sitedata['domain_live4'] -> 
Site::$sitedata['domain_live5'] -> 
Site::$sitedata['domain_live6'] -> 
Site::$sitedata['domain_dev'] -> bl1.ansta.co.uk
Site::$sitedata['color1'] -> #991d1e
Site::$sitedata['color2'] -> #050505
Site::$sitedata['color3'] -> 
Site::$sitedata['color4'] -> 
Site::$sitedata['color5'] -> 
Site::$sitedata['strapline'] -> 
Site::$sitedata['incvat'] -> 0
Site::$sitedata['delivery_charge'] -> 0.00
Site::$sitedata['add1'] -> Unit 11
Site::$sitedata['add2'] -> Langham Barns Business Centre
Site::$sitedata['add3'] -> Langham Lane, Langham
Site::$sitedata['add4'] -> 
Site::$sitedata['town'] -> Colchester
Site::$sitedata['county'] -> Essex
Site::$sitedata['postcode'] -> CO4 5ZS
Site::$sitedata['country'] -> 
Site::$sitedata['tel1'] -> (01206) 272020
Site::$sitedata['tel2'] -> 
Site::$sitedata['fax'] -> (01206) 272090
Site::$sitedata['twitter'] -> https://twitter.com/BuildingLink1
Site::$sitedata['facebook'] -> https://www.facebook.com/pages/Building-Link-Ltd/514352448649458
Site::$sitedata['google'] -> https://plus.google.com/u/0/b/108593987737743673864/108593987737743673864/posts
Site::$sitedata['pinterest'] -> #missing_pinterest
Site::$sitedata['linkedin'] -> http://www.linkedin.com/company/building-link-ltd?trk=top_nav_home
Site::$sitedata['geoposition'] -> 
Site::$sitedata['geoplacename'] -> 
Site::$sitedata['georegion'] -> 
Site::$sitedata['analytics'] -> 
Site::$sitedata['copyright'] -> Ansta
Site::$sitedata['email_public'] -> sales@buildinglink.co.uk
Site::$sitedata['email_contact'] -> sales@buildinglink.co.uk
Site::$sitedata['locked'] -> 0
Site::$sitedata['is_live'] -> 1
Site::$sitedata['ttv_start'] -> 2013-09-26 10:11:31
Site::$sitedata['ttv_end'] -> 
Site::$sitedata['create_by'] -> Loki Wijnen
Site::$sitedata['create_id'] -> 1
Site::$sitedata['create_action'] -> update
Site::$sitedata['modify_by'] -> 
Site::$sitedata['modify_id'] -> 0
Site::$sitedata['modify_action'] -> 
Site::$sitedata['image_path'] -> /image.php?i=/sites/uploads/image/1/original/roundel-logo-scalerulers-co-2.png

<?php */?>