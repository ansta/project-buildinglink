<?php
/* 
 * Sites: holds sitewide data that makes sense to have held in the database rather than have it hard coded
 * Any number of additional columns can be added so long as ID and TITLE are preserved intact
 * The table is plural because the plan is to eventually support multiple sites from a single admin
 */

self::$settings['sites'] =
array(
	'orderby'=>'id ASC'
,	'singular'=>'Site'
,	'plural'=>'Sites'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'image'=>array(
		'admin'	=>array('width'=>39,'height'=>39,'format'=>'jpg','method'=>'crop')
	,	'footer'=>array('width'=>28,'height'=>28,'format'=>'jpg','method'=>'resize')
	,	'list'	=>array('width'=>39,'height'=>39,'format'=>'jpg','method'=>'resize')
	,	'header'=>array('width'=>70,'height'=>70,'format'=>'jpg','method'=>'resize')
	)
);
self::$fields['sites'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Link ID',				'protected'=>true,	'hint'=>'')
,	array('name'=>'title',			'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Company name',		'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'statement',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Statement',			'protected'=>false,	'hint'=>'Text entered here will appear just to the left of the main banner. If no text is entered, testimonials will show instead.')
,	array('name'=>'image',			'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Logo',				'protected'=>false,	'hint'=>'Requirements:<br />Format: png (must have transparent background)<br />Image size in pixels (px) must be: 70(W) x 70(H)', 'filetype'=>'png', 'maxsize'=>'5097152', 'root'=>'sites', 'groupby'=>'id')
,	array('name'=>'id_list',		'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'List of Site IDs to show','protected'=>false,	'hint'=>'')
,	array('name'=>'domain_live1',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Domain (with www)',	'protected'=>false,	'hint'=>'Enter the full domain name including www but without http://<br />For example: www.mywebsite.co.uk')
,	array('name'=>'domain_live2',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Domain (without www)','protected'=>false,	'hint'=>'Enter the full domain name without www and without http://<br />For example: mywebsite.co.uk')
,	array('name'=>'domain_live3',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Domain',				'protected'=>false,	'hint'=>'')
,	array('name'=>'domain_live4',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Domain',				'protected'=>false,	'hint'=>'')
,	array('name'=>'domain_live5',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Domain',				'protected'=>false,	'hint'=>'')
,	array('name'=>'domain_live6',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Domain',				'protected'=>false,	'hint'=>'')
,	array('name'=>'domain_dev',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Development Domain',	'protected'=>true,	'hint'=>'Typically in the format of: project.ansta.co.uk (do not add http in front of it)')
,	array('name'=>'strapline',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Strapline',			'protected'=>false,	'hint'=>'Not currently in use')
,	array('name'=>'incvat',			'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Prices include VAT',	'protected'=>false,	'hint'=>'Allows you to set whether or not you want product prices on this website to display including or excluding UK VAT depending on the audience type expected.', 'default'=>false)
,	array('name'=>'delivery_charge','required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Delivery Charge',		'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'A flat fee delivery charge is applied to the basket at checkout.<br />Do not include the pound sign and simply enter the price in pounds and pence (e.g.: for &pound;12 simply enter 12.00)<br />Set to 0.00 for free carriage.')
,	array('name'=>'color1',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'color',	'format'=>'s',	'label'=>'Highlight Colour',	'protected'=>false,	'hint'=>'If left blank, colour will default to <span style="color:#B21A2D">red: #B21A2D</span>')
,	array('name'=>'color2',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'color',	'format'=>'s',	'label'=>'Background Colour',	'protected'=>false,	'hint'=>'If left blank, colour will default to <span style="color:#000000">black: #000000</span>')
,	array('name'=>'color3',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Colour 3',			'protected'=>false,	'hint'=>'Not currently in use')
,	array('name'=>'color4',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Colour 4',			'protected'=>false,	'hint'=>'Not currently in use')
,	array('name'=>'color5',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Colour 5',			'protected'=>false,	'hint'=>'Not currently in use')
,	array('name'=>'add1',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Address line 1',		'protected'=>false,	'hint'=>'')
,	array('name'=>'add2',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Address line 2',		'protected'=>false,	'hint'=>'')
,	array('name'=>'add3',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Address line 3',		'protected'=>false,	'hint'=>'')
,	array('name'=>'add4',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Address line 4',		'protected'=>false,	'hint'=>'')
,	array('name'=>'town',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Town',				'protected'=>false,	'hint'=>'')
,	array('name'=>'county',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'County',				'protected'=>false,	'hint'=>'')
,	array('name'=>'postcode',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Postcode',			'protected'=>false,	'hint'=>'')
,	array('name'=>'country',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Country',				'protected'=>false,	'hint'=>'')
,	array('name'=>'tel1',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Telephone',			'protected'=>false,	'hint'=>'')
,	array('name'=>'tel2',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Secondary Telephone',	'protected'=>false,	'hint'=>'')
,	array('name'=>'fax',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Fax',					'protected'=>false,	'hint'=>'')
,	array('name'=>'email_public',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Display Email',		'protected'=>false,	'hint'=>'This email address is visible on the website.')
,	array('name'=>'email_contact',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Contact Form Email',	'protected'=>false,	'hint'=>'This email address is where contact form submissions will be sent.')
,	array('name'=>'twitter',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Twitter page address','protected'=>false,	'hint'=>'')
,	array('name'=>'facebook',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Facebook page address','protected'=>false,'hint'=>'Enter the full address of the page that you would like users to land on when visiting. You must include the http part of the address.')
,	array('name'=>'google',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Google+ page address','protected'=>false,	'hint'=>'Enter the full address of the page that you would like users to land on when visiting. You must include the http part of the address.')
,	array('name'=>'pinterest',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Pinterest page address','protected'=>false,'hint'=>'Enter the full address of the page that you would like users to land on when visiting. You must include the http part of the address.')
,	array('name'=>'linkedin',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Linkedin page address','protected'=>false,'hint'=>'Enter the full address of the page that you would like users to land on when visiting. You must include the http part of the address.')
,	array('name'=>'copyright',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Copyright',			'protected'=>true,	'hint'=>'Enter the full address of the page that you would like users to land on when visiting. You must include the http part of the address.')
,	array('name'=>'geoposition',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Geo Position',		'protected'=>false,	'hint'=>'')
,	array('name'=>'geoplacename',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Geo Placename',		'protected'=>false,	'hint'=>'')
,	array('name'=>'georegion',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Geo Region',			'protected'=>false,	'hint'=>'')
,	array('name'=>'analytics',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Google Analytics',	'protected'=>true,	'hint'=>'')
,	array('name'=>'locked',			'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'hidden',	'format'=>'b',	'label'=>'Site is Locked',		'protected'=>true,	'hint'=>'Prevent this site from being deleted', 'default'=>true)
,	array('name'=>'is_live',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Website is Live',		'protected'=>false,	'hint'=>'Warning: When set to <strong>No</strong>, you\'re customers will only see a holding page, they will not be able to browse your website!.', 'default'=>true)
);


/*

CREATE TABLE IF NOT EXISTS `sites` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `strapline` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `add1` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `add2` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `add3` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `add4` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `town` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `postcode` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `tel` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `geoposition` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `geoplacename` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `georegion` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `analytics` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `ttv_start` timestamp NULL DEFAULT NULL,
  `ttv_end` timestamp NULL DEFAULT NULL,
  `create_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `create_id` int(11) NOT NULL DEFAULT '0',
  `create_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `modify_by` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `modify_id` int(11) NOT NULL DEFAULT '0',
  `modify_action` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`auto_id`, `id`, `title`, `strapline`, `add1`, `add2`, `add3`, `add4`, `town`, `county`, `postcode`, `country`, `tel`, `fax`, `geoposition`, `geoplacename`, `georegion`, `analytics`, `copyright`, `email`, `locked`, `ttv_start`, `ttv_end`, `create_by`, `create_id`, `create_action`, `modify_by`, `modify_id`, `modify_action`) VALUES
(1, 1, 'Ansta Demo CMS', 'genero v2.x', 'Unit 4', 'Stour Valley Business Centre', 'Brundon Lane', '', 'Sudbury', 'Suffolk', 'CO10 7GB', 'United Kingdom', '01787 319658', '', '', '', '', '', 'Ansta', 'ask@ansta.co.uk', 1, '2012-09-14 14:22:19', NULL, 'install', 1, 'install', NULL, 0, NULL);

*/















/*

				THIS STUFF IS OLD!!!



CREATE TABLE sites (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) DEFAULT NULL,
  `strapline` VARCHAR(255) DEFAULT NULL,
  `copyright` VARCHAR(255) DEFAULT NULL,
  `email` VARCHAR(255) DEFAULT NULL,
  `locked` TINYINT(1) NOT NULL,
  `updatedby` VARCHAR(255) DEFAULT NULL,
  `updated` TIMESTAMP NULL,
  `createdby` VARCHAR(255) DEFAULT NULL,
  `created` TIMESTAMP NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;

INSERT INTO `sites` (`title`,`strapline`,`copyright`,``,``,``,``) VALUES ('Demo Site','Welcome','Me','','','','');
*/