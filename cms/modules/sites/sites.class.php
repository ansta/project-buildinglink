<?php

class Sites {

	/*
	*
	*
	public static 	$id = 0,
					$title = '',
					$url = '',
					$newwin = '';
	*/
	
	public static function initialise() {
		self::loadSite();
		self::setActiveSite();
	}
	
	/*
	* Get the chosen site based on the domain name in the users address bar
	*/
	public static function loadSite() {
		$version_control = Config::$settings['sites']['version_control'];
		$version_control = 'audit';
		$query = "
			SELECT
				`a`.*
			FROM
				`sites` a
			WHERE
				`a`.`id` > 0
			AND	(
					`a`.`domain_live1` = '".db::link()->real_escape_string(Page::$host)."'
				OR	`a`.`domain_live2` = '".db::link()->real_escape_string(Page::$host)."'
				OR	`a`.`domain_live3` = '".db::link()->real_escape_string(Page::$host)."'
				OR	`a`.`domain_live4` = '".db::link()->real_escape_string(Page::$host)."'
				OR	`a`.`domain_live5` = '".db::link()->real_escape_string(Page::$host)."'
				OR	`a`.`domain_live6` = '".db::link()->real_escape_string(Page::$host)."'
				OR	`a`.`domain_dev` = '".db::link()->real_escape_string(Page::$host)."'
			)
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS NULL";
		}
		$query .= "	LIMIT 1";
		
		if (USER_IP == OFFICE_IP) {
		#	echo $query;
		#	die();
		}
		$site = db::link()->query($query);
		if ($site->num_rows) {
			Site::$sitedata = $site->fetch_assoc();
			if (isset(Site::$sitedata['image'])) {
				Site::$sitedata['image_path'] = '/image.php?i=/sites/uploads/image/'.Site::$sitedata['id'].'/original/'.Site::$sitedata['image'];
			}
		} else {
			if (db::checkDb()) {
				if (USER_IP == OFFICE_IP) {
					die('Error: Database found but required tables are missing.<br />The real reason that you are seeing this page is because there is <strong>no site setup for this domain name</strong>.<br />(only Ansta can see this message, other people see a 404 page)');
				} else {
					Error::type(404);
				}
			} else {
				die('Error: Sites table not found or cannot connect to database.');
			}
		}
	}
	
	
	/*
	* Sets a session variable containing the ID of the chosen site
	* Used by admin to only show items for that particular site
	*/
	public static function setActiveSite() {
		if (Page::$slug[1] == ADMIN &&
			(!isset($_SESSION['active_site_id']) || $_SESSION['active_site_id'] < 1 || trim($_SESSION['active_site_id']) == '')
			) {
			$_SESSION['active_site_id'] = Site::$sitedata['id'];
		#	if (Page::$path != '/') {
			header('Location: '.Page::$path);
			exit();
		#	}
		}
	}
	
	
	/*
	* Get a list of all the items available and return an object
	*/
	public static function getList($exclude_site_id = 0) {
		$version_control = Config::$settings['sites']['version_control'];
		$query = "
			SELECT
				`a`.*
			FROM
				`sites` a
			WHERE
				`a`.`id` > 0
		";
		if ($exclude_site_id > 0) {
			$query .= "	AND `id` != ".(int)db::link()->real_escape_string($exclude_site_id);
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS NULL";
		}
		$query .= "	ORDER BY title ASC, id ASC";
	#	echo $query;	
		$results = db::link()->query($query);
		if ($results->num_rows > 0) {
			return $results;
		}
		return NULL;
	}
	
	
	/*
	* 
	*/
	public static function getSiteIconPath($site_id) {
		$version_control = Config::$settings['sites']['version_control'];
		$query = "
			SELECT
				`a`.`id`
			,	`a`.`image`
			FROM
				`sites` a
			WHERE
				`a`.`id` = ".(int)db::link()->real_escape_string($site_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS NULL";
		}
	#	echo $query;	
		$results = db::link()->query($query);
		if ($results->num_rows > 0) {
			if ($result = $results->fetch_object()) {
				return '/image.php?i=/sites/uploads/image/'.$result->id.'/original/'.$result->image;
			}
		}
		return Site::$sitedata['image_path'];
	}
	
	
	/*
	* Get a list of all the active items available and return an object
	*/
	public static function getActiveList($exclude_site_id = 0) {
		$version_control = Config::$settings['sites']['version_control'];
		$query = "
			SELECT
				`a`.*
			FROM
				`sites` a
			WHERE
				`a`.`id` > 0
			AND	`a`.`image` != ''
			AND	`a`.`image` != ''
			AND	`a`.`is_live` = 1
			AND `a`.`id` IN (".Site::$sitedata['id_list'].")
		";
		if ($exclude_site_id > 0) {
			$query .= "	AND `id` != ".(int)db::link()->real_escape_string($exclude_site_id);
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS NULL";
		}
		$query .= "	ORDER BY title ASC, id ASC";
	#	echo $query;	
		$results = db::link()->query($query);
		if ($results->num_rows > 0) {
			return $results;
		}
		return NULL;
	}
	
	
	/*
	* Get a list of all the items available and return an object
	*/
	public static function createDefaultWebsiteContent($site_id) {
		
		// Automatically create default content for this new website
		$result = TRUE;
		
		// Create default pages
		$result['pages'] = Pages::createDefaultPages($site_id);
	#	if (!$result['pages']) {
	#		$result = FALSE;
	#	}
		
		// Create default Product Categories
		$result['shop_categories'] = Shop_categories::createDefaultCategories($site_id);
	#	if (!$result['shop_categories']) {
	#		$result = FALSE;
	#	}
	
		$html = '<p>New site created with ID of '.$site_id.'. The create array is below.</p>';
		$html .= Tool::my_dump($result);
		
		Email::send('loki.ansta@gmail.com','Building link - New site setup report',$html);
	
	
		return $result;
	}
	
	
	/*
	* Get domain name back for the supplied site ID
	*/
	public static function getDomain($item_id) {
		$version_control = Config::$settings['sites']['version_control'];
		$version_control = 'audit';
		$query = "
			SELECT
				`a`.*
			FROM
				`sites` a
			WHERE
				`a`.`id` > 0
			AND `a`.`id` = ".(int)db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS NULL";
		}
		$query .= "	LIMIT 1";
	#	echo $query;
		$sites = db::link()->query($query);
		if ($sites->num_rows) {
			if ($site = $sites->fetch_object()) {
				
				$domain_name = '';
				if (trim($site->domain_live1) != '') {
					$domain_name = $site->domain_live1;
				}
				if ($domain_name == '' && trim($site->domain_live2) != '') {
					$domain_name = $site->domain_live2;
				}
				if ($domain_name == '' && trim($site->domain_dev) != '') {
					$domain_name = $site->domain_dev;
				}
				return '//'.$domain_name;
			}
		}
		return NULL;
	}
	
	
	/*
	* Get domain name back for the supplied site ID
	*/
	public static function showHoldingPage() {
		include(SYSTEM_ROOT.'modules/sites/front_end/holding-page.php');
	}
	
	
}
