<?php

$submission = TRUE;
		
foreach (Config::$fields[Page::$slug[2]] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Your form submission contains error, please check your data and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}

if ($submission) {
	$result = Form::processData();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'New item has been created.';
		
		// Automatically create default content for this new website
		if (Sites::createDefaultWebsiteContent($result['item_id'])) {
			$_SESSION['form']['form-success'] = 'Your new site has been created AND default content has been generated for you.';
		} else {
			$_SESSION['form']['form-error'] = 'Your new site has been created but there was a problem setting up the default content! Please contact Ansta to get this fixed. In the mean time, you can setup the default content manually or wait for the issue to be investigated and fixed.';
		}
		
		header("Location: ".$result['destination']);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}
