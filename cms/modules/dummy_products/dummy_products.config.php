<?php
/* 
 * 
 */
self::$settings['dummy_products'] =
array(
	'orderby'=>'title ASC'
,	'singular'=>'Product'
,	'plural'=>'Products'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
,	'protected'=>true
,	'attachments'=> array(
		array('name'=>'shop_categories', 'formtype'=>'multi', 'column'=>'title', 'label'=>'Categories', 'source'=>'shop_categories', 'where'=>'ttv_end IS null /* AND site_id = '.$_SESSION['active_site_id'].'*/', 'orderby'=>'site_id ASC, pos ASC', 'hasdepth'=>true, 'hint'=>'To select multiple categories, press and hold down the Ctrl button on your keyboard before clicking with your mouse')
	)
,	'image'=>array(
		'admin'		=>array('width'=> 70,'height'=> 70,'format'=>'jpg','method'=>'crop')
	,	'list' 		=>array('width'=>230,'height'=>230,'format'=>'jpg','method'=>'crop')
	,	'gallery'	=>array('width'=>300,'height'=>300,'format'=>'jpg','method'=>'crop')
	)
,	'link_to_item'=>true
,	'child_module_filter'=>array(
		0=>'products_variants'
	)
);

self::$fields['dummy_products'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',					'protected'=>true,	'hint'=>'')
,	array('name'=>'site_id',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Site ID',				'protected'=>true,	'hint'=>'')
,	array('name'=>'title',			'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',				'protected'=>false,	'hint'=>'This is the title of the item from an <abbr title="Search Engine Optimisation">SEO</abbr> perspective', 'istitle'=>true)
,	array('name'=>'slug',			'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'URL Identifier',		'protected'=>false,	'regex'=>'/^[a-z0-9\-]*$/',	'hint'=>'Identifies this item within the URL structure.<br />Must only contain lowercase letters, numbers or a minus symbol. All other characters are not permitted. Warning: spaces are not permitted.<br /><strong style="color:#955;">Once set, it is strongly advised NOT to change it in the future as this can affect your <abbr title="Search Engine Optimisation">SEO</abbr> page ranking.</strong>')
,	array('name'=>'sku',			'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Product No',			'protected'=>false,	'hint'=>'')
,	array('name'=>'attachments',	'required'=>false,											'formtype'=>'attachment',												'protected'=>false)
,	array('name'=>'setup_fee',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Setup Fee',			'protected'=>false,	'hint'=>'If this product has a one off setup fee, enter in the value in pounds and pence without the unit, so for example if the setup fee is &pound;10, simply enter 10.00 otherwise enter 0.')
,	array('name'=>'special_offer',	'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'On Special Offer',	'protected'=>false,	'hint'=>'Select <strong>Yes</strong> to highlight this product as "Special Offer".', 'default'=>false)
,	array('name'=>'metakeys',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Keywords',			'protected'=>false,	'hint'=>'Recommendation: no more than 5 comma separated keywords all of which <strong>MUST</strong> appear within the page content')
,	array('name'=>'metadesc',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Description',			'protected'=>false,	'hint'=>'Recommendation: a short description of approx. 150 characters')
,	array('name'=>'summary',		'required'=>false,	'list'=>false,	'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',	'label'=>'Short Description',	'protected'=>false,	'hint'=>'',	'editor'=>false)
,	array('name'=>'content',		'required'=>false,	'list'=>false,	'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',	'label'=>'Full Description',	'protected'=>false,	'hint'=>'',	'editor'=>true)
,	array('name'=>'artwork',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Allow Artwork Upload','protected'=>false,	'hint'=>'Allows shoppers to upload artwork for a given product by selecting <strong>Yes</strong>.', 'default'=>true)
,	array('name'=>'active',			'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Item is Active',		'protected'=>false,	'hint'=>'Allows creation of an item without publishing it immediately by selecting <strong>No</strong>.', 'default'=>true)
,	array('name'=>'image',			'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Photo',				'protected'=>false,	'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 300(W) x 300(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'5097152', 'root'=>'dummy_products', 'groupby'=>'id')
,	array('name'=>'other_sites',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Other Webites NO LONGER IN USE!',		'protected'=>false,	'hint'=>'Enter a comma separated list of SITE IDs to make this product allso appear in the listings of those sites.<br />Default must be set to 0 (zero) which will mean that this product will only appear on the single site that it belongs to.<br />This does not affect related products.')
,	array('name'=>'related_products','required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Related Products',	'protected'=>false,	'hint'=>'Enter a comma separated list of <strong>Product No</strong>s to show those products at the bottom of this product view page.')
);