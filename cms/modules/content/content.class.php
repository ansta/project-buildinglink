<?php
/*
*
*/
class Content{
	
	/*
	*
	*/
	public static function getZoneContent($page_id, $zone_id = 1, $custom_id = '', $editor=false){
		if ($page_id > 0) {
			
			// If $custom_id is set then prefix it with an underscore
			if (strlen(trim($custom_id)) > 0) {
				$custom = '_'.$custom_id;
			} else {
				$custom = '';
			}
			
			// The letters are prefixes to help differenciate ID blocks
			// Each block is separated by an underscore
			//  - P Indicates the the next digits are a page ID
			//  - Z Indicates that the next digits are a zone ID
			//  - $custom can contain any Letter(s) we choose and will be follows by digits which are those custom IDs
			$id = 'P'.$page_id.'_Z'.$zone_id.$custom;
			
			// For debugging purposes show the $id identifier
			if (USER_IP == '77.76.67.173') {
				echo '<!-- '.$id.' -->';
			#	echo ''.$id.'';
			}
			
			return self::getContentByContentId($id,$editor);
		}
	}
	
	
	/*
	*
	*/			
	public static function getContentByContentId($id,$editor=false){
		$content = '';
		$query = "
			SELECT
				id
			,	content
			FROM
				content
			WHERE
				content_id='".db::link()->real_escape_string($id)."'
			AND ttv_end IS NULL
		";
		$pagelookup = db::link()->query($query);
		if ($pagelookup->num_rows) { 
			if (User::isAdmin() && !$editor) {
				if (USER_IP == '77.76.67.173') {
					$link_title = $id;
				} else {
					$link_title = 'click here to edit this zone';
				}
				$content = '<div class="edit_content admin-edit"><a href="#" rel="'.$id.'" title="'.$link_title.'"><span>edit</span></a></div>';
				$content .= '<div id="content_'.$id.'">'.$pagelookup->fetch_object()->content.'</div>';
			}
			else if (User::isAdmin() && $editor)
			{
				$content = $pagelookup->fetch_object();
				$data=array();
				$data['id'] = $content->id;
				$data['content'] = $content->content;
				return $data;
			}
			else
			{
				$content = $pagelookup->fetch_object()->content;
			}
		}
		else
		{
			$content_result = Content::saveContent('','',$id);
			if (User::isAdmin()) {
				$content = '<div class="edit_content admin-edit"><a href="#" rel="'.$id.'"><span>edit</span></a>';
				$content .= '</div><div id="content_'.$id.'">'.$content_result.'</div>';
			}
			else
			{
				$content = $content_result;
			}
		}
		return $content;
	}
	
	/*
	*
	*/
	public static function saveContent($id='', $content, $content_id, $editor=false){
		if ($id=='') {
			$update_pages = array(
			'module' => 'content',
			'action' => 'new',
			'content' => $content,
			'content_id' => $content_id
			);
			Form::setAuditAction('insert');
		}
		else
		{
			$update_pages = array(
			'id' => $id,
			'module' => 'content',
			'action' => 'edit',
			'content' => $content,
			'content_id' => $content_id
			);
			Form::setAuditAction('update');
		}
		Form::processData('content',$update_pages);
		
		if ($editor) {
		 	$content = Content::getContentByContentId($content_id,$editor);
			return $content['content'];
		}
		else
		{
			return $content = Content::getContentByContentId($content_id);
		}
	}
	
	/*
	*
	*/
	public static function addContent($id,$content){
		$update_pages = array(
			'id' => $id,
			'module' => 'content',
			'action' => 'edit',
			'content' => $content
		);
		Form::setAuditAction('update');
		Form::processData('content',$update_pages);
		return $content = Content::getContentById($id);
	}
}