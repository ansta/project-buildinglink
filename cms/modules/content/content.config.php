<?php
self::$settings['content'] =
array(
	'orderby'=>'auto_id ASC'
,	'singular'=>'Content'
,	'plural'=>'Contents'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
);

self::$fields['content'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,		'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Page ID',			'protected'=>true,	'hint'=>'')
,	array('name'=>'content',	'required'=>false,	'list'=>false,		'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',		'label'=>'Page Content',	'protected'=>false,	'hint'=>'')
,   array('name'=>'content_id',	'required'=>false,	'list'=>false,		'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Content ID',		'protected'=>true,	'hint'=>'')
);

/*

CREATE TABLE content (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  `content_id` INT(11) NOT NULL DEFAULT 0,
  `content` TEXT DEFAULT NULL,
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;

*/