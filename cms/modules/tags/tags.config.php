<?php
/* 
 * 
 * 
 * 
 * 
 * 
 * 
 */
self::$settings['tags'] =
array(
	'orderby'=>'title ASC'
,	'singular'=>'tag'
,	'plural'=>'Tags'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
);

self::$fields['tags'] =
array(
	array('name'=>'id',		'required'=>false,	'list'=>false,	'unique'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'User ID',			'protected'=>false,	'hint'=>'')
,	array('name'=>'title',	'required'=>true,	'list'=>true,	'unique'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Title',			'protected'=>false,	'regex'=>'/^[A-Za-z0-9\-\+\*]*$/',	'hint'=>'Any alpha numeric characters are permitted inluding + - and * but no spaces or punctuation', 'istitle'=>true)
);

/*

CREATE TABLE tags (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  `title` VARCHAR(255) DEFAULT NULL,
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;

*/