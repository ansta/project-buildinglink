<?php
class Tags {
	
	public static 	$view_item_object,
				 	$item_id = 0;
	
	/*
	* Look for tag exactly matching the supplied string and return the corresponding ID
	* Used by front end of website
	*/
	public static function getItemList($module,$item_id) {
		$query = "
			SELECT
				a.id
			,	a.title
			,	a.title AS slug
			FROM
				`tags` a
			LEFT JOIN
				attachments x ON
					a.id = x.attachment_id
				AND	attachment_name = 'tags'
				AND	module_name = '".db::link()->real_escape_string($module)."'
				AND	module_id = ".(int)db::link()->real_escape_string($item_id)."
				AND	x.ttv_end IS null
			WHERE
				a.ttv_end IS null
			AND x.id IS NOT null
			ORDER BY
				a.title ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		$output = '';
		if ($items->num_rows > 0) {
			return $items;
		}
		return NULL;
	}
	
	
	/*
	* Returns a JSON style output for use by JQCloud javascript plugin
	* Used by front end of website
	*/
	public static function getCloudForJCloud($module = 'all',$item_id = 0) {
		$query = "
			SELECT
				a.`title`
			,	count(*) AS count 
			FROM 
				`tags` a
			LEFT JOIN
				`attachments` x
				ON	a.`id` = x.`attachment_id`
				AND	x.`attachment_name` = 'tags'
				AND	x.`ttv_end` IS null
		";
		if ($module != 'all') {
			$query .= "	AND	x.`module_name` = '".db::link()->real_escape_string($module)."'";
		}
		if ($item_id > 0) {
			$query .= "	AND	x.`module_id` = ".(int)db::link()->real_escape_string($item_id)."";
		}
		if ($module != 'all') {
			$query .= "
				LEFT JOIN
					`".$module."` b
					ON b.id = x.module_id
					AND b.ttv_end IS null
			";
			
			// Check to see if this module uses the ACTIVE field
			foreach (Config::$fields[$module] AS $k => $v) {
				if ($v['name'] == 'active') {
					$query .= "	AND	b.active = 1";
				}
			}
		}
		$query .= "
			WHERE
				a.`ttv_end` IS null
			AND	x.`id` IS NOT null 
			AND	x.`module_id` IS NOT null
		";
		if ($module != 'all') {
			$query .= "	AND	b.id IS NOT null";
		}
		$query .= "
			GROUP BY
				a.`title`
		";
	#	echo $query.PHP_EOL;
		$items = db::link()->query($query);
		$output = '';
		$i = 0;
		$num_rows = $items->num_rows;
		if ($num_rows > 0) {
			while ($item = $items->fetch_object()) {
				$i++;
				$output .= '{';
					$output .= 'text:"'.$item->title.'"';
					$output .= ', weight:'.$item->count.'';
					$output .= ', link:"'.Module::$root.'tag/'.$item->title.'/"';
				#	$output .= ', html:';
				#		$output .= '{';
				#			$output .= 'title:"'.$ccccc.'"';
				#		$output .= '}';
				$output .= '}';
				if ($i < $num_rows) {
					$output .= ','.PHP_EOL;
				}
			}
		}
		return $output;
	}
	
	
	/*
	* This function is called near the end of Form::processData()
	* Used by website admin
	*/
	public static function processData($id,$data) {
		
		// Check to see if this module uses tags (does it have a field of type tags)
		$uses_tags = FALSE;
		foreach (Config::$fields[$data['module']] AS $k => $v) {
			if ($v['formtype'] == 'tags') {
				$uses_tags = TRUE;
			}
		}
		if (!$uses_tags) {
			return;
		}
		
		// If we are still here, then proceed with the tag processing
		// Get the ID to attach these tags to
		if($data['id'] == NULL) {
			$data['id'] = $id;
		}
		
		// Separate out each TAG from the string
		$tags = explode(' ',$data['tags']);
		
		// Remove any existing TAGs for this item
		self::removeAllAttachedTagsForThisItem($data);
		
		// Add one by one the chosen TAGs for this item
		foreach ($tags AS $tag) {
			$tag_id = self::tagExistsInTagsTable($tag);
			if ($tag_id == 0) {
				$new_tag_id = self::insertNewTagIntoTagsTable($tag);
				self::addNewTagAttachmentForThisItem($data,$new_tag_id);
			} else {
				self::addNewTagAttachmentForThisItem($data,$tag_id);
			}
		}
	}
	
	
	/*
	* Look for tag exactly matching the supplied string and return the corresponding ID
	* Used by website admin
	*/
	public static function tagExistsInTagsTable($tag) {
		$query = "
			SELECT
				id
			,	title
			FROM
				`tags`
			WHERE
				ttv_end IS null
			AND	title = '".db::link()->real_escape_string($tag)."'
		";
		$items = db::link()->query($query);
		$output = '';
		if ($items->num_rows > 0) {
			while ($item = $items->fetch_object()) {
				return $item->id;
			}
		}
		// If none found, return 0
		return 0;
	}
	
	
	/*
	* Insert a new tag into the tags table
	* Used by website admin
	*/
	public static function insertNewTagIntoTagsTable($tag) {
		echo ' insertNewTagIntoTagsTable';
		$item_array = array(
			'module' => 'tags',
			'action' => 'new',
			'title' => $tag
		);
	#	Form::setAuditAction('new');
		$result = Form::processData('tags',$item_array);
		if ($result['query_status'] == 'OK') {
			return $result['item_id'];
		}
		return 0;
	}
	
	
	/*
	* Clears out all tags associated with the current item and then leaving
	* room to have the new tags added back in.
	* Used by website admin
	*/
	public static function removeAllAttachedTagsForThisItem($data) {
		$attdata = array(
			'id' => $data['id']
		,	'module' => $data['module']
		);
		$attachments['name'] = 'tags';
		Attachments::removeAttachments($attdata,$attachments);
	}
	
	
	/*
	* Add new tags back into the attachments table
	* Used by website admin
	*/
	public static function addNewTagAttachmentForThisItem($data,$tag_id) {
		$attachment_data = array(
			'module' => 'attachments',
			'action' => 'new',
			'module_id' => $data['id'],
			'module_name' => $data['module'],
			'attachment_id' => $tag_id,
			'attachment_name' => 'tags'
		);
#		echo '<pre>addNewTagAttachmentForThisItem: ';
#		var_dump($attachments);
#		var_dump($data);
#		var_dump($tag_id);
		$result = Form::processData('attachments',$attachment_data);
#		var_dump($result);
	}
	
}