<?php

/*

		THIS IS NO LONGER IN USE

*/





/* 
 * 
 */
self::$settings['pages_featured'] =
array(
	'orderby'=>'pos ASC'
,	'singular'=>'Featured Page'
,	'plural'=>'Featured Pages'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
,	'parent_module'=>'pages'
,	'image'=>array(
		'admin'	=>array('width'=>113,'height'=> 70,'format'=>'jpg','method'=>'crop')
	,	'small' =>array('width'=>170,'height'=>106,'format'=>'jpg','method'=>'crop')
	,	'large'	=>array('width'=>229,'height'=>143,'format'=>'jpg','method'=>'crop')
	)
,	'parent_id'=>Pages_featured::getAdminSelect()
);

self::$fields['pages_featured'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',			'protected'=>true,	'hint'=>'')
,	array('name'=>'site_id',	'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Site ID',		'protected'=>true,	'hint'=>'')
,	array('name'=>'title',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'hidden',	'format'=>'s',	'label'=>'Page Title',	'protected'=>false,	'istitle'=>true,	'hint'=>'This is just for Admin purposes so that you have something to identify this item with in the list page.')
,	array('name'=>'parent_id',	'required'=>true,	'list'=>true,	'datatype'=>'int',		'formtype'=>'select',	'format'=>'i',	'label'=>'Page',		'protected'=>false,	'hint'=>'',	'source'=>'pages_featured')
,	array('name'=>'tagline',	'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Tag Line',	'protected'=>false, 'hint'=>'This will appear as the <abbr title="Alternative text">ALT text</abbr> for the image',	'istitle'=>true)
,	array('name'=>'image',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',	'label'=>'Photo',		'protected'=>false,	'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 740(W) x 310(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'9097152', 'root'=>'pages_featured', 'groupby'=>'id', 'manual_crop'=>true, 'cropratio'=>'large')
,	array('name'=>'pos',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Position',	'protected'=>false,	'hint'=>'')
);

