<?php

class Pages_featured {
	
	/*
	* Get items for this particular WEBSITE ID
	*/
	public static function getList() {
		$version_control = Config::$settings['pages_featured']['version_control'];
		$query = "
			SELECT
				`a`.*
			,	`p`.`url`
			,	`p`.`label` as title
			FROM
				pages_featured a
			LEFT JOIN
				pages p
				ON `p`.`id` = `a`.`parent_id`
				AND `p`.`ttv_end` IS null
				AND	`p`.`active` = 1
			WHERE
				`a`.`id` > 0
			AND `a`.`site_id` = ".(int)db::link()->real_escape_string(Site::$sitedata['id'])."
			
		";
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
		
		$query .= " ORDER BY `a`.`pos` ASC";
		$query .= " LIMIT 7";
	#	echo $query;
		$items = db::link()->query($query);
		
		if ($items->num_rows > 0) {
			// Some records are found, so return them
			return $items;
			
		}
		
		// Otherwise give up
		return NULL;
		
	}
	
	public static function getAdminSelect() {
		$version_control = Config::$settings['pages']['version_control'];
		$query = "
			SELECT
				`a`.`id`
			,	`a`.`label`
			,	`a`.`depth`
			FROM
				pages a
			WHERE
				`a`.`id` > 0
			AND `a`.`site_id` = ".(int)db::link()->real_escape_string($_SESSION["active_site_id"])."
			AND	`a`.`active` = 1
		";
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
		
		$query .= " ORDER BY `a`.`pos` ASC, `a`.`id` ASC";
	#	echo $query;
		$items = db::link()->query($query);
		
		if ($items->num_rows > 0) {
			while($item = $items->fetch_object()) {
				$depth_marker = '';
				for ($i=1;$i<$item->depth;$i++) {
					$depth_marker .= '&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				$output[$item->id] = $depth_marker.$item->label;
			}
			return $output;
		}
		
		// Otherwise give up
		return NULL;
	}
}
