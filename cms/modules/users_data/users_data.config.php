<?php
/* 
 * 
 */
self::$settings['users_data'] =
array(
	'orderby'=>'user_id ASC'
,	'singular'=>'User Data'
,	'plural'=>'Users Data'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false
,	'parent_module'=>'users'
);

self::$fields['users_data'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',			'protected'=>false,		'hint'=>'')
,	array('name'=>'user_id',	'required'=>false,	'list'=>true,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'User ID',		'protected'=>false,		'hint'=>'')
,	array('name'=>'data1',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Data Line 1',	'protected'=>false,		'hint'=>'')
,	array('name'=>'data2',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Data Line 2',	'protected'=>false,		'hint'=>'')
);

/*

CREATE TABLE users_data (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  
  `user_id` INT(11) NOT NULL DEFAULT 0,
  `data1` VARCHAR(35) DEFAULT NULL,
  `data2` VARCHAR(35) DEFAULT NULL,
  
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;










REGEX:

To apply a reegex pattern to a field, add the following element:
	'regex'=>'xxx'

Replace xxx with a pattern such as below:

 - Any email address:
	/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/
	
 - Limited extention email address:
	version 1:	/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]*(pixacms.com|pixxacms.com)$/
	version 2:	/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@]([A-z0-9_]+pixacms.com|pixxacms.com)$/
		
 - Price:
	/^[0-9]{1,5}[.][0-9]{2}$/
	
 - Number between 0 and 99
	/^[0-9]{1,2}$/

*/