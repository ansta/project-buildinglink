<?php

class Users_data {
	
	/*
	* 
	*/
	public static function createItemIfNotExists($user_id) {
		$query_check_users = "
			SELECT
				`id`
			FROM
				`users_data`
			WHERE
				`ttv_end` IS NULL
			AND `user_id` = ".(int)db::link()->real_escape_string($user_id)."
		";
	#	echo $query;
		$users_lookup = db::link()->query($query_check_users);
		
		// If a user data record is found then we don't need to do anything except return the record ID for this users data
		if ($users_lookup->num_rows > 0) {
			if ($user_lookup = $users_lookup->fetch_object()) {
				return $user_lookup->id;
			}
			die('ERROR: failed to retrieve user data record, please contact support.');
		
		// Otherwise we need to create one for this user
		} else {
			// First create a record for this user
			$item_array = array(
				'module' => 'users_data',
				'action' => 'new',
				'user_id' => $user_id
			);
			Form::setAuditAction('insert');
			Form::processData('users_data',$item_array);
			
			// Then retrieve the ID of the record belonging to this users data so that we can return it.
			$query_check_data = "
				SELECT
					`id`
				FROM
					`users_data`
				WHERE
					`ttv_end` IS NULL
				AND `user_id` = ".(int)db::link()->real_escape_string($user_id)."
			";
			$datas_lookup = db::link()->query($query_check_data);
			if ($datas_lookup->num_rows > 0) {
				if ($data_lookup = $datas_lookup->fetch_object()) {
					return $data_lookup->id;
				}
			}
			die('ERROR: failed to create user data record, please contact support.');
		}
		die('ERROR: failed to locate user data record, please contact support.');
	}
	
	/*
	* 
	*/
	public static function getUserId($data_id) {
		$query_check_users = "
			SELECT
				`user_id`
			FROM
				`users_data`
			WHERE
				`ttv_end` IS NULL
			AND `id` = ".(int)db::link()->real_escape_string($data_id)."
		";
		$users_lookup = db::link()->query($query_check_users);
		
		// If a user data record is found then return the user_id
		if ($users_lookup->num_rows > 0) {
			if ($user_lookup = $users_lookup->fetch_object()) {
				return $user_lookup->user_id;
			}
		}
		
		// Otherwise return 0
		return 0;
	}
	
}












