<?php

Admin::allowNew(1);
Admin::allowEdit(1);
Admin::allowDelete(1);
Admin::allowView(1);

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	Core::setModuleTask('default'); // Optional to specify default as it would be default anyway!
	
} elseif (Page::$slug[3] == 'new') {
	if ($_SESSION['id'] > 1) {
		header("Location: /admin/users/");
	} else {
		Core::setModuleTask('new');
		Form::prefillValues();
	}
	
} elseif (Page::$slug[3] == 'edit' && Page::$slug[4] > 0) {
	Core::setModuleTask('edit');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'view' && Page::$slug[4] > 0) {
	Core::setModuleTask('view');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'delete' && Page::$slug[4] > 0) {
	if ($_SESSION['id'] > 1) {
		header("Location: /admin/users/");
	} else {
		Core::setModuleTask('delete');
		if (!Form::prefillValues()) {
			Error::type(404); // If values not found for the requested record then 404
		}
	}
	
} else {
	Error::type(404);
}