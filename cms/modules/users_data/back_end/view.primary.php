<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
View <?php echo Config::$settings[Page::$slug[2]]['singular']; ?>: <?php echo Form::getItemTitle(); ?></h1>

<section>
	<?php
	$item = Admin::getItem(Page::$slug[4]);
	
	echo '<a href="../../../users/edit/'.Form::$values['user_id'].'/">Return to User Account</a>';
	
	foreach (Config::$fields[Module::$name] AS $field) {
		if ($field['formtype'] == 'hidden' || $field['formtype'] == 'protected') {
		} else {
			echo '<div class="field"><span class="label">'.$field['label'].'</span>: <span class="value">'.$item->$field['name'].'</span></div>';
		}
	}
	?>
</section>