<?php

class Products_variants {
	
	/*
	*
	*
	*/
	public static 	$id = 0,
					$title = '',
					$url = '',
					$newwin = '',
					$view_item_object,
					$item_id = 0;
	
	
	/*
	* This generates a data object with purpose of generating a list of items in the front end of the website
	*/
	public static function getList($parent_id = 0) {
		$version_control = Config::$settings['products_variants']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`products_variants` a
            WHERE
				a.id > 0
		";
		if ($parent_id > 0) {
			$query .= "
				AND a.parent_id = ".(int)db::link()->real_escape_string($parent_id)."
			";
		}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
		$query .= "
			ORDER BY
				a.`pos` ASC
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			return $items;
		} else {
			return FALSE;
		}
	}
	
	/*
	* Checks and item by a given id and returns a data object
	*/
	public static function checkItemIsValid($item_id,$parent_id) {
		$version_control = Config::$settings['products_variants']['version_control'];
		$query = "
			SELECT
				a.*
			FROM
				`products_variants` a
		";
		$query .= "
			WHERE
				a.id > 0
			AND	a.id = ".(int)db::link()->real_escape_string($item_id)."
			AND	a.parent_id = ".(int)db::link()->real_escape_string($parent_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND a.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows == 1) {
			while ($item = $items->fetch_object()) {
				self::$view_item_object = $item;
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/*
	* Get the cheapest price from all variants for a given product id
	* This is used by the product page
	*/
	public static function getCheapestPrice($parent_id) {
		$version_control = Config::$settings['products_variants']['version_control'];
		$query = "
			SELECT
				`a`.`value_1`
			,	`a`.`value_2`
			,	`a`.`value_3`
			,	`a`.`value_4`
			,	`a`.`value_5`
			,	`a`.`value_6`
			FROM
				products_variants a
			WHERE
				`a`.`parent_id` = ".(int)db::link()->real_escape_string($parent_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		$all_prices = array();
		if ($items->num_rows > 0) {
			while($item = $items->fetch_object()) {
				$all_prices[] = $item->value_1;
				$all_prices[] = $item->value_2;
				$all_prices[] = $item->value_3;
				$all_prices[] = $item->value_4;
				$all_prices[] = $item->value_5;
				$all_prices[] = $item->value_6;
			}
			$all_prices = array_filter($all_prices);
			sort($all_prices);
			return $all_prices[0];
		} else {
			return 'n/a';
		}
	}
	
	/*
	* Get the minimum order quantity that a shopper can add to basket
	* This is used by the product page and by the UPDATE and ADD to basket process
	*/
	public static function getMinimumOrderQuantity($parent_id) {
		$version_control = Config::$settings['products_variants']['version_control'];
		$query = "
			SELECT
				`a`.`label_1`
			,	`a`.`label_2`
			,	`a`.`label_3`
			,	`a`.`label_4`
			,	`a`.`label_5`
			,	`a`.`label_6`
			FROM
				products_variants a
			WHERE
				`a`.`parent_id` = ".(int)db::link()->real_escape_string($parent_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		$all_prices = array();
		if ($items->num_rows > 0) {
			while($item = $items->fetch_object()) {
				$all_quantities[] = $item->label_1;
				$all_quantities[] = $item->label_2;
				$all_quantities[] = $item->label_3;
				$all_quantities[] = $item->label_4;
				$all_quantities[] = $item->label_5;
				$all_quantities[] = $item->label_6;
			}
			$all_quantities = array_filter($all_quantities);
			sort($all_quantities);
			return $all_quantities[0];
		} else {
			return 'error';
		}
	}
	
	
	/*
	* Determine the unit price of an item for a given quantity based on the the selected variant
	*/
	public static function getUnitPriceBasedOnQuantity($variant_id,$quantity_requested) {
		$version_control = Config::$settings['products_variants']['version_control'];
		$query = "
			SELECT
				`a`.*
			FROM
				products_variants a
			WHERE
				`a`.`id` = ".(int)db::link()->real_escape_string($variant_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		$all_prices = array();
		if ($items->num_rows == 1) {
			while ($item = $items->fetch_object()) {
				$labels[1] = (trim($item->label_1) != ''?trim($item->label_1):1000000000000);
				$labels[2] = (trim($item->label_2) != ''?trim($item->label_2):2000000000000);
				$labels[3] = (trim($item->label_3) != ''?trim($item->label_3):3000000000000);
				$labels[4] = (trim($item->label_4) != ''?trim($item->label_4):4000000000000);
				$labels[5] = (trim($item->label_5) != ''?trim($item->label_5):5000000000000);
				$labels[6] = (trim($item->label_6) != ''?trim($item->label_6):6000000000000);
				
				$values[1] = $item->value_1;
				$values[2] = $item->value_2;
				$values[3] = $item->value_3;
				$values[4] = $item->value_4;
				$values[5] = $item->value_5;
				$values[6] = $item->value_6;
			}
			
			if ($quantity_requested >= $labels[1] && $quantity_requested < $labels[2]) {
				$unit_price = $values[1];
				
			} elseif ($quantity_requested >= $labels[2] && $quantity_requested < $labels[3]) {
				$unit_price = $values[2];
				
			} elseif ($quantity_requested >= $labels[3] && $quantity_requested < $labels[4]) {
				$unit_price = $values[3];
				
			} elseif ($quantity_requested >= $labels[4] && $quantity_requested < $labels[5]) {
				$unit_price = $values[4];
				
			} elseif ($quantity_requested >= $labels[5] && $quantity_requested < $labels[6]) {
				$unit_price = $values[5];
				
			} elseif ($quantity_requested >= $labels[6]) {
				$unit_price = $values[6];
				
			}
			
			return $unit_price;
			
		}
	#	echo 'error 1';
		return 'error: too many variant records found for this variant id';
	}
	
	
	
	/*
	* This generates an array of data of all products and feeds it to the CSV download file
	*/
	public static function getListForCsvExport() {
	#	$version_control1 = Config::$settings['products']['version_control'];
	#	$version_control2 = Config::$settings['products_variants']['version_control'];
		$version_control1 = 'audit';
		$version_control2 = 'audit';
		
/*
		product_ref	data_type	label	price1	price2	price3	price4	price5	price6	special
		E31110		setup		10							
		E31110		labels				10		25		50		100		125		150		0
		E31110		variant		Red		10		9		8		7		6		5	
		E31110		variant		Green	11		10		9		8		7		6	
		E31110		variant		Blue	12.99	11.99	10.99	9.99	8.99	7.99	
		E31110		variant		Black	15		14		13		12		11		10	
		
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',					'protected'=>true,											'hint'=>'')
,	array('name'=>'parent_id',		'required'=>true,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Product ID',			'protected'=>false,											'hint'=>'',	'source'=>'products_variants')
,	array('name'=>'title',			'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',				'protected'=>false,											'hint'=>'', 'istitle'=>true)
,	array('name'=>'label_1',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 1',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Enter the quantity break as a whole number, for example 100. Leave blank if not applicable.',	'separator'=>'before')
,	array('name'=>'value_1',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 1',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Enter in the price in pounds and pence without the unit, so for example if the price is &pound;10, simply enter 10.00 otherwise enter 0.')
,	array('name'=>'label_2',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 2',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_2',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 2',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_3',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 3',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_3',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 3',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_4',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 4',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_4',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 4',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_5',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 5',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_5',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 5',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_6',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 6',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_6',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 6',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'pos',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'i',	'label'=>'Position',			'protected'=>true,											'hint'=>'')

*/

		$query1 = "
			SELECT
				`p`.`id`
			,	`p`.`sku`
			,	`p`.`setup_fee`
			,	`p`.`special_offer`
			FROM
				`products` p
			WHERE
				`p`.`sku` != ''
		";
		if($version_control1 == 'audit' || $version_control1 == 'full') {
			$query1 .= "	AND `p`.`ttv_end` IS null";
		}
		$query1 .= "
			ORDER BY
				`p`.`site_id` ASC
			,	`p`.`sku` ASC
		";
	#	echo $query1;
		$products = db::link()->query($query1);
		$i = 0;
		if ($products->num_rows > 0) {
			while ($product = $products->fetch_assoc()) {
				$i++;
				$data[$i] = array(
					'product_no'=> $product['sku']
				,	'data_type'	=> 'setup'
				,	'label'		=> $product['setup_fee']
				,	'price1'	=> ''
				,	'price2'	=> ''
				,	'price3'	=> ''
				,	'price4'	=> ''
				,	'price5'	=> ''
				,	'price6'	=> ''
				,	'special'	=> $product['special_offer']
				);

				$query2 = "
					SELECT
						`v`.*
					FROM
						`products_variants` v
					WHERE
						`v`.`parent_id` = ".$product['id']."
				";
				if($version_control2 == 'audit' || $version_control2 == 'full') {
					$query2 .= "	AND `v`.`ttv_end` IS null";
				}
				$query2 .= "
					ORDER BY
						`v`.`parent_id` ASC
					,	`v`.`pos` ASC
				";
			#	echo $query2;
				$variants = db::link()->query($query2);
				if ($variants->num_rows > 0) {
					$i++;
					$labels	= FALSE;
					$data[$i] = array(
						'product_no'=> $product['sku']
					,	'data_type'	=> 'setup'
					,	'label'		=> $product['setup_fee']
					,	'price1'	=> ''
					,	'price2'	=> ''
					,	'price3'	=> ''
					,	'price4'	=> ''
					,	'price5'	=> ''
					,	'price6'	=> ''
					,	'special'	=> $product['special_offer']
					);
					
					while ($variant = $variants->fetch_assoc()) {
						if (!$labels) {
							$labels = TRUE;
							$data[$i] = array(
								'product_no'=> $product['sku']
							,	'data_type'	=> 'labels'
							,	'label'		=> ''
							,	'price1'	=> $variant['label_1']
							,	'price2'	=> $variant['label_2']
							,	'price3'	=> $variant['label_3']
							,	'price4'	=> $variant['label_4']
							,	'price5'	=> $variant['label_5']
							,	'price6'	=> $variant['label_6']
							,	'special'	=> ''
							);
						}
						$i++;
						$data[$i] = array(
							'product_no'=> $product['sku']
						,	'data_type'	=> 'variant'
						,	'label'		=> $variant['title']
						,	'price1'	=> $variant['value_1']
						,	'price2'	=> $variant['value_2']
						,	'price3'	=> $variant['value_3']
						,	'price4'	=> $variant['value_4']
						,	'price5'	=> $variant['value_5']
						,	'price6'	=> $variant['value_6']
						,	'special'	=> ''
						);
						
					}
				}
			}
			return $data;
		}
		return NULL;
	}
	
}
