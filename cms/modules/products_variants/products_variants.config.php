<?php
/* 
 * 
 * 
 */

self::$settings['products_variants'] =
array(
	'orderby'=>'pos ASC'
,	'singular'=>'Product Variant'
,	'plural'=>'Product Variants'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'parent_id'=>Products::getAdminSelect()
,	'parent_module'=>'products'
,	'short'=>'Variants'
,	'icon_class'=>'btn-pound'
,	'filter_by_site'=>true
);

self::$fields['products_variants'] =
array(
	array('name'=>'id',				'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',					'protected'=>true,											'hint'=>'')
,	array('name'=>'site_id',		'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Site ID',				'protected'=>true,	'hint'=>'')
,	array('name'=>'parent_id',		'required'=>true,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'Product ID',			'protected'=>false,											'hint'=>'',	'source'=>'products_variants')
,	array('name'=>'title',			'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Title',				'protected'=>false,											'hint'=>'', 'istitle'=>true)
,	array('name'=>'out_of_stock',	'required'=>false,	'list'=>true,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Variant is out of stock?','protected'=>false, 'default'=>false,					'hint'=>'Select  <strong>Yes</strong> if item is out of stock.')
,	array('name'=>'label_1',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 1',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Enter the quantity break as a whole number, for example 100. Leave blank if not applicable.',	'separator'=>'before')
,	array('name'=>'value_1',		'required'=>true,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 1',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Enter in the price in pounds and pence without the unit, so for example if the price is &pound;10, simply enter 10.00 otherwise enter 0.')
,	array('name'=>'label_2',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 2',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_2',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 2',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_3',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 3',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_3',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 3',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_4',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 4',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_4',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 4',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_5',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 5',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_5',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 5',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'label_6',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Quantity 6',			'protected'=>false, 'regex'=>'/^[0-9]{1,7}$/',				'hint'=>'Leave blank if not applicable',	'separator'=>'before')
,	array('name'=>'value_6',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Price 6',				'protected'=>false, 'regex'=>'/^[0-9]{1,5}[.][0-9]{2}$/',	'hint'=>'Leave blank if not applicable')
,	array('name'=>'pos',			'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'i',	'label'=>'Position',			'protected'=>true,											'hint'=>'')
);


/*
	
,	'image'=>array(
		'admin'=>array('width'=>100,'height'=> 70,'format'=>'jpg','method'=>'crop')
	,	'list' =>array('width'=>200,'height'=>140,'format'=>'jpg','method'=>'crop')
	,	'gallery' =>array('width'=>400,'height'=>400,'format'=>'jpg','method'=>'resize')
	)
	,	array('name'=>'image',		'required'=>false,	'list'=>false,		'datatype'=>'varchar',	'formtype'=>'img',		'format'=>'s',		'label'=>'Photo',			'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Accepted formats: jpg, gif, png<br>Ideal image size: 330(W) x 150(H)<br>Other image sizes will be resized and cropped', 'filetype'=>'jpg,jpeg,gif,png', 'maxsize'=>'2097152', 'root'=>'products', 'groupby'=>'id')
,	array('name'=>'vat',		'required'=>true,	'list'=>false,		'datatype'=>'int',		'formtype'=>'select',	'format'=>'i',		'label'=>'VAT',				'protected'=>false,	'formset'=>'new,edit,seo',	'hint'=>'VAT rate that applies to this product',	'settings'=>'vat')
,	'vat'=>Vat::getSelectList()


*/
