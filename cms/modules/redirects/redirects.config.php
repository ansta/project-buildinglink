<?php
/* 
 * 
 * 
 * 
 * 
 * 
 * 
 */
self::$settings['redirects'] =
array(
	'orderby'=>'auto_id DESC'
,	'singular'=>'Redirect'
,	'plural'=>'Redirects'
,	'version_control'=>'audit'
,	'in_admin_menu'=>false	// hidden as it hasn't been used at all since launch, re-enable it if and when its needed
,	'allow_subpages'=>false
,	'protected'=>true
);

self::$fields['redirects'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',		'protected'=>false,	'hint'=>'')
,	array('name'=>'old_url',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Old URL',	'protected'=>false,	'hint'=>'', 'istitle'=>true)
,	array('name'=>'new_url',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'New URL',	'protected'=>false,	'hint'=>'')
);






