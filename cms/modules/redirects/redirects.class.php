<?php

class Redirects {
	
	/*
	* Look for a url match on the current one to see if this url needs redirecting to something new
	*/
	public static function check() {
		
		$slugs = explode("/", $_SERVER['REQUEST_URI']);
		$noqry = explode("?", $_SERVER['REQUEST_URI']);
	#	Page::$scheme = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on") ? "https://" : "http://";
	#	Page::$host = $_SERVER['SERVER_NAME'];
		$current_url = $noqry[0];
	#	Page::$query = (isset($noqry[1]) && $noqry[1] != '') ? '?'.$noqry[1] : '';
	#	Page::$fulluri = Page::$scheme.Page::$host.Page::$path.Page::$query;
		
		$version_control = Config::$settings['redirects']['version_control'];
		$query = "
			SELECT
				`a`.*
			FROM
				redirects a
			WHERE
				`a`.`id` > 0
			AND	`a`.`old_url` = '".db::link()->real_escape_string($_SERVER['REQUEST_URI'])."'
			AND	`a`.`new_url` != '".db::link()->real_escape_string($current_url)."'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `a`.`ttv_end` IS null";
		}
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			while ($item = $items->fetch_object()) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: ".$item->new_url."");
				exit();
			}
		}
		return;
	}
}

