<?php
/* 
 * 
 */
self::$settings['pages_common'] =
array(
	'orderby'=>'pos ASC'
,	'singular'=>'Common Pages'
,	'plural'=>'Common Pages'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
);

self::$fields['pages_common'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'ID',					'protected'=>true,	'hint'=>'')
,	array('name'=>'title',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Page Title',			'protected'=>false,	'formset'=>'new,edit,seo',	'hint'=>'This is the title of the page from an <abbr title="Search Engine Optimisation">SEO</abbr> perspective')
,	array('name'=>'label',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Navigation Title',	'protected'=>false,	'formset'=>'new,edit',		'hint'=>'How this item will appear in the website navigation', 'istitle'=>true)
,	array('name'=>'slug',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'URL Identifier',		'protected'=>false,	'formset'=>'new,edit',		'regex'=>'/^[a-z0-9\-]*$/',	'hint'=>'Identifies this page within the URL structure.<br /><strong style="color:#955;">Once set, it is strongly advised NOT to change it in the future as this can affect your <abbr title="Search Engine Optimisation">SEO</abbr> page ranking.</strong>')
,	array('name'=>'metakeys',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Keywords',			'protected'=>false,	'formset'=>'new,seo',		'hint'=>'Recommendation: no more than 5 comma separated keywords all of which <strong>MUST</strong> appear within the page content')
,	array('name'=>'metadesc',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Description',			'protected'=>false,	'formset'=>'new,seo',		'hint'=>'Recommendation: a short description of approx. 150 characters')
#,	array('name'=>'metafollow',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'yesno',	'format'=>'s',	'label'=>'Follow',				'protected'=>false,	'formset'=>'new,seo',		'hint'=>'',	'default'=>'1')
#,	array('name'=>'metaindex',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'yesno',	'format'=>'s',	'label'=>'Index',				'protected'=>false,	'formset'=>'new,seo',		'hint'=>'',	'default'=>'1')
#,	array('name'=>'metarevisit','required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'select',	'format'=>'s',	'label'=>'Revisit After',		'protected'=>false,	'formset'=>'new,seo',		'hint'=>'',	'config'=>'pages')
#,	array('name'=>'parent',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'select',	'format'=>'i',	'label'=>'Parent',				'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Specify which Parent page this page should appear')
,	array('name'=>'active',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Page is Active',		'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Allows creation of a page without publishing it immediately by selecting <strong>No</strong>.', 'default'=>true)
,	array('name'=>'nav1',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Primary Navigation',	'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Specify if it should appear in the navigation menu (Top level pages will appear in the left hand column above the video link, sub pages will appear as tabs below the banner)', 'default'=>true)
,	array('name'=>'nav2',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'hidden',	'format'=>'b',	'label'=>'Action Navigation',	'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Select yes if you want this item to appear between the Newsletter and the video link in the left hand column')
,	array('name'=>'nav3',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'hidden',	'format'=>'b',	'label'=>'Additional Navigation','protected'=>false,'formset'=>'new,edit',		'hint'=>'Select yes if you want this item to appear after the newsletter link in the left hand column')
,	array('name'=>'footer1',	'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Footer Navigation',	'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Select yes if you want this item to appear in the footer navigation menu above the grey divider line')
,	array('name'=>'footer2',	'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'hidden',	'format'=>'b',	'label'=>'Extra Footer Navigation','protected'=>false,'formset'=>'new,edit',	'hint'=>'Select yes if you want this item to appear in the footer navigation menu below the grey divider line')
,	array('name'=>'locked',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'hidden',	'format'=>'b',	'label'=>'Page is Locked',		'protected'=>true,	'formset'=>'new,edit',		'hint'=>'Prevent this page from being deleted (requires unlocking before deleting)')
,	array('name'=>'use_module',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Module Name',			'protected'=>true,	'formset'=>'new,edit',		'hint'=>'')
,	array('name'=>'pos',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'i',	'label'=>'Position',			'protected'=>true,	'formset'=>'new,edit',		'hint'=>'Relative to other pages: Small number will put item nearer to the top or left of a list, large number will put nearer to the bottom or right of a list.')
,	array('name'=>'depth',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'readonly',	'format'=>'i',	'label'=>'Page Depth',			'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Auto-generated!')
,	array('name'=>'url',		'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'readonly',	'format'=>'s',	'label'=>'Full URL',			'protected'=>false,	'formset'=>'new,edit',		'hint'=>'Auto-generated!')
);

