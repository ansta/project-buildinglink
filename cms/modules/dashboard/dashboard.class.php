<?php
/*
*
*
*
*/
class Dashboard {
	
	/*
	* Creates the list of links to modules on the Dashboard screen
	*/
	public static function modulesList() {
		foreach (Config::$settings AS $k => $v) {
			if ($v['in_admin_menu'] && $v['parent_module'] == '') {
		#		$output .= '<div class="dashbox"><a href="/admin/'.$k.'/">'.$v['plural'].'</a></div>';
		#		$output .= '<div class="dashbox"><a href="/admin/'.$k.'/"><img src="/image.php?i=/'.$k.'/images/icon-main.png" alt="'.$v['plural'].'" /></a></div>';
				
				
			}
			if ($v['in_admin_menu'] && $v['parent_module'] == '') {
		#		$output .= '<div class="dashbox"><a href="/admin/'.$k.'/">'.$v['plural'].'</a></div>';
				$output .= '<div class="dashbox"><a href="/admin/'.$k.'/"><span>'.$v['plural'].'</span></a></div>';
				
				
			}
		}
		return '<div class="tilewrapper">'.$output.'</div>';
	}
	
	/*
	 * Generates 2 levels of admin nav
	 * 1: top level nav, only modules that don't have a parent_module specified in the settings config.
	 * 2: sub level nav, only items that do have a parent_module specified and if we are currently viewing the parent.
	 *    When viewing child module, a back link appears instead and linking back to the parent module.
	 */
	public static function nav($depth = 1) {
		if ($depth == 1) {
			$output .= '<h2>CMS Navigation</h2>';
			$output .= '<ul>';
			$output .= '<li class="admin-nav-dashboard" title="dashboard"><a href="/admin/dashboard/"><img src="/images/admin/icon-dashboard.png" alt="Dashboard" class="icon" />Dashboard</a></li>';
		#	if(Page::$slug[1] == 'admin') {
				$output .= '<li class="admin-nav-frontend right" title="'.$navitem.'"><a href="/"><img src="/images/admin/icon-frontend.png" alt="Front End" class="icon" />Front End</a></li>';
		#	} else {
		#		$output .= '<li class="admin-nav-backend right" title="'.$navitem.'"><a href="/admin/"><img src="/images/admin/icon-frontend.png" alt="Admin Area" class="icon" />Admin Area</a></li>';
		#	}
			$output .= '<li class="admin-nav-logout right" title="'.$navitem.'"><a href="/admin/dashboard/?logout=true"><img src="/images/admin/icon-logout.png" alt="Logout" class="icon" />Logout</a></li>';
			$output .= '</ul>';
		}
		
		$output .= '<h2>Modules</h2>';
		$output .= '<ul>';
		foreach (Config::$settings AS $k => $v) {
			if ($v['in_admin_menu']) {
				if ($v['protected']) {
					$style_protected = ' for-super-user';
				} else {
					$style_protected = '';
				}
				if ($v['protected'] && $_SESSION['id'] > 1) {
					$show = FALSE;
				} else {
					$show = TRUE;
				}
				if ($show) {
					$permitted_admin_slug[] = $k;
					if ($depth == 2 && Page::$slug[2] == $v['parent_module']) {
						$output .= '<li class="admin-nav-'.$k.$style_protected.'" title="'.$v['plural'].'"><a href="/admin/'.$k.'/">'.$v['plural'].'</a></li>';
					
					} elseif ($depth == 2 && $v['parent_module'] != '' && Page::$slug[2] != $v['parent_module'] && Page::$slug[2] == $k) {
						$output .= '<li class="admin-nav-'.$k.$style_protected.'" title="'.$v['plural'].'"><a href="/admin/'.$v['parent_module'].'/">Back to '.Config::$settings[$v['parent_module']]['plural'].'</a></li>';
					
					} elseif ($depth == 1 && $v['parent_module'] == '') {
						$output .= '<li class="admin-nav-'.$k.$style_protected.'" title="'.$v['plural'].'"><a href="/admin/'.$k.'/"><img src="/image.php?i=/'.$k.'/images/icon-20.png" alt="'.$k.'" class="icon">'.$v['plural'].'</a></li>';
					}
				}
			}
		}
		$output .= '</ul>';
		
		return $output;
	}
	
}
	
	
	
