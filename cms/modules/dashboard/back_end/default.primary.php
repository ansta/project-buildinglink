<?php
echo '<h1>';
	echo '<img src="/images/admin/icon-dashboard.png" alt="Dashboard" class="icon">';
	echo 'Dashboard';
echo '</h1>';
?>
<section>
	<?php
	echo '<div class="tilewrapper">';
	foreach (Config::$settings AS $k => $v) {
		$super_user_class = '';
		if ($v['in_admin_menu'] && $v['parent_module'] == '') {
			$show_item = TRUE;
		} else {
			$show_item = FALSE;
		}
		if ($v['in_admin_menu'] && $v['parent_module'] == '' && $v['protected'] && $_SESSION['id'] > 1) {
			$show_item = FALSE;
		} elseif ($v['in_admin_menu'] && $v['parent_module'] == '' && $v['protected'] && $_SESSION['id'] == 1) {
			$show_item = TRUE;
			$super_user_class = ' for-super-user';
		}
		if ($show_item) {
			echo '<div class="dashbox'.$super_user_class.'">';
			echo '<a href="/admin/'.$k.'/" style="background-image:url(/image.php?i=/'.$k.'/images/icon-main.png) !important">';
			echo '<span>'.$v['plural'].'</span>';
			if (isset($v['dashboard_count'])) {
				if ($v['dashboard_count'] == 0) {
					echo '<span class="count count-zero">0</span>';
				} elseif ($v['dashboard_count'] > 9) {
					echo '<span class="count count-max">9+</span>';
				} else {
					echo '<span class="count count-num">'.$v['dashboard_count'].'</span>';
				}
			}
			echo '</a>';
			echo '</div>';
		}
	}
	echo '</div>';
	?>
</section>