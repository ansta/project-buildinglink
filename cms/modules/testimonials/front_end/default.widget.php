<?php
	
	$items = Testimonials::getNewList(Site::$sitedata['id']);
	
	if ($items->num_rows > 0) {
		while ($item = $items->fetch_object()) {
			?>
			<!-- .quote -->
			<blockquote class="quote">
			
				<div class="quote-bg"></div>
				
				<p><?php echo $item->content; ?></p>
				<span class="quote-left"></span>
				<span class="quote-right"></span>
				
			</blockquote>
			<!-- /.quote -->
			
			<!-- .by -->
			<div class="by">
				<span class="name"><?php echo $item->title; ?></span>
				<span class="job"><?php echo $item->company; ?></span>
			</div>
			<!-- /.by -->
			<?php
		}
	} else {
		Page::getElement('widget','sites','default');
	}
