<?php
/* 
 * 
 */
self::$settings['testimonials'] =
array(
	'orderby'=>'id DESC'
,	'singular'=>'Testimonial'
,	'plural'=>'Testimonials'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'attachments'=> array(
		array('name'=>'sites', 'formtype'=>'multi', 'column'=>'title', 'label'=>'Sites', 'source'=>'sites', 'hint'=>'')
	)
);

self::$fields['testimonials'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'ID',					'protected'=>true,	'hint'=>'')
#,	array('name'=>'site_id',	'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',		'label'=>'Site ID',				'protected'=>true,	'hint'=>'')
,	array('name'=>'title',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Name',				'protected'=>false,	'hint'=>'Try to keep this short',	'istitle'=>true)
,	array('name'=>'company',	'required'=>false,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',		'label'=>'Company / Job Title',	'protected'=>false,	'hint'=>'Try to keep this short')
,	array('name'=>'content',	'required'=>true,	'list'=>false,	'datatype'=>'text',		'formtype'=>'textarea',	'format'=>'s',		'label'=>'Message',				'protected'=>false,	'hint'=>'Try to keep this message under 180 characters as otherwise the last part will be cut off due to the design.<br>As a rough guide, try to keep the text on just just under 2 whole lines in this form field.')
,	array('name'=>'active',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',		'label'=>'Item is Active',		'protected'=>false,	'hint'=>'Allows creation of an item without publishing it immediately by selecting <strong>No</strong>.', 'default'=>true)
);

