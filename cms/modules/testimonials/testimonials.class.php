<?php

class Testimonials {
	
	/*
	* Get a random testimonial
	*/
	public static function getList($site_id = 0) {
		$version_control = Config::$settings['testimonials']['version_control'];
		$query = "
			SELECT
				*
			FROM
				testimonials b
			WHERE
				`b`.`id` > 0
			AND	`b`.`active` = 1
		";
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
	#	if (isset(Config::$settings['testimonials']['filter_by_site']) && Config::$settings['testimonials']['filter_by_site'] == true) {
	#		$query .= " AND site_id = ".(int)db::link()->real_escape_string(Site::$sitedata['id']);
	#	}
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `b`.`ttv_end` IS null";
		}
		$query .= " ORDER BY RAND()";
		$query .= " LIMIT 1";
	#	echo '<!-- DEBUG '.$query.' -->';
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			// Some records are found, so return them
			return $items;
		}
		// Otherwise give up
		return NULL;
	}
	
	/*
	* Get a random testimonial for this particular WEBSITE ID
	*/
	public static function getNewList($site_id = 0) {
		$version_control = Config::$settings['testimonials']['version_control'];
		$query = "
			SELECT
				t.*
			FROM
				testimonials t
			LEFT JOIN
				attachments x
				ON x.module_name = 'testimonials'
				AND x.module_id = t.id
				AND x.attachment_name = 'sites'
				AND x.attachment_id = ".(int)db::link()->real_escape_string($site_id)."
    			AND x.ttv_end IS null
			WHERE
				t.active = 1
			AND x.auto_id IS NOT null
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `t`.`ttv_end` IS null";
		}
		$query .= " ORDER BY RAND()";
		$query .= " LIMIT 1";
	#	echo '<!-- DEBUG '.$query.' -->';
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			// Some records are found, so return them
			return $items;
		}
		// Otherwise give up
		return NULL;
	}
	
	

}
