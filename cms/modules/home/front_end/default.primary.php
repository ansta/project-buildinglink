<?php
$items = Shop_categories::getFeaturedList();
$i = 0;
if ($items->num_rows > 0) {
	echo '
		<!-- .product-columns -->
		<div class="product-columns">
	';
	while ($item = $items->fetch_object()) {
		
		// Prepare the item image
		if (file_exists(SYSTEM_ROOT.'modules/shop_categories/uploads/image/'.$item->id.'/small/'.$item->image)) {
			$item_image_small = '/image.php?i=/shop_categories/uploads/image/'.$item->id.'/small/'.$item->image.'';
			$item_image_large = '/image.php?i=/shop_categories/uploads/image/'.$item->id.'/large/'.$item->image.'';
		} else {
			$item_image_small = '/images/no-image-small.jpg" alt="'.$item->title.'';
			$item_image_large = '/images/no-image-small.jpg" alt="'.$item->title.'';
		}
		
		// Prepare the item url
		$item_url = str_replace("//", "/", Page::getModuleUrl('shop_categories').$item->url);
			
		$i++;
		if ($i <= 3) {
			if ($i == 1) {
				echo '
					<!-- .row -->
					<div class="row">
				';
			}
			echo '
				<!-- .column three -->
				<div class="column three">
					<!-- .info-box -->
					<div class="info-box">
						<div class="thumb">
							<img src="'.$item_image_large.'" alt="'.$item->title.'" />
							<div class="overlay">
								<h3>'.$item->title.'</h3>
								<div class="btn-wrap"><a href="'.$item_url.'" class="btn">View</a></div>
							</div>
						</div>
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.column three -->
			';
			if ($i == 3 || $i == $items->num_rows) {
				echo '
					</div>
					<!-- /.row -->
				';
			}
		} else {	
			if ($i == 4) {
				echo '
					<!-- .row -->
					<div class="row">
				';
			}
			echo '
				<!-- .column four -->
				<div class="column four">
					<!-- .info-box -->
					<div class="info-box alt">
						<div class="thumb">
							<img src="'.$item_image_small.'" alt="Rulers" />
							<div class="overlay">
								<h3>'.$item->title.'</h3>
								<div class="sub-heading"><a href="'.$item->url.'"> '.$item->tagline.' <span class="box-icon"><i class="icon-angle-right"></i></span></a></div>
							</div>
						</div>
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.column four -->
			';
			if ($i == 7 || $i == $items->num_rows) {
				echo '
					</div>
					<!-- /.row -->
				';
			}
		}
	}
	echo '
		</div>
		<!-- /.product-columns -->
	';
}

?>
<?php /*?>
<!-- .product-columns -->
<div class="product-columns">
	
	<!-- .row -->
	<div class="row">
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box">
				<div class="thumb">
					<img src="/images/thumb-ruler.jpg" alt="Rulers" />
					<div class="overlay">
						<h3>Quotation Request</h3>
						<div class="btn-wrap"><a href="#" class="btn">Get Quote</a></div>
					</div>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box">
				<div class="thumb">
					<img src="/images/thumb-stopwatch.jpg" alt="Rulers" />
					<div class="overlay">
						<h3>Express 5 Day Service</h3>
						<div class="btn-wrap"><a href="#" class="btn">More info</a></div>
					</div>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
		
		<!-- .column three -->
		<div class="column three">
			<!-- .info-box -->
			<div class="info-box">
				<div class="thumb">
					<img src="/images/thumb-artwork.jpg" alt="Rulers" />
					<div class="overlay">
						<h3>Arwork Guidelines</h3>
						<div class="btn-wrap"><a href="#" class="btn">More info</a></div>
					</div>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column three -->
	
	</div>
	<!-- /.row -->
	
	<!-- .row -->
	<div class="row">
	
		<!-- .column four -->
		<div class="column four">
			<!-- .info-box -->
			<div class="info-box alt">
				<div class="thumb">
					<img src="/images/thumb-ruler-1.jpg" alt="Rulers" />
					<div class="overlay">
						<h3>Quotation Request</h3>
						<div class="sub-heading"><a href="#"> Perfect for direct email <span class="box-icon"><i class="icon-angle-right"></i></span></a></div>
					</div>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column four -->
		
		<!-- .column four -->
		<div class="column four">
			<!-- .info-box -->
			<div class="info-box alt">
				<div class="thumb">
					<img src="/images/thumb-ruler-2.jpg" alt="Rulers" />
					<div class="overlay">
						<h3>Express 5 Day Service</h3>
						<div class="sub-heading"><a href="#">0.77mm &amp; 1mm <span class="box-icon"><i class="icon-angle-right"></i></span></a></div>
					</div>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column four -->
		
		<!-- .column four -->
		<div class="column four">
			<!-- .info-box -->
			<div class="info-box alt">
				<div class="thumb">
					<img src="/images/thumb-ruler-3.jpg" alt="Rulers" />
					<div class="overlay">
						<h3>Arwork Guidelines</h3>
						<div class="sub-heading"><a href="#">Strong &amp; rugged <span class="box-icon"><i class="icon-angle-right"></i></span></a></div>
					</div>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column four -->
		
		<!-- .column four -->
		<div class="column four">
			<!-- .info-box -->
			<div class="info-box alt">
				<div class="thumb">
					<img src="/images/thumb-ruler-4.jpg" alt="Rulers" />
					<div class="overlay">
						<h3>Arwork Guidelines</h3>
						<div class="sub-heading"><a href="#">Various Scales <span class="box-icon"><i class="icon-angle-right"></i></span></a></div>
					</div>
				</div>
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.column four -->
	
	</div>
	<!-- /.row -->

</div>
<!-- /.product-columns -->
<?php */?>
<!-- .page-content -->
<div class="page-content clearfix">

	<!-- .content-inner -->
	<div class="content-inner">
		
		<!-- .entry-content -->
		<div class="entry-content">
			<?php
			$zone_1 = Content::getZoneContent(Page::$id[0],1);
			if (strlen($zone_1) > 0) {
				echo ''.$zone_1.'';
			}
			?>
		</div>
		<!-- /.entry-content -->
		
	</div>
	<!-- /.content-inner -->
	
	<?php Page::getElement('widget','blogs','home'); ?>
	
</div>
<!-- /.page-content -->











<?php #Page::getElement('SubNav','default','default'); ?>
<?php /*?>
<div class="inner-main">
	<?php #Page::getElement('beforeModule'); ?>
	
	<?php #Page::getElement('afterModule'); ?>
</div>
<?php */?>
<?php /*?>
<div class="inner-side">
	<?php
	$zone_6 = Content::getZoneContent(Page::$id[0],6);
	if (strlen($zone_6) > 0) {
		echo '<div class="minibox first-minibox box-wysiwyg clearfix"><div class="inner">'.$zone_6.'</div></div>';
	}
	?>
	<?php
	$zone_7 = Content::getZoneContent(Page::$id[0],7);
	if (strlen($zone_7) > 0) {
		echo '<div class="minibox box-wysiwyg clearfix"><div class="inner">'.$zone_7.'</div></div>';
	}
	?>
	<?php
	$zone_8 = Content::getZoneContent(Page::$id[0],8);
	if (strlen($zone_8) > 0) {
		echo '<div class="minibox box-wysiwyg clearfix"><div class="inner">'.$zone_8.'</div></div>';
	}
	?>
</div>
<?php */?>