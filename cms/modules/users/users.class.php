<?php

class Users {

	private static 	$filter_query = '';
	public static	$user_status_list = array(
												1 => 'invited',
												2 => 'accepted',
												3 => 'active',
												4 => 'disabled'
											);
		
	
	
	
	/*
	* 
	*/
	public static function userExists($email) {
		$checkemail_query = "
			SELECT
				*
			FROM
				`users`
			WHERE
				`email` = '".db::link()->real_escape_string($email)."'
			AND
				`ttv_end` IS null
		";
	#	die($checkemail_query);
		$useremail = db::link()->query($checkemail_query);
		if ($useremail->num_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/*
	* 
	*/
	public static function checkNewUserHasChangedPassword() {
		$checkpw_query = "
			SELECT
				*
			FROM
				`users`
			WHERE
				`id` = ".(int)db::link()->real_escape_string($_SESSION['id'])."
			AND `modify_action` LIKE '%password%'
		";
		$userpw = db::link()->query($checkpw_query);
		if ($userpw->num_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	
	/*
	* 
	*/
	public static function clinicalSupportUserArray() {
		
		$version_control_u = Config::$settings['users']['version_control'];
#		echo 'u:'.$version_control_u;
		$version_control_o = Config::$settings['organisations_users']['version_control'];
#		echo 'o:'.$version_control_o;
		$orderby = Config::$settings['users']['orderby'];
		$query = "
			SELECT
				u.*
			,	o.*
			FROM
				`users` u
			LEFT JOIN
				`organisations_users` o ON o.`user_id` = u.`id`
			WHERE
				u.`user_type` = 8
				/* ".$version_control_u." */
				AND u.`ttv_start` < NOW()
				AND u.`ttv_end` IS NULL
		";
		if($version_control_u == 'audit' || $version_control_u == 'full') {
			$query .= "
				AND u.`ttv_start` < NOW()
				AND u.`ttv_end` IS NULL
			";
		}
		if($version_control_o == 'audit' || $version_control_o == 'full') {
			$query .= "
				AND o.`ttv_start` < NOW()
				AND o.`ttv_end` IS NULL
			";
		}
		
		$query .= " AND o.`organisation_id` IN (".$_SESSION['orgs'].")";
		
#		$query .= " ORDER BY ".$orderby;
#		echo $query;
		$csus = db::link()->query($query);
		if ($csus->num_rows) {
		#	 $csus->fetch_object();
			 return $csus;
		#	while ($csu = $csus->fetch_object()) {
			#	$backup_user_array = Users::clinicalSupportUserArray();
			#	Config::$settings['users']['backup_user_id'][$csu->id] = $csu->salutation.' '.$csu->firstname.' '.$csu->lastname;

			#	array_push(Config::$settings['users']['backup_user_id'],);
			#	$output[$csu->id] = $csu->salutation.' '.$csu->firstname.' '.$csu->lastname;
		#	}
	#		var_dump($output);
	#		return $output;
		}

	#	return array(
	#		1=>'a',
	#		2=>'b'
	#	);
	}
	
	

	/*
	* 
	*/
	public static function bulkInvite() {
		$emails = $_REQUEST['emails'];
		$emails = str_replace ( "\r\n" , "\n", $emails );
		$email_array = explode("\n", $emails);
		$org_id = array('org_id'=>$_SESSION['orgs']);
		$user_type = 4;
		foreach ($email_array AS $key => $invite_email) {
			$invite_email = trim($invite_email);
			if (strlen($invite_email) > 6 && !self::checkIfUserExists($invite_email)) {
				self::processInvitation($org_id,$user_type,$invite_email);
				$_SESSION['form']['form-success'] = $_SESSION['form']['form-success'].'<br />Invitation sent to: <strong>'.$invite_email.'</strong>';
			}
		}
		$result['query_status'] == 'OK';
		$result['redirect'] = true;
		return $result;
	}
	
	/*
	* 
	*/
	public static function checkIfUserExists($email) {
		$query = "
			SELECT
				*
			FROM 
				`users`
			WHERE
				`ttv_start` <= NOW()
			AND `ttv_end` IS NULL
			AND `email` = ".db::link()->real_escape_string($email)."
		";
		$results = db::link()->query($query);
		if ($results->num_rows > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	/*
	* 
	*/
	public static function processInvitation($data,$in_user_type = '',$in_email = '') {
	#	$type = 'insert'
	#	$existing_user_id = 0
				
	#	return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);
				
#		$password = self::createRandomString(8);
#		$password_hash = Security::encrypt($password,'md5','uehdhwe8w9w89sd78udhdew89qw9sisdjsdjx7xdysgwb1vswyw8d8dhsbshs');

		if ($data['user'] == 'update') {
			$type = 'update';
		} else {
			$type = 'insert';
		}
		
		if ($in_user_type == '') {
			$chosen_user_type = $_REQUEST['user_type'];
		} else {
			$chosen_user_type = $in_user_type;
		}
		
		if ($in_email == '') {
			$chosen_email = $_REQUEST['email'];
		} else {
			$chosen_email = $in_email;
		}
		
		
		
		$result['query_status'] = 'OK';
		
		
		$password = User::makePassword(); # then choose from either $password['plain'] or $password['hash']
		
		# Create the first query
		# Create a new user record and fill it in with the little data that has been provided
		if ($type == 'insert') {
			$result['query1'] = "
				INSERT INTO `users`
					(
						`id`,
						`email`,
						`password`,
						`user_type`,
						`organisation_id`,
						`ttv_start`,
						`ttv_end`,
						`create_by`,
						`create_id`,
						`create_action`
					) VALUES (
						0,
						'".db::link()->real_escape_string($chosen_email)."',
						'".db::link()->real_escape_string($password['hash'])."',
						".(int)db::link()->real_escape_string($chosen_user_type).",
						".(int)db::link()->real_escape_string($data['org_id']).",
						NULL,
						NULL,
						'".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
						".(int)db::link()->real_escape_string($_SESSION['id']).",
						'invite'
					)
			";
			# Providing that the query is not blank, execute the query
			if ($result['query1'] != '') {
				if(db::link()->query($result['query1'])) {
					$r1 = db::link()->query("SELECT LAST_INSERT_ID() AS insert_id");
					$r1_last_id = mysqli_fetch_assoc($r1);
				} else {
					$result['query_status'] = 'ERROR';
					$result['query_err_msg'] = db::link()->error;
					die('error1: '.db::link()->error.' - '.$result['query1']);
				}
			}
			
			
			# Create the second query
			# Update the newly created user record and add in the auto_id value into the id column
			if ($r1_last_id['insert_id'] > 0) {
				$result['query2'] = "
					UPDATE 
						`users`
					SET
						`id` = ".(int)db::link()->real_escape_string($r1_last_id['insert_id']).",
						`ttv_start` = NOW()
					WHERE
						`ttv_start` IS NULL
					AND `ttv_end` IS NULL
					AND `auto_id` = ".(int)db::link()->real_escape_string($r1_last_id['insert_id'])."
				";
				
				# Providing that the query is not blank, execute the query
				if ($result['query2'] != '') {
					if(db::link()->query($result['query2'])) {
					#	echo $result['query2'];
					} else {
						$result['query_status'] = 'ERROR';
						$result['query_err_msg'] = db::link()->error;
						die('error2: '.db::link()->error.' - '.$result['query2']);
					}
				}
			} else {
				$result['query_status'] = 'ERROR';
			}
		}
		
		# Create the third query
		# If this user is to be associated with an organisation, then add that link in
		if ($type == 'update') {
			$invite_user_id = $data['user_id'];
		} else {
			$invite_user_id = $r1_last_id['insert_id'];
		}

#		$_SESSION['debugg']['org_id'] = 'org_id'.$data['org_id'];
#		$_SESSION['debugg']['invite_user_id'] = 'invite_user_id'.$invite_user_id;

		$ut = $chosen_user_type;
		if ($data['org_id'] && $invite_user_id > 0) {
			$result['query3'] = "
				INSERT INTO `organisations_users`
					(
						`id`,
						`user_id`,
						`organisation_id`,
						`status`,
						`ttv_start`,
						`ttv_end`,
						`create_by`,
						`create_id`,
						`create_action`
					) VALUES (
						0,
						".(int)db::link()->real_escape_string($invite_user_id).",
						".(int)db::link()->real_escape_string($data['org_id']).",
						1,
						NULL,
						NULL,
						'".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
						".(int)db::link()->real_escape_string($_SESSION['id']).",
						'invite'
					)
			";
			
			# Providing that the query is not blank, execute the query
			if ($result['query3'] != '') {
				if(db::link()->query($result['query3'])) {
					$r3 = db::link()->query("SELECT LAST_INSERT_ID() AS insert_id");
					$r3_last_id = mysqli_fetch_assoc($r3);
				} else {
					$result['query_status'] = 'ERROR';
					$result['query_err_msg'] = db::link()->error;
					die('error3: '.db::link()->error.' - '.$result['query3']);
				}
			}
		
			if ($r3_last_id['insert_id'] > 0) {
				# Create the fourth query
				# Update the newly created organisations_users record and add in the auto_id into the id column
				$result['query4'] = "
					UPDATE 
						`organisations_users`
					SET
						`id` = ".(int)db::link()->real_escape_string($r3_last_id['insert_id']).",
						`ttv_start` = NOW()
					WHERE
						`ttv_start` IS NULL
					AND `ttv_end` IS NULL
					AND `auto_id` = ".(int)db::link()->real_escape_string($r3_last_id['insert_id'])."
				";
				
				# Providing that the query is not blank, execute the query
				if ($result['query4'] != '') {
					if(db::link()->query($result['query4'])) {
					#	echo $result['query2'];
					} else {
						$result['query_status'] = 'ERROR';
						$result['query_err_msg'] = db::link()->error;
						die('error4: '.db::link()->error.' - '.$result['query4']);
					}
				}
			} else {
				$result['query_status'] = 'ERROR';
			}
		}
		
		
#		$_SESSION['debugg']['query3'] = 'query3'.$result['query3'];
#		$_SESSION['debugg']['query4'] = 'query4'.$result['query4'];

		
		if ($result['query_status'] == 'OK') {
			
			# Create the fifth query
			# Providing that all went ok, we need to send an email to the newly invited person
			# and need to get the human readable user_type
			$result['query5'] = "
				SELECT
					*
				FROM 
					`user_types`
				WHERE
					`ttv_start` <= NOW()
				AND `ttv_end` IS NULL
				AND `id` = ".(int)db::link()->real_escape_string($chosen_user_type)."
			";
			
			$utypes = db::link()->query($result['query5']);
			$user_type = ' --undefined-- ';
			if ($utypes->num_rows) {
				while( $utype = $utypes->fetch_object() ) {
					$user_type = $utype->title;
				}
			}
			
			if ($type == 'insert') {
				$html  = '<p>You have been invited to join our network as a <strong>'.$user_type.'</strong>.</p>
				<p>You may now login using your email address as your username: '.$chosen_email.'<br>
				Your password will arrive shortly in a seperate email</p>
				<p>For all enquiries, please contact us via the website.</p>';
				
				$html2  = '<p>You have been invited to join our network as a <strong>'.$user_type.'</strong>.</p>
				<p>You may now login using your username that has been emailed to you seperately in combination with the following password: '.$password['plain'].'</p>
				<p>For all enquiries, please contact us via the website.</p>';
				
			} elseif ($type == 'update') {
				$html  = '<p>You have been invited to join our network as a <strong>'.$user_type.'</strong>.</p>
				<p>Please use your existing username and password to gain access</p>
				
				<p>For all enquiries, please contact us via the website.</p>';
				
			}
			if ($html != '' && $chosen_email != '') {
				Email::sendEmail($chosen_email,'Invitation to join as a '.$user_type,$html);
				
				if ($html2 != '' && $chosen_email != '') {
					Email::sendEmail($chosen_email,'Invitation to join as a '.$user_type,$html2);
				}
				
			} else {
				$_SESSION['form']['form-error'] = 'Invitation process was a partial success, unfortunately the system was unable to send an email to the user.';
				$result['query_status'] = 'ERROR';
			}
			
			$result['redirect'] = true;
		}
		
		return $result;
		
		
	}
	
	
	/*
	* Check to make sure this user can invite the requested type of user
	*/
	public static function canInviteThisType($type_to_check) {
		$version_control = Config::$settings['user_types']['version_control'];
		$query = "
			SELECT
				*
			FROM
				`user_types`
			WHERE
				`can_invite` LIKE '%".db::link()->real_escape_string($_SESSION['user_type'])."%'
			AND	id = ".db::link()->real_escape_string($type_to_check)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$items = db::link()->query($query);
		if ($items->num_rows) {
			return TRUE;
		}
		return FALSE;
	}
	
	
	/*
	* Returns an object containing the list of user_types that a user can invite others to become
	*/
	public static function getPermittedInviteList() {
		$version_control = Config::$settings['user_types']['version_control'];
		$orderby = Config::$settings['user_types']['orderby'];
		$query = "
			SELECT
				*
			FROM
				`user_types`
			WHERE
				`can_invite` LIKE '%".db::link()->real_escape_string($_SESSION['user_type'])."%'
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$query .= " ORDER BY ".$orderby;
	#	echo $query;
		if ($uts = db::link()->query($query)) {
			return $uts;
		}
		return;
	}
	
	
	/*
	* Returns an object containing the list of organisations that a user can invite others to
	*/
	public static function getPermittedOrgsList() {
		$version_control = Config::$settings['organisations']['version_control'];
		$orderby = Config::$settings['organisations']['orderby'];
		$query = "
			SELECT
				*
			FROM
				`organisations`
			WHERE
				`id` IN (".db::link()->real_escape_string($_SESSION['orgs']).")
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				AND `ttv_start` <= NOW()
				AND `ttv_end` IS NULL
			";
		}
		$query .= " ORDER BY ".$orderby;
		if ($orgs = db::link()->query($query)) {
			return $orgs;
		}
		return;
	}
	
	
	/*
	* Check to make sure this user can invite the requested type of user
	*/
	public static function getCuList() {
		$orderby = Config::$settings['users']['orderby'];
		
		$query = "
			SELECT
				u.*
			FROM
				`users` u
			LEFT JOIN
				organisations_users o ON u.`id` = o.`user_id`
		";
		
	}
	
	
	/*
	* 
	*/
	public static function applyFilters() {
		
		if (array_key_exists($_REQUEST['user_status'], self::$user_status_list)) {
			$filter_status = $_REQUEST['user_status'];
		}
		
		if ($filter_status == 1) {
			self::$filter_query .= " AND u.`tnc` IS null";
			self::$filter_query .= " AND u.`modify_action` IS null";
		}
		if ($filter_status == 2) {
			self::$filter_query .= " AND u.`tnc` IS NOT null";
			self::$filter_query .= " AND u.`firstname` IS null";
		}
		if ($filter_status == 3) {
			self::$filter_query .= " AND u.`tnc` IS NOT null";
			self::$filter_query .= " AND u.`firstname` IS NOT null";
		}
		if ($filter_status == 4) {
	#		self::$filter_query .= " AND `modify_action` = 'delete'";
		}
		
		if (isset($_REQUEST['filter_user_type']) && $_REQUEST['filter_user_type'] > 0) {
			self::$filter_query .= " AND user_type = ".(int)db::link()->real_escape_string($_REQUEST['filter_user_type'])."";
		}
		
		if (isset($_REQUEST['name']) && $_REQUEST['name'] != '') {
			self::$filter_query .= " AND (";
			self::$filter_query .= "    LCASE(firstname) LIKE '%".db::link()->real_escape_string(strtolower($_REQUEST['name']))."%'";
			self::$filter_query .= " OR LCASE(lastname) LIKE '%".db::link()->real_escape_string(strtolower($_REQUEST['name']))."%'";
			self::$filter_query .= " OR LCASE(email) LIKE '%".db::link()->real_escape_string(strtolower($_REQUEST['name']))."%'";
			self::$filter_query .= ")";
		}
		
	}
	
	
	/*
	* Check to make sure this user can invite the requested type of user
	*/
	public static function getList() {
	#	$version_control = Config::$settings['user_types']['version_control'];
		$orderby = Config::$settings['users']['orderby'];
		
		if (isset($_SESSION['orgs'])) {
			$org_specific = TRUE;
		#	$pop_orgs = explode(',',$_SESSION['orgs']);
		#	$org_count = count($pop_orgs);
		#	if ($org_count > 1) {
		#		return;
		#	}
		} else {
			$org_specific = FALSE;
		}
		
		// If user is not TM or TA and ORGS is not set then bail out as they should not see the full list
		if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
			if (!$org_specific) {
				return; # If not an admin, to view the list of users you must have 1 or more orgs in $_SESSION['orgs']
			}
		}
		
		// Start with a basic query of users
		$query = "
			SELECT
				u.*
			FROM
				`users` u
		";
		// If this user has an org, ie: user is org specific
		if ($org_specific) {
			$query .= "
				LEFT JOIN
					organisations_users o ON u.`id` = o.`user_id` AND o.`ttv_end` IS NULL 
			";
		}
		
	#	if($version_control == 'audit' || $version_control == 'full') {
		$query .= "	WHERE ";
		if ($_REQUEST['user_status'] == 4) {
			if ($org_specific && $_REQUEST['user_status'] != 4) {
				if ($_SESSION['user_type'] == 3) {
			#		$query .= "
			#			o.`status` = 3
			#		";
			#		$query .= "
			#			o.`status` = 3
			#		";
				}
			} else {
				if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
					$query .= "
							u.`ttv_end` IS NOT NULL
						AND u.`modify_action` = 'delete'
					";
				} elseif ($_SESSION['user_type'] == 3) {
					$query .= "
						((
							u.`user_type` = 4
						AND	u.`ttv_end` IS NOT NULL
						AND u.`modify_action` = 'delete'
						)
						OR
						(
							u.`user_type` != 4
						AND	u.`ttv_end` IS NULL
						AND o.`status` = 3
						))
					";
				}
			}
#			$query .= "(
#						u.`ttv_end` IS NOT NULL
#					AND	o.`ttv_end` IS NOT NULL
#					AND (
#							u.`modify_action` = 'delete'
#						OR	o.`status` = 3
#						)
#					AND u.`id` NOT IN
#						(
#						SELECT `id` FROM `users` c WHERE c.`id` = u.`id` AND c.`ttv_end` is NULL
#						)
#					)
#			";
		} else {
			$query .= "
					u.`ttv_start` <= NOW()
				AND u.`ttv_end` IS NULL
			";
			if ($org_specific) {
				$query .= "
					AND o.`ttv_start` <= NOW()
					AND o.`ttv_end` IS NULL
					AND (
							o.`status` = 1
						OR	o.`status` = 2
						)
				";
			}
		}
		$query .= "";
	#	}
		
		if (self::$filter_query != '') {
			$query .= self::$filter_query;
		}
		
		if (isset($_SESSION['orgs'])) {
			$query .= " AND o.organisation_id IN (".$_SESSION['orgs'].")";
		}
		
		$query .= " GROUP BY u.`id`";
		
		if ($orderby != '') {
			$query .= ' ORDER BY '.$orderby;
		}
		
		$query .= Paging::$limit;
		
	#	echo $query;
		
		// Run the query
		$items = db::link()->query($query);
		if ($items->num_rows) {
			return $items;
		}
		return;
	}
	
	
	/*
	* Check to make sure this user can invite the requested type of user
	*/
	public static function getStatus($item) {
		/*
		// If user has not accepted TnCs then return a status of invited
		if ($item->tnc == 0) {
			return 1;
		}
		
		// If user has not completed their personal profile then return a status of TnCs accepted
		if ($item->firstname == '') {
			return 2;
		}
		
		if ($item->user_type == 1 || $item->user_type == 2) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		} elseif ($item->user_type == 3) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		} elseif ($item->user_type == 4) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		} elseif ($item->user_type == 5) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		} elseif ($item->user_type == 6) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		} elseif ($item->user_type == 7) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		} elseif ($item->user_type == 8) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		} elseif ($item->user_type == 9) {
			if ($item->modify_action == 'delete') {
				return 4;
			} else {
				return 3;
			}
			
		}
		*/
		
		$status = 'invited';
		if ($item->tnc == 0) {
			# no change
		} else {
			$status = 'accepted';
			if ($item->firstname == '') {
				# no change
			} else {
				$status = 'active';
				
				if ($item->modify_action == 'delete') {
					$status = 'disabled';
				}
				if ($item->user_type == 7) {
					$query = "
						SELECT
							o.*
						FROM
							`organisations_users` o
						WHERE
							o.`user_id` = ".$item->id."
						AND o.`organisation_id` IN (".$_SESSION['orgs'].")
						AND	o.`ttv_start` <= NOW()
						AND o.`ttv_end` IS NULL
					";
					// Run the query
					$items = db::link()->query($query);
					if ($items->num_rows) {
						$ou = $items->fetch_object();
						if ($ou->status == 3) {
							$status = 'unlinked';
						}
					}
				}
			}
		}
		
		return $status;
	}
	
	
	/*
	* Check to make sure this user can invite the requested type of user
	*/
	public static function listTools($item,$status) {
			
		$output .= '<ul class="menu">';
		
		if ($status == 'unlinked') {
			$output .= '	<li><a href="'.Page::$slug[0].'activate/'.$item->id.'/"><span>Re-activate</span></a></li>';
			
		} else {
			if ($item->modify_action != 'delete') {
				$output .= '	<li><a href="'.Page::$slug[0].'disable/'.$item->id.'/"><span>Disable</span></a></li>';
			}
	/*		if ($item->tnc != 1) {
				$output .= '	<li><a href="'.Page::$slug[0].'invite/'.$item->id.'/"><span>Re-send invite</span></a></li>';
			}
	*/		if ($item->modify_action == 'delete') {
				$output .= '	<li><a href="'.Page::$slug[0].'activate/'.$item->id.'/"><span>Re-activate</span></a></li>';
			}
			#	$output .= '	<li><a href="#"><span>Send message</span></a></li>';

		}
		
		$output .= '</ul>';
		
		return $output;
	}
	
	
	/*
	* Check to make sure this user can invite the requested type of user
	*/
	public static function getItem($item_id,$type = 'active') {
		$version_control = Config::$settings['users']['version_control'];
		$orderby = Config::$settings['users']['orderby'];
		$query = "
			SELECT
				*
			FROM
				`users`
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				WHERE
				(";
				if ($type == 'active' || $type == 'both') {
					$query .= "
					(`ttv_start` <= NOW()
					AND `ttv_end` IS NULL)";
				}
				if ($type == 'both') {
					$query .= " OR ";
				}
				if ($type == 'deleted' || $type == 'both') {
					$query .= "
					(`ttv_end` IS NOT NULL
					AND `modify_action` = 'delete')";
				}
			$query .= "
				)
			";
		}
		$query .= ' AND id = '.db::link()->real_escape_string($item_id);
	#	$query .= ' AND status = 0';
		
		if (self::$filter_query != '') {
		#	$query .= self::$filter_query;
		}
		
		if ($_SESSION['id'] == 1) {
	#		echo $query;
		}
			
		$items = db::link()->query($query);
		if ($items->num_rows) {
			return $items;
		}
		return;
	}
	
	
	/*
	* Looks up a user by email address
	*/
	public static function checkByEmail($email) {

		$version_control = Config::$settings['users']['version_control'];
		$orderby = Config::$settings['users']['orderby'];
		$query = "
			SELECT
				*
			FROM
				`users`
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				WHERE
				(
					`ttv_start` <= NOW()
					AND `ttv_end` IS NULL
				)
			";
		}
		$query .= " AND `email` = '".db::link()->real_escape_string($email)."'";
		$user_lookup = db::link()->query($query);
	#			die($query);
		if ($user_lookup->num_rows) {
			$user = $user_lookup->fetch_assoc();
			return $user;
		}
		return FALSE;
	}
	
	
	/*
	* Creates a blank new organisation and returns its ID
	* This is used by TMs (user_type: 1) to directly invite new CU's (user_type: 3), but CU's have to be attached to an organisation 
	*/
	public static function createNewOrg() {
		$query1 = "
			INSERT INTO
				`organisations`
			(	id
			,	ttv_start
			,	ttv_end
			,	create_by
			,	create_id
			,	create_action
			)
			VALUES
			(	0
			,	NULL
			,	NULL
			,	'".db::link()->real_escape_string($_SESSION['firstname'].' '.$_SESSION['lastname'])."'
			,	".(int)db::link()->real_escape_string($_SESSION['id'])."
			,	'insert'
			)
		";

		if(db::link()->query($query1)) {
			$r1_last_id = db::link()->insert_id; 
			if ($r1_last_id > 0) {
				$query2 = "
					UPDATE
						`organisations`
					SET
						id = auto_id
					,	ttv_start = NOW()
					WHERE
						auto_id = ".(int)db::link()->real_escape_string($r1_last_id)."
					AND ttv_start IS NULL
					AND id = 0
				";
				if(db::link()->query($query2)) {
					return $r1_last_id;
				}
			}
		}
		return FALSE;
	}
	
	
	/*
	* Check to see if this user is already in the chosen organisation
	*/
	public static function isValidAdminEmail($email) {
		if (preg_match('/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@](ansta.co.uk|themoleclinic.co.uk)$/', $email)) {
			return TRUE;
		}
		return FALSE;
	}
	/*
	* This is used in invite.action.php to validate the submitted invitation
	* If the validation passes without error, this function will also return some
	* extra pieces of data that may be useful for the invitation process.
	*/
	public static function invitationLogic() {
		
		$st		= $_SESSION['user_type'];	// This is the user_type of the user making the invitation
		$rt		= $_REQUEST['user_type'];	// This is the future user_type of the new invited person
		$email	= $_REQUEST['email'];		// This is the email of the new invited user
		$org_id	= $_REQUEST['organisation_id']; // This is the org_id that the new user will be assigned to if they get invited
		
		// Check if the invited person already exists or not
		if ($user = self::checkByEmail($email)) {
			$user_exists = TRUE;			// They DO exist already
			$ut = $user['user_type'];		// This is the current user_type of the existing invited person
			$user_id = $user['id'];			// This is the current user_id of the existing invited person
		} else {
			$user_exists = FALSE;			// They DO NOT exist already
		}
		
	#	var_dump($st.' - '.$rt.' - '.$email.' - '.$org_id.' - '.$user_exists.' - '.$ut.' - '.$user_id);
	#	die();
		
		$already_exists_no_reinvite = 'This email address is already registered on the system and cannot be invited again.';
		$invalid_org_id				= 'There appears to be a problem with your organisation identification on the system, please contact support.';
		$already_in_this_org		= 'This email address appears to already be registered to this organisation so there is no point in inviting them again.';
		$not_permitted_to_invite	= 'You don\'t appear to be permitted to invite the selected type of user';
		$is_another_type_invite_not_permitted = 'This user is already registered on the system but as a different type of user than the one that you have chosen.';
		$invalid_user_type			= 'Please choose a User Type';
		$invalid_admin_email		= 'Admin invitations an only be sent to emails ending in @themoleclinic.co.uk';
		
		if ($st == 1)
		{
			if ($rt == 1) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					if (self::isValidAdminEmail($email)) {
						return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
					} else {
						$_SESSION['form']['field-error']['email'] = $invalid_admin_email;
						return FALSE;	// Error
					}
				}
			} elseif ($rt == 2) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					if (self::isValidAdminEmail($email)) {
						return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
					} else {
						$_SESSION['form']['field-error']['email'] = $invalid_admin_email;
						return FALSE;	// Error
					}
				}
			} elseif ($rt == 3) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					if (self::isValidAdminEmail($email)) {
						if ($org_id = self::createNewOrg()) {
							return array('user'=>'insert','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>FALSE);	// Invite
						} else {
							$_SESSION['form']['field-error']['email'] = 'system error: unable to create an organisation';
							return FALSE;	// Error
						}
					} else {
						$_SESSION['form']['field-error']['email'] = $invalid_admin_email;
						return FALSE;	// Error
					}
				}
			} elseif ($rt == 5) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			} elseif ($rt == 6) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			} elseif ($rt == 9) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		
		elseif ($st == 2)
		{
			if ($rt == 9) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		
		elseif ($st == 3)
		{
			if ($rt == 3 || $rt == 4) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					if ($org_id > 0) {
						return array('user'=>'insert','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>FALSE);	// Invite
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				}
			} elseif ($rt == 7) {
				if ($user_exists) {
					if ($org_id > 0) {
						if ($ut == 7) {
							if (self::isAlreadyInOrg($user_id,$org_id)) {
								$_SESSION['form']['field-error']['email'] = $already_in_this_org;
								return FALSE;	// Error
							} else {
								return array('user'=>'update','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>$user_id);	// Invite
							}
						} else {
							$_SESSION['form']['field-error']['email'] = $is_another_type_invite_not_permitted;
							return FALSE;	// Error
						}
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				} else {
					if ($org_id > 0) {
						return array('user'=>'insert','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>FALSE);	// Invite
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				}
			}
		}
		
		elseif ($st == 4) {
			if ($rt == 7) {
				if ($user_exists) {
					if ($org_id > 0) {
						if ($ut == 7) {
							if (self::isAlreadyInOrg($user_id,$org_id)) {
								$_SESSION['form']['field-error']['email'] = $already_in_this_org;
								return FALSE;	// Error
							} else {
								return array('user'=>'update','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>$user_id);	// Invite
							}
						} else {
							$_SESSION['form']['field-error']['email'] = $is_another_type_invite_not_permitted;
							return FALSE;	// Error
						}
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				} else {
					if ($org_id > 0) {
						return array('user'=>'insert','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>FALSE);	// Invite
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				}
			} elseif ($rt == 8) {
				if ($user_exists) {
					if ($org_id > 0) {
						if ($ut == 8) {
							if (self::isAlreadyInOrg($user_id,$org_id)) {
								$_SESSION['form']['field-error']['email'] = $already_in_this_org;
								return FALSE;	// Error
							} else {
								return array('user'=>'update','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>$user_id);	// Invite
							}
						} else {
							$_SESSION['form']['field-error']['email'] = $is_another_type_invite_not_permitted;
							return FALSE;	// Error
						}
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				} else {
					if ($org_id > 0) {
						return array('user'=>'insert','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>FALSE);	// Invite
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				}
			} elseif ($rt == 9) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		
		elseif ($st == 5)
		{
			if ($rt == 9) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		
		elseif ($st == 6)
		{
			if ($rt == 9) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		
		elseif ($st == 7)
		{
			if ($rt == 7) {
				if ($user_exists) {
					if ($org_id > 0) {
						if ($ut == 7) {
							if (self::isAlreadyInOrg($user_id,$org_id)) {
								$_SESSION['form']['field-error']['email'] = $already_in_this_org;
								return FALSE;	// Error
							} else {
								return array('user'=>'update','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>$user_id);	// Invite
							}
						} else {
							$_SESSION['form']['field-error']['email'] = $is_another_type_invite_not_permitted;
							return FALSE;	// Error
						}
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				} else {
					if ($org_id > 0) {
						return array('user'=>'insert','user_type'=>$rt,'org_id'=>$org_id,'user_id'=>FALSE);	// Invite
					} else {
						$_SESSION['form']['field-error']['organisation_id'] = $invalid_org_id;
						return FALSE;	// Error
					}
				}
			} elseif ($rt == 9) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		
		elseif ($st == 8)
		{
			if ($rt == 4) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		
		elseif ($st == 9)
		{
			if ($rt == 9) {
				if ($user_exists) {
					$_SESSION['form']['field-error']['email'] = $already_exists_no_reinvite;
					return FALSE;	// Error
				} else {
					return array('user'=>'insert','user_type'=>$rt,'org_id'=>FALSE,'user_id'=>FALSE);	// Invite
				}
			}
		}
		if ($rt >= 1 && $rt <= 9) {
			$_SESSION['form']['field-error']['user_type'] = $not_permitted_to_invite;
		} else {
			$_SESSION['form']['field-error']['user_type'] = $invalid_user_type;
		}
		return FALSE;
	}
	
	
	/*
	* 
	*/
	public static function reLink() {
		
		$st		= $_SESSION['user_type'];	// This is the user_type of the user making the invitation
		$rt		= $_REQUEST['user_type'];	// This is the future user_type of the new invited person
		$email	= $_REQUEST['email'];		// This is the email of the new invited user
		$org_id	= $_REQUEST['organisation_id']; // This is the org_id that the new user will be assigned to if they get invited
		
	$_SESSION['relink_debug'] = '<hr>st: '.$st.'<hr>rt: '.$rt.'<hr>email: '.$email.'<hr>org_id: '.$org_id;
	
		$search_query = "
			SELECT
				*
			FROM 
				`organisations_users`
			WHERE
				`ttv_start` <= NOW()
			AND `ttv_end` IS NULL
			AND `user_id` = ".(int)db::link()->real_escape_string($user_id)."
			AND `organisation_id` = ".(int)db::link()->real_escape_string($org_id)."
		";
	$_SESSION['relink_debug'] .= '<hr>: '.$search_query;
		$search_result = db::link()->query($search_query);
		if ($search_result->num_rows) {
			$r = $search_result->fetch_object();
			
	$_SESSION['relink_debug'] .= '<hr>r->id: '.$r->id;
	
			$item_array = array(
				'module' => 'organisations_users',
				'action' => 'edit',
				'id' => $r->id,
			);
			// Add a login attemt record to the database
			Form::setAuditAction('RELINK');
			Form::processData('organisations_users',$item_array);
			return TRUE;
		}
		return FALSE;
	}
	
	/*
	* Check to see if this user is already in the chosen organisation
	*/
	public static function isAlreadyInOrg($user_id,$org_id) {
		$query = "
			SELECT
				*
			FROM 
				`organisations_users`
			WHERE
				`ttv_start` <= NOW()
			AND `ttv_end` IS NULL
			AND `user_id` = ".(int)db::link()->real_escape_string($user_id)."
			AND `organisation_id` = ".(int)db::link()->real_escape_string($org_id)."
		";
		$user_org_lookup = db::link()->query($query);
		if ($user_org_lookup->num_rows) {
			return TRUE;
		}
		return FALSE;
	}
	
	
	
	
	
	/*
	* Check to make sure this user can invite the requested type of user
	*/
	public static function processReactivate() {
		$data = $_REQUEST;
		
		$result['query_status'] = 'OK';
		
		# Ensure that the module and action requested are valid
		$module = Security::checkModuleList($data['module']);
		$action = Security::checkActionList($data['action']);
		
		// Make sure that we use the correct ID column depending on whether or not this module has version control or not
		$version_control = Config::$settings[$module]['version_control'];
		if($version_control == 'audit' || $version_control == 'full') {
			$item_id = $data['id'];
			$has_version = TRUE;
		} else {
			if (in_array('auto_id', Config::$fields[$module])) {
				$item_id = $data['auto_id'];
				$has_version = FALSE;
			} else {
				$item_id = $data['id'];
				$has_version = FALSE;
			}
		}
		
		# Determin what action is requested
#		$result['action_type'] = 'update';
#		if (Form::getAuditAction() == '') {
#			Form::setAuditAction('update');
#		}
		
		if ($has_version) {
			
			
			# Create the first query
			# This is to get the auto_id of the record that we are going to copy and then re-activate
			$query1 = "
				SELECT
					*
				FROM
					`".$module."`
				WHERE
					`ttv_end` IS NOT NULL
				AND	`modify_action` = 'delete'
				AND `id` = ".(int)db::link()->real_escape_string($item_id)."
			";
			if ($results1 = db::link()->query($query1)) {
				$r1 = $results1->fetch_assoc();
				if ($r1['firstname'] != '') {
					$_SESSION['form']['form-item'] = $r1['firstname'].' '.$r1['lastname'];
				} else {
					$_SESSION['form']['form-item'] = $r1['email'];
				}
				if (DEBUG_MODE > 0) {$_SESSION['processes'] .= ' 1 '; $_SESSION['qry1'] = $query1; }
			} else {
				if (DEBUG_MODE > 0) {$_SESSION['err1'] = 'Query 1 failed: '.$query1;}
				$result['query_status'] == 'ERROR';
			}
			
			
			# From the list of fields in the config...
			foreach (Config::$fields[$module] as $key => $value) {
				if ($value['name'] == 'auto_id') {
					# For all columns listed in this IF, don't auto populate the query
				} else {
					# Otherwise create an array of all the fields to copy over from the old record to the new one
					$insert2_columni[$value['name']] = $key;
					$insert2_columns[$value['name']] = $key;
				}
			}
			# Create the second query
			# Copies the current data and inserts it into a new row
			$query2 = "
				INSERT INTO
					`".$module."`
					(
						`".implode("`,`", array_keys($insert2_columni))."`
					)
				SELECT
					`".implode("`,`", array_keys($insert2_columns))."`
				FROM
					`".$module."` d
				WHERE
					`d`.`auto_id` = ".$r1['auto_id']."
			";
			if ($results2 = db::link()->query($query2)) {
				$r2 = db::link()->query("SELECT LAST_INSERT_ID() AS insert_id");
				$last_id = mysqli_fetch_assoc($r2);
				if (DEBUG_MODE > 0) {
					$_SESSION['processes'] .= ' 2 ';
					$_SESSION['processes'] .= ' (lid: '.(int)$last_id['insert_id'].') ';
					$_SESSION['qry2'] = $query2;
				}
			} else {
				if (DEBUG_MODE > 0) {
					$_SESSION['err2'] = 'Query 2 failed: '.$query2;
				}
				$result['query_status'] == 'ERROR';
			}
			
		
			# Create the third query
			# To enter the new data into the new record
			$query3 = "
				UPDATE 
					`".$module."`
				SET
					`comments`	= '".db::link()->real_escape_string($_REQUEST['comments'])."',
					`ttv_start` = NOW(),
					`create_by` = '".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
					`create_id` = ".(int)db::link()->real_escape_string($_SESSION['id']).",
					`create_action` = 'activate'
				WHERE
					`ttv_start` IS NULL
				AND `ttv_end` IS NULL
				AND `id` = ".(int)db::link()->real_escape_string($item_id)."
			";
			if ($results3 = db::link()->query($query3)) {
				if (DEBUG_MODE > 0) {
					$_SESSION['processes'] .= ' 3 ';
					$_SESSION['qry3'] = $query3;
				}
			} else {
				if (DEBUG_MODE > 0) {
					$_SESSION['err3'] = 'Query 3 failed: '.$query4;
				}
				$result['query_status'] == 'ERROR';
			}
			
			
		} else {
			# Create the query
			$query0 = "
				UPDATE
					".$module."
				SET
					".implode(", ", $update_colvals).",
					`modify_by` = '".db::link()->real_escape_string($_SESSION['firstname']).' '.db::link()->real_escape_string($_SESSION['lastname'])."',
					`modify_id` = ".(int)db::link()->real_escape_string($_SESSION['id']).",
					`modify_action` = '".db::link()->real_escape_string(self::getAuditAction())."'
				WHERE
					
			";
			if (in_array('auto_id', Config::$fields[$module])) {
				$query0 .= "`auto_id` = ".(int)db::link()->real_escape_string($item_id);
			} else {
				$query0 .= "`id` = ".(int)db::link()->real_escape_string($item_id);
			}
		}

		
		
		# Providing that the query is not blank, execute the query
		if ($query0 != '') {
			if(db::link()->query($query0)) {
		#		die('success0: '.$result['query0'].' - '.db::link()->affected_rows);
			} else {
				$result['query_status'] = 'ERROR';
				$result['query_err_msg'] = db::link()->error;
				if (DEBUG_MODE > 0) {
					die('error0: '.db::link()->error.' - '.$query0);
				}
			}
		}
		
	
		# If the process went OK and NEXT was set to CONTINUE EDITING then redirect back to the edit page
		if ($result['query_status'] == 'OK' && $data['next'] == 1) {
			$result['redirect'] = true;
			$result['destination'] = Page::$slug[0];
			$_SESSION['next'] = 1;
			
		# If the process went OK and NEXT was not set to CONTINUE EDITING then redirect back to the listing page
		} elseif ($result['query_status'] == 'OK') {
			if (isset($data['next']) && $data['next'] == 0) {
				$_SESSION['next'] = 0;
			}
			$result['redirect'] = true;
			if (Page::$slug[1] == ADMIN) {
				$result['item_id'] = $item_id;
				if ($result['action_type'] == 'update') {
					$result['destination'] = '../../?done';
				} else {
					$result['destination'] = '../?done';
				}
			} else {
				$result['destination'] = Page::$slug[0];
			}
			
		# otherwise redirect back to the edit page
		} else {
			$result['redirect'] = false;
			$result['destination'] = '';
		}
		
		return $result;
	}
	
	
	
	
	
	/*
	* Password Reset functionality
	*/
	public static function processPasswordReset() {
		$email = $_REQUEST['email'];
		
		// Look for an active user with the email address supplied
		$query = "
			SELECT
				*
			FROM
				`users`
			WHERE
				`ttv_start` <= NOW()
			AND `ttv_end` IS NULL
			AND `email` = '".db::link()->real_escape_string($email)."'
		";
		$results = db::link()->query($query);
		// If a user is found...
		if ($results->num_rows) {
			$user = $results->fetch_object();
			
			$password = User::createRandomString(8);
			
			// Prepare new password and associated data for processing
			$item_array = array(
				'module'	=> 'users',
				'action'	=> 'edit',
				'id'		=> $user->id,
				'password'	=> $password,
			);
			
			Form::setAuditAction('password reset');
			$output = Form::processData('users',$item_array);
			
			$html  = '<p>Dear '.$user->firstname.' '.$user->lastname.',</p>
<p>Your temporary password to access the TELEDerm System is <strong>'.$password.'</strong>.</p>
<p>This password will expire after '. TEMP_PASSWORD_LIFE .' hours and therefore must be changed within that period.</p>
<p>If you require any assistance, please contact TELEDerm Support on https://telederm.freshdesk.com</p>
<p>Kind regards,</p>
<p>The TELEDerm Support Team</p>
<p>The MOLE Clinic Ltd, London
Approved NHS Business Partner
Regulated by the Care Quality Commission</p>
			';
			
			if ($html != '') {
				Email::sendEmail($email,'Password reset',$html);
			}
			
			return $output;
		}
		$result['query_status'] = 'OK';
		$result['redirect'] = true;
		return $result;
		
	}
	
	
	/*
	* 
	*/
	public static function getOrgUserId($item_id,$org_id) {

		$version_control = Config::$settings['organisations_users']['version_control'];
		$query = "
			SELECT
				*
			FROM
				`organisations_users`
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "
				WHERE
				(
					`ttv_start` <= NOW()
					AND `ttv_end` IS NULL
				)
			";
		}
		$query .= " AND `user_id` = ".(int)db::link()->real_escape_string($item_id)."";
		$query .= " AND `organisation_id` = ".(int)db::link()->real_escape_string($org_id)."";
		$item_lookup = db::link()->query($query);
		if ($item_lookup->num_rows) {
			$item = $item_lookup->fetch_object();
			return $item->id;
		}
		return FALSE;
	}
	
	
	/*
	* 
	*/
	public static function getPasswordExpiryDate() {
		$query1 = "
			SELECT
				CASE
					WHEN `create_action` = 'password change' THEN ttv_start + INTERVAL ".USER_PASSWORD_LIFE." HOUR
					WHEN `create_action` = 'password reset' THEN ttv_start + INTERVAL ".TEMP_PASSWORD_LIFE." HOUR
				END AS ttv_start
			FROM
				`users`
			WHERE
				( `create_action` = 'password change' OR `create_action` = 'password reset' )
			AND `id` = ".(int)db::link()->real_escape_string($_SESSION['id'])."
			ORDER BY auto_id DESC
			LIMIT 1
		";
		
		$item_lookup1 = db::link()->query($query1);
		
		if ($item_lookup1->num_rows > 0) {
			$item = $item_lookup1->fetch_object();
			return $item->ttv_start;
		} else {
			$query2 = "
				SELECT
					ttv_start + INTERVAL ".TEMP_PASSWORD_LIFE." HOUR AS ttv_start
				FROM
					`users`
				WHERE
					(
						`create_action` = 'password reset'
					OR
						`create_action` = 'invite'
					)
				AND `id` = ".(int)db::link()->real_escape_string($_SESSION['id'])."
				ORDER BY auto_id DESC
				LIMIT 1
			";
			$item_lookup2 = db::link()->query($query2);
			if ($item_lookup2->num_rows) {
				$item = $item_lookup2->fetch_object();
				return $item->ttv_start;
			}
		}
		return FALSE;
	}
	
	
	/*
	* 
	*/
	public static function getCsuList() {
		$query = "
			SELECT
				u.id
			,	u.salutation
			,	u.firstname
			,	u.lastname
			,	u.job
			FROM
				users u
			LEFT JOIN
				organisations_users o ON o.user_id = u.id AND o.ttv_end IS null
			WHERE
				u.ttv_end IS null
			AND u.user_type = 8
			AND o.organisation_id = ".(int)db::link()->real_escape_string($_SESSION['orgs'])."
			ORDER BY
				u.lastname ASC
		";
		$csus = db::link()->query($query);
		$output[0] = '-- None --';
		if ($csus->num_rows > 0) {
			while ($csu = $csus->fetch_object()) {
				$output[$csu->id] = $csu->salutation.' '.$csu->firstname.' '.$csu->lastname.' - '.$csu->job;
			}
		}
		return $output;
	}
	
	/*
	* 
	
	public static function profileIsComplete() {
		$query = "
			SELECT
				firstname
			,	lastname
			FROM
				`users`
			WHERE
				`ttv_end` IS NULL
			AND `id` = ".(int)db::link()->real_escape_string($_SESSION['id'])."
		";
	#	echo $query;
		$item_lookup = db::link()->query($query);
		if ($item_lookup->num_rows) {
			$item = $item_lookup->fetch_object();
			if (trim($item->firstname) != '') {
				return TRUE;
			}
		}
		return FALSE;
	}
	*/
}

















































