<?php
/* 
 * Users holds the login details of all admins
 * Renaming this table to admins might be a good idea and would leave room to use
 * the name for other purposes such as registered users and members
 * Alternatively, all users, both admins and members could use the same table although
 * the downsite of this would be that there would be less flexibility when it comes to
 * what columns could be used for either situation
 * This table must always contain at minimum 1 record or ID = 1 in order for Super Users to be able to login
 * Should this "special" entry be deleted, the system should automatically re-create it again (currently this is not implemented)
 */
self::$settings['users'] =
array(
	'orderby'=>'firstname ASC, lastname ASC, email ASC'
,	'singular'=>'User'
,	'plural'=>'Users'
,	'version_control'=>'audit'
,	'in_admin_menu'=>true
,	'disable_autocomplete'=>true
,	'type'=>array(
		0=>'-- please select the appropriate account type --'
	,	2=>'Member: will only be able to access member material'
	,	1=>'Admin: will be able to access member material AND access the admin area'
	)
);
/*	,	'field_name'=>ClassName::FunctionName() allows for dynamic array construction */

self::$fields['users'] =
array(
	array('name'=>'id',			'required'=>false,	'list'=>false,	'datatype'=>'int',		'formtype'=>'hidden',	'format'=>'i',	'label'=>'User ID',		'protected'=>false,		'hint'=>'')
,	array('name'=>'firstname',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'First Name',	'protected'=>false,		'hint'=>'', 'istitle'=>true)
,	array('name'=>'lastname',	'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Last Name',	'protected'=>false,		'hint'=>'', 'istitle'=>true)
,	array('name'=>'email',		'required'=>true,	'list'=>true,	'datatype'=>'varchar',	'formtype'=>'text',		'format'=>'s',	'label'=>'Email',		'protected'=>false,		'hint'=>'', 'regex'=>'/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/')
,	array('name'=>'password',	'required'=>false,	'list'=>false,	'datatype'=>'varchar',	'formtype'=>'password',	'format'=>'s',	'label'=>'Password',	'protected'=>false,		'hint'=>'Please use a minimum of 8 characters containing a mix of letters and numbers', 'autocomplete'=>'false')
,	array('name'=>'type',		'required'=>true,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'select',	'format'=>'i',	'label'=>'User Type',	'protected'=>false, 	'hint'=>'This user has access to this admin area', 'settings'=>'users')
,	array('name'=>'locked',		'required'=>false,	'list'=>false,	'datatype'=>'boolean',	'formtype'=>'yesno',	'format'=>'b',	'label'=>'Locked',		'protected'=>true, 		'default'=>false,	'hint'=>'Prevent this user from being deleted (requires unlocking before deleting)')
);

/*

CREATE TABLE users (
  `auto_id` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NOT NULL DEFAULT 0,
  
  `firstname` VARCHAR(35) DEFAULT NULL,
  `lastname` VARCHAR(35) DEFAULT NULL,
  `email` VARCHAR(128) DEFAULT NULL,
  `password` VARCHAR(128) DEFAULT NULL,
  `type` INT(11) NOT NULL DEFAULT 0,
  `locked` int(1) NOT NULL DEFAULT '0',
  
  `ttv_start` TIMESTAMP NULL,
  `ttv_end` TIMESTAMP NULL,
  `create_by` VARCHAR(70) DEFAULT NULL,
  `create_id` INT(11) NOT NULL DEFAULT 0,
  `create_action` VARCHAR(32) DEFAULT NULL,
  `modify_by` VARCHAR(70) DEFAULT NULL,
  `modify_id` INT(11) NOT NULL DEFAULT 0,
  `modify_action` VARCHAR(32) DEFAULT NULL,
  PRIMARY KEY (auto_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_bin;


INSERT INTO `users` (`auto_id`, `id`, `firstname`, `lastname`, `email`, `password`, `type`, `locked`, `ttv_start`, `ttv_end`, `create_by`, `create_id`, `create_action`, `modify_by`, `modify_id`, `modify_action`) VALUES
(1, 1, 'Ansta', 'SuperUser', 'ask@ansta.co.uk', '8dheh38d78ehyu3hr889fyerg3u849r8ry3hr8f9yue4h3ur9f89fr7y4g48r9f84h4hejof0f94une4jfo9', 1, 0, NOW(), null, 'install', 1, 'install', null, 0, null),
(2, 2, 'Loki', 'Admin', 'loki.ansta@gmail.com', '0028600289a93454944e10de4b86ada1', 1, 0, NOW(), null, 'install', 1, 'install', null, 0, null)



CREATE TABLE IF NOT EXISTS `logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `user_hash` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `user_type` varchar(16) COLLATE utf8_bin NOT NULL,
  `user_ip` varchar(16) COLLATE utf8_bin NOT NULL,
  `lastaction` timestamp NULL DEFAULT NULL,
  `firstaction` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`user_hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;



REGEX:

To apply a regex pattern to a field, add the following element:
	'regex'=>'xxx'

Replace xxx with a pattern such as below:

 - Any email address:
	/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/
	
 - Limited extention email address:
	version 1:	/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]*(pixacms.com|pixxacms.com)$/
	version 2:	/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@]([A-z0-9_]+pixacms.com|pixxacms.com)$/
		
 - Price:
	/^[0-9]{1,5}[.][0-9]{2}$/
	
 - Number between 0 and 99
	/^[0-9]{1,2}$/

*/