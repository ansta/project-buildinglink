<?php

$submission = TRUE;

$_REQUEST['type'] = 2;

// Generic data validation
foreach (Config::$fields['users'] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}
// Password specific validation
if ($_REQUEST['password'] == '') {
	$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
	$_SESSION['form']['field-error']['password'] = 'This field is mandatory.';
	$submission = Error::storeMsg($something_needs_to_go_here);
}
if ($_REQUEST['password_confirm'] == '') {
	$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
	$_SESSION['form']['field-error']['password_confirm'] = 'This field is mandatory.';
	$submission = Error::storeMsg($something_needs_to_go_here);
}
if ($_REQUEST['password'] != $_REQUEST['password_confirm']) {
	$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
	$_SESSION['form']['field-error']['password_confirm'] = 'Your password fields don\'t match, please again.';
	$submission = Error::storeMsg($something_needs_to_go_here);
}
// Email (aka Username) specific validation
if (Users::userExists($_REQUEST['email'])) {
	$_SESSION['form']['form-error'] = 'Please check the form below for errors and try again.';
	$_SESSION['form']['field-error']['email'] = 'The email address that you are trying to use is already registered on the system.';
	$submission = Error::storeMsg($something_needs_to_go_here);
	#echo'exists';
}
#die($_REQUEST['email']);

if ($submission) {
	$result = Form::processData();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'Your registration is now complete, please go ahead and <a href="'.Page::$slug[0].'">sign in now</a> with your username and password.';
		header("Location: ".Page::$slug[0]);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}
