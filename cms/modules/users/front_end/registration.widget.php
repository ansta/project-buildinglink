<?php
echo '<a name="commentform"></a>';
echo Page::showErrors();
?>
<form action="<?php echo Page::$slug[0]; ?>?registration#commentform" method="post">
	<fieldset>
		<legend>Registration Form</legend>
<?php	Form::setErrorMessagePosition('inline');
		echo Form::makeField(array('formtype'=>'text','name'=>'firstname','label'=>'First name','required'=>true));
		echo Form::makeField(array('formtype'=>'text','name'=>'lastname','label'=>'Last name','required'=>true));
		echo Form::makeField(array('formtype'=>'text','name'=>'email','label'=>'Email address','required'=>true,'hint'=>'Your email address will never be displayed.'));
		echo Form::makeField(array('formtype'=>'password','name'=>'password','label'=>'Password','required'=>true));
		echo Form::makeField(array('formtype'=>'password','name'=>'password_confirm','label'=>'Confirm Password','required'=>true));

		echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'new'));
		echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>'users'));
		echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Send'));
?>	</fieldset>
</form>
<p><a href="<?php echo Page::$slug[0]; ?>">return to login page</a>.</p>