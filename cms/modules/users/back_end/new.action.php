<?php

$submission = TRUE;

foreach (Config::$fields[Page::$slug[2]] as $key => $field) {
	if (!Form::validate($field)) {
		$_SESSION['form']['field-error'][$field['name']] = 'This field is mandatory';
		$_SESSION['form']['form-error'] = 'Your form submission contains error, please check your data and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
	if ($field['formtype'] == 'password' && $_REQUEST[$field['name']] == '') {
		$_SESSION['form']['field-error'][$field['name']] = 'Please enter a password for this user';
		$_SESSION['form']['form-error'] = 'Your form submission contains error, please check your data and try again.';
		$submission = Error::storeMsg($something_needs_to_go_here);
	}
}

if ($submission) {
	
	Security::checkTableList(Page::$slug[2]);
	Security::checkModuleList($_REQUEST['module']);
	Security::checkActionList($_REQUEST['action']);
	
	$result = Form::processData();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'New item has been created.';
		header("Location: ".$result['destination']);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}
