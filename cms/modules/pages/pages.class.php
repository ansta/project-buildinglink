<?php

class Pages {

	/*
	*
	*	public static 	$id = 0,
					$title = '',
					$url = '',
					$newwin = '';
	*/
	
	
	public static  function getChildren($table,$id=0) {
		$version_control = Config::$settings['pages']['version_control'];
		$query = "
			SELECT
				id
			,	label
			,	depth
			FROM
				pages
			WHERE
				parent = ".db::link()->real_escape_string($id)."
			AND	slug != ''
			AND	slug != '".ADMIN."'
		";
		
		// When working with multiple sites, some modules need to display filtered results depending on the current active site ID
		if (isset(Config::$settings['pages']['filter_by_site']) && Config::$settings['pages']['filter_by_site'] == true) {
			$query .= " AND site_id = ".(int)db::link()->real_escape_string($_SESSION['active_site_id']);
		}
		
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS NULL";
		}
		$query .= " ORDER BY `pos` ASC";
		$pages = db::link()->query($query);
		if ($pages->num_rows) {
			while( $page = $pages->fetch_object() ) {
				if ($page->depth == 1) {
					$prefix = '';
				} elseif ($page->depth == 2) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 3) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 4) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 5) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} elseif ($page->depth == 6) {
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|_';
				} else {
					$prefix = 'depth missing! - ';
				}
				Form::setOption('parent',$page->id,$prefix.$page->label);
				self::getChildren($table,$page->id);
			}
		}
	}
	
	
	/*
	* Allows us to look up the POS number of a given PAGE
	*/
	public static  function getPosFromParentId($item_id = 0) {
		$version_control = Config::$settings['pages']['version_control'];
		$query = "
			SELECT
				pos
			FROM
				pages
			WHERE
				id = ".db::link()->real_escape_string($item_id)."
		";
		if($version_control == 'audit' || $version_control == 'full') {
			$query .= "	AND `ttv_end` IS NULL";
		}
		$results = db::link()->query($query);
		if ($results->num_rows > 0) {
			if($result = $results->fetch_object()) {
				return $result->pos;
			}
		}
		return 0;
	}
	
	
	/*
	* Create default pages based on the ID of the new site that has been created in SITES table
	* This function is used by Sites::createDefaultWebsiteContent();
	*/
	public static  function createDefaultPages($site_id) {
		
		$module_name = 'pages';
		$i = 1;
		
		// Create Home page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Home'
		,	'label'=>'Home'
		,	'slug'=>''
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>1
		,	'footer1'=>0
		,	'locked'=>1
		,	'use_module'=>'home'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/'
		);
		$result['home'] = Form::processData($module_name,$data);
		$result['home']['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		// Create About us page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'About us'
		,	'label'=>'About us'
		,	'slug'=>'about-us'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>1
		,	'footer1'=>0
		,	'locked'=>0
		,	'use_module'=>''
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/about-us/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		// Create Artwork page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Artwork'
		,	'label'=>'Artwork'
		,	'slug'=>'artwork'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>1
		,	'footer1'=>0
		,	'locked'=>0
		,	'use_module'=>''
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/artwork/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		// Create How to Order page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'How to Order'
		,	'label'=>'How to Order'
		,	'slug'=>'how-to-order'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>1
		,	'footer1'=>0
		,	'locked'=>0
		,	'use_module'=>''
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/how-to-order/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		
		// Create Blog page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Blog'
		,	'label'=>'Blog'
		,	'slug'=>'blog'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>1
		,	'footer1'=>0
		,	'locked'=>1
		,	'use_module'=>'blogs'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/blog/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		// Create Get a Quote page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Get a Quote'
		,	'label'=>'Get a Quote'
		,	'slug'=>'get-a-quote'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>1
		,	'footer1'=>0
		,	'locked'=>0
		,	'use_module'=>'contacts'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/get-a-quote/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		
		// Create Shop Category listing page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Category'
		,	'label'=>'Category'
		,	'slug'=>'category'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>0
		,	'footer1'=>0
		,	'locked'=>1
		,	'use_module'=>'shop_categories'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/category/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		
		// Create Shop Product view page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Product'
		,	'label'=>'Product'
		,	'slug'=>'product'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>0
		,	'footer1'=>0
		,	'locked'=>1
		,	'use_module'=>'products'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/product/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		
		// Create Shop Basket view page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Basket'
		,	'label'=>'Basket'
		,	'slug'=>'basket'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>0
		,	'footer1'=>0
		,	'locked'=>1
		,	'use_module'=>'baskets'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/basket/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		
		// Create Shipping view page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Shipping'
		,	'label'=>'Shipping'
		,	'slug'=>'shipping'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>0
		,	'footer1'=>0
		,	'locked'=>1
		,	'use_module'=>'shippings'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/shipping/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		
		// Create Shop Order page
		$data = array (
			'action'=>'new'
		,	'module'=>$module_name
		,	'site_id'=>$site_id
		,	'title'=>'Your order'
		,	'label'=>'Your order'
		,	'slug'=>'order'
		,	'parent'=>0
		,	'active'=>true
		,	'nav1'=>0
		,	'footer1'=>0
		,	'locked'=>1
		,	'use_module'=>'orders'
		,	'pos'=>$i++
		,	'depth'=>'1'
		,	'url'=>'/order/'
		);
		$result[$data['slug']] = Form::processData($module_name,$data);
		$result[$data['slug']]['data'] = $data;
		if ((int)$result[$data['slug']]['item_id'] > 0) {
			$result[$data['slug']]['success'] = TRUE;
		} else {
			$result[$data['slug']]['success'] = FALSE;
		}
		
		return $result;
	}
	
}












