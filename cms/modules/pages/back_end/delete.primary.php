<h1><?php echo '<a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>'; ?>
Delete <?php echo Config::$settings[Page::$slug[2]]['singular']; ?>: <?php echo Form::getItemTitle(); ?></h1>

<section>
	<?php
		if (isset($_POST['submit'])) {
			if ($submission) {
			#	echo "<br>A match was found.";
			} else {
				echo '<p style="color:#900;padding:10px 20px;border:1px solid #900;background-color:#FCC;display:block;text-align:center;">Please fill in all required fields marked with a *</p>';
				unset($_SESSION['error']);
			}
		}
	?>	
	<form action="" method="post">
		<fieldset>
			<legend></legend>
			<p>Are you sure you want to delete <strong><?php echo Form::getItemTitle(); ?></strong>?</p>
			<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'action','value'=>'delete')); ?>
			<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'module','value'=>Module::$name)); ?>
			<?php echo Form::makeField(array('formtype'=>'hidden','name'=>'id')); ?>
			<?php echo Form::makeField(array('formtype'=>'submit','name'=>'submit','value'=>'Yes, Delete Now!')); ?>
			<p class="btn-cancel"><a href="../../"><span>Cancel</span></a></p>
		</fieldset>
	</form>
</section>