<?php
Admin::allowButton('new',1);
Admin::allowButton('edit',1);
Admin::allowButton('delete',1);
Admin::applyMaxItems();

if ($_SESSION['id'] == 1) {
	Admin::allowButton('seo',0);
} else {
	Admin::allowButton('seo',0);
}

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	Core::setModuleTask('default'); // Optional to specify default as it would be default anyway!
	
} elseif (Page::$slug[3] == 'new' && Page::$slug[4] == '') {
	Core::setModuleTask('new');
#	Form::prefillValues();
	
} elseif (Page::$slug[3] == 'new' && Page::$slug[4] > 0) {
	Core::setModuleTask('new');
	Form::$values['parent'] = Page::$slug[4];
#	Form::prefillValues();
	
} elseif (Page::$slug[3] == 'edit' && Page::$slug[4] > 0) {
	Core::setModuleTask('edit');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'seo' && Page::$slug[4] > 0) {
	Core::setModuleTask('seo');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} elseif (Page::$slug[3] == 'delete' && Page::$slug[4] > 0) {
	Core::setModuleTask('delete');
	if (!Form::prefillValues()) {
		Error::type(404); // If values not found for the requested record then 404
	}
	
} else {
	Error::type(404);
}
