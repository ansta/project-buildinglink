<?php
echo '<h1>';
	echo '<img src="/image.php?i=/'.Page::$slug[2].'/images/icon-20.png" alt="'.Config::$settings[Page::$slug[2]]['plural'].'" class="icon">';
	echo Config::$settings[Page::$slug[2]]['plural'];
echo '</h1>';
?>
<section>
	
	<?php
	if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-error'].'</p>';
		unset($_SESSION['form']['form-error']);
	}
	if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
		unset($_SESSION['form']['form-success']);
	}
	?>
	
	<div class="left-50">
		<h2>Products</h2><a href="import/products/">Import</a> | <a href="/download_csv.php?file=products">Export</a>
	</div>
	
	<div class="right-50">
		<h2>Variants (i.e: Prices)</h2> <a href="import/products_variants/">Import</a> | <a href="/download_csv.php?file=products_variants">Export</a>
	</div>
	
	<div class="left-50">
		<?php
		$items = Sites::getList();
		if ($items->num_rows > 0) {
			echo '<h2>Websites</h2>';
			echo '<table style="">';
				echo '<tr>';
					echo '<th>';
						echo 'ID';
					echo '</th>';
					echo '<th>';
						echo 'Name';
					echo '</th>';
				echo '</tr>';
			while ($item = $items->fetch_object()) {
				echo '<tr>';
					echo '<td>';
						echo $item->id;
					echo '</td>';
					echo '<td>';
						echo $item->title;
					echo '</td>';
				echo '</tr>';
			}
			echo '</table>';
		}
		?>
	</div>
	
	<div class="right-50">
		<?php
		$items = Shop_categories::getListForDataAdminPage(1);
		if ($items->num_rows > 0) {
			echo '<h2>Product Categories</h2>';
			echo '<table style="">';
				echo '<tr>';
					echo '<th>';
						echo 'ID';
					echo '</th>';
					echo '<th>';
						echo 'Name';
					echo '</th>';
				echo '</tr>';
			while ($item = $items->fetch_object()) {
				echo '<tr>';
					echo '<td>';
						echo $item->id;
					echo '</td>';
					echo '<td>';
						echo $item->title;
					echo '</td>';
				echo '</tr>';
				
				$subs = Shop_categories::getListForDataAdminPage(2,$item->id);
				if ($subs->num_rows > 0) {
					while ($sub = $subs->fetch_object()) {
						echo '<tr>';
							echo '<td>';
								echo $sub->id;
							echo '</td>';
							echo '<td>';
								echo '&nbsp;&nbsp;-&nbsp;'.$sub->title;
							echo '</td>';
						echo '</tr>';
					}
				}
				
			}
			echo '</table>';
		}
		?>
	</div>
</section>
