<?php

Admin::allowNew(0);
Admin::allowEdit(0);
Admin::allowDelete(0);
Admin::allowView(0);

if (Page::$slug[2] != '' && Page::$slug[3] == '') {
	Core::setModuleTask('default'); // Optional to specify default as it would be default anyway!

} elseif (Page::$slug[3] == 'import' && Page::$slug[4] == ('products' || 'products_variants')) {
	Core::setModuleTask('import');
	
} elseif (Page::$slug[3] == 'export' && Page::$slug[4] == ('products' || 'products_variants')) {
	Core::setModuleTask('export');
	
} else {
	Error::type(404);
}



// Setting the page title
if (Page::$slug[4] == 'products') {
	Data::$title = 'Products';
	
} elseif (Page::$slug[4] == 'products_variants') {
	Data::$title = 'Product Variants';
	
} else {
	Data::$title = '<em>title not set in controller</em>';
	
}