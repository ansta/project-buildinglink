<?php

$submission = TRUE;

if ($_FILES["file"]["size"] < 10) {
	$_SESSION['form']['form-error'] = 'There was a problem with the file submitted, file size too small';
	$submission = Error::storeMsg($something_needs_to_go_here);
}

if ($submission) {
	
		error_log('Starting import process');
		
	$result = Data::importFromCSV();
	if ($result['query_status'] == 'OK' && $result['redirect'] == true) {
		$_SESSION['form']['form-success'] = 'Data has been successfully upload.<br />Please do some spot checks to ensure that the process went well.';
		header("Location: ".Page::$slug[0]);
		die();
	} else {
		# If in debug mode, show the error on screen
		if (DEBUG_MODE > 1) {
			echo 'ooops!';
			echo $result['query'];
		}
	}
}
