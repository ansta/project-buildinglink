<?php
echo '<h1><a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>
Import: '.Data::$title.'</h1>';
?>
<section>
	
	<?php
	if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-error'].'</p>';
		unset($_SESSION['form']['form-error']);
	}
	if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
		unset($_SESSION['form']['form-success']);
	}
	?>
	
	<?php
	if (isset($_SESSION['form']['export']['report_path'])) {
		include ($_SESSION['form']['export']['report_path']);
		unset($_SESSION['form']['export']['report_path']);
	}
	?>
</section>
