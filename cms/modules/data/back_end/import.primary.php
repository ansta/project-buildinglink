<?php
echo '<h1><a href="/'.Page::$slug[1].'/'.Page::$slug[2].'/" class="back-button"><img src="/images/admin/back-button.png" /></a>
Import: '.Data::$title.'</h1>';
?>
<section>
	
	<?php
	if (isset($_SESSION['form']['form-error']) && $_SESSION['form']['form-error'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-error'].'</p>';
		unset($_SESSION['form']['form-error']);
	}
	if (isset($_SESSION['form']['form-success']) && $_SESSION['form']['form-success'] != '') {
		echo '<p class="successbanner">'.$_SESSION['form']['form-success'].'</p>';
		unset($_SESSION['form']['form-success']);
	}
	?>
	
	<form method="post" action="" enctype="multipart/form-data" style="padding:0.5em 0 1em 0;">
		<input type="file" name="file" value="" />
		<input type="hidden" name="module" value="data" />
		<input type="hidden" name="action" value="import" />
		<input type="submit" name="submit" value="submit" />
	</form>
	
	<?php
	if (isset($_SESSION['form']['import']['report_path'])) {
		include ($_SESSION['form']['import']['report_path']);
		unset($_SESSION['form']['import']['report_path']);
	}
	?>
</section>
