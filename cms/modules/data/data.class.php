<?php

class Data {

	/*
	*
	*
	*/
	public static 	$title = '';
	
	/*
	*
	*
	*/
	private static 	$import_message = ''
					
				,	$filename_date = ''
					
				,	$custom_data = array()
				
				,	$temporary_table = ''
				,	$final_table = ''
				,	$parent_table = '';
	
	
			
	/*
	* Called manually this performs the full data import from csv to database of all items
	*/
	public static function importFromCSV() {
		
		error_log('loading csv');
		
		self::$import_message = '';
		$start_microtime= microtime(true);
		
		if (Page::$slug[4] == 'products') {
			self::$temporary_table	= 'import_products';
			self::$final_table		= 'products';
			$tables_to_backup		= array(self::$final_table,'attachments');
			
		} elseif (Page::$slug[4] == 'products_variants') {
			self::$temporary_table	= 'import_products_variants';
			self::$final_table		= 'products_variants';
			self::$parent_table		= 'products';
			$tables_to_backup		= array(self::$final_table,self::$parent_table,'attachments');
			
		} else {
			self::$import_message .= '<li class="bad">Error, '.Page::$slug[4].' is not a valid import!</li>';
			echo 'whoops!';
			return FALSE;
		}
		
		// Backup the tables that we are about to import into, just in case we need to revert
		if (Tool::backup_tables($tables_to_backup)) {
			self::$import_message .= '<li class="good">The database table '.implode(' and ',$tables_to_backup).' have been successfully backed up.</li>';
		} else {
			echo 'Backup of '.self::$final_table.' table failed. Aborting import process. Please contact the developer team.';
			die();
		}
		
		if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
			$new_file_data = self::processFileUpload($form_data);
			self::$import_message .= '<li class="good">Import file successfully uploaded to the server. Filename: '.$new_file_data['filename'].' (original: '.$new_file_data['original'].')</li>';
		} else {
			self::$import_message .= '<li class="bad">There was a problem uploading the file to the server.</li>';
			$result['query_status'] = 'NOT OK';
		}
		
		// Set some variables
		$source_file	= $new_file_data['filename'];
		$source_path	= $new_file_data['full_path'];
		$csv_file_size	= filesize ($source_path);
		
		// Clean out the temporary table where the CSV file data gets loaded into
		self::cleanOutTemporaryImportTable();
		
		// Just some debugging stuff
	#	echo self::$import_message;
	#	$result['query_status'] = 'OK';
	#	$result['redirect'] = true;
	#	return $result;
		
		// Check that there is a file of expected name available to begin the import process
		// If import file does not exist then stop!
		if (!file_exists($source_path)) {
			self::$import_message .= '<li class="bad">Import file not found</li>';	// <!-- '.$source_file.' -->
			
		// If import file does exist then continue
		} else {
			self::$import_message .= '<li class="good">Import file found</li>';
			
			// Check that the CSV file is greater than en expected blank file
			if ($csv_file_size < 32) {
				self::$import_message .= '<li class="bad">Import file contains no data (File size: '.$csv_file_size.')</li>';
				self::$import_message .= '<li class="bad">Try uploading a new data file.</li>';
			
			} else {
				self::$import_message .= '<li class="good">Beginning data import process.</li>';
				
				// Import the CSV data into the temporary MySQL table
				$query1 = "
					LOAD DATA LOCAL INFILE
						'".$source_path."'
					INTO TABLE
						`".self::$temporary_table."`
					FIELDS TERMINATED BY ','
					OPTIONALLY ENCLOSED BY '".'"'."'
					IGNORE 1 LINES
				";
				#	LINES TERMINATED BY '\\n'
				#	IGNORE 1 LINES
				$result1 = db::link()->query($query1);
				
				self::$import_message .= '<li class="good">Beginning check to make sure data was imported.</li>';
				// Check to see if data was successfully imported or not
				$query2 = "
					SELECT
						*
					FROM
						`".self::$temporary_table."`
				";
				$result2 = db::link()->query($query2);
				if ($result2->num_rows > 0) {
					$imported_lines_count = $result2->num_rows;
				} else {
					$imported_lines_count = 0;
				}
				
				// If data was not imported then stop!
				if (!$result1) {
					self::$import_message .= '<li class="bad">Data import into database failed. '.$query1.'</li>';
					
				// If data was imported then try to copy data from temp table into orders table
				} else {
					
					self::$import_message .= '<li class="good">Data successfully imported into temporary database table. ('.$imported_lines_count.' rows)';	//  <!-- '.$query1.' -->
					$copy_process = self::copyNewItemsToTheirRealTable();
					self::$import_message .= '</li>';

					// Check if data copy process went ok
					// If data copy failed then stop!
					if (!$copy_process) {
						self::$import_message .= '<li class="bad">Failed to merge data into permanent table. ('.$imported_lines_count.' rows found in temporary table)</li>';
						self::$import_message .= '<li class="bad">Please re-upload a new data file.</li>';
						
					// If data copy was successfull then carry on
					} else {
						self::$import_message .= '<li class="good">New data successfully merged into permanent table</li>';
					}
				}
			}
		}
		
		// Start putting the report together in preparation for output
		$end_microtime	= microtime(true);
		$tot_microtime	= $end_microtime-$start_microtime;
		if ($tot_microtime < 1) {
			$process_time_css = 'good';
		} else {
			$process_time_css = 'bad';
		}
		self::$import_message .= '<li class="'.$process_time_css.'">Process completed in '.number_format($tot_microtime,3).' seconds</li>';
		
		// Send the report email
		$email_prefix	= "<style>li.good {color:#060;} li.bad {color:#C30;}</style>START: ".$start_microtime."<br />----------------------------<ul>";
		$email_Suffix	= "</ul>----------------------------<br />END: ".$end_microtime."<br />";
		$email_message	= $email_prefix.self::$import_message.$email_Suffix;
		$email_message	= str_replace('<',PHP_EOL.'<',$email_message);
		Email::sendEmail('loki.ansta@gmail.com','Building Link '.Page::$slug[4].' import',$email_message);
		
		// Save report file
		$filename = self::$filename_date.'-report';
		$full_path = SYSTEM_ROOT.'/modules/data/uploads/'.Page::$slug[4].'/'.$filename.'.txt';
		$handle = fopen($full_path,'w+');
		fwrite($handle,$email_message);
		fclose($handle);
		
		$_SESSION['form']['import']['report_path'] = $full_path;
		
		$output_result['query_status'] = 'OK';
		$output_result['redirect'] = true;
		return $output_result;
	}
	
	
	
	/*
	* Process a file upload
	*/
	private static function processFileUpload($form_data) {
		
		$upload_dir = SYSTEM_ROOT.'/modules/data/uploads/'.Page::$slug[4].'/';
		$filename_original = $_FILES['file']['name'];
		$filename_cleansed = Form::clenseFilename($filename_original);
		
		self::$filename_date = date('Ymd-His');
		$filename_new = self::$filename_date.'-'.$filename_cleansed;
		
		if (!is_dir(SYSTEM_ROOT.'/modules/data/uploads/')) {
			mkdir(SYSTEM_ROOT.'/modules/data/uploads/',0775);
		}
		if (!is_dir(SYSTEM_ROOT.'/modules/data/uploads/'.Page::$slug[4].'/')) {
			mkdir(SYSTEM_ROOT.'/modules/data/uploads/'.Page::$slug[4].'/',0775);
		}
		
		$full_upload_path = $upload_dir . $filename_new;
		
		if (!move_uploaded_file($_FILES['file']['tmp_name'], $full_upload_path)) {
			die('<br><br> file upload failed during insert: '.$full_upload_path);
		} else {
			self::$import_message .= '<li class="good">File uploaded: '.$filename_original.'</li>';
			self::$import_message .= '<li class="good">Filename cleansed: '.$filename_cleansed.'</li>';
			self::$import_message .= '<li class="good">File renamed: '.$filename_new.'</li>';
		}
		
		$output = array(
			'original'=>$filename_original
		,	'cleansed'=>$filename_cleansed
		,	'filename'=>$filename_new
		,	'path_to_dir'=>$upload_dir
		,	'full_path'=>$full_upload_path
		);
		return $output;
	}
	

	
	/*
	* This looks up all the records currently listed in the temporary table and
	* loops through each one to check if the specified order number already exists or not
	* If the order number exists we do not add it again and just skip it.
	*/
	private static function copyNewItemsToTheirRealTable() {
		
	#	if (Page::$slug[4] == 'products') {
	#		$temporary_table = 'import_products';
	#		$final_table = 'dummy_products';
	#		
	#	} elseif (Page::$slug[4] == 'products_variants') {
	#		$temporary_table = 'import_products_variants';
	#		$final_table = 'dummy_products_variants';
	#		
	#	} else {
	#		self::$import_message .= '<li class="bad">Error, '.Page::$slug[4].' is not a valid import!</li>';
	#		return FALSE;
	#	}
		
		$query = "
			SELECT
				*
			FROM
				".self::$temporary_table."
		";
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			$updated = 0;
			$update_skipped = 0;
			$inserted = 0;
			$insert_skipped = 0;
			
			self::$import_message .= '<ol>';
			while ($temporary_table_data = $items->fetch_object()) {
				// Set some custom data into an array if the import process requires it
				self::setCustomData($temporary_table_data);
	#			echo '<pre>';
	#			var_dump(self::$custom_data);
				
				// Check if an item already exists to decide whether to insert or update a record
				if ($item_id = self::checkExists($temporary_table_data->product_no)) {
					$result = self::processData('edit',$temporary_table_data,$item_id);
					if ($result['query_status'] == 'OK') {$updated++;} else {$update_skipped++;}
				} else {
					$result = self::processData('new',$temporary_table_data);
					if ($result['query_status'] == 'OK') {$inserted++;} else {$insert_skipped++;}
				}
			}
			self::$import_message .= '</ol>';
			
			self::$import_message .= '<li class="good">Updated: '.$updated.' items</li>';
			if ($update_skipped > 0) {
				self::$import_message .= '<li class="bad">Failed to update: '.$update_skipped.' items</li>';
			}
			self::$import_message .= '<li class="good">Inserted: '.$inserted.' items</li>';
			if ($insert_skipped > 0) {
				self::$import_message .= '<li class="bad">Failed to insert: '.$insert_skipped.' items</li>';
			}
			
		} else {
			self::$import_message .= '<li class="bad">Error, no data in temporary table! ('.$query.')</li>';
			return FALSE;
		}
		return TRUE;
	}
	
	
				

	/*
	*
	*/
	private static function setCustomData($temporary_table_data) {
		// Put the supplied data into a smaller named variable called $item
		$item = $temporary_table_data;
		
		
		if (self::$temporary_table == 'import_products_variants') {
			// Just for debugging
			self::$custom_data['type'] = $item->data_type;
			
			// Process the different row types
			if ($item->data_type == 'labels') {
				self::$custom_data['label1'] = $item->price1;
				self::$custom_data['label2'] = $item->price2;
				self::$custom_data['label3'] = $item->price3;
				self::$custom_data['label4'] = $item->price4;
				self::$custom_data['label5'] = $item->price5;
				self::$custom_data['label6'] = $item->price6;
				self::$custom_data['product_no'] = $item->product_no;
				self::$custom_data['pos'] = 0;
				
				self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] = 0;
				if (trim($item->price1) != '') {
					self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] = 1;
					if (trim($item->price2) != '') {
						self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] = 2;
						if (trim($item->price3) != '') {
							self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] = 3;
							if (trim($item->price4) != '') {
								self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] = 4;
								if (trim($item->price5) != '') {
									self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] = 5;
									if (trim($item->price6) != '') {
										self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] = 6;
									}
								}
							}
						}
					}
				}
				
				
			} elseif ($item->data_type == 'variant') {
				if (isset(self::$custom_data['product_no']) && self::$custom_data['product_no'] == $item->product_no) {
					self::$custom_data['product_no'] = $item->product_no;
					self::$custom_data['pos'] = self::$custom_data['pos'] + 1;
				}
				if (isset(self::$custom_data['product_no']) && self::$custom_data['product_no'] != $item->product_no) {
					self::$custom_data['product_no'] = $item->product_no;
					self::$custom_data['pos'] = 1;
				}
				if (!isset(self::$custom_data['product_no'])) {
					self::$custom_data['product_no'] = $item->product_no;
					self::$custom_data['pos'] = 1;
				}
			
			} elseif ($item->data_type == 'setup') {
				self::$custom_data['product_no'] = $item->product_no;
				self::$custom_data['pos'] = 0;
			}
			
			// Some output formatting 
			if (self::$custom_data['pos'] == 1) {
				self::$custom_data['ul'] = 'open';
				self::$import_message .= '<ul>';
			}
			if (self::$custom_data['pos'] == 0 && self::$custom_data['ul'] == 'open') {
				self::$custom_data['ul'] = 'close';
				self::$import_message .= '</ul>';
			}
			if (self::$custom_data['pos'] == 0 && self::$custom_data['ul'] != 'open') {
				self::$custom_data['ul'] = '';
			}
		}
	}
	
	
				

	/*
	*
	*/
	private static function processData($type,$item,$item_id=0) {
		// $item is the data object from the temporary import table where the CSV data was uploaded to
		
		
		if (self::$temporary_table == 'import_products') {
			if (!self::checkSiteIdExists($item->site_id)) {
			#	self::$import_message .= Tool::my_dump($item);
				$result['query_status'] = 'NOT OK';
				return $result;
			}
			
			if ($type == 'new') {
				$data_array = array(
					'module'		=>	self::$final_table
				,	'action'		=>	'new'
				,	'site_id'		=>	$item->site_id
				,	'title'			=>	trim($item->title)
				,	'sku'			=>	self::enforceSkuFormat($item->product_no)
				,	'slug'			=>	self::createSlug($item->title)
				,	'metakeys'		=>	trim($item->meta_keywords)
				,	'metadesc'		=>	trim($item->meta_description)
				,	'summary'		=>	$item->product_overview
				,	'content'		=>	$item->product_description
				,	'artwork'		=>	$item->artwork
				,	'active'		=>	$item->status
				,	'related_products'=>self::cleansCsvListOfRelatedProducts($item->related_products)
				);
				Form::setAuditAction('new');
				$result = Form::processData(self::$final_table,$data_array);
				self::processProductCategoryAssignment($result,$item,0);
			} elseif ($type == 'edit') {
				$data_array = array(
					'module'		=>	self::$final_table
				,	'action'		=>	'edit'
				,	'id'			=>	$item_id
				,	'site_id'		=>	$item->site_id
				,	'title'			=>	trim($item->title)
				,	'metakeys'		=>	trim($item->meta_keywords)
				,	'metadesc'		=>	trim($item->meta_description)
				,	'summary'		=>	$item->product_overview
				,	'content'		=>	$item->product_description
				,	'artwork'		=>	$item->artwork
				,	'active'		=>	$item->status
				,	'related_products'=>self::cleansCsvListOfRelatedProducts($item->related_products)
				);
				Form::setAuditAction('edit');
				$result = Form::processData(self::$final_table,$data_array);
			} else {
				self::$import_message .= '<li class="bad">Failed to process this item as the TYPE set in processData() is set to '.$type.' which is not a recognised type!</li>';
				$result['query_status'] = 'NOT OK';
			}
			
			if ($result['query_status'] == 'OK') {
				if ($type == 'edit') {
					self::$import_message .= '<li class="good">Updating '.$item->product_no.': '.$item->title.' ('.$result['item_id'].')</li>';
				} elseif ($type == 'new') {
					self::$import_message .= '<li class="good">Inserting new '.$item->product_no.': '.$item->title.' ('.$result['item_id'].')</li>';
				}
				self::processProductCategoryAssignment($result,$item,$item_id);
			} else {
				if ($type == 'edit') {
					self::$import_message .= '<li class="bad">'.$item->product_no.': Failed to update existing item '.$item->title.'</li>';
				} elseif ($type == 'new') {
					self::$import_message .= '<li class="bad">'.$item->product_no.': Failed to add new item '.$item->title.'</li>';
				}
			}
			
		} elseif (self::$temporary_table == 'import_products_variants') {
			if ($item->data_type == 'setup') {
				self::setProductSetupFeePrice($item_id,$item->label);
				self::setProductAsSpecial($item_id,$item->special);
				$result['query_status'] = 'OK';
				
			} elseif ($item->data_type == 'labels') {
				// Do nothing
				self::deleteExistingVariantsForThisProduct($item_id,self::enforceSkuFormat($item->product_no));
				$result['query_status'] = 'OK';
						
			} elseif ($item->data_type == 'variant') {
				if ($type == 'new') {
					self::$import_message .= '<li class="bad">Product does not appear to exist, no point adding any variant data for it!</li>';
					$result['query_status'] = 'NOT OK';
					
				} elseif ($type == 'edit') {
					
					// Get site_id of product so as to assign it to this variant.
					// If the lookup fails it's not a major issue, it just means that this variant won't be editable via the admin.
					$data_array['site_id']	= self::getSiteIdFromProduct(self::enforceSkuFormat($item->product_no));
					if ($data_array['site_id'] == 0) {
						self::$import_message .= '<li class="bad">Warning: error finding the site_id for product '.self::enforceSkuFormat($item->product_no).'. This isn\'t necessarily a problem but it does mean that you won\'t be able to edit this variant via the admin.</li>';
					} else {
					#	self::$import_message .= '<li class="good">Found the site_id for product '.$item->product_no.' to be '.$data_array['site_id'].'</li>';
					}
					
					// Only import the columns that have data in them based on:
					//	- How many columns were supplied when setting the labels
					//	- How many columns of prices actually contain a value
					self::$custom_data['missing'] = '';
					
					if (self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] >= 1) {
						if (trim($item->price1) != '') {
							$data_array['label_1']	= self::$custom_data['label1'];
							$data_array['value_1']	= number_format($item->price1,2);
						} else { self::$custom_data['missing'] .= ', '.self::$custom_data['label1']; }
					}
					
					if (self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] >= 2) {
						if (trim($item->price2) != '') {
							$data_array['label_2']	= self::$custom_data['label2'];
							$data_array['value_2']	= number_format($item->price2,2);
						} else { self::$custom_data['missing'] .= ', '.self::$custom_data['label2']; }
					}
					
					if (self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] >= 3) {
						if (trim($item->price3) != '') {
							$data_array['label_3']	= self::$custom_data['label3'];
							$data_array['value_3']	= number_format($item->price3,2);
						} else { self::$custom_data['missing'] .= ', '.self::$custom_data['label3']; }
					}
					
					if (self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] >= 4) {
						if (trim($item->price4) != '') {
							$data_array['label_4']	= self::$custom_data['label4'];
							$data_array['value_4']	= number_format($item->price4,2);
						} else { self::$custom_data['missing'] .= ', '.self::$custom_data['label4']; }
					} 
					
					if (self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] >= 5) {
						if (trim($item->price5) != '') {
							$data_array['label_5']	= self::$custom_data['label5'];
							$data_array['value_5']	= number_format($item->price5,2);
						} else { self::$custom_data['missing'] .= ', '.self::$custom_data['label5']; }
					}
					
					if (self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] >= 6) {
						if (trim($item->price6) != '') {
							$data_array['label_6']	= self::$custom_data['label6'];
							$data_array['value_6']	= number_format($item->price6,2);
						} else { self::$custom_data['missing'] .= ', '.self::$custom_data['label6']; }
					}
					
					if (self::$custom_data['missing'] != '') {
						$blank_columns = str_replace('#####,','','#####'.self::$custom_data['missing']);
						self::$import_message .= '<li class="bad"> Quantity breaks '.$blank_columns.' are missing values for variant '.$item->label.'!</li>';
					} else {
						if (self::$custom_data['quantity_break_column_count'][self::$custom_data['product_no']] > 0) {
							$data_array['module']	= self::$final_table;
							$data_array['action']	= 'new';
							$data_array['parent_id']= $item_id;
							$data_array['title']	= $item->label;
							$data_array['pos']		= self::$custom_data['pos'];
							Form::setAuditAction('new');
							$result = Form::processData(self::$final_table,$data_array);
							if ($result['query_status'] != 'OK') {
								self::$import_message .= '<li class="bad">Error inserting variant into database</li>';
							}
						} else {
							self::$import_message .= '<li class="bad">The quantity break columns appear to be empty!</li>';
						}
					}
					
				} else {
					self::$import_message .= '<li class="bad">Failed to process this item as the TYPE set in processData() is set to '.$type.' which is not a recognised type!</li>';
					$result['query_status'] = 'NOT OK';
				}
				
				if ($result['query_status'] == 'OK') {
						self::$import_message .= '<li class="good">Adding new variant '.$item->label.' (item id: '.$result['item_id'].')</li>';
				} else {
						self::$import_message .= '<li class="bad">Failed to add new variant '.$item->label.'</li>';
				}
			}
			
		} else {
			self::$import_message .= '<li class="bad">Failed to process this item as the source table set in processData() is set to '.self::$temporary_table.' which is not a recognised table!</li>';
			$result['query_status'] = 'NOT OK';
		}

		return $result;
	}
	
	
	
	/*
	*
	*/
	private static function checkExists($item_identifier) {
		
		if (self::$temporary_table == 'import_products') {
			$query = "
				SELECT
					`a`.`id`
				FROM
					`".self::$final_table."` a
				WHERE
					`a`.`id` > 0
				AND	`a`.`sku` = '".db::link()->real_escape_string(self::enforceSkuFormat($item_identifier))."'
				AND `a`.`ttv_end` IS null
			";
			
		} elseif (self::$temporary_table == 'import_products_variants') {
			$query = "
				SELECT
					`a`.`id`
				FROM
					`".self::$parent_table."` a
				WHERE
					`a`.`id` > 0
				AND	`a`.`sku` = '".db::link()->real_escape_string(self::enforceSkuFormat($item_identifier))."'
				AND `a`.`ttv_end` IS null
			";
		} else {
			self::$import_message .= 'checkExists() is currently not setup to handle imports on '.self::$temporary_table.'!';
			die();
		}
		
		#echo $query;
		// If all went well then return the ID or FALSE depending on if a record was found or not
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			while ($item = $items->fetch_object()) {
				return $item->id;
			}
		}
		return FALSE;
	}
	
	
	
	/*
	* Used to wipe the temporary orders table empty.
	* Data is only in this table for a short amount of time as it's simply to hold data that is
	* imported from a CSV and needs to be inserted in to the actual orders table.
	*/
	private static function cleanOutTemporaryImportTable() {
		$query = "TRUNCATE `".self::$temporary_table."`";
		$result = db::link()->query($query) or die("<li class=\"bad\">Error cleaning out temporary database table</li>");
		
		if (!$result) {
			self::$import_message .= '<li class="bad">Failed to clean out temporary database table</li>';
			
		} else {
			self::$import_message .= '<li class="good">Temporary database table successfully cleaned out and ready to receive new data</li>';
			
		}
	}
	
	
	
	/*
	*
	*/
	private static function createSlug($title) {
		
		$title = strtolower(trim($title));
		$title = str_replace('  ',' ',$title);
		$title = str_replace('  ',' ',$title);
		$title = str_replace('  ',' ',$title);
		$title = str_replace('  ',' ',$title);
		$title = str_replace('  ',' ',$title);
		$title = str_replace('  ',' ',$title);
		$title = str_replace('  ',' ',$title);
		$title = str_replace('  ',' ',$title);
		$title = str_replace(array('`','¬','!','"','£','$','%','^','*','(',')','=','{','}','[',']',':','@','~','#','<','>',',','.','/','?','|'),'',$title);
		$title = str_replace(array('+','&'),'and',$title);
		$title = str_replace(array(' ','_'),'-',$title);
		$title = str_replace('--','-',$title);
		$title = str_replace('--','-',$title);
		$title = str_replace('--','-',$title);
		$title = str_replace('--','-',$title);
		$title = str_replace('--','-',$title);
		$title = str_replace('--','-',$title);
		$title = str_replace('--','-',$title);
		
		return $title;
	}
	
	
	
	/*
	*
	*/
	private static function processProductCategoryAssignment($product_result,$item,$item_id) {
		self::$import_message .= '<ul>';
		
		// Set the products table name for use in the ATTACHMENTS table in the module_name column
		$products_table_name = self::$final_table;
		
		// Cache the PRODUCT ID
		if ($item_id == 0) {
			$product_id = $product_result['item_id'];
		} else {
			$product_id = $item_id;
		}
		// Delete all current SHOP_CATEGORIES for this PRODUCT ID from ATTACHMENTS
		self::deletePreviousAttachments($products_table_name,$product_id,'shop_categories');
		
		if (trim($item->categories) != '') {
			// Loop through the supplied list of SHOP_CATEGORIES and insert each one into ATTACHMENTS
			$category_array = array_unique(explode(',',trim($item->categories)));
			foreach ($category_array AS $category_id) {
				self::addAttachmentIfCategoryExists($products_table_name,$product_id,'shop_categories',$category_id);
			}
		} else {
			self::$import_message .= '<li class="bad">Product has not been assigned to any Categories, please assign these manually or re-upload a new file. Please note that Products not assigned to a Category will not be listed on the website.</li>';
		}
		
		self::$import_message .= '</ul>';
	}
	
	
	/*
	*
	*/
	private static function deletePreviousAttachments($module_name,$module_id,$attachment_name) {
		$query = "
			UPDATE
				attachments
			SET
				ttv_end = NOW()
			WHERE
				module_name = '".db::link()->real_escape_string($module_name)."'
			AND	module_id = ".(int)db::link()->real_escape_string($module_id)."
			AND	attachment_name = '".db::link()->real_escape_string($attachment_name)."'
			AND ttv_end IS null
		";
		$result = db::link()->query($query);
		if ($result) {
			self::$import_message .= '<li class="good">Successfully cleared out old '.$module_name.'/'.$attachment_name.' attachments relating to this '.$module_name.'</li>';
		} else {
			self::$import_message .= '<li class="bad">Error clearing out old '.$module_name.'/'.$attachment_name.' attachments: '.$query.'</li>';
		}
	}
	
	
	/*
	*
	*/
	private static function checkSiteIdExists($site_id) {
		$query = "
			SELECT
				id
			FROM
				sites
			WHERE
				id = ".db::link()->real_escape_string($site_id)."
			AND ttv_end IS null
		";
		$results = db::link()->query($query);
		if ($results->num_rows == 1) {
			return TRUE;
		}
		self::$import_message .= '<li class="bad">Site ID '.$site_id.' does not appear to exist!</li>';
		return FALSE;
	}
	
	
	/*
	*
	*/
	private static function addAttachmentIfCategoryExists($module_name,$product_id,$attachment_name,$category_id) {
		$query = "
			SELECT
				id
			,	title
			FROM
				shop_categories
			WHERE
				id = ".(int)db::link()->real_escape_string($category_id)."
			AND ttv_end IS null
		";
		$categories = db::link()->query($query);
		if ($categories->num_rows == 1) {
			$data_array = array(
				'module'		=>	'attachments'
			,	'action'		=>	'new'
			,	'module_name'	=>	$module_name
			,	'module_id'		=>	(int)$product_id
			,	'attachment_name'=>	$attachment_name
			,	'attachment_id'	=>	(int)$category_id
			);
			Form::setAuditAction('new');
			$result = Form::processData('attachments',$data_array);
			$category = $categories->fetch_object();
			if ($result['query_status'] == 'OK') {
				self::$import_message .= '<li class="good">Linking product to category: '.(int)$category_id.' ('.$category->title.')</li>';
			} else {
				self::$import_message .= '<li class="bad">Error: Linking product to category: '.(int)$category_id.' ('.$category->title.')</li>';
			}
			return TRUE;
		}
		self::$import_message .= '<li class="bad">Category ID: '.(int)$category_id.' does not appear to exist, please add category manually or correct data and re-upload.</li>';
		return FALSE;
	}
	
	
	/*
	*
	*/
	private static function setProductSetupFeePrice($product_id,$setup_fee) {
		$query = "
			SELECT
				`id`
			,	`setup_fee`
			,	`sku`
			FROM
				`".self::$parent_table."`
			WHERE
				id = ".(int)db::link()->real_escape_string($product_id)."
			AND	ttv_end IS null
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			if ($item = $items->fetch_object()) {
				if (number_format($setup_fee,2) == number_format($item->setup_fee,2)) {
					self::$import_message .= '<li class="good">'.$item->sku.': Setup Fee remains unchanged at &pound;'.number_format($item->setup_fee,2).'</li>';
				} else {
					$data_array = array(
						'module'		=>	self::$parent_table
					,	'action'		=>	'edit'
					,	'id'			=>	$item->id
					,	'setup_fee'		=>	number_format($setup_fee,2)
					);
					Form::setAuditAction('edit');
					$result = Form::processData(self::$parent_table,$data_array);
					if ($result['query_status'] == 'OK') {
						self::$import_message .= '<li class="good">'.$item->sku.': Product Setup Fee updated from &pound;'.number_format($item->setup_fee,2).' to &pound;'.number_format($setup_fee,2).'</li>';
					} else {
						self::$import_message .= '<li class="bad">'.$item->sku.' Error updating Product Setup Fee from &pound;'.number_format($item->setup_fee,2).' to &pound;'.number_format($setup_fee,2).'</li>';
					}
				}
			}
		} else {
			self::$import_message .= '<li class="bad">'.self::$custom_data['product_no'].' Failed to find product record in order to update Product Setup Fee, product not found!</li>';
		}
	}
	
	
	/*
	*
	*/
	private static function getSiteIdFromProduct($product_no) {
		$query = "
			SELECT
				`site_id`
			FROM
				`".self::$parent_table."`
			WHERE
				sku = '".db::link()->real_escape_string(self::enforceSkuFormat($product_no))."'
			AND	ttv_end IS null
		";
	#	echo $query;
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			if ($item = $items->fetch_object()) {
				return $item->site_id;
			}
		}
	#	self::$import_message .= '<li class="bad">site id lookup fail: ! '.$query.'</li>';
		return 0;
	}
	
	
	/*
	*
	*/
	private static function setProductAsSpecial($product_id,$new_special_status) {
		$query = "
			SELECT
				`id`
			,	`sku`
			,	`special_offer`
			FROM
				`".self::$parent_table."`
			WHERE
				id = ".(int)db::link()->real_escape_string($product_id)."
			AND	ttv_end IS null
		";
		
		$items = db::link()->query($query);
		if ($items->num_rows > 0) {
			if ($item = $items->fetch_object()) {
				if ((int)$new_special_status == (int)$item->special_offer) {
					self::$import_message .= '<li class="good">'.$item->sku.': Special Offer status remains unchanged as '.(int)$item->special_offer.'</li>';
				} else {
					$data_array = array(
						'module'		=>	self::$parent_table
					,	'action'		=>	'edit'
					,	'id'			=>	$item->id
					,	'special_offer'	=>	(int)$new_special_status
					);
					Form::setAuditAction('edit');
					$result = Form::processData(self::$parent_table,$data_array);
					if ($result['query_status'] == 'OK') {
						self::$import_message .= '<li class="good">Special Offer Status changed from '.(int)$item->special_offer.' to '.(int)$new_special_status.'</li>';
					} else {
						self::$import_message .= '<li class="bad">Error updating Product Special Offer Status from '.(int)$item->special_offer.' to '.(int)$new_special_status.'</li>';
					}
				}
			}
		} else {
			self::$import_message .= '<li class="bad">Failed to find product record in order to update Product Special Offer Status, product not found!</li>';
		}
	}
	
	
	/*
	*
	*/
	private static function deleteExistingVariantsForThisProduct($product_id,$product_no) {
		$query = "
			UPDATE
				`".self::$final_table."`
			SET
				ttv_end = NOW()
			WHERE
				id > 0
			AND	parent_id = ".(int)db::link()->real_escape_string($product_id)."
			AND ttv_end IS null
		";
		$result = db::link()->query($query);
		if ($result) {
			self::$import_message .= '<li class="good">'.self::enforceSkuFormat($product_no).': Successfully removed existing variants</li>';
		} else {
			self::$import_message .= '<li class="bad">'.self::enforceSkuFormat($product_no).': Error clearing out old variants</li>';
		}
	}
			
	
	
	/*
	*
	*/
	public static function makeCsvColumnHeaders($table) {
		$query = 'SHOW COLUMNS FROM '.$table;
		$columns = db::link()->query($query);
		if ($columns->num_rows > 0) {
			while ($column = $columns->fetch_assoc()) {
				$column_array['name'][]		= $column['Field'];
				$column_array['type'][]		= $column['Type'];
				$column_array['null'][]		= $column['Null'];
				$column_array['key'][]		= $column['Key'];
				$column_array['default'][]	= $column['Default'];
				$column_array['extra'][]	= $column['Extra'];
			}
			foreach ($column_array['name'] as $value) {
				$line[$value] = $value;
			}
			return $line;
		}
		return NULL;
	}
	
	
	
	/*
	* Enforce the format of 0000.00 for all Product Numbers
	*/
	public static function enforceSkuFormat($input) {
		
		$pop = explode('.',$input);
		$prefix = $pop[0];
		$suffix = $pop[1];
		
		$prefix = str_pad($prefix, 4, '0', STR_PAD_LEFT);
		$suffix = str_pad($suffix, 2, '0', STR_PAD_RIGHT);
		
		$output = $prefix.'.'.$suffix;
		
		return $output;
	}
	
	
	
	/*
	* Enforce the format of 0000.00 for all Product Numbers
	*/
	public static function cleansCsvListOfRelatedProducts($input) {
		
		// Remove spaces
		$input = str_replace(' ','',trim($input));
		
		// Get each code and make sure if fits the SKU formatting rule
		$codes = explode(',',$input);
		foreach ($codes AS $code) {
			$clean[] = self::enforceSkuFormat($code);
		}
		$output = implode(',',$clean);
		
		return $output;
	}
	
}







